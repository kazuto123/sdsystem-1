-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2014 at 04:45 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bc_sdsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `ac_periods`
--

CREATE TABLE IF NOT EXISTS `ac_periods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `ac_year` int(11) NOT NULL,
  `ac_period` int(11) NOT NULL,
  `st_date` datetime NOT NULL,
  `ed_date` datetime DEFAULT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `ac_periods`
--

INSERT INTO `ac_periods` (`id`, `comp_code`, `ac_year`, `ac_period`, `st_date`, `ed_date`, `status`, `created_at`, `updated_at`) VALUES
(1, '01', 2010, 201008, '2010-08-01 00:00:00', '2010-08-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(2, '01', 2010, 201009, '2010-09-01 00:00:00', '2010-09-30 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(3, '01', 2010, 201010, '2010-10-01 00:00:00', '2010-10-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(4, '01', 2011, 201101, '2011-01-01 00:00:00', '2011-01-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(5, '01', 2011, 201102, '2011-02-01 00:00:00', '2011-02-28 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(6, '01', 2011, 201103, '2011-03-01 00:00:00', '2011-03-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(7, '01', 2011, 201104, '2011-04-01 00:00:00', '2011-04-30 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(8, '01', 2011, 201105, '2011-05-01 00:00:00', '2011-05-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(9, '01', 2011, 201106, '2011-06-01 00:00:00', '2011-06-30 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(10, '01', 2011, 201107, '2011-07-01 00:00:00', '2011-07-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(11, '01', 2011, 201108, '2011-08-01 00:00:00', '2011-08-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(12, '01', 2011, 201109, '2011-09-01 00:00:00', '2011-09-30 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(13, '01', 2011, 201110, '2011-10-01 00:00:00', '2011-10-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(14, '01', 2011, 201111, '2011-11-01 00:00:00', '2011-11-30 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(15, '01', 2011, 201112, '2011-12-01 00:00:00', '2011-12-31 00:00:00', '1', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(16, '01', 2012, 201201, '2012-01-01 00:00:00', '2012-01-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(17, '01', 2012, 201202, '2012-02-01 02:54:50', '2012-02-29 02:54:50', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(18, '01', 2012, 201203, '2012-03-01 00:00:00', '2012-03-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(19, '01', 2012, 201204, '2012-04-01 00:00:00', '2012-04-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(20, '01', 2012, 201205, '2012-05-01 00:00:00', '2012-05-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(21, '01', 2012, 201206, '2012-06-01 00:00:00', '2012-06-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(22, '01', 2012, 201207, '2012-07-01 00:00:00', '2012-07-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(23, '01', 2012, 201208, '2012-08-01 00:00:00', '2012-08-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(24, '01', 2012, 201209, '2012-09-01 00:00:00', '2012-09-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(25, '01', 2012, 201210, '2012-10-01 00:00:00', '2012-10-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(26, '01', 2012, 201211, '2012-11-01 00:00:00', '2012-11-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(27, '01', 2012, 201212, '2012-12-01 00:00:00', '2012-12-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(28, '01', 2013, 201301, '2013-01-01 00:00:00', '2013-01-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(29, '01', 2013, 201302, '2013-02-01 00:00:00', '2013-02-28 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(30, '01', 2013, 201303, '2013-03-01 00:00:00', '2013-03-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(31, '01', 2013, 201304, '2013-04-01 00:00:00', '2013-04-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(32, '01', 2013, 201305, '2013-05-01 00:00:00', '2013-05-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(33, '01', 2013, 201306, '2013-06-01 00:00:00', '2013-06-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(34, '01', 2013, 201307, '2013-07-01 00:00:00', '2013-07-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(35, '01', 2013, 201308, '2013-08-01 00:00:00', '2013-08-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(36, '01', 2013, 201309, '2013-09-01 00:00:00', '2013-09-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(37, '01', 2013, 201310, '2013-10-01 00:00:00', '2013-10-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(38, '01', 2013, 201311, '2013-11-01 00:00:00', '2013-11-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(39, '01', 2013, 201312, '2013-12-01 00:00:00', '2013-12-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(40, '01', 2014, 201401, '2014-01-01 00:00:00', '2014-01-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(41, '01', 2014, 201402, '2014-02-01 00:00:00', '2014-02-28 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(42, '01', 2014, 201403, '2014-03-01 00:00:00', '2014-03-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(43, '01', 2014, 201404, '2014-04-01 00:00:00', '2014-04-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(44, '01', 2014, 201405, '2014-05-01 00:00:00', '2014-05-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(45, '01', 2014, 201406, '2014-06-01 00:00:00', '2014-06-30 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(46, '01', 2014, 201407, '2014-07-01 00:00:00', '2014-07-31 00:00:00', '0', '2014-04-03 04:34:06', '2014-04-03 04:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issalesman` tinyint(4) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_roles_user_id_foreign` (`user_id`),
  KEY `assigned_roles_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `cat_pos` tinyint(4) NOT NULL,
  `cat_len` tinyint(4) NOT NULL,
  `sub_cat` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `comp_code`, `cat_pos`, `cat_len`, `sub_cat`, `descr`, `inactive`, `created_at`, `updated_at`) VALUES
(1, '01', 1, 3, 'CUP', 'CUPS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(2, '01', 1, 3, 'FLR', 'FLOUR', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(3, '01', 1, 3, 'FRC', 'FROZEN CHICKEN', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(4, '01', 1, 3, 'MRC', 'MARINATED CHICKEN', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(5, '01', 1, 3, 'OIL', 'OIL', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(6, '01', 1, 3, 'OTH', 'OTHERS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(7, '01', 1, 3, 'PAT', 'PATTIES', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(8, '01', 1, 3, 'PCK', 'PACKAGES', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(9, '01', 1, 3, 'PTS', 'POTATOES', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(10, '01', 1, 3, 'SAU', 'SAUCES', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(11, '01', 1, 3, 'SFD', 'SEAFOOD', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(12, '01', 1, 3, 'SLD', 'SALADS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(13, '01', 1, 3, 'SSN', 'SEASONING', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `user_id` int(11) NOT NULL,
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `device_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gcm_device_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` decimal(5,3) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descr` text COLLATE utf8_unicode_ci,
  `display_order` tinyint(4) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `comp_code`, `type`, `key`, `value`, `descr`, `display_order`, `inactive`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '01', 'vgroup', 'vend_grp', 'LOC', 'Local', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '01', 'vgroup', 'vend_grp', 'OVS', 'Oversea', 2, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '01', 'cgroup', 'cust_grp', 'LOC', 'Local', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '01', 'cgroup', 'cust_grp', 'OVS', 'Oversea', 2, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '01', 'orderlistfilter', 'filterby', 'driver', 'Driver (alphabetical)\n', 3, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '01', 'orderlistfilter', 'filterby', 'latest_delivery_date', 'Latest Date', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '01', 'orderlistfilter', 'filterby', 'latest_order_no', 'Latest Order Number\n', 2, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '01', 'deliverytimeslot', 'time', '07:00', 'EARYLY MORNING DELIVERY', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '01', 'deliverytimeslot', 'time', '15:00', 'AFTERNOON DELIVERY', 2, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '01', 'itempriceseq', 'priceseq', 'in_item_cust_price', 'none', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '01', 'itempriceseq', 'priceseq', 'in_item_custgrp_price', 'none', 2, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '01', 'itempriceseq', 'priceseq', 'in_item_period_price', 'none', 4, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '01', 'itempriceseq', 'priceseq', 'in_item_list_price', 'none', 5, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '01', 'currency', 'code', 'EUR', 'EURO', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(15, '01', 'currency', 'code', 'INR', 'INDIAN RUPEES', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(16, '01', 'currency', 'code', 'MYR', 'MALAYSIAN RINGITT', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(17, '01', 'currency', 'code', 'NOK', 'KRONER', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(18, '01', 'currency', 'code', 'PHP', 'PHILIPPINO PESO', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(19, '01', 'currency', 'code', 'SGD', 'SINGAPORE $', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(20, '01', 'currency', 'code', 'USD', 'USD $', 1, 0, NULL, '2014-03-23 22:08:26', '2014-03-23 22:08:26'),
(21, '01', 'po_counts', 'count', '109', 'running number for counting PO\n', 1, 0, NULL, '0000-00-00 00:00:00', '2014-04-04 09:30:46'),
(22, '01', 'to_counts', 'count', '105', 'running number for counting TO\n', 1, 0, NULL, '0000-00-00 00:00:00', '2014-04-04 09:28:01'),
(23, '01', 'porderlistfilter', 'filterby', 'vend_name', 'Vendor Name (Alphabetical)\n', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '01', 'porderlistfilter', 'filterby', 'latest_date', 'Latest Updated Date', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '01', 'gst_rate', 'GST7', '7', 'GST7', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '01', 'sys_param', 'frmt_numeric', '###,###,###', 'Numeric Format', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(27, '01', 'sys_param', 'frmt_qty_dec', '2', 'Qty Decimal places', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(28, '01', 'sys_param', 'frmt_price_dec', '2', 'Price Decimal places', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(29, '01', 'sys_param', 'frmt_cost_dec', '2', 'Cost Decimal places', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(30, '01', 'sys_param', 'frmt_total_dec', '2', 'Total Decimal places', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(31, '01', 'sys_param', 'frmt_exch_dec', '4', 'Exchange Rate Decimal places', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(32, '01', 'sys_param', 'frmt_cfqty_dec', '4', 'Conversion Factor for Qty', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(33, '01', 'sys_param', 'frmt_cfpric_dec', '4', 'Conversion Factor for Price', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(34, '01', 'sys_param', 'currency', 'SGD', 'Currency', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(35, '01', 'sys_param', 'taxtype', 'GST7', 'Tax Type', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(36, '01', 'sys_param', 'default_whse', 'MAIN', 'Default Warehouse', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(37, '01', 'sys_param', 'country', 'SIN', 'Country', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(38, '01', 'sys_param', 'web_so_subtype', '07', 'Default Web Sales Order Subtype', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(39, '01', 'sys_param', 'web_po_subtype', '07', 'Default Web Purchase Order Subtype', NULL, 0, NULL, '2014-04-01 08:53:44', '2014-04-01 08:53:44'),
(40, '01', 'compcode', 'value', '01', 'Company Code', NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '01', 'server_settings', 'server_username', 'kremote', 'Username to logged into SD main Server\n', NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '01', 'server_settings', 'server_password', 'kremote132', 'Password to access SD server', NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '01', 'server_settings', 'server_url', '203.125.118.241:8080', 'URL to server IP address\n', NULL, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '01', 'gst_rate', 'ZERO', '0', 'ZERO', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '01', 'gst_rate', 'EXEM', '0', 'Exempted\n', 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '01', 'bom_id_count', 'id', '29', 'ID Count for BOM', 1, 0, NULL, '0000-00-00 00:00:00', '2014-04-27 10:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `curr_exch_rate`
--

CREATE TABLE IF NOT EXISTS `curr_exch_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `curr_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ac_period` int(11) NOT NULL,
  `exch_rate` decimal(8,6) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=79 ;

--
-- Dumping data for table `curr_exch_rate`
--

INSERT INTO `curr_exch_rate` (`id`, `comp_code`, `curr_code`, `ac_period`, `exch_rate`, `inactive`, `created_at`, `updated_at`) VALUES
(1, '01', 'EUR', 201104, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(2, '01', 'EUR', 201105, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(3, '01', 'EUR', 201106, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(4, '01', 'EUR', 201109, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(5, '01', 'EUR', 201204, '1.400000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(6, '01', 'EUR', 201206, '2.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(7, '01', 'EUR', 201303, '1.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(8, '01', 'EUR', 201310, '2.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(9, '01', 'EUR', 201402, '2.300000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(10, '01', 'MYR', 201402, '2.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(11, '01', 'MYR', 201403, '2.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(12, '01', 'NOK', 201008, '4.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(13, '01', 'NOK', 201108, '4.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(14, '01', 'NOK', 201205, '0.500000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(15, '01', 'PHP', 201307, '0.030300', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(16, '01', 'SGD', 200911, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(17, '01', 'SGD', 200912, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(18, '01', 'SGD', 201001, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(19, '01', 'SGD', 201003, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(20, '01', 'SGD', 201007, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(21, '01', 'SGD', 201008, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(22, '01', 'SGD', 201009, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(23, '01', 'SGD', 201010, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(24, '01', 'SGD', 201011, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(25, '01', 'SGD', 201101, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(26, '01', 'SGD', 201102, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(27, '01', 'SGD', 201103, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(28, '01', 'SGD', 201104, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(29, '01', 'SGD', 201105, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(30, '01', 'SGD', 201106, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(31, '01', 'SGD', 201107, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(32, '01', 'SGD', 201108, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(33, '01', 'SGD', 201109, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(34, '01', 'SGD', 201110, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(35, '01', 'SGD', 201111, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(36, '01', 'SGD', 201112, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(37, '01', 'SGD', 201201, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(38, '01', 'SGD', 201202, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(39, '01', 'SGD', 201203, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(40, '01', 'SGD', 201204, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(41, '01', 'SGD', 201205, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(42, '01', 'SGD', 201206, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(43, '01', 'SGD', 201207, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(44, '01', 'SGD', 201208, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(45, '01', 'SGD', 201209, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(46, '01', 'SGD', 201210, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(47, '01', 'SGD', 201211, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(48, '01', 'SGD', 201212, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(49, '01', 'SGD', 201301, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(50, '01', 'SGD', 201302, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(51, '01', 'SGD', 201303, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(52, '01', 'SGD', 201304, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(53, '01', 'SGD', 201305, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(54, '01', 'SGD', 201306, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(55, '01', 'SGD', 201307, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(56, '01', 'SGD', 201308, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(57, '01', 'SGD', 201309, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(58, '01', 'SGD', 201310, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(59, '01', 'SGD', 201311, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(60, '01', 'SGD', 201312, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(61, '01', 'SGD', 201402, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(62, '01', 'SGD', 201403, '1.000000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(63, '01', 'USD', 200911, '1.370000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(64, '01', 'USD', 200912, '1.370000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(65, '01', 'USD', 201008, '1.200000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(66, '01', 'USD', 201104, '1.200000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(67, '01', 'USD', 201204, '1.200000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(68, '01', 'USD', 201205, '1.200000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(69, '01', 'USD', 201206, '1.300000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(70, '01', 'USD', 201209, '1.343000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(71, '01', 'USD', 201210, '1.272700', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(72, '01', 'USD', 201307, '1.300000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(73, '01', 'USD', 201308, '1.280000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(74, '01', 'USD', 201309, '1.260000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(75, '01', 'USD', 201310, '1.670000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(76, '01', 'USD', 201312, '1.200000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(77, '01', 'USD', 201402, '1.280000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06'),
(78, '01', 'USD', 201403, '1.280000', 0, '2014-04-03 04:34:06', '2014-04-03 04:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `customeraddrs`
--

CREATE TABLE IF NOT EXISTS `customeraddrs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `cust_no` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `addr_ref` int(11) NOT NULL,
  `addr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ctry_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cont_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cont_off_tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_res_tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_hp_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_fax_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `customeraddrs`
--

INSERT INTO `customeraddrs` (`id`, `comp_code`, `cust_no`, `addr_ref`, `addr`, `zip`, `ctry_code`, `cont_name`, `cont_off_tel`, `cont_res_tel`, `cont_hp_no`, `cont_fax_no`, `cont_email`, `active`, `created_at`, `updated_at`) VALUES
(1, '01', '1000', 2, '249 Boonlay Way', '562890', 'SIN', 'Rina', '68474439', NULL, NULL, '68563322', 'bch@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(2, '01', '1000', 3, '20 Woodlands Terrace', '340890', 'SIN', 'TINA', '67894439', NULL, NULL, '67893322', 'bch@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(3, '01', '1001', 2, '15 Tai Seng Drive', '450678', 'SIN', 'TAN', '63987666', NULL, NULL, '63987890', 'tan@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(4, '01', '1001', 3, '10 Penjuru Lane', '890221', 'SIN', 'Lim', '67839212', NULL, NULL, '67832101', NULL, 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(5, '01', '1002', 2, '15 Woodlands Loop', '189892', 'SIN', 'siewlian', '67787890', NULL, NULL, '68938989', 'siewlian@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(6, '01', '1002', 3, '200 Pandan Loop', '378933', 'SIN', 'SERENE', '65627890', NULL, NULL, '65628989', 'serene@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(7, '01', '1003', 2, '9 Jalan Tepong', '619327', 'SIN', 'MS. LILY', '62658911', NULL, NULL, '62657877', 'sales@afcoeast.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(8, '01', '1004', 2, '10, Fishery Port Rd\r\n', '619808', 'SIN', 'MS. JULIE', '67833321', NULL, NULL, '67823221', 'ahbee@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(9, '01', '1005', 2, '20 Woodlands Loop', '738980', 'SIN', 'MR. ALAN', '67563243', NULL, NULL, '67569108', 'allbig@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(10, '01', '1006', 2, '20 Ang Mo Kio Indl Pk 2', '379680', 'SIN', 'MR. STEVEN', '64562121', NULL, NULL, '64568912', 'angliss@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(11, '01', '1007', 2, '32 Kaki Bukit Terrace', '234242', 'SIN', 'MR. LAU', '67453321', NULL, NULL, '67458902', 'beeguan@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(12, '01', '1008', 2, 'Woodlands Rd', '445566', 'SIN', 'YVONNE', NULL, NULL, NULL, NULL, NULL, 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(13, '01', '2000', 2, '15/1 Market Street', '453422', 'CHN', 'MS. CHUA', '0086298732341', NULL, NULL, '0086245893214', 'chua.lim@soonboonliat.com.cn', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(14, '01', '2001', 2, '28/9 Taman Perling, Johor.', '342432', 'MAL', 'MR. TAN BOON LIAT', '012 68393922', NULL, NULL, '012 68281999', 'soonlee@maxis.com.my', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(15, '01', '2002', 2, '20/4 Taman Utama, Petaling Jaya, KL.', '238902', 'MAL', 'MR. LEE', '013 601232302', NULL, NULL, '012 340022121', 'cheesong@gmail.com', 1, '2014-04-03 04:34:04', '2014-04-03 04:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `cust_no` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cust_group_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cust_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cust_cont_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cust_curr_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cust_tax_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cust_absorb_tax` tinyint(4) NOT NULL,
  `salesman` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_id_unique` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2003 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `comp_code`, `cust_no`, `user_id`, `cust_group_code`, `cust_name`, `cust_cont_name`, `cust_curr_code`, `cust_tax_type`, `cust_absorb_tax`, `salesman`, `inactive`, `created_at`, `updated_at`) VALUES
(1000, '01', 1000, 1, 'LOC', 'Bee Cheng Hiang', 'MS. TINA', 'SGD', 'GST7', 0, 'ryan', 0, '2014-04-03 04:34:02', '2014-04-03 04:34:02'),
(1001, '01', 1001, 2, 'LOC', 'BreadTalk Group Limited', 'MR. TAN', 'SGD', 'GST7', 0, 'durai', 0, '2014-04-03 04:34:02', '2014-04-03 04:34:02'),
(1002, '01', 1002, 3, 'LOC', 'Clouet Trading Pte. Ltd', 'MS. SERENE', 'SGD', 'GST7', 0, 'francis', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1003, '01', 1003, 4, 'LOC', 'Afco East Pte Ltd', 'MS. LILY', 'SGD', 'GST7', 0, 'erwin', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1004, '01', 1004, 5, 'LOC', 'Ah Bee Frozen Food', 'MS. JULIE', 'SGD', 'GST7', 0, 'ryan', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1005, '01', 1005, 6, 'LOC', 'All-Big Trading Co', 'MR. ALAN', 'SGD', 'GST7', 0, 'jialin', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1006, '01', 1006, 7, 'LOC', 'Angliss Singapore Pte Ltd', 'MR. STEVEN', 'SGD', 'GST7', 0, 'durai', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1007, '01', 1007, 8, 'LOC', 'Bee Guan Chan Food Supplier', 'MR. LAU', 'SGD', 'GST7', 0, 'erwin', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(1008, '01', 1008, 9, 'LOC', 'Fei Siong Pte Ltd', 'YVONNE', 'SGD', 'GST7', 0, 'DBA', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(2000, '01', 2000, 10, 'OVS', 'Soon Boon Liat Trading Ltd', 'MS. CHUA', 'USD', 'EXEM', 0, 'arun', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(2001, '01', 2001, 11, 'OVS', 'Soon Lee Trading Sdn Bhd', 'MR. TAN BOON LIAT', 'MYR', 'EXEM', 0, 'francis', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03'),
(2002, '01', 2002, 12, 'OVS', 'Chee Song Frozen Food Sdn Bhd', 'MR. LEE', 'SGD', 'EXEM', 0, 'jialin', 0, '2014-04-03 04:34:03', '2014-04-03 04:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `delivery_driver` int(11) NOT NULL,
  `cust_no` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_items`
--

CREATE TABLE IF NOT EXISTS `delivery_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `delivery_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forecasts`
--

CREATE TABLE IF NOT EXISTS `forecasts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `user_id` int(11) NOT NULL,
  `vend_do` int(11) NOT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forecast_daily`
--

CREATE TABLE IF NOT EXISTS `forecast_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `forecast_id` int(11) NOT NULL,
  `forecast_weekly_id` int(11) NOT NULL,
  `forecast_day` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forecast_items`
--

CREATE TABLE IF NOT EXISTS `forecast_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `forecast_daily_id` int(11) NOT NULL,
  `forecast_qty` int(11) NOT NULL,
  `forecast_qty_produced` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forecast_monthly`
--

CREATE TABLE IF NOT EXISTS `forecast_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `forecast_id` int(11) NOT NULL,
  `forecast_from` date NOT NULL,
  `forecast_to` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forecast_weekly`
--

CREATE TABLE IF NOT EXISTS `forecast_weekly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `forecast_id` int(11) NOT NULL,
  `forecast_monthly_id` int(11) NOT NULL,
  `forecast_from` date NOT NULL,
  `forecast_to` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_no` int(11) NOT NULL,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_descr` text COLLATE utf8_unicode_ci,
  `cat_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `is_sell` tinyint(1) NOT NULL DEFAULT '0',
  `is_buy` tinyint(1) NOT NULL DEFAULT '0',
  `is_inventory` tinyint(1) NOT NULL DEFAULT '0',
  `flang_descr` text COLLATE utf8_unicode_ci,
  `is_new` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_no`, `comp_code`, `item_descr`, `cat_code`, `uom`, `is_sell`, `is_buy`, `is_inventory`, `flang_descr`, `is_new`, `inactive`, `created_at`, `updated_at`) VALUES
(3, 201, '01', 'Spring Chicken Cut(BBQ/Fried)', 'MRC', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(4, 202, '01', 'Spring Chicken Cut(1/2, 1/4)(Fried)', 'MRC', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(5, 301, '01', 'Seafood Pkt', 'SFD', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(6, 302, '01', 'Onion Ring', 'SFD', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(9, 501, '01', 'Fish Patties', 'PAT', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(10, 502, '01', 'Chicken Patties', 'PAT', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(14, 702, '01', 'Marinates Seasioning (2 in 1)', 'OTH', 'KG', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(17, 901, '01', 'Box(GR/TB/TF/R/G)', 'PCK', 'BOX', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(22, 401, '01', 'French Fries - Shoesting(TF)', 'PTS', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(23, 402, '01', 'French Fries - Crinkle Cut(SL)', 'PTS', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(24, 601, '01', 'Brown Sauce', 'SAU', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(25, 602, '01', 'Merengo Sauce', 'SAU', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(26, 701, '01', 'Chicken Breading Mix', 'OTH', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(27, 801, '01', 'Cooking Oil(TF)', 'OIL', 'TIN', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(28, 802, '01', 'Small Bun(MS/G)', 'OTH', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(29, 902, '01', 'Rice Box/Burger Box', 'PCK', 'ROLL', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(30, 101, '01', 'Spring Chicken Whole 800gm', 'FRC', 'PCS', 1, 1, 1, 'Spring Chicken Whole 800gm', 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(31, 102, '01', 'Spring Chicken Cut(1/2,1/4)', 'FRC', 'PCS', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(32, 710, '01', 'Spaghetti', 'SAU', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(33, 711, '01', 'Salt', 'SAU', 'GM', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(34, 713, '01', 'Fresh Chilli', 'SSN', 'PCS', 1, 1, 1, '', 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(35, 714, '01', 'BBQ chilli', 'SSN', 'PCS', 1, 1, 1, '', 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(36, 810, '01', 'Water', 'OIL', 'LTR', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(37, 715, '01', 'Rice Chilli', 'SSN', 'PCS', 1, 1, 1, '', 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(38, 712, '01', 'Chicken Nuggets', 'CKP', 'PKT', 1, 1, 1, NULL, 0, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `item_cust_grp_prices`
--

CREATE TABLE IF NOT EXISTS `item_cust_grp_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `cust_group_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `item_cust_grp_prices`
--

INSERT INTO `item_cust_grp_prices` (`id`, `comp_code`, `item_id`, `cust_group_code`, `uom`, `price`, `created_at`, `updated_at`) VALUES
(1, '01', 5, 'LOC', 'PKT', '1.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(2, '01', 25, 'LOC', 'PKT', '7.20', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(3, '01', 6, 'OVS', 'PKT', '6.08', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(4, '01', 24, 'OVS', 'PKT', '6.60', '2014-04-03 04:34:07', '2014-04-03 04:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `item_cust_prices`
--

CREATE TABLE IF NOT EXISTS `item_cust_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `cust_no` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item_cust_prices`
--

INSERT INTO `item_cust_prices` (`id`, `comp_code`, `item_id`, `cust_no`, `uom`, `price`, `created_at`, `updated_at`) VALUES
(1, '01', 4, 1000, 'PCS', '5.70', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(2, '01', 3, 1001, 'PCS', '4.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(3, '01', 30, 1002, 'PCS', '3.53', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(4, '01', 9, 2000, 'PCS', '4.27', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(5, '01', 30, 2000, 'PCS', '3.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(6, '01', 10, 2001, 'PCS', '4.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(7, '01', 31, 2001, 'PCS', '4.95', '2014-04-03 04:34:07', '2014-04-03 04:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `item_list_prices`
--

CREATE TABLE IF NOT EXISTS `item_list_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `item_list_prices`
--

INSERT INTO `item_list_prices` (`id`, `comp_code`, `item_id`, `uom`, `price`, `created_at`, `updated_at`) VALUES
(1, '01', 1, 'CTN', '3.60', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(2, '01', 2, 'PCS', '5.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(3, '01', 3, 'PCS', '4.70', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(4, '01', 4, 'PCS', '6.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(5, '01', 5, 'PKT', '1.40', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(6, '01', 6, 'PKT', '6.40', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(7, '01', 7, 'PKT', '5.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(8, '01', 8, 'PKT', '5.20', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(9, '01', 9, 'PCS', '4.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(10, '01', 10, 'PCS', '4.70', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(11, '01', 11, 'KG', '6.80', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(12, '01', 12, 'KG', '8.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(13, '01', 13, 'KG', '23.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(14, '01', 14, 'CTN', '84.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(15, '01', 14, 'KG', '7.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(16, '01', 15, 'KG', '28.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(17, '01', 16, 'KG', '0.13', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(18, '01', 17, 'BOX', '36.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(19, '01', 18, 'PCS', '4.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(20, '01', 19, 'PCS', '80.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(21, '01', 20, 'PCS', '80.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(22, '01', 21, 'PCS', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(23, '01', 22, 'PKT', '5.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(24, '01', 23, 'PKT', '5.20', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(25, '01', 24, 'PKT', '6.80', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(26, '01', 25, 'PKT', '8.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(27, '01', 26, 'PKT', '23.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(28, '01', 27, 'CTN', '171.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(29, '01', 27, 'KG', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(30, '01', 27, 'TIN', '28.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(31, '01', 28, 'PCS', '0.13', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(32, '01', 29, 'ROLL', '4.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(33, '01', 30, 'PCS', '3.60', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(34, '01', 31, 'PCS', '5.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(35, '01', 32, 'KG', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(36, '01', 32, 'PKT', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(37, '01', 33, 'GM', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(38, '01', 34, 'PCS', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(39, '01', 35, 'PCS', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(40, '01', 36, 'LTR', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(41, '01', 37, 'PCS', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(42, '01', 38, 'PKT', '0.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `item_period_prices`
--

CREATE TABLE IF NOT EXISTS `item_period_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `cust_group_code` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_no` int(11) DEFAULT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `s_no` int(11) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `item_period_prices`
--

INSERT INTO `item_period_prices` (`id`, `comp_code`, `item_id`, `cust_group_code`, `cust_no`, `uom`, `s_no`, `from_date`, `to_date`, `price`, `created_at`, `updated_at`) VALUES
(1, '01', 3, NULL, NULL, 'PCS', 6, '2014-02-14 00:00:00', '2014-02-18 00:00:00', '4.61', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(2, '01', 5, NULL, NULL, 'PKT', 8, '2014-02-14 00:00:00', '2014-02-24 00:00:00', '1.20', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(3, '01', 22, NULL, NULL, 'PKT', 1, '2014-02-14 00:00:00', '2014-02-20 00:00:00', '4.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(4, '01', 23, NULL, NULL, 'PKT', 2, '2014-02-22 00:00:00', '2014-02-24 00:00:00', '5.00', '2014-04-03 04:34:07', '2014-04-03 04:34:07'),
(5, '01', 30, NULL, NULL, 'PCS', 7, '2014-02-14 00:00:00', '2014-02-20 00:00:00', '3.50', '2014-04-03 04:34:07', '2014-04-03 04:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `item_price_seqs`
--

CREATE TABLE IF NOT EXISTS `item_price_seqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `price_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seq` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `item_price_seqs`
--

INSERT INTO `item_price_seqs` (`id`, `comp_code`, `price_type`, `seq`, `created_at`, `updated_at`) VALUES
(1, '01', 'in_item_cust_price', 1, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(2, '01', 'in_item_custgrp_price', 2, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(3, '01', 'in_item_period_price', 4, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(4, '01', 'in_item_list_price', 5, '2014-04-03 04:34:05', '2014-04-03 04:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `login_session`
--

CREATE TABLE IF NOT EXISTS `login_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `client_id` int(11) NOT NULL,
  `cleint_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_ref_table` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_red_id` int(11) NOT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lots`
--

CREATE TABLE IF NOT EXISTS `lots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `type` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `lot_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_barcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_02_19_023556_confide_setup_users_table', 1),
('2014_02_23_121124_entrust_setup_tables', 1),
('2014_02_24_030727_create_customers_table', 1),
('2014_02_24_162738_create_customeraddrs_table', 1),
('2014_02_24_234251_create_vendors_table', 1),
('2014_02_25_100842_create_items_table', 1),
('2014_02_26_001909_create_categories_table', 1),
('2014_02_26_174338_create_configs_table', 1),
('2014_02_26_174957_create_torders_table', 1),
('2014_02_26_235350_create_torder_items_table', 1),
('2014_02_27_000434_create_uom_table', 1),
('2014_02_27_001146_create_preset_items_table', 1),
('2014_02_27_001808_create_item_price_seqs_table', 1),
('2014_02_27_002332_create_ac_periods_table', 1),
('2014_02_27_010727_create_admins_table', 1),
('2014_02_27_144929_create_curr_exch_rate_table', 1),
('2014_02_27_152912_create_item_list_prices_table', 1),
('2014_02_27_152950_create_item_cust_grp_prices_table', 1),
('2014_02_27_153030_create_item_cust_prices_table', 1),
('2014_02_27_153103_create_item_period_prices_table', 1),
('2014_03_06_174944_create_drivers_table', 1),
('2014_03_08_165138_create_porders_table', 1),
('2014_03_08_165214_create_pordered_items_table', 1),
('2014_03_08_165343_create_porder_log_entries_table', 1),
('2014_03_08_165923_create_forecasts_table', 1),
('2014_03_08_165958_create_forecast_items_table', 1),
('2014_03_08_170119_create_forecast_daily_table', 1),
('2014_03_08_170144_create_forecast_weekly_table', 1),
('2014_03_10_095733_create_clients_table', 1),
('2014_03_10_100227_create_login_session_table', 1),
('2014_03_10_160650_create_forecast_monthly_table', 1),
('2014_03_10_164416_create_lots_table', 1),
('2014_03_10_165502_create_delivery_table', 1),
('2014_03_10_165523_create_delivery_items_table', 1),
('2014_03_10_165857_create_return_order_table', 1),
('2014_03_10_170720_create_return_items_table', 1),
('2014_03_12_081009_create_staffs_table', 1),
('2014_04_02_010433_create_orders_xml_table', 1),
('2014_04_02_215818_create_ac_period_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_xml`
--

CREATE TABLE IF NOT EXISTS `orders_xml` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `xml_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curl_msg_raw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `send_status` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xml_contents` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=99 ;

--
-- Dumping data for table `orders_xml`
--

INSERT INTO `orders_xml` (`id`, `type`, `xml_name`, `curl_msg_raw`, `send_status`, `xml_contents`, `deleted_at`, `created_at`, `updated_at`) VALUES
(43, 'to', 'SOR-07140400101', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><doc_date>2014/04/04</doc_date><cust_no>1003</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MS. LILY</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>erwin</salesman><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>121.80</tax_amt><disc_amt>0</disc_amt><total>1740.00</total><so_status>N</so_status><remarks></remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Patties</item_descr><cust_part_no>501</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>2</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chicken Cut(BBQ/Fried)</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>4.70</price><total>940.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>940.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>65.8</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>3</s_no><item_id>5</item_id><item_no>301</item_no><item_descr>Seafood Pkt</item_descr><cust_part_no>301</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>1.00</price><total>200.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>14</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 06:45:18', '2014-04-04 06:45:18'),
(44, 'toc', 'SOR-07140400101', NULL, '-7', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><doc_date>2014/04/04</doc_date><cust_no>1003</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MS. LILY</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>erwin</salesman><curr_code>SGD</curr_code><exch_rate>1.0000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>128.80</tax_amt><disc_amt>0</disc_amt><total>1840.00</total><so_status>N</so_status><remarks></remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Pattie</item_descr><cust_part_no>501</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>2</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chic</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>4.70</price><total>940.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>940.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>65.80</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>3</s_no><item_id>5</item_id><item_no>301</item_no><item_descr>Seafood Pkt</item_descr><cust_part_no>301</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>300</qty_order><price>1.00</price><total>300.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>14.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 06:45:51', '2014-04-04 06:45:51'),
(45, 'po', 'POR-07140400101', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400101</doc_no><doc_date>2014/04/04</doc_date><vend_no>3001</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Ms. Lau</cont_name><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><tax_amt>113.4</tax_amt><disc_amt>0</disc_amt><total>1733.40</total><status>N</status><remarks></remarks></master>23456</array></plist>', NULL, '2014-04-04 06:50:41', '2014-04-04 06:50:41'),
(46, 'po', 'POR-07140400104', NULL, '-1', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master>\n<comp_code></comp_code>\n<doc_no>POR-07140400104</doc_no>\n<doc_date>2014/04/04</doc_date>\n<vend_no>4001</vend_no>\n<ac_period>201404</ac_period>\n<ref_info>-</ref_info>\n<cont_name>Mr. Steven Lee</cont_name>\n<curr_code>MYR</curr_code><exch_rate>2.500000</exch_rate><tax_amt>0.49</tax_amt><disc_amt>0</disc_amt><total>7.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400104</doc_no><s_no>1</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><vend_part_no>302</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>1</qty_order><price>5.00</price><total>5.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>5.00</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400104</doc_no><s_no>2</s_no><item_id>23</item_id><item_no>402</item_no><item_descr>French Fries - Crinkle Cut(SL)</item_descr><vend_part_no>402</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>1</qty_order><price>2.80</price><total>2.80</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2.80</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400104</doc_no><s_no>3</s_no><item_id>28</item_id><item_no>802</item_no><item_descr>Small Bun(MS/G)</item_descr><vend_part_no>802</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>1</qty_order><price>0.06</price><total>0.06</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>0.06</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 07:46:48', '2014-04-04 07:46:48'),
(47, 'po', 'POR-07140400105', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400105</doc_no><doc_date>2014/04/04</doc_date><vend_no>4001</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Mr. Steven Lee</cont_name><curr_code>MYR</curr_code><exch_rate>2.500000</exch_rate><tax_amt>44.38</tax_amt><disc_amt>0</disc_amt><total>634.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400105</doc_no><s_no>1</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><vend_part_no>302</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>10</qty_order><price>5.00</price><total>50.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>50.00</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400105</doc_no><s_no>2</s_no><item_id>23</item_id><item_no>402</item_no><item_descr>French Fries - Crinkle Cut(SL)</item_descr><vend_part_no>402</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>2.80</price><total>560.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>560.00</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400105</doc_no><s_no>3</s_no><item_id>28</item_id><item_no>802</item_no><item_descr>Small Bun(MS/G)</item_descr><vend_part_no>802</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>400</qty_order><price>0.06</price><total>24.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>24.00</net_amount><tax_type>ZERO</tax_type><tax_rate>0</tax_rate><tax_amt>0</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 07:49:11', '2014-04-04 07:49:12'),
(48, 'po', 'POR-07140400106', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400106</doc_no><doc_date>2014/04/04</doc_date><vend_no>3000</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Mr. Andrew</cont_name><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><tax_amt>318.50</tax_amt><disc_amt>0</disc_amt><total>4550.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400106</doc_no><s_no>1</s_no><item_id>14</item_id><item_no>702</item_no><item_descr>Marinates Seasioning (2 in 1)</item_descr><vend_part_no>702</vend_part_no><uom>KG</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.80</price><total>480.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>480.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>33.6</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400106</doc_no><s_no>2</s_no><item_id>24</item_id><item_no>601</item_no><item_descr>Brown Sauce</item_descr><vend_part_no>601</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>300</qty_order><price>4.50</price><total>1350.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1350.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>94.5</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400106</doc_no><s_no>3</s_no><item_id>25</item_id><item_no>602</item_no><item_descr>Merengo Sauce</item_descr><vend_part_no>602</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>6.00</price><total>1200.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>84</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400106</doc_no><s_no>4</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>400</qty_order><price>3.80</price><total>1520.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1520.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>106.4</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 08:18:17', '2014-04-04 08:18:17'),
(49, 'toc', 'SOR-07140400101', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><doc_date>2014/04/04</doc_date><cust_no>1003</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MS. LILY</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>erwin</salesman><curr_code>SGD</curr_code><exch_rate>1.0000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>98.70</tax_amt><disc_amt>0</disc_amt><total>1410.00</total><so_status>N</so_status><remarks></remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Pattie</item_descr><cust_part_no>501</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>2</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chic</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.70</price><total>470.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>470.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>65.80</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400101</doc_no><s_no>3</s_no><item_id>5</item_id><item_no>301</item_no><item_descr>Seafood Pkt</item_descr><cust_part_no>301</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>340</qty_order><price>1.00</price><total>340.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>340.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>14.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 08:20:48', '2014-04-04 08:27:28'),
(50, 'to', 'SOR-07140400102', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400102</doc_no><doc_date>2014/04/04</doc_date><cust_no>1003</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MS. LILY</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>erwin</salesman><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>119.70</tax_amt><disc_amt>0</disc_amt><total>1710.00</total><so_status>N</so_status><remarks>tets</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400102</doc_no><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Patties</item_descr><cust_part_no>501</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400102</doc_no><s_no>2</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chicken Cut(BBQ/Fried)</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.70</price><total>470.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>470.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>32.9</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400102</doc_no><s_no>3</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><cust_part_no>302</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.40</price><total>640.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>640.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>44.8</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 08:23:13', '2014-04-04 08:28:11'),
(51, 'to', 'SOR-07140400103', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400103</doc_no><doc_date>2014/04/04</doc_date><cust_no>1003</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MS. LILY</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>erwin</salesman><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>42.00</tax_amt><disc_amt>0</disc_amt><total>600.00</total><so_status>N</so_status><remarks></remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400103</doc_no><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Patties</item_descr><cust_part_no>501</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42</tax_amt><so_etd>2014-04-10</so_etd><so_eta>2014-04-10</so_eta></details></array></plist>', NULL, '2014-04-04 08:27:05', '2014-04-04 08:28:11'),
(52, 'to', 'SOR-07140400104', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><doc_date>2014/04/04</doc_date><cust_no>1001</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MR. TAN</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>durai</salesman><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>638.40</tax_amt><disc_amt>0</disc_amt><total>9120.00</total><so_status>N</so_status><remarks>test remark</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>1</s_no><item_id>26</item_id><item_no>701</item_no><item_descr>Chicken Breading Mix</item_descr><cust_part_no>701</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>23.00</price><total>2300.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>161</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>2</s_no><item_id>27</item_id><item_no>801</item_no><item_descr>Cooking Oil(TF)</item_descr><cust_part_no>801</cust_part_no><uom>TIN</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>29.00</price><total>5800.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>5800.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>406</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>3</s_no><item_id>30</item_id><item_no>101</item_no><item_descr>Spring Chicken Whole 800gm</item_descr><cust_part_no>101</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>300</qty_order><price>3.40</price><total>1020.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1020.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>71.4</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 08:29:38', '2014-04-04 08:29:38'),
(53, 'toc', 'SOR-07140400104', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><doc_date>2014/04/04</doc_date><cust_no>1001</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MR. TAN</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>durai</salesman><curr_code>SGD</curr_code><exch_rate>1.0000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>387.80</tax_amt><disc_amt>0</disc_amt><total>5540.00</total><so_status>N</so_status><remarks>test remark</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>1</s_no><item_id>26</item_id><item_no>701</item_no><item_descr>Chicken Bre</item_descr><cust_part_no>701</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>23.00</price><total>2300.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>161.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>2</s_no><item_id>27</item_id><item_no>801</item_no><item_descr>Cooking Oil</item_descr><cust_part_no>801</cust_part_no><uom>TIN</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>29.00</price><total>2900.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2900.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>406.00</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400104</doc_no><s_no>3</s_no><item_id>30</item_id><item_no>101</item_no><item_descr>Spring Chic</item_descr><cust_part_no>101</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.40</price><total>340.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>340.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>71.40</tax_amt><so_etd>2014-04-17</so_etd><so_eta>2014-04-17</so_eta></details></array></plist>', NULL, '2014-04-04 08:30:31', '2014-04-04 08:30:31'),
(54, 'po', 'POR-07140400107', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400107</doc_no><doc_date>2014/04/04</doc_date><vend_no>3000</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Mr. Andrew</cont_name><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><tax_amt>133.70</tax_amt><disc_amt>0</disc_amt><total>1910.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400107</doc_no><s_no>1</s_no><item_id>14</item_id><item_no>702</item_no><item_descr>Marinates Seasioning (2 in 1)</item_descr><vend_part_no>702</vend_part_no><uom>KG</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.80</price><total>480.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>480.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>33.6</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400107</doc_no><s_no>2</s_no><item_id>24</item_id><item_no>601</item_no><item_descr>Brown Sauce</item_descr><vend_part_no>601</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.50</price><total>450.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>450.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>31.5</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400107</doc_no><s_no>3</s_no><item_id>25</item_id><item_no>602</item_no><item_descr>Merengo Sauce</item_descr><vend_part_no>602</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400107</doc_no><s_no>4</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.80</price><total>380.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>380.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>26.6</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 08:52:03', '2014-04-04 08:52:03'),
(55, 'po', 'POR-07140400108', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><doc_date>2014/04/04</doc_date><vend_no>3000</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Mr. Andrew</cont_name><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><tax_amt>394.80</tax_amt><disc_amt>0</disc_amt><total>5640.00</total><status>N</status><remarks>Test</remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>1</s_no><item_id>14</item_id><item_no>702</item_no><item_descr>Marinates Seasioning (2 in 1)</item_descr><vend_part_no>702</vend_part_no><uom>KG</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.80</price><total>480.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>480.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>33.6</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>2</s_no><item_id>24</item_id><item_no>601</item_no><item_descr>Brown Sauce</item_descr><vend_part_no>601</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>500</qty_order><price>4.50</price><total>2250.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2250.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>157.5</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>3</s_no><item_id>25</item_id><item_no>602</item_no><item_descr>Merengo Sauce</item_descr><vend_part_no>602</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>6.00</price><total>1200.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>84</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>4</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>450</qty_order><price>3.80</price><total>1710.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1710.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>119.7</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 08:53:08', '2014-04-04 08:53:08'),
(56, 'poc', 'POR-07140400108', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><doc_date>2014/04/04</doc_date><vend_no>3000</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Mr. Andrew</cont_name><curr_code>SGD</curr_code><exch_rate>1.00</exch_rate><tax_amt>388.50</tax_amt><disc_amt>0</disc_amt><total>5550.00</total><status>N</status><remarks>Test</remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>1</s_no><item_id>14</item_id><item_no>702</item_no><item_descr>Marinates Seasioning (2 in 1)</item_descr><vend_part_no>702</vend_part_no><uom>KG</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.80</price><total>480.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>480.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>7.00</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>2</s_no><item_id>24</item_id><item_no>601</item_no><item_descr>Brown Sauce</item_descr><vend_part_no>601</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>500</qty_order><price>4.50</price><total>2250.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2250.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>7.00</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>3</s_no><item_id>25</item_id><item_no>602</item_no><item_descr>Merengo Sauce</item_descr><vend_part_no>602</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>6.00</price><total>1200.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>7.00</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>4</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>400</qty_order><price>3.80</price><total>1520.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1520.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>7.00</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400108</doc_no><s_no>5</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><vend_part_no>302</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>1.00</price><total>100.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>100.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>7.00</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 09:10:14', '2014-04-04 09:10:14'),
(57, 'to', 'SOR-07140400105', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><doc_date>2014/04/04</doc_date><cust_no>1005</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MR. ALAN</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>jialin</salesman><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>119.70</tax_amt><disc_amt>0</disc_amt><total>1710.00</total><so_status>N</so_status><remarks>this is new order</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chicken Cut(BBQ/Fried)</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.70</price><total>470.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>470.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>32.9</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><item_descr>Spring Chicken Cut(1/2, 1/4)(Fried)</item_descr><cust_part_no>202</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.00</price><total>600.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>3</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><cust_part_no>302</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>6.40</price><total>640.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>640.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>44.8</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details></array></plist>', NULL, '2014-04-04 09:28:01', '2014-04-04 09:28:01'),
(58, 'toc', 'SOR-07140400105', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><doc_date>2014/04/04</doc_date><cust_no>1005</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MR. ALAN</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>jialin</salesman><curr_code>SGD</curr_code><exch_rate>1.0000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>296.10</tax_amt><disc_amt>0</disc_amt><total>4230.00</total><so_status>N</so_status><remarks>this is new order</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chic</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.70</price><total>470.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>470.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>32.90</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><item_descr>Spring Chic</item_descr><cust_part_no>202</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>6.00</price><total>1200.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42.00</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>3</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><cust_part_no>302</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>400</qty_order><price>6.40</price><total>2560.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>2560.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>44.80</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details></array></plist>', NULL, '2014-04-04 09:28:38', '2014-04-04 09:28:39'),
(59, 'toc', 'SOR-07140400105', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><doc_date>2014/04/04</doc_date><cust_no>1005</cust_no><ac_period>201404</ac_period><cust_po_no>-</cust_po_no><cust_po_date>2014/04/04</cust_po_date><cont_name>MR. ALAN</cont_name><ship_addr_ref>2</ship_addr_ref><salesman>jialin</salesman><curr_code>SGD</curr_code><exch_rate>1.0000</exch_rate><absorb_tax>0</absorb_tax><tax_amt>251.30</tax_amt><disc_amt>0</disc_amt><total>3590.00</total><so_status>N</so_status><remarks>this is new order</remarks> </master><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chic</item_descr><cust_part_no>201</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>4.70</price><total>470.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>470.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>32.90</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><item_descr>Spring Chic</item_descr><cust_part_no>202</cust_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>6.00</price><total>1200.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1200.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>42.00</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details><details><comp_code>01</comp_code><doc_no>SOR-07140400105</doc_no><s_no>3</s_no><item_id>6</item_id><item_no>302</item_no><item_descr>Onion Ring</item_descr><cust_part_no>302</cust_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>300</qty_order><price>6.40</price><total>1920.00</total><disc_type>0</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1920.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>44.80</tax_amt><so_etd>2014-04-18</so_etd><so_eta>2014-04-18</so_eta></details></array></plist>', NULL, '2014-04-04 09:29:14', '2014-04-04 09:29:15'),
(60, 'po', 'POR-07140400109', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><doc_date>2014/04/04</doc_date><vend_no>3002</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Ms. Siew Lan</cont_name><curr_code>SGD</curr_code><exch_rate>1.000000</exch_rate><tax_amt>259.00</tax_amt><disc_amt>0</disc_amt><total>3700.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chicken Cut(BBQ/Fried)</item_descr><vend_part_no>201</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>21</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>2</s_no><item_id>10</item_id><item_no>502</item_no><item_descr>Chicken Patties</item_descr><vend_part_no>502</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>21</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>3</s_no><item_id>26</item_id><item_no>701</item_no><item_descr>Chicken Breading Mix</item_descr><vend_part_no>701</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>16.00</price><total>1600.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>112</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>4</s_no><item_id>30</item_id><item_no>101</item_no><item_descr>Spring Chicken Whole 800gm</item_descr><vend_part_no>101</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>2.10</price><total>210.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>210.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>14.7</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>5</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>21</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>6</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Patties</item_descr><vend_part_no>501</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>300</qty_order><price>3.30</price><total>990.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>990.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>69.3</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 09:30:46', '2014-04-04 09:30:46'),
(61, 'poc', 'POR-07140400109', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><doc_date>2014/04/04</doc_date><vend_no>3002</vend_no><ac_period>201404</ac_period><ref_info>-</ref_info><cont_name>Ms. Siew Lan</cont_name><curr_code>SGD</curr_code><exch_rate>1.00</exch_rate><tax_amt>235.90</tax_amt><disc_amt>0</disc_amt><total>3370.00</total><status>N</status><remarks></remarks></master><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><item_descr>Spring Chicken Cut(BBQ/Fried)</item_descr><vend_part_no>201</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>2</s_no><item_id>10</item_id><item_no>502</item_no><item_descr>Chicken Patties</item_descr><vend_part_no>502</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>3</s_no><item_id>26</item_id><item_no>701</item_no><item_descr>Chicken Breading Mix</item_descr><vend_part_no>701</vend_part_no><uom>PKT</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>16.00</price><total>1600.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>1600.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>4</s_no><item_id>30</item_id><item_no>101</item_no><item_descr>Spring Chicken Whole 800gm</item_descr><vend_part_no>101</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>2.10</price><total>210.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>210.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>5</s_no><item_id>31</item_id><item_no>102</item_no><item_descr>Spring Chicken Cut(1/2,1/4)</item_descr><vend_part_no>102</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>100</qty_order><price>3.00</price><total>300.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>300.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details><details><comp_code>01</comp_code><doc_no>POR-07140400109</doc_no><s_no>6</s_no><item_id>9</item_id><item_no>501</item_no><item_descr>Fish Patties</item_descr><vend_part_no>501</vend_part_no><uom>PCS</uom><uom_cf>1.0000</uom_cf><qty_order>200</qty_order><price>3.30</price><total>660.00</total><disc_type>N</disc_type><disc_value>0</disc_value><disc_amount>0</disc_amount><net_amount>660.00</net_amount><tax_type>GST7</tax_type><tax_rate>7</tax_rate><tax_amt>46.20</tax_amt><po_etd>2014/04/04</po_etd><po_eta>2014/04/04</po_eta></details></array></plist>', NULL, '2014-04-04 09:31:08', '2014-04-04 09:31:08'),
(62, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>This is a test bom00002</descr><item_id>2</item_id><item_no>6</item_no><qty>10</qty><uom>PKT-1.000000</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>Test Remarks</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>Spring Chicken Cut(BBQ/Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><qty>Spring Chicken Cut(1/2, 1/4)(Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-21 09:52:32', '2014-04-21 09:52:32'),
(63, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>testing</descr><item_id>2</item_id><item_no>6</item_no><qty>10</qty><uom>PKT-1.000000</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>remarks</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>Spring Chicken Cut(BBQ/Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><qty>Spring Chicken Cut(1/2, 1/4)(Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-21 10:06:18', '2014-04-21 10:06:18'),
(64, 'bo', 'BOMXML-BOM00005', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00005</bom_code><revision_no>1</revision_no><descr>description boom</descr><item_id>2</item_id><item_no>5</item_no><qty>3</qty><uom>PKT-1.000000</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remarks</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>Spring Chicken Whole 800gm</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>2</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>Spring Chicken Cut(1/2,1/4)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 00:29:58', '2014-04-22 00:29:58');
INSERT INTO `orders_xml` (`id`, `type`, `xml_name`, `curl_msg_raw`, `send_status`, `xml_contents`, `deleted_at`, `created_at`, `updated_at`) VALUES
(65, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?>\n<plist version="1.0">\n<array>\n<master>\n<comp_code>01</comp_code>\n<bom_id>0</bom_id>\n<bom_code>BOM00002</bom_code>\n<revision_no>1</revision_no>\n<descr>description</descr>\n<item_id>3</item_id>\n<item_no>4</item_no>\n<qty>100</qty>\n<uom>PCS-1.000000</uom>\n<uom_cf>1.000000</uom_cf>\n<status>1</status>\n<is_kitting_bom>1</is_kitting_bom>\n<remarks>remark</remarks>\n<approved>1</approved>\n</master>\n<details><comp_code>01</comp_code><bom_id>3</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>Spring Chicken Whole 800gm</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>3</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>Spring Chicken Cut(1/2,1/4)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 00:32:59', '2014-04-22 00:32:59'),
(66, 'bo', 'BOMXML-BOM00004', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?>\n<plist version="1.0">\n<array>\n<master>\n<comp_code>01</comp_code>\n<bom_id>0</bom_id>\n<bom_code>BOM00004</bom_code>\n<revision_no>1</revision_no>\n<descr>Test Description</descr>\n<item_id>4</item_id>\n<item_no>17</item_no>\n<qty>10</qty>\n<uom>BOX-1.000000</uom>\n<uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>Remark for BOM</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>4</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>Spring Chicken Cut(BBQ/Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>4</bom_id><s_no>2</s_no><item_id>14</item_id><item_no>702</item_no><qty>Marinates Seasioning (2 in 1)</qty><uom>KG</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 02:31:56', '2014-04-22 02:31:56'),
(67, 'bo', 'BOMXML-BOM10004', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM10004</bom_code><revision_no>1</revision_no><descr>test</descr><item_id>5</item_id><item_no>23</item_no><qty>10</qty><uom>PKT-1.000000</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>test</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>5</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>Spring Chicken Whole 800gm</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>5</bom_id><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><qty>Spring Chicken Cut(1/2, 1/4)(Fried)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 02:36:05', '2014-04-22 02:36:05'),
(68, 'bo', 'BOMXML-BOM00006', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00006</bom_code><revision_no>1</revision_no><descr>test</descr><item_id>6</item_id><item_no>4</item_no><qty>10</qty><uom>none</uom><uom_cf></uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>test</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>6</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>Spring Chicken Whole 800gm</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type></bom_type></details><details><comp_code>01</comp_code><bom_id>6</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>Spring Chicken Cut(1/2,1/4)</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type></bom_type></details></array></plist>', NULL, '2014-04-22 02:38:56', '2014-04-22 02:38:56'),
(69, 'bo', 'BOMXML-BOM00004', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00004</bom_code><revision_no>1</revision_no><descr>desc</descr>\n<item_id>7</item_id>\n<item_no>4</item_no>\n<qty>100</qty>\n<uom>PCS</uom>\n<uom_cf>1.000000</uom_cf>\n<status>1</status>\n<is_kitting_bom>0</is_kitting_bom>\n<remarks>test</remarks>\n<approved>1</approved>\n</master>\n<details>\n<comp_code>01</comp_code>\n<bom_id>7</bom_id>\n<s_no>1</s_no>\n<item_id>3</item_id>\n<item_no>201</item_no>\n<qty>Spring Chicken Cut(BBQ/Fried)</qty>\n<uom>PCS</uom>\n<uom_cf>1.0000</uom_cf>\n<bom_type>R</bom_type>\n</details>\n<details>\n<comp_code>01</comp_code>\n<bom_id>7</bom_id>\n<s_no>2</s_no>\n<item_id>4</item_id>\n<item_no>202</item_no>\n<qty>Spring Chicken Cut(1/2, 1/4)(Fried)</qty>\n<uom>PCS</uom>\n<uom_cf>1.0000</uom_cf>\n<bom_type>R</bom_type>\n</details>\n</array>\n</plist>', NULL, '2014-04-22 02:41:42', '2014-04-22 02:41:42'),
(70, 'bo', 'BOMXML-BOM00100', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?>\n<plist version="1.0">\n<array>\n<master>\n<comp_code>01</comp_code>\n<bom_id>0</bom_id>\n<bom_code>BOM00100</bom_code>\n<revision_no>1</revision_no>\n<descr>test bom description</descr>\n<item_id>8</item_id>\n<item_no>4</item_no>\n<qty>1</qty>\n<uom>PCS</uom>\n<uom_cf>1.000000</uom_cf>\n<status>1</status>\n<is_kitting_bom>0</is_kitting_bom>\n<remarks>test remark</remarks>\n<approved>1</approved></master>\n\n<details>\n<comp_code>01</comp_code>\n<bom_id>8</bom_id>\n<s_no>1</s_no>\n<item_id>30</item_id>\n<item_no>101</item_no>\n<qty>100</qty>\n<uom>PCS</uom>\n<uom_cf>1.0000</uom_cf>\n<bom_type>R</bom_type>\n</details>\n\n<details>\n<comp_code>01</comp_code>\n<bom_id>8</bom_id>\n<s_no>2</s_no>\n<item_id>31</item_id>\n<item_no>102</item_no>\n<qty>100</qty>\n<uom>PCS</uom>\n<uom_cf>1.0000</uom_cf>\n<bom_type>R</bom_type>\n</details>\n</array></plist>', NULL, '2014-04-22 02:48:15', '2014-04-22 02:48:15'),
(71, 'bo', 'BOMXML-BOM00101', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00101</bom_code><revision_no>1</revision_no><descr>test</descr><item_id>9</item_id><item_no>5</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>bom remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>9</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>9</bom_id><s_no>2</s_no><item_id>26</item_id><item_no>701</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 03:10:58', '2014-04-22 03:10:58'),
(72, 'bo', 'BOMXML-BOM00101', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00101</bom_code><revision_no>1</revision_no><descr>BOM description</descr><item_id>10</item_id><item_no>14</item_no><qty>1</qty><uom>KG</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>Bom remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>10</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>10</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 03:15:22', '2014-04-22 03:15:22'),
(73, 'bo', 'BOMXML-BOM00101', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00101</bom_code><revision_no>1</revision_no><descr>test</descr><item_id>11</item_id><item_no>5</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>test</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>11</bom_id><s_no>1</s_no><item_id>9</item_id><item_no>501</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 03:17:04', '2014-04-22 03:17:04'),
(74, 'bo', 'BOMXML-BOM00101', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00101</bom_code><revision_no>1</revision_no><descr>test</descr><item_id>12</item_id><item_no>22</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>bom</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>12</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>12</bom_id><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 04:37:39', '2014-04-22 04:37:39'),
(75, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>description</descr><item_id>13</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>test descr</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>13</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 04:39:48', '2014-04-22 04:39:48'),
(76, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>description</descr><item_id>14</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>14</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 04:42:58', '2014-04-22 04:42:58'),
(77, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>descr</descr><item_id>15</item_id><item_no>4</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>item</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>15</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>15</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 04:44:37', '2014-04-22 04:44:37'),
(78, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>BOM Description</descr><item_id>16</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>16</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:07:51', '2014-04-22 05:07:51'),
(79, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>descr</descr><item_id>17</item_id><item_no>5</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>item</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>17</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>200</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:16:19', '2014-04-22 05:16:19'),
(80, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>descr</descr><item_id>18</item_id><item_no>5</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>something</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>18</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type></bom_type></details></array></plist>', NULL, '2014-04-22 05:17:42', '2014-04-22 05:17:42'),
(81, 'bo', 'BOMXML-BOM00003', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>descr</descr><item_id>18</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>item</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>18</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:20:50', '2014-04-22 05:20:50'),
(82, 'bo', 'BOMXML-BOM00003', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>item</descr><item_id>18</item_id><item_no>4</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remakr</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>18</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:22:00', '2014-04-22 05:22:00'),
(83, 'bo', 'BOMXML-BOM00002', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>item</descr><item_id>18</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>teim</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>18</bom_id><s_no>1</s_no><item_id>27</item_id><item_no>801</item_no><qty>100</qty><uom>TIN</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:23:11', '2014-04-22 05:23:11'),
(84, 'bo', 'BOMXML-BOM00002', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00002</bom_code><revision_no>1</revision_no><descr>item</descr><item_id>18</item_id><item_no>5</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>tian</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>18</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:24:19', '2014-04-22 05:24:19'),
(85, 'bo', 'BOMXML-BOM00003', NULL, 'NN', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>item</descr><item_id>19</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>item direc</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>19</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>19</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 05:38:20', '2014-04-22 05:38:20'),
(86, 'bo', 'BOMXML-BOM00003', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>descr</descr><item_id>20</item_id><item_no>4</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>20</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 06:17:36', '2014-04-22 06:17:36'),
(87, 'bo', 'BOMXML-BOM00003', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>why code is available???</descr><item_id>21</item_id><item_no>5</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>21</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 06:18:43', '2014-04-22 06:18:43'),
(88, 'bo', 'BOMXML-BOM00003', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>item</descr><item_id>22</item_id><item_no>6</item_no><qty>10</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>teaaf</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>22</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 06:22:08', '2014-04-22 06:22:12'),
(89, 'bo', 'BOMXML-BOM00003', NULL, 'NN', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>WHY!!</descr><item_id>23</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>LFC</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>23</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>23</bom_id><s_no>2</s_no><item_id>27</item_id><item_no>801</item_no><qty>100</qty><uom>TIN</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 07:06:25', '2014-04-22 07:06:26'),
(90, 'bo', 'BOMXML-BOM0004', NULL, 'NN', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM0004</bom_code><revision_no>1</revision_no><descr>bom description</descr><item_id>24</item_id><item_no>4</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>102</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 17:11:12', '2014-04-22 17:11:12'),
(91, 'bo', 'BOMXML-BOM0002', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM0002</bom_code><revision_no>1</revision_no><descr>BOM Description</descr><item_id>25</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>remakr</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 17:14:51', '2014-04-22 17:14:51'),
(92, 'bo', 'BOMXML-BOM00003', NULL, 'NN', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>tehjts</descr><item_id>26</item_id><item_no>6</item_no><qty>1</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>This is wrong for ourpose</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>2</s_no><item_id>31</item_id><item_no>This is wrong for ourpose</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-22 17:22:08', '2014-04-22 17:22:08'),
(93, 'bo', 'BOMXML-B1010', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>B1010</bom_code><revision_no>1</revision_no><descr>des</descr><item_id>27</item_id><item_no>4</item_no><qty>1</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type></bom_type></details></array></plist>', NULL, '2014-04-22 17:24:17', '2014-04-22 17:24:18'),
(94, 'sto', 'STOXML-2014/04/27', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>1</s_no><item_id>4</item_id><item_no>202</item_no><uom>PCS</uom><qty>100</qty></stocktake><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>2</s_no><item_id>9</item_id><item_no>501</item_no><uom>PCS</uom><qty>100</qty></stocktake><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>3</s_no><item_id>10</item_id><item_no>502</item_no><uom>PCS</uom><qty>100</qty></stocktake><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>4</s_no><item_id>24</item_id><item_no>601</item_no><uom>PKT</uom><qty>100</qty></stocktake><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>5</s_no><item_id>30</item_id><item_no>101</item_no><uom>PCS</uom><qty>100</qty></stocktake><stocktake><comp_code>01</stocktake><wh_id>4</wh_id><doc_date>2014-04-28</doc_date><s_no>6</s_no><item_id>35</item_id><item_no>714</item_no><uom>PCS</uom><qty>100</qty></stocktake></array></plist>', NULL, '2014-04-27 08:55:16', '2014-04-27 08:55:16'),
(95, 'bo', 'BOMXML-BOM00003', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>This is a new bom</descr><item_id>28</item_id><item_no>4</item_no><qty>10</qty><uom>PCS</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>newly added</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-27 10:42:57', '2014-04-27 10:42:57'),
(96, 'bo', 'BOMXML-BOM00003', NULL, '0', '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00003</bom_code><revision_no>1</revision_no><descr>lalal</descr><item_id>28</item_id><item_no>5</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>1</is_kitting_bom><remarks>remark</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>30</item_id><item_no>101</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-27 10:44:04', '2014-04-27 10:44:04'),
(97, 'bo', 'BOMXML-BOM00005', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>BOM00005</bom_code><revision_no>1</revision_no><descr>Newly added</descr><item_id>29</item_id><item_no>6</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>afafa</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>3</item_id><item_no>201</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>2</s_no><item_id>4</item_id><item_no>202</item_no><qty>100</qty><uom>PCS</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-27 10:46:55', '2014-04-27 10:46:55'),
(98, 'bo', 'BOMXML-ad', NULL, NULL, '<?xml version="1.0" encoding="UTF-8"?><plist version="1.0"><array><master><comp_code>01</comp_code><bom_id>0</bom_id><bom_code>ad</bom_code><revision_no>1</revision_no><descr>alala</descr><item_id>29</item_id><item_no>6</item_no><qty>100</qty><uom>PKT</uom><uom_cf>1.000000</uom_cf><status>1</status><is_kitting_bom>0</is_kitting_bom><remarks>awdafa</remarks><approved>1</approved></master><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>1</s_no><item_id>27</item_id><item_no>801</item_no><qty>100</qty><uom>TIN</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details><details><comp_code>01</comp_code><bom_id>0</bom_id><s_no>2</s_no><item_id>36</item_id><item_no>810</item_no><qty>100</qty><uom>LTR</uom><uom_cf>1.0000</uom_cf><bom_type>R</bom_type></details></array></plist>', NULL, '2014-04-27 10:50:51', '2014-04-27 10:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pordered_items`
--

CREATE TABLE IF NOT EXISTS `pordered_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `item_no` int(11) DEFAULT NULL,
  `descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `porder_id` int(11) NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `uom_cf` decimal(8,4) DEFAULT NULL,
  `unitprice` decimal(8,2) NOT NULL,
  `gst` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=93 ;

--
-- Dumping data for table `pordered_items`
--

INSERT INTO `pordered_items` (`id`, `comp_code`, `item_id`, `item_no`, `descr`, `porder_id`, `added_by`, `qty`, `uom`, `uom_cf`, `unitprice`, `gst`, `total`, `deleted_at`, `created_at`, `updated_at`) VALUES
(82, '01', 14, 702, 'Marinates Seasioning (2 in 1)', 25, 21, 100, 'KG', '1.0000', '4.80', '7.00', '480.00', NULL, '2014-04-04 08:53:08', '2014-04-04 09:10:14'),
(83, '01', 24, 601, 'Brown Sauce', 25, 21, 500, 'PKT', '1.0000', '4.50', '7.00', '2250.00', NULL, '2014-04-04 08:53:08', '2014-04-04 09:10:14'),
(84, '01', 25, 602, 'Merengo Sauce', 25, 21, 200, 'PKT', '1.0000', '6.00', '7.00', '1200.00', NULL, '2014-04-04 08:53:08', '2014-04-04 09:10:14'),
(85, '01', 31, 102, 'Spring Chicken Cut(1/2,1/4)', 25, 21, 400, 'PCS', '1.0000', '3.80', '7.00', '1520.00', NULL, '2014-04-04 08:53:08', '2014-04-04 09:10:14'),
(86, '01', 6, 302, 'Onion Ring', 25, 21, 100, 'PKT', '1.0000', '1.00', '7.00', '100.00', NULL, '2014-04-04 08:53:49', '2014-04-04 09:10:14'),
(87, '01', 3, 201, 'Spring Chicken Cut(BBQ/Fried)', 26, 21, 100, 'PCS', '1.0000', '3.00', '46.20', '300.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08'),
(88, '01', 10, 502, 'Chicken Patties', 26, 21, 100, 'PCS', '1.0000', '3.00', '46.20', '300.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08'),
(89, '01', 26, 701, 'Chicken Breading Mix', 26, 21, 100, 'PKT', '1.0000', '16.00', '46.20', '1600.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08'),
(90, '01', 30, 101, 'Spring Chicken Whole 800gm', 26, 21, 100, 'PCS', '1.0000', '2.10', '46.20', '210.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08'),
(91, '01', 31, 102, 'Spring Chicken Cut(1/2,1/4)', 26, 21, 100, 'PCS', '1.0000', '3.00', '46.20', '300.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08'),
(92, '01', 9, 501, 'Fish Patties', 26, 21, 200, 'PCS', '1.0000', '3.30', '46.20', '660.00', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `porders`
--

CREATE TABLE IF NOT EXISTS `porders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `user_id` int(11) NOT NULL,
  `vend_no` int(11) NOT NULL,
  `tax_type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_value` int(4) DEFAULT NULL,
  `vend_grp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vend_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vend_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curr_code` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curr_rate` decimal(8,2) DEFAULT NULL,
  `doc_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `gst` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `status` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `porders`
--

INSERT INTO `porders` (`id`, `comp_code`, `user_id`, `vend_no`, `tax_type`, `tax_value`, `vend_grp`, `vend_name`, `vend_addr`, `cont_person`, `cont_no`, `curr_code`, `curr_rate`, `doc_no`, `remark`, `subtotal`, `gst`, `total`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(25, '01', 21, 3000, 'GST7', 7, 'LOC', 'Boisson Singapore Private Limited', '63 Hillview Avenue #03-04 Lam Soon Industrial Building', 'Mr. Andrew', '65638899', 'SGD', '1.00', 'POR-07140400108', 'Test', '5550.00', '388.50', '5938.50', 'CHG', NULL, '2014-04-04 08:53:08', '2014-04-04 09:10:14'),
(26, '01', 21, 3002, 'GST7', 7, 'LOC', 'HU LEE IMPEX PTE LTD', '16A Chin Bee Ave', 'Ms. Siew Lan', '66756821', 'SGD', '1.00', 'POR-07140400109', '', '3370.00', '235.90', '3605.90', 'CHG', NULL, '2014-04-04 09:30:46', '2014-04-04 09:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `porder_log_entries`
--

CREATE TABLE IF NOT EXISTS `porder_log_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `user_id` int(11) NOT NULL,
  `porder_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `porder_log_entries`
--

INSERT INTO `porder_log_entries` (`id`, `comp_code`, `user_id`, `porder_id`, `title`, `content`, `deleted_at`, `created_at`, `updated_at`) VALUES
(21, '01', 21, 16, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 14:06:12<br> New Ordered Placed', NULL, '2014-04-04 06:06:12', '2014-04-04 06:06:12'),
(22, '01', 21, 17, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 14:50:41<br> New Ordered Placed', NULL, '2014-04-04 06:50:41', '2014-04-04 06:50:41'),
(23, '01', 21, 21, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 15:46:48<br> New Ordered Placed', NULL, '2014-04-04 07:46:48', '2014-04-04 07:46:48'),
(24, '01', 21, 22, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 15:49:11<br> New Ordered Placed', NULL, '2014-04-04 07:49:12', '2014-04-04 07:49:12'),
(25, '01', 21, 23, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 16:18:17<br> New Ordered Placed', NULL, '2014-04-04 08:18:17', '2014-04-04 08:18:17'),
(26, '01', 21, 23, 'arun - Amended the PO', 'New Item:9 is added with qty:200@price: $/PCS<br>', NULL, '2014-04-04 08:42:34', '2014-04-04 08:42:34'),
(27, '01', 21, 24, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 16:52:03<br> New Ordered Placed', NULL, '2014-04-04 08:52:03', '2014-04-04 08:52:03'),
(28, '01', 21, 25, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 16:53:08<br> New Ordered Placed', NULL, '2014-04-04 08:53:08', '2014-04-04 08:53:08'),
(29, '01', 21, 25, 'arun - Amended the PO', 'New Item:6 is added with qty:200@price: $1.00/PKT<br>', NULL, '2014-04-04 08:53:49', '2014-04-04 08:53:49'),
(30, '01', 21, 25, 'arun - Amended the PO', 'Item No:302 Qty Changed from: 200 to: 2000<br>', NULL, '2014-04-04 08:54:15', '2014-04-04 08:54:15'),
(31, '01', 21, 25, 'arun - Amended the PO', 'Item No:302 Qty Changed from: 2000 to: 200<br>', NULL, '2014-04-04 09:08:43', '2014-04-04 09:08:43'),
(32, '01', 21, 25, 'arun - Amended the PO', 'Item No:302 Qty Changed from: 200 to: 100<br>', NULL, '2014-04-04 09:09:30', '2014-04-04 09:09:30'),
(33, '01', 21, 25, 'arun - Amended the PO', 'Item No:102 Qty Changed from: 450 to: 400<br>', NULL, '2014-04-04 09:10:14', '2014-04-04 09:10:14'),
(34, '01', 21, 26, 'arun - Placed New Order', 'Order placed datetime: 2014-04-04 17:30:46<br> New Ordered Placed', NULL, '2014-04-04 09:30:46', '2014-04-04 09:30:46'),
(35, '01', 21, 26, 'arun - Amended the PO', 'Item No:501 Qty Changed from: 300 to: 200<br>', NULL, '2014-04-04 09:31:08', '2014-04-04 09:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `preset_items`
--

CREATE TABLE IF NOT EXISTS `preset_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `type` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `s_no` int(11) NOT NULL,
  `preset_by` int(11) NOT NULL,
  `item_no` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `curr_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `preset_items`
--

INSERT INTO `preset_items` (`id`, `comp_code`, `type`, `s_no`, `preset_by`, `item_no`, `item_id`, `descr`, `price`, `uom`, `curr_code`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '01', 'C', 1, 1000, 101, 1, 'Spring Chicken Whole 800gm', '3.50', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(2, '01', 'C', 2, 1002, 101, 1, 'Spring Chicken Whole 800gm', '3.20', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(3, '01', 'C', 3, 1002, 201, 3, 'Spring Chicken Cut(BBQ/Fried)', '5.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(4, '01', 'C', 4, 2001, 201, 3, 'Spring Chicken Cut(BBQ/Fried)', '6.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(5, '01', 'V', 1, 3002, 201, 3, 'Spring Chicken Cut(BBQ/Fried)', '3.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(6, '01', 'V', 2, 3001, 201, 3, 'Spring Chicken Cut(BBQ/Fried)', '3.50', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(7, '01', 'C', 2, 2000, 202, 4, 'Spring Chicken Cut(1/2, 1/4)(Fried)', '6.50', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(8, '01', 'V', 1, 3001, 202, 4, 'Spring Chicken Cut(1/2, 1/4)(Fried)', '3.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(9, '01', 'C', 2, 2000, 301, 5, 'Seafood Pkt', '1.80', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(10, '01', 'V', 1, 4000, 301, 5, 'Seafood Pkt', '0.50', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(11, '01', 'C', 2, 2001, 302, 6, 'Onion Ring', '7.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(12, '01', 'V', 1, 4001, 302, 6, 'Onion Ring', '5.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(13, '01', 'C', 2, 2001, 501, 9, 'Fish Patties', '4.20', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(14, '01', 'C', 3, 1003, 501, 9, 'Fish Patties', '6.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(15, '01', 'V', 1, 3001, 501, 9, 'Fish Patties', '3.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(16, '01', 'C', 2, 1002, 502, 10, 'Chicken Patties', '4.80', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(17, '01', 'C', 3, 1000, 502, 10, 'Chicken Patties', '6.50', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(18, '01', 'V', 1, 3002, 502, 10, 'Chicken Patties', '3.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(19, '01', 'C', 2, 1000, 702, 14, 'Marinates Seasioning (2 in 1)', '6.00', 'KG', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(20, '01', 'V', 1, 3000, 702, 14, 'Marinates Seasioning (2 in 1)', '4.80', 'KG', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(21, '01', 'C', 2, 1000, 901, 17, 'Box(GR/TB/TF/R/G)', '36.50', 'BOX', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(22, '01', 'V', 1, 4000, 901, 17, 'Box(GR/TB/TF/R/G)', '25.00', 'BOX', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(23, '01', 'C', 2, 1002, 401, 22, 'French Fries - Shoesting(TF)', '5.20', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(24, '01', 'V', 1, 4000, 401, 22, 'French Fries - Shoesting(TF)', '2.80', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(25, '01', 'C', 2, 1000, 402, 23, 'French Fries - Crinkle Cut(SL)', '5.30', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(26, '01', 'V', 1, 4001, 402, 23, 'French Fries - Crinkle Cut(SL)', '2.80', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(27, '01', 'C', 2, 2001, 601, 24, 'Brown Sauce', '7.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(28, '01', 'V', 1, 3000, 601, 24, 'Brown Sauce', '4.50', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(29, '01', 'C', 2, 2000, 602, 25, 'Merengo Sauce', '8.20', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(30, '01', 'V', 1, 3000, 602, 25, 'Merengo Sauce', '6.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(31, '01', 'C', 2, 1001, 701, 26, 'Chicken Breading Mix', '23.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(32, '01', 'V', 1, 3002, 701, 26, 'Chicken Breading Mix', '16.00', 'PKT', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(33, '01', 'C', 2, 1001, 801, 27, 'Cooking Oil(TF)', '29.00', 'TIN', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(34, '01', 'V', 1, 4000, 801, 27, 'Cooking Oil(TF)', '21.00', 'TIN', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(35, '01', 'C', 2, 1002, 802, 28, 'Small Bun(MS/G)', '0.15', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(36, '01', 'V', 1, 4001, 802, 28, 'Small Bun(MS/G)', '0.06', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(37, '01', 'C', 2, 1002, 902, 29, 'Rice Box/Burger Box', '4.20', 'ROLL', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(38, '01', 'V', 1, 3001, 902, 29, 'Rice Box/Burger Box', '2.80', 'ROLL', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(39, '01', 'C', 3, 1000, 101, 30, 'Spring Chicken Whole 800gm', '3.50', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(40, '01', 'C', 4, 1001, 101, 30, 'Spring Chicken Whole 800gm', '3.40', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(41, '01', 'V', 1, 4000, 101, 30, 'Spring Chicken Whole 800gm', '2.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(42, '01', 'V', 2, 3002, 101, 30, 'Spring Chicken Whole 800gm', '2.10', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(43, '01', 'C', 3, 2000, 102, 31, 'Spring Chicken Cut(1/2,1/4)', '5.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(44, '01', 'C', 4, 1002, 102, 31, 'Spring Chicken Cut(1/2,1/4)', '6.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(45, '01', 'V', 1, 3000, 102, 31, 'Spring Chicken Cut(1/2,1/4)', '3.80', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(46, '01', 'V', 2, 3001, 102, 31, 'Spring Chicken Cut(1/2,1/4)', '3.90', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18'),
(47, '01', 'V', 5, 3002, 102, 31, 'Spring Chicken Cut(1/2,1/4)', '3.00', 'PCS', 'SGD', NULL, '2014-04-04 06:17:18', '2014-04-04 06:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `return_items`
--

CREATE TABLE IF NOT EXISTS `return_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `return_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `return_order`
--

CREATE TABLE IF NOT EXISTS `return_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `delivery_driver` int(11) NOT NULL,
  `cust_no` int(11) NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `return_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE IF NOT EXISTS `staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'STAFF',
  `staff_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issalesman` tinyint(4) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `comp_code`, `type`, `staff_no`, `user_id`, `first_name`, `last_name`, `issalesman`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '01', 'ADMIN', NULL, 19, 'DBA', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(2, '01', 'MGMT', NULL, 20, 'Bala Kumar', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(3, '01', 'MGMT', NULL, 21, 'Arun Bala', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(4, '01', 'SALES', NULL, 22, 'Durai Ng', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(5, '01', 'SALES', NULL, 23, 'Erwin Katigbak', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(6, '01', 'SALES', NULL, 24, 'Francis Betonio', NULL, 1, NULL, '2014-04-03 04:34:08', '2014-04-03 04:34:08'),
(7, '01', 'WHSE', NULL, 25, 'Jialin Tan', NULL, 1, NULL, '2014-04-03 04:34:09', '2014-04-03 04:34:09'),
(8, '01', 'WHSE', NULL, 26, 'Ryan', NULL, 1, NULL, '2014-04-03 04:34:09', '2014-04-03 04:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `torders`
--

CREATE TABLE IF NOT EXISTS `torders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `cust_grp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `torder_from` int(11) NOT NULL,
  `torder_to` int(11) NOT NULL,
  `cont_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_addr` int(2) NOT NULL,
  `delivery_addr_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_date` date NOT NULL,
  `delivery_time` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_driver` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `absorb_tax` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_value` int(4) DEFAULT NULL,
  `curr_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curr_rate` decimal(8,4) NOT NULL,
  `doc_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `gst` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `remark` text COLLATE utf8_unicode_ci,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `torders_id_unique` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `torders`
--

INSERT INTO `torders` (`id`, `comp_code`, `cust_grp`, `torder_from`, `torder_to`, `cont_person`, `cont_no`, `cust_name`, `delivery_addr`, `delivery_addr_text`, `delivery_date`, `delivery_time`, `delivery_driver`, `absorb_tax`, `tax_type`, `tax_value`, `curr_code`, `curr_rate`, `doc_no`, `subtotal`, `gst`, `total`, `remark`, `inactive`, `deleted_at`, `created_at`, `updated_at`) VALUES
(31, '01', 'LOC', 21, 1003, 'MS. LILY', '62658911', 'Afco East Pte Ltd', 2, '9 Jalan Tepong zip: 619327', '2014-04-17', '07:00', 'erwin', '0', 'GST7', 7, 'SGD', '1.0000', 'SOR-07140400101', '1410.00', '98.70', '1508.70', '', 0, NULL, '2014-04-04 06:45:18', '2014-04-04 08:20:48'),
(32, '01', 'LOC', 21, 1003, 'MS. LILY', '62658911', 'Afco East Pte Ltd', 2, '9 Jalan Tepong zip: 619327', '2014-04-17', '07:00', 'erwin', '0', 'GST7', 7, 'SGD', '1.0000', 'SOR-07140400102', '1710.00', '119.70', '1829.70', 'tets', 0, NULL, '2014-04-04 08:23:13', '2014-04-04 08:23:13'),
(33, '01', 'LOC', 21, 1003, 'MS. LILY', '62658911', 'Afco East Pte Ltd', 2, '9 Jalan Tepong zip: 619327', '2014-04-10', '07:00', 'erwin', '0', 'GST7', 7, 'SGD', '1.0000', 'SOR-07140400103', '600.00', '42.00', '642.00', '', 0, NULL, '2014-04-04 08:27:05', '2014-04-04 08:27:05'),
(34, '01', 'LOC', 21, 1001, 'MR. TAN', '63987666', 'BreadTalk Group Limited', 2, '15 Tai Seng Drive zip: 450678', '2014-04-17', '07:00', 'durai', '0', 'GST7', 7, 'SGD', '1.0000', 'SOR-07140400104', '5540.00', '387.80', '5927.80', 'test remark', 0, NULL, '2014-04-04 08:29:38', '2014-04-04 08:30:31'),
(35, '01', 'LOC', 21, 1005, 'MR. ALAN', '67563243', 'All-Big Trading Co', 2, '20 Woodlands Loop zip: 738980', '2014-04-18', '07:00', 'jialin', '0', 'GST7', 7, 'SGD', '1.0000', 'SOR-07140400105', '3590.00', '251.30', '3841.30', 'this is new order', 0, NULL, '2014-04-04 09:28:01', '2014-04-04 09:29:14');

-- --------------------------------------------------------

--
-- Table structure for table `torder_items`
--

CREATE TABLE IF NOT EXISTS `torder_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_no` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_desc` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `torder_no` int(11) NOT NULL,
  `qty` int(10) unsigned NOT NULL,
  `uom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uom_cf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitprice` decimal(8,2) NOT NULL,
  `gst` decimal(8,2) NOT NULL,
  `totalamt` decimal(8,2) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `torder_items`
--

INSERT INTO `torder_items` (`id`, `comp_code`, `item_no`, `item_id`, `item_desc`, `torder_no`, `qty`, `uom`, `uom_cf`, `unitprice`, `gst`, `totalamt`, `inactive`, `deleted_at`, `created_at`, `updated_at`) VALUES
(42, '01', 501, 9, 'Fish Pattie', 31, 100, 'PCS', '1.0000', '6.00', '42.00', '600.00', 0, NULL, '2014-04-04 06:45:18', '2014-04-04 08:20:48'),
(43, '01', 201, 3, 'Spring Chic', 31, 100, 'PCS', '1.0000', '4.70', '65.80', '470.00', 0, NULL, '2014-04-04 06:45:18', '2014-04-04 08:20:48'),
(44, '01', 301, 5, 'Seafood Pkt', 31, 340, 'PKT', '1.0000', '1.00', '14.00', '340.00', 0, NULL, '2014-04-04 06:45:18', '2014-04-04 08:20:48'),
(45, '01', 501, 9, 'Fish Pattie', 32, 100, 'PCS', '1.0000', '6.00', '42.00', '600.00', 0, NULL, '2014-04-04 08:23:13', '2014-04-04 08:23:13'),
(46, '01', 201, 3, 'Spring Chic', 32, 100, 'PCS', '1.0000', '4.70', '32.90', '470.00', 0, NULL, '2014-04-04 08:23:13', '2014-04-04 08:23:13'),
(47, '01', 302, 6, 'Onion Ring', 32, 100, 'PKT', '1.0000', '6.40', '44.80', '640.00', 0, NULL, '2014-04-04 08:23:13', '2014-04-04 08:23:13'),
(48, '01', 501, 9, 'Fish Pattie', 33, 100, 'PCS', '1.0000', '6.00', '42.00', '600.00', 0, NULL, '2014-04-04 08:27:05', '2014-04-04 08:27:05'),
(49, '01', 701, 26, 'Chicken Bre', 34, 100, 'PKT', '1.0000', '23.00', '161.00', '2300.00', 0, NULL, '2014-04-04 08:29:38', '2014-04-04 08:30:31'),
(50, '01', 801, 27, 'Cooking Oil', 34, 100, 'TIN', '1.0000', '29.00', '406.00', '2900.00', 0, NULL, '2014-04-04 08:29:38', '2014-04-04 08:30:31'),
(51, '01', 101, 30, 'Spring Chic', 34, 100, 'PCS', '1.0000', '3.40', '71.40', '340.00', 0, NULL, '2014-04-04 08:29:38', '2014-04-04 08:30:31'),
(52, '01', 201, 3, 'Spring Chic', 35, 100, 'PCS', '1.0000', '4.70', '32.90', '470.00', 0, NULL, '2014-04-04 09:28:01', '2014-04-04 09:29:14'),
(53, '01', 202, 4, 'Spring Chic', 35, 200, 'PCS', '1.0000', '6.00', '42.00', '1200.00', 0, NULL, '2014-04-04 09:28:01', '2014-04-04 09:29:14'),
(54, '01', 302, 6, 'Onion Ring', 35, 300, 'PKT', '1.0000', '6.40', '44.80', '1920.00', 0, NULL, '2014-04-04 09:28:01', '2014-04-04 09:29:14');

-- --------------------------------------------------------

--
-- Table structure for table `uom`
--

CREATE TABLE IF NOT EXISTS `uom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `item_id` int(11) NOT NULL,
  `item_no` int(11) NOT NULL,
  `s_no` int(11) NOT NULL,
  `uom` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cf_qty` int(10) unsigned NOT NULL,
  `uom_basic` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `uom`
--

INSERT INTO `uom` (`id`, `comp_code`, `item_id`, `item_no`, `s_no`, `uom`, `cf_qty`, `uom_basic`, `inactive`, `created_at`, `updated_at`) VALUES
(1, '01', 3, 201, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(2, '01', 4, 202, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(3, '01', 5, 301, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(4, '01', 6, 302, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(5, '01', 9, 501, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(6, '01', 10, 502, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(7, '01', 14, 702, 1, 'KG', 1, 'KG', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(8, '01', 14, 702, 2, 'CTN', 12, 'KG', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(9, '01', 17, 901, 1, 'BOX', 1, 'BOX', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(10, '01', 22, 401, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(11, '01', 23, 402, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(12, '01', 24, 601, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(13, '01', 25, 602, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(14, '01', 26, 701, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(15, '01', 27, 801, 1, 'TIN', 1, 'TIN', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(16, '01', 27, 801, 2, 'CTN', 6, 'TIN', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(17, '01', 27, 801, 3, 'KG', 1, 'TIN', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(18, '01', 28, 802, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(19, '01', 29, 902, 1, 'ROLL', 1, 'ROLL', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(20, '01', 30, 101, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(21, '01', 31, 102, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(22, '01', 32, 710, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(23, '01', 32, 710, 2, 'KG', 50, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(24, '01', 33, 711, 1, 'GM', 1, 'GM', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(25, '01', 34, 713, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(26, '01', 35, 714, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(27, '01', 36, 810, 1, 'LTR', 1, 'LTR', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(28, '01', 37, 715, 1, 'PCS', 1, 'PCS', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(29, '01', 38, 712, 1, 'PKT', 1, 'PKT', 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_roles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `user_dob`, `user_roles`, `confirmation_code`, `confirmed`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'customer1000', 'customer1000@sample.com', '$2y$10$6dzN2sUMJKPVMtod04Hj3OYNwxv8f2KI6ZSJu8udDpVJB41HXR7IK', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'customer1001', 'customer1001@sample.com', '$2y$10$lVdCoSV9EQNPIKtoomhF1uKWI1h6pIYleoOHueCEaJDImKBrHx0O.', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'customer1002', 'customer1002@sample.com', '$2y$10$aH8MBUakHXPyKKbNP6xY5uE//tmVvA0AokhhDzLOliOPrySoQmVV2', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'customer1003', 'customer1003@sample.com', '$2y$10$FP4f.KTCCLGA8DCSHYQC0e1oh0t/Cx.vGB8sOc0C4Cp5SCzXKw2cO', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'customer1004', 'customer1004@sample.com', '$2y$10$y85gpmvIMwY0gfeTzpQXNu1kRAxYeDZ7l7oAaCYM2YZ.tqrOhf4oa', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'customer1005', 'customer1005@sample.com', '$2y$10$fqpbyH/N6lXF7vv6ZhTckeVflGZRA//qVpgvUXolXF5rP4cxs1ucu', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'customer1006', 'customer1006@sample.com', '$2y$10$3I91VEOrMPFDYhoTpcncwOWs4M7lnZ8q4vNXto/ExgOtPCN/6FnOq', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'customer1007', 'customer1007@sample.com', '$2y$10$LfK99aqQ0NXRSKIJcuXyKOkhfhATMyVFBQSJR7lbWK28RFqA4IBPC', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'customer1008', 'customer1008@sample.com', '$2y$10$VAL1HDL0NfGbN6fpBqLt6.EvfHLb0DuEuGxWse0qvu9x2MJdes6uu', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'customer2000', 'customer2000@sample.com', '$2y$10$1kQoJZ3F.AF7M4H0I9mhJeM8oBOeXLeADGtIczQ..5QtrDAJNHdMy', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'customer2001', 'customer2001@sample.com', '$2y$10$R2us5xnMHIWKV/VOaZ5dT.xs2mTQQAWd.538IJW1BwLl2rHJtpoGq', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'customer2002', 'customer2002@sample.com', '$2y$10$ehUPiBcEKEUtxNz8JGHu8OmIvrRiSsdwhL33tK9yLW7bHXntYvKNS', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'vendor3000', 'vendor3000@sample.com', '$2y$10$xLjkA5urov3Ebun6Dpq3S.7eR8MHul3uOhf0aJXVci3ng64D6zPTG', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'vendor3001', 'vendor3001@sample.com', '$2y$10$f/XmSsiwgPhyGMBbMPmVqewR1Il0tq21kVzmJPp8N7IFud7Jd/wWy', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'vendor3002', 'vendor3002@sample.com', '$2y$10$HH1F0.DEhi8m1jSddGm1feAk.COwTZBQgvWr1nRUTxOf98vSXkk9m', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'vendor4003', 'vendor4003@sample.com', '$2y$10$Z4Iz.P.cAGUXdxXwy24cOewz0yIpWJFWItMccyfAMwTXtj6oQ62Bm', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'vendor4000', 'vendor4000@sample.com', '$2y$10$MuOqWzSJi2QkGPL3sGT94.OtBTJs67xsn74/T.UAbsPd8iQwweLwK', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'vendor4001', 'vendor4001@sample.com', '$2y$10$3qWB33S.hfEkT/a32eodeeT6WmnpcCVedhuO0TSJE2BwF8PGCAUtq', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'DBA', 'DBA@sample.comm.sg', '$2y$10$PS7mHGTsEJgqmjltF8XMUe.qUpKK9a/aLyKEqw1zniEpEZNUIdl2O', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Bala', 'Bala@sample.comm.sg', '$2y$10$vDT75Y4wD8T9kLPnHO0/8O3lAw6qcMeOprI2VeY2ymQd.Y.cZZ3GG', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'arun', 'arun@sample.comm.sg', '$2y$10$7Y06aeYPjHZ2RbztGtZTAu7hVA8Zp4viRnLfZ7BRErg1LMVpn0ZKC', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'durai', 'durai@sample.comm.sg', '$2y$10$pSy6xNzjwa4hWamraDRmleHm2H2Dkt76sPSIxXt423Va19Cs7KpRS', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'erwin', 'erwin@sample.comm.sg', '$2y$10$51UEt0084Tlt8gNSheZYE.Ngao77VvZWwvRDFiqNsa5jiBaXwvSJq', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'francis', 'francis@sample.comm.sg', '$2y$10$GnoNDwAZGSGKxpztE4/ux.vFMiJzKTLpGqByGSoJuzrrJ1ocaropi', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'jialin', 'jialin@sample.comm.sg', '$2y$10$nl4SXfzrayZnERxmVyBm3OOCXNvPWf78PILKj4cF7wYH3NNYWhULy', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'ryan', 'ryan@sample.comm.sg', '$2y$10$2V.iHQsxUfh7a8lvHjRQvepxb64IrAzotFdvxcXNQ4wWunpUU08Ou', NULL, NULL, 'somerandomcode', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `vend_no` int(11) NOT NULL,
  `comp_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '01',
  `vend_grp_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `vend_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `curr_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tax_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `addr` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctry_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `cont_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cont_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vendors_id_unique` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4004 ;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `user_id`, `vend_no`, `comp_code`, `vend_grp_code`, `vend_name`, `curr_code`, `tax_type`, `addr`, `zip`, `ctry_code`, `cont_name`, `phone_no1`, `phone_no2`, `fax_no`, `cont_email`, `inactive`, `created_at`, `updated_at`) VALUES
(3000, 13, 3000, '01', 'LOC', 'Boisson Singapore Private Limited', 'SGD', 'GST7', '63 Hillview Avenue #03-04 Lam Soon Industrial Building', NULL, 'SIN', 'Mr. Andrew', '65638899', NULL, NULL, '65690912', 0, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(3001, 14, 3001, '01', 'LOC', 'GOH YEOW SENG PTE LTD', 'SGD', 'GST7', '94C Jln Senang ', NULL, 'SIN', 'Ms. Lau', '63452221', NULL, '63459887', NULL, 0, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(3002, 15, 3002, '01', 'LOC', 'HU LEE IMPEX PTE LTD', 'SGD', 'GST7', '16A Chin Bee Ave', NULL, 'SIN', 'Ms. Siew Lan', '66756821', NULL, '67352211', NULL, 0, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(4000, 17, 4000, '01', 'OVS', 'Harris & Steven JV', 'USD', 'ZERO', '23A Burlington Tower, #87-78', NULL, 'USA', 'Mr. Micheal Steven Raj', '001 90392030200', NULL, '001 90372382889', NULL, 0, '2014-04-03 04:34:04', '2014-04-03 04:34:04'),
(4001, 18, 4001, '01', 'OVS', 'Amisan Products (M) Sdn Bhd', 'MYR', 'ZERO', 'Batu 4, Jalan Kapar, P.O. Box 177, Klang, Selangor', NULL, 'MAL', 'Mr. Steven Lee', '013-3291 2620', NULL, '013-3291 2345', NULL, 0, '2014-04-03 04:34:05', '2014-04-03 04:34:05'),
(4003, 16, 4003, '01', 'LOC', 'Equinox Pte Ltd', 'SGD', 'GST7', 'Raffles City', NULL, 'SIN', 'John Smith', NULL, NULL, NULL, NULL, 0, '2014-04-03 04:34:04', '2014-04-03 04:34:04');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
