<?php

//Route the root access of index to login ...
Route::get('/sdorder', function(){ return Redirect::to('/user/login');  });


//SDAPI -- routing for resource getting from Solution Details API -------------------
Route::get('sdapi/all', 					'SdapiController@API_getAll');

Route::get('sdapi/customer', 			   	'SdapiController@API_getCustomer');
Route::get('sdapi/customer_addr', 		   	'SdapiController@API_getCustAddr');
Route::get('sdapi/vendor', 				   	'SdapiController@API_getVendor');
Route::get('sdapi/item', 				   	'SdapiController@API_getItems');
Route::get('sdapi/category', 			   	'SdapiController@API_getCategory');
Route::get('sdapi/uom', 				   	'SdapiController@API_getUom');
Route::get('sdapi/pis', 				   	'SdapiController@API_getPriceItemSeq');
Route::get('sdapi/sys', 				   	'SdapiController@API_getSystemParams');
Route::get('sdapi/acp', 				   	'SdapiController@API_getAcPeriod');
Route::get('sdapi/currexch', 			   	'SdapiController@API_getCurrExchRate');
Route::get('sdapi/ilp', 				   	'SdapiController@API_getItemListPrice');
Route::get('sdapi/cgp', 				   	'SdapiController@API_getCustGrpItemPrice');
Route::get('sdapi/cp', 					   	'SdapiController@API_getCustPrice');
Route::get('sdapi/pp', 				    	'SdapiController@API_getPeriodPrice');
Route::get('sdapi/preset', 					'SdapiController@API_getPreset');
Route::get('sdapi/users', 					'SdapiController@API_getMember');
Route::get('sdapi/currcode', 				'SdapiController@API_getCurrCode');


Route::get('sdapi/get/list_bom/{bomcode}', 		'SdapiController@API_getListBOM');
Route::get('sdapi/get/allbom', 					'SdapiController@API_getAllBOM');
Route::get('sdapi/get/chk_bomcode/{bomcode}', 	'SdapiController@API_checkBomCodeValid');
Route::get('sdapi/get/bom/item/cat/{catcode}', 	'SdapiController@API_bomGetItembyCAT');
Route::get('sdapi/get/bom/search/itemdesc/{itemdesc}/itemno/{itemno}/catcode/{catcode}/itemtype/{itemtype}'
											, 	'SdapiController@API_bomSearchItem');
Route::get('sdapi/get/bom/itemuom/{itemid}'	, 	'SdapiController@API_bomGetItemUOM');

Route::get('sdapi/get/stock/warehouse/all' 	, 'SdapiController@API_stockGetWarehouses');
Route::get('sdapi/get/stock/warehouse/wh_item/{whid}/{trandate}', 'SdapiController@API_stockGetWarehouseItems');


//User login Routing --------------------------------------------------------------
Route::get ('user/create',                  'UserController@create');
Route::post('user',                        	'UserController@store');
Route::get ('user/login',                   'UserController@login');
Route::post('user/login',                  	'UserController@do_login');
Route::get ('user/confirm/{code}',          'UserController@confirm');
Route::get ('user/forgot_password',         'UserController@forgot_password');
Route::post('user/forgot_password',        	'UserController@do_forgot_password');
Route::get ('user/reset_password/{token}',  'UserController@reset_password');
Route::post('user/reset_password',         	'UserController@do_reset_password');
Route::get ('user/logout',                  'UserController@logout');
Route::get ('user/index',      			   	'UserController@index');
Route::get ('user/{id}/dashboard', 			'UserController@showDashboard');
Route::get ('user/{id}/profile', 			'UserController@showProfile');
//user dashboard
Route::resource ('user', 'UserController');

//item routes
Route::get('/item/{itemid}/cust/{uid}', 'ItemController@getItemById');
Route::get('/item/{itemid}/vend/{vendno}/user/{userid}', 'ItemController@getVendorItemByItemNo');
Route::get('/item/{itemno}/vendpreset/{vendno}/user/{userid}', 'ItemController@getVendorItemByItemNo');
Route::get('/all/items', 'ItemController@getAllItems');


Route::resource ('user.torder', 'UserTorderController');
//get the xml files save into the filesystem (TO)
Route::get('user/{user}/torder/{torder}/xml', function($user, $torder){
	$formatdate = date('ymd', strtotime(TOrder::where('id', '=', $torder)->first()->created_at));
	$xml = File::get(storage_path().'/xml/to_'.$torder.$formatdate.'.xml');
	return Response::make($xml, 200)->header('Content-Type', 'application/xml');
});

//customer and its address routing
//Route::resource ('customer', 'CustomerController');
Route::get ('customer/group/{cgrp}', 'CustomerController@getCustomerCodeByGroup');
Route::get ('customer/info/{custno}', 'CustomerController@getCustomerInfo');

//vendor related routing
Route::get ('vendor/{grp}/group',          'VendorController@getVendorByGrp');
Route::resource ('vendor', 				   'VendorController');

Route::get('currency/exch_rate/{code}/period/{period}', 'CurrencyController@getCurrencyRate');

Route::resource('user.porder', 'UserPOrderController');
//get the xml files save into the filesystem (PO)
Route::get('user/{user}/porder/{porder}/xml', function($user, $porder){
	$formatdate = date('ymd', strtotime(POrder::where('id', '=', $porder)->first()->created_at));
	$xml = File::get(storage_path().'/xml/po_'.$porder.$formatdate.'.xml');
	return Response::make($xml, 200)->header('Content-Type', 'application/xml');
});



Route::resource('user.forecast', 'ForecastController');



//SD BOM
Route::get ('sdbom/login'				, 'BomController@showLogin');
Route::post('sdbom/login'				, 'BomController@doLogin');
Route::get ('sdbom/logout' 				, 'BomController@doLogout');
Route::get ('sdbom/bom/'				, 'BomController@index');
Route::get ('sdbom/bom/create'			, 'BomController@create');
Route::post('sdbom/bom/create' 			, 'BomController@store');
Route::get ('sdbom/bom/{bomcode}/edit'	, 'BomController@edit');
Route::get ('sdbom/bom/{bomcode}'   	, 'BomController@show');

//SD Stock Take
Route::get ('sdstock/login'			, 'StockTakeController@showLogin');
Route::post('sdstock/login'			, 'StockTakeController@doLogin');
Route::get ('sdstock/logout' 		, 'StockTakeController@doLogout');
Route::get ('sdstock/warehouse'		, 'StockTakeController@showWarehouse');
Route::post('sdstock/warehouse'		, 'StockTakeController@processWarehouse');



Route::get ('fail/', 'SdapiController@API_fail');

//-------------------------------------------------------------------------------------------------------------------
//closure
Route::get('/sample', function(){
//return sprintf( '%08d', 1 );
	$to = array(
  		'bla' => 'blub',
  		'foo' => 'bar',
  		'another_array' => array (
    		'stack' => 'overflow',
  		));
});


Route::get('/session', function(){
	return Session::all();
});



//Route::post('/send/xml', 'XmlSendController@post_index');


//File::put(storage_path().'/file.xml', $xml);
// $xml = File::get(storage_path().'/file.xml');
// return Response::make($xml, 200)->header('Content-Type', 'application/xml');

// });

//Demo ------
Route::get('mcomm/items', 							'MobileStoreController@showStoreItems');
Route::get('mcomm/items/purchase/message/{msg}',	'MobileStoreController@showPurchaseMsg');
Route::post('mcomm/items/purchase', 				'MobileStoreController@processPurchase');
Route::get('demo/item', 							'SdapiController@API_getdemoItem');
Route::post('demo/item/purchase', 					'SdapiController@API_so_sync');

Route::get('/demo/test/post', function(){
	$url = 'http://50.112.173.46/demo/item/purchase';
	//$url = 'http://sdsystem.dev/demo/item/purchase';
	$data = array('items' => 
				array(
					['comp_code' 		=> '01', 
					 'item_id'			=> '39', 
					 'item_no'		 	=> '803',  
					 'item_descr' 		=> 'Chocolate Cake', 
					 'item_qty' 		=> '10', 
					 'item_uom' 		=> 'SLICE',
					 'item_price'		=> '4.50'], 

					['comp_code' 		=> '01',
					 'item_id'			=> '41', 
					 'item_no' 			=> '805', 
					 'item_descr' 		=> 'Cappuccino (Hot)', 
					 'item_qty' 		=> '20', 
					 'item_uom' 		=> 'CUP',
					 'item_price'		=> '4.30']

				 ), 'customer'=>array(
				 			'comp_code'=>'01', 
				 			'cust_no'=>'1001', 
				 			'cust_username'=>'john.smith',
				 			'cust_name'=>'John Smith', 
				 			'cust_country' => 'Singapore', 
				 			'cust_addr' => '1', 
				 			'cust_contact' => '99990000',
				 			'cust_curr_code' => 'SGD'));
	//echo json_encode($data, 128);
	//dd($data_send);
	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'content' => http_build_query($data),
	    ),
	);

	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	
	return  json_decode($result)->message;
	//$return = json_decode($result);
	//return $return;
});


//closure for User Role creation
Route::get('/backend/role_perm', function(){
	$customer = new Role;
	$customer->name = 'Customer';
	$customer->save();

	$vendor = new Role;
	$vendor->name = 'Vendor';
	$vendor->save();

	//get all the customer
	$user = User::all();


	//get all the vendor

	//get all staff
});

