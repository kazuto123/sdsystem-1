<?php

class UserPOrderController extends \BaseController {

	public function __construct(){
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($user)
	{
		$usertype="";
		$search = Request::get('q'); //query string

		$filterby = Configs::where('type', '=', 'orderlistfilter')->get();

		//$porder = POrder::all();

		$loggeduser = User::where('id', '=', $user)->first();

		if($loggeduser){
			if($loggeduser->staff){
				$usertype='staff';
				//$porder = POrder::where('user_id', '=', $loggeduser->staff->user_id)->orderBy('updated_at', 'desc')->get();
				$porder = $search 
					? DB::table('porders')
						->where('user_id', '=', $loggeduser->staff->user_id)
						->whereNull('deleted_at')
						->where(function($query) use ($search){
							$query->where('vend_no', 'LIKE', '%'.$search.'%')
								  ->orwhere('vend_name', 'LIKE', '%'.$search.'%')
								  ->orwhere('vend_addr', 'LIKE', '%'.$search.'%')
								  ->orwhere('status', 'LIKE', '%'.$search.'%')
								  ->orWhere('updated_at', 'LIKE', '%'.$search.'%');
						})->paginate(10)	

					: POrder::where('user_id', '=', $loggeduser->staff->user_id)
							->orderBy('updated_at', 'desc')
							->paginate(10);

			}
			else if($loggeduser->vendor){
				$porder = POrder::where('vend_no', '=', $loggeduser->vendor->vend_no)
								->orWhere('user_id', '=', $loggeduser->vendor->user_id)
								->orwhere('vend_addr', 'LIKE', '%'.$search.'%')
								->orwhere('status', 'LIKE', '%'.$search.'%')
								->orderBy('updated_at', 'desc')
								->paginate(10);
				$usertype="vendor";
			}
		}
		else{
			echo 'unknowntype';
		}

		$filterby = Configs::where('type', '=', 'porderlistfilter')->get();
		
		return View::make('porders.index')
			->with('userid', $user)
			->with('filterby', $filterby)
			->with('usertype', $usertype)
			->with('porders', $porder);		
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($user)
	{
		//get all item
		$items = Item::all();

		//get all category
		$cats = Category::all();

		//get vendor group
		$vend_groups = Configs::where('type', '=', 'cgroup')->get();
		foreach($vend_groups as $vgrp){
			$vendor_groups [] = $vgrp->value;
		}
		
		//get all currency code
		$all_curr_codes = Configs::where('type', '=', 'currency')->get();
		foreach($all_curr_codes as $codes){
			$curr_codes [] = $codes->value;
		}
		
		return View::make('porders.create')
			->with('items', $items)
			->with('cats', $cats)
			->with('curr_codes', $curr_codes)
			->with('userid', $user)
			->with('vgroups', $vendor_groups);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		//create new record ....
		$input = Input::all();
		$orderdetail = [];
		$itemdetail = [];
		
		$odform = [];
		$oditem = [];
		
		//process the input form.
		foreach($input as $key=>$value){
			$isItem = explode("_", $key);
			if($isItem[0] == 'oditem'){
				$itemdetail[$isItem[2]] = $value;
				$oditem[$isItem[1]] = $itemdetail;
			}
			else{
				$orderdetail[$key] = $value;
				$odform[0] = $orderdetail;
				//$order['order'] = $odform;
			}
		}

		#all config files
		$compcode = Configs::where('type', '=', 'compcode')->first()->value;

		$po_type = Configs::where('type', '=', 'sys_param')
							->where('key', '=', 'web_po_subtype')
							->first();	
		$vendor_info = Vendor::where('vend_no', '=', $odform[0]['vendor_no'])->first();
		
		$tax_rate = Configs::where('type', '=', 'gst_rate')
							->where('key', '=', $vendor_info->tax_type)
							->first();		
		$running_no = Configs::where('type', '=', 'po_counts')
									->first();		

		//$data = [$odform, $oditem];
		#insert the order into database
		$porder = new POrder();
		$porder->comp_code 			= $compcode;
		$porder->user_id 			= $odform[0]['user_id'];
		$porder->vend_grp 			= $odform[0]['vendor_group'];
		$porder->vend_no 			= $odform[0]['vendor_no'];
		$porder->vend_name 			= $odform[0]['vendor_name'];
		$porder->cont_person 		= $odform[0]['vendor_contact_person'];
		$porder->cont_no 			= $odform[0]['vendor_contact_phone'];
		$porder->vend_addr 			= $odform[0]['vendor_addr'];
		$porder->curr_code 			= $odform[0]['vendor_curr_code'];
		$porder->curr_rate 			= $odform[0]['vendor_curr_rate'];
		$porder->remark				= $odform[0]['order_remark'];
		$porder->subtotal 			= $odform[0]['order_subtotal'];
		$porder->tax_type 			= $vendor_info->tax_type;
		$porder->tax_value 			= $tax_rate->value;
		$porder->gst 				= $odform[0]['order_gst'];
		$porder->total 				= $odform[0]['order_total'];
		$porder->status  			= 'NEW';

		$save = $porder->save();

		$porderid = $porder->id;

		$porderdate_retrieve = strtotime($porder->created_at);
		$porderdate = date("Y/m/d", $porderdate_retrieve);
		$porderdate2 = date('ymd', $porderdate_retrieve);
		$porderdate3 = date("Y-m-d", $porderdate_retrieve);
		$porderdate_por = date('ym', $porderdate_retrieve);

		#ac_period
		$ac_period = date('Ym', strtotime($porder->created_at));

		#Owner or creator of form
		$user_name = User::where('id', '=', $odform[0]['user_id'])->first()->username; 
		
		//get the running nummber.
		$prev_running_no = $running_no->value;
		$curr_running_no = $prev_running_no + 1;
		$running_no->value = $curr_running_no;
		$running_no->save();

		//generate and save the doc no into porder.
		$doc_no = 'POR-'.$po_type->value.$porderdate_por.str_pad($curr_running_no, 5, '0', STR_PAD_LEFT);
		$porder->doc_no = $doc_no;
		$porder->save();

		#insert item related to the order into the database and geneate an XML
		$oditemxml = "";
		$sno_count = 1;
		$total_tax_amount = 0;

		foreach($oditem as $item){
			$tax_amount = ($tax_rate->value / 100) * $item['total'];

			$odi 				= new POrderedItem();
			$odi->item_no 		= $item['itemno'];
			$odi->item_id 		= $item['itemid'];
			$odi->descr   		= $item['descr'];
			$odi->porder_id 	= $porderid;
			$odi->added_by 		= $odform[0]['user_id'];
			$odi->qty 			= $item['qty'];
			$odi->uom 			= $item['uom']; 
			$odi->uom_cf 		= $item['uomcf'];
			$odi->unitprice 	= $item['unitprice'];
			$odi->gst 			= $tax_amount;
			$odi->total 		= $item['total'];
			
			$itemsave = $odi->save();

			$oditemxml .= $this->generateItemDetailXML($odi, $porder, $sno_count);

			$sno_count += 1;
			$total_tax_amount += $tax_amount;
		}//end foreach

		$po_xml = $this->generateOrderMasterXML($porder, $oditemxml);

		$order_xml = new OrderXml();
		$order_xml->type 			= 'po';
		$order_xml->xml_name 		= $doc_no;
		$order_xml->xml_contents 	= $po_xml;
		$order_xml->save();

		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;

		$server_url=$url_server.'/konvergefb/web_createPO';
		$url = 'http://'.$server_username.':'.$server_password.'@'.$server_url;

		$post = 'compcode=01&docno='. $doc_no. '&docdate=' . $porderdate . '&xmlfile='.$po_xml . '&userid='.$user_name;
		$status_code = $this->sendByCurl($post, $url);	

		$order_xml->send_status = $status_code;
		$order_xml->save();	

		//return Response::make($send_resp, 200)->header('Content-Type', 'application/xml');
		//echo $odform[0]['cust_grp'];

		#Logs ------
		$log 					= new PorderLogEntry();
		$log->user_id 			= $odform[0]['user_id'];
		$log->porder_id 		= $porderid;
		$log->title 			= User::where('id', '=', $odform[0]['user_id'])->first()->username . ' - Placed New Order';
		$log->content 			= 'Order placed datetime: '.$porder->created_at . '<br> New Ordered Placed';
		$logsuccess = $log->save();


		if($save){
			return Redirect::to('/user/'. $odform[0]['user_id'] . '/porder')->with('message', 'Order Placed');
		} else{
			Redirect::back()->withInput()
							->with('errors', $porder->errors());
		}

	}

	public function sendByCurl($post_field, $url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		//curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
		
		//Execute the request and also time the transaction ( optional )
		$start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		
		//$responseInfo = curl_getinfo($ch);
		//$httpResponseCode = $responseInfo['http_code'];
		//echo $responseInfo;

		$stop = array_sum(explode(' ', microtime()));
		$totalTime = $stop - $start;
		//echo $result; 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    //print_r(curl_getinfo($ch)); 

		    switch($returnCode){
		        case 200:
		        	echo 'success';
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            echo 'failed';
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);
		 
		//Output the results and time
		echo 'Total time for request: ' . $totalTime . "\n";
		echo $result;
		$json = json_decode($result, true);
		return $json[0]['ReturnValue']; 
	}	

	public function generateItemDetailXML($item, $porder, $count){
		$po_created_date = date('Y/m/d', strtotime($porder->created_at));
		return
			'<details>'. 
				'<comp_code>'.$porder->comp_code.'</comp_code>'.
				'<doc_no>'.$porder->doc_no.'</doc_no>'.
				'<s_no>'.$count.'</s_no>'.
				'<item_id>'.$item->item_id.'</item_id>'. 
				'<item_no>'.$item->item_no.'</item_no>'. 
				'<item_descr>'.$item->descr.'</item_descr>'.
				'<vend_part_no>'.$item->item_no.'</vend_part_no>'. //need to change
				'<uom>'.$item->uom.'</uom>'.
				'<uom_cf>'.$item->uom_cf.'</uom_cf>'.
				'<qty_order>'.$item->qty.'</qty_order>'.
				'<price>'.$item->unitprice.'</price>'.
				'<total>'.$item->total.'</total>'.
				'<disc_type>N</disc_type>'.
				'<disc_value>0</disc_value>'.
				'<disc_amount>0</disc_amount>'.
				'<net_amount>'.$item->total.'</net_amount>'.
				'<tax_type>'.$porder->tax_type.'</tax_type>'.
				'<tax_rate>'.$porder->tax_value.'</tax_rate>'.
				'<tax_amt>'.$item->gst.'</tax_amt>'.
				'<po_etd>'.$po_created_date.'</po_etd>'.
				'<po_eta>'.$po_created_date.'</po_eta>'.
			'</details>';		
	}

	public function generateOrderMasterXML($porder, $itemxml){
		$doc_date = date('Y/m/d', strtotime($porder->created_at));
		$ac_period = date('Ym', strtotime($porder->created_at));
		return
			'<?xml version="1.0" encoding="UTF-8"?>'.
			'<plist version="1.0">'.
			'<array>'.
				'<master>'.
					'<comp_code>'.$porder->comp_code.'</comp_code>'. //need to include compcode in orders form ....
					'<doc_no>'.$porder->doc_no.'</doc_no>'.
					'<doc_date>'.$doc_date.'</doc_date>'.
					'<vend_no>'.$porder->vend_no.'</vend_no>'.
					'<ac_period>'.$ac_period.'</ac_period>'.
					'<ref_info>-</ref_info>'.
					'<cont_name>'.$porder->cont_person.'</cont_name>'.
					'<curr_code>'.$porder->curr_code.'</curr_code>'.
					'<exch_rate>'.$porder->curr_rate.'</exch_rate>'.
					'<tax_amt>'.$porder->gst.'</tax_amt>'.
					'<disc_amt>0</disc_amt>'.
					'<total>'.$porder->subtotal.'</total>'.
					'<status>N</status>'.
					'<remarks>'.$porder->remark.'</remarks>'. 
				'</master>'.
				$itemxml .
			'</array>'.
			'</plist>';		

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($user, $porder)
	{
		//
		$porder_info = POrder::where('id', '=', $porder)->first();

		$porder_item = POrderedItem::where('porder_id', '=', $porder_info->id)->get();

		return View::make('porders.show')
					->with('userid', $user)
					->with('porder_item', $porder_item)
					->with('porder_details', $porder_info);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($user, $porder)
	{
		//get all category and all items 
		$cats = Category::all();
		$items = Item::all();
		$loggeduser = User::where('id', '=', $user)->first();

		$usertype="";
		if($loggeduser->staff){
			$usertype = 'staff';
		}
		else if($loggeduser->vendor){
			$usertype = 'vendor';
		}

		//get the information of the orders
		$porder_info = POrder::where('id', '=', $porder)->first();

		//$po_items = POrderedItem::where('porder_id', '=', $porder)->get();
		
		$conf_group = Configs::where('type', '=', 'cgroup')->get();
		foreach($conf_group as $vgrp){ $vend_grp[] = $vgrp->value; }

		//get all currency code
		$all_curr_codes = Configs::where('type', '=', 'currency')->get();
		foreach($all_curr_codes as $codes){
			$curr_codes [] = $codes->value;
		}

		//get all the item of the orders
		if($porder_info->vend_grp == 'LOC'){
			$vendors = Vendor::where('vend_grp_code', '=', 'LOC')->get();
			foreach ($vendors as $vendor){
				$vend_no [] = array(
								'code'=>$vendor->vend_no,
								'name'=>$vendor->vend_name
							  );
			} 
		}

		else if($porder_info->vend_grp == 'OVS'){
			$vendors = Vendor::where('vend_grp_code', '=', 'OVS')->get();
			foreach ($vendors as $vendor){
				$vend_no [] = array(
								'code'=>$vendor->vend_no,
								'name'=>$vendor->vend_name
							  );
			} 
		}
		$vendor_addr = Vendor::where('vend_no', '=', $porder_info->vend_no)->first()->addr;

		$logs = PorderLogEntry::where('porder_id', '=', $porder_info->id)
								->orderBy('updated_at', 'desc')
								->get();

		
		//get all items from logs
		$all_proders_item = POrderedItem::where('porder_id', '=', $porder_info->id)->get();
		foreach($all_proders_item as $porder_item){
			$po_items [] = array(
								'comp_code'=>$porder_item->comp_code, 
								'item_id'=>$porder_item->item_id,
								'item_no'=>$porder_item->item_no,
								'item_descr'=>$porder_item->descr,
								'porder_id'=>$porder_item->porder_id,
								'qty'=>$porder_item->qty,
								'uom'=>$porder_item->uom,
								'uom_cf'=>$porder_item->uom_cf,
								'price'=>$porder_item->unitprice,
								'total'=>$porder_item->total,
								'uom_all'=>Uom::where('item_no', '=', $porder_item->item_no)->get()->toArray()
							);
		}
		return View::make('porders.edit')
				->with('usertype', $usertype)
				->with('userid', $user)
				->with('cats', $cats)
				->with('items', $items)
				->with('vgroups', $vend_grp)
				->with('vnos', $vend_no)
				->with('logs', $logs)
				->with('curr_codes', $curr_codes)
				->with('vend_addr', $vendor_addr)
				->with('porder_info', $porder_info)
				->with('pordered_items', $po_items);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();
		$orderdetail = [];
		$itemdetail = [];
		
		$odform = [];
		$oditem = [];
		
		//process the input form.
		foreach($input as $key=>$value){
			$isItem = explode("_", $key);
			if($isItem[0] == 'oditem'){
				//echo $isItem[1] . ',' . $isItem[2];
				$itemdetail[$isItem[2]] = $value;
				$oditem[$isItem[1]] = $itemdetail;
				//$item['items'] = $oditem;
			}
			else{
				$orderdetail[$key] = $value;
				$odform[0] = $orderdetail;
				//$order['order'] = $odform;
			}
		}

		#all config files
		$compcode = Configs::where('type', '=', 'compcode')->first()->value;

		$po_type = Configs::where('type', '=', 'sys_param')
							->where('key', '=', 'web_po_subtype')
							->first();	
		$vendor_info = Vendor::where('vend_no', '=', $odform[0]['vendor_no'])->first();
		
		$tax_rate = Configs::where('type', '=', 'gst_rate')
							->where('key', '=', $vendor_info->tax_type)
							->first();		
		$running_no = Configs::where('type', '=', 'po_counts')
									->first();	

		$porder_info = POrder::where('id', '=', $odform[0]['porder_id'])->first();
		
		$porder_info->vend_grp 			= Input::get('vendor_group');
		$porder_info->vend_no 			= Input::get('vendor_no');
		$porder_info->vend_name 		= Input::get('vendor_name');
		$porder_info->vend_addr 		= Input::get('vendor_addr');
		$porder_info->cont_person 		= Input::get('vendor_contact_person');
		$porder_info->cont_no 			= Input::get('vendor_contact_phone');
		$porder_info->curr_code 		= Input::get('vendor_curr_code');
		$porder_info->curr_rate 		= Input::get('vendor_curr_rate');
		$porder_info->remark			= Input::get('order_remark');
		$porder_info->subtotal 			= Input::get('order_subtotal');
		$porder_info->gst 				= Input::get('order_gst');
		$porder_info->total 			= Input::get('order_total');
		$porder_info->status  			= 'CHG';

		$save = $porder_info->save();
		$porderid = $porder_info->id;
		#insert item related to the order into the database

		$ordered_items = POrderedItem::where('porder_id', '=', $porderid)->get();
		foreach ($ordered_items as $items){
			$old_itemlist[] = $items->item_id;
		}

		foreach ($oditem as $item){
			$new_itemlist [] = $item['itemid'];
		}

		//filter item that is tobe deleted, to be added or updated
		$tobe_del = array_diff($old_itemlist, $new_itemlist);
		$tobe_add = array_diff($new_itemlist, $old_itemlist);
		$tobe_upd = array_intersect($new_itemlist, $old_itemlist);		

		//create new log
		$log 					= new PorderLogEntry();
		$log->user_id 			= $odform[0]['user_id'];
		$log->porder_id 		= $porderid;
		$log->title 			= User::where('id', '=', $odform[0]['user_id'])->first()->username . ' - Amended the PO';

		foreach ($tobe_add as $itemno){
			$tax_amount = ($tax_rate->value / 100) * $item['total'];

			$add 			= new POrderedItem();
			$add->comp_code = $compcode;
			$add->item_no 	= Input::get('oditem_'.$itemno.'_itemno');
			$add->item_id 	= Input::get('oditem_'.$itemno.'_itemid');
			$add->descr 	= Input::get('oditem_'.$itemno.'_descr');
			$add->porder_id = $porderid;
			$add->added_by  = $odform[0]['user_id'];
			$add->qty 		= Input::get('oditem_'.$itemno.'_qty');
			$add->uom 		= Input::get('oditem_'.$itemno.'_uom'); 
			$add->uom_cf 	= Input::get('oditem_'.$itemno.'_uomcf');
			$add->unitprice = Input::get('oditem_'.$itemno.'_unitprice');
			$add->gst 		= $tax_amount;
			$add->total 	= Input::get('oditem_'.$itemno.'_total');		
			$add->save();
			
			$log->content 	.= 'New Item:'.$itemno.' is added with qty:'
			                   . Input::get('oditem_'.$itemno.'_qty') 
			                   . '@price: $' . Input::get('oditem_'.$itemno.'_unitprice') 
			                   . '/' .Input::get('oditem_'.$itemno.'_uom') . '<br>';
		}

		foreach ($tobe_upd as $itemno){
			$tax_amount = ($tax_rate->value / 100) * $item['total'];

			//echo 'tobe update:';
			$upd = POrderedItem::where('item_id', '=', $itemno)->where('porder_id', '=', $porderid)->first();	
			$input_itemno 	= Input::get('oditem_'.$itemno.'_itemno');
			$input_itemid 	= Input::get('oditem_'.$itemno.'_itemid');
			$input_qty		= Input::get('oditem_'.$itemno.'_qty');	 
			$input_uom 		= Input::get('oditem_'.$itemno.'_uom');
			$input_uomcf 	= Input::get('oditem_'.$itemno.'_uomcf'); 
			$input_price 	= Input::get('oditem_'.$itemno.'_unitprice');
			$input_total 	= Input::get('oditem_'.$itemno.'_total');			
			$isChanged 		= 0;
			$changes 		= '';

			if($upd->qty != $input_qty) {
				$changes .= ' Qty Changed from: ' . $upd->qty . ' to: '. $input_qty;
				$upd->qty = $input_qty;
				$isChanged = 1;
			}

			if($upd->uom != $input_uom) {
				$changes .= ' Uom Changed from: ' . $upd->uom . ' to: ' . $input_uom;
				$upd->uom = $input_uom;
				$isChanged = 1;
			}

			if($upd->unitprice != $input_price) {
				$changes .= ' Price Changed from: $' . $upd->unitprice .' to: $' . $input_price;
				$upd->unitprice = $input_price;
				$isChanged = 1;
			}

			if($isChanged == 1){
				$upd->save();
				$log->content .= 'Item No:'. $input_itemno . $changes . '<br>';
			}

			$upd->item_no  	= $input_itemno;
			$upd->item_id 	= $input_itemid;
			$upd->qty 		= $input_qty;
			$upd->uom 		= $input_uom; 
			$upd->uom_cf 	= $input_uomcf;
			$upd->unitprice = $input_price;
			$upd->gst 		= $tax_amount;
			$upd->total 	= $input_total;
			$upd->save();
		}

		foreach ($tobe_del as $itemno){
			$delete = POrderedItem::where('item_id', '=', $itemno)->where('porder_id', '=', $porderid)->first();			 
			
			$log->content 	.= 'Item No:'.$itemno.' - ('. $delete->qty . 'qty@$' . $delete->unitprice .') is deleted from order. <br>';
			$delete->delete();
		}

		$log->save();

		//get the updated PO
		$updatedPO = POrder::where('id', '=', $odform[0]['porder_id'])->first();

		//get updated PO items
		$updatedPOItems = POrderedItem::where('porder_id', '=', $updatedPO->id)->get();

		//generate the XML
		$count = 1;
		$updxml = '';
		foreach($updatedPOItems as $upditem){
			$updxml .= $this->generateItemDetailXML($upditem, $updatedPO, $count);
			$count+=1;
		}

		$updatedPOrderPost = $this->generateOrderMasterXML($updatedPO, $updxml);
		$user_name = User::where('id', '=', $odform[0]['user_id'])->first()->username; 

		//send the XML 
		$porderdate = date('Y/m/d', strtotime($updatedPO->created_at));
		$order_xml = new OrderXml();
		$order_xml->type 			= 'poc';
		$order_xml->xml_name 		= $updatedPO->doc_no;
		$order_xml->xml_contents 	= $updatedPOrderPost;
		$order_xml->save();

		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;

		$server_url=$url_server.'/konvergefb/web_createPO';
		$url = 'http://'.$server_username.':'.$server_password.'@'.$server_url;

		$post = 'compcode='.$updatedPO->comp_code.'&docno='. $updatedPO->doc_no. '&docdate=' . $porderdate . '&xmlfile='.$updatedPOrderPost . '&userid='.$user_name;
		$status_code = $this->sendByCurl($post, $url);	

		$order_xml->send_status = $status_code;
		$order_xml->save();

		return Redirect::to('/user/'. $odform[0]['user_id'] . '/porder')->with('message', 'Order Amended');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($user, $porder)
	{
		//delete order record
		$record = POrder::where('id', '=', $porder);
		$record->delete();

		//Session::flash('message', 'successfully deleted the order!');
		return Redirect::to('/user/'. $user . '/porder');
	}

}