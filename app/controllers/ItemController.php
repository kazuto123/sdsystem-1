<?php

class ItemController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getAllItems(){
		$items = Item::all();
		return Response::json($items);
	}

	public function getVendorItemByItemNo($itemid, $vendno, $userid){
		$item = Item::where('id', '=', $itemid)->first()->toArray();
		$uom = Uom::where('item_id', '=', $itemid)->get()->toArray();
		$vendor = Vendor::where('vend_no', '=', $vendno)->first();		
		$vendgrp = $vendor->vend_grp_code;
		$vendno = $vendor->vend_no;

		//Get last order items
		$lastPO = POrder::where('user_id', '=', $userid)
				->orderBy('created_at', 'desc')->first();
		
		$price = '';
		if(count($lastPO) > 0){
			$lastPOItem = POrderedItem::where('porder_id', '=', $lastPO->id)
	 								->where('item_id', '=', $itemid)->first();	

		 	#if item has a last order record, retrieve the price of item from there
			if($lastPOItem){
				$price = $lastPOItem->unitprice;
			}
		}
		//else get the item price from the presets.
		else{
			$presetitemfound = PresetItem::where('type', '=', 'V')
									->where('preset_by', '=', $vendno)
									->where('item_id', '=', $itemid)->first();
			//return $presetitemfound;
			if(count($presetitemfound) > 0){
				$price = $presetitemfound->price;
			}
			else{
				$price="";
			}
		}	 	

		//else take the price from the preset order


		//else if the items not in preset order / new items, allow user to input the qty

		
		$data = ['item'=>$item, 'uom'=>$uom, 'price'=>$price, 'vendgrp'=>$vendgrp, 'vendno'=>$vendno];

		return Response::json($data);
	}

	//get vendor preset item
	public function getVendorPresetItems($itemno, $vendno, $userid){
		//get vendor preset item
		//if the preset item have last order, get its price

		//if the preset item don have last order, get the leave the price blank

	}

	public function getItemById($itemid, $uid){
		//
		$item = Item::where('id', '=', $itemid)->get()->toArray();
		$uom = Uom::where('item_id', '=', $itemid)->get()->toArray();
		$user = Customer::where('cust_no', '=', $uid)->first();
		//dd($user);
		$custgrp = $user->cust_group_code;
		$custcode = $user->cust_no;
		
		//$phptime = now()
		//$currdate = date ("Y-m-d H:i:s", $phptime);
		//dd ($item);
		//get price
		$price = $this->price($item[0]['comp_code'], $item[0]['item_no'], $item[0]['uom'], $custgrp, $custcode, '$currdate')->toArray();
		//dd($[0]['uom']);
		$data = ['item'=>$item, 'uom'=>$uom, 'price'=>$price, 'custgrp'=>$custgrp, 'custcode'=>$custcode];

		return Response::json($data);

	}

	public function getItemUom($itemno){
		$uom = Uom::where('item_id', '=', $itemno)->get();
		return $uom;
		//return UOM associate with the Item No
	}

	protected function price($compcode, $itemno, $uom, $custgrp, $custcode, $orderdates){
		$seqs = PriceItemSeq::orderBy('seq', 'ASC')->get();
		//$data=$seqs;
		date_default_timezone_set('Asia/Singapore');
		$orderdate = date('Y-m-d H:i:s', time());
		//echo $orderdate;
		
		$itemid = Item::where('item_no', '=', $itemno)->first()->id; 


		$curr_price="";
		foreach($seqs as $seq){
			$table = substr($seq->price_type, 3);
			//echo $table;
			//$price = $this->getPrice($tablename, $compcode, $itemid, $uom, $custgrp, $custcode, $orderdate);
			
			if($table=='item_cust_price'){
				
				$price = ItemCustPrice::
				      		  where('comp_code', '=', $compcode)
							->where('item_id', '=', $itemid)
							->where('uom', '=', $uom)
							->where('cust_no', '=', $custcode)
							->get();
				//echo 'price1:'.$price;
				$curr_price = $price;
				//echo $curr_price;
			}
			else if ($table=='item_custgrp_price'){
				
				$price = ItemCustGrpPrice::
						  where('comp_code', '=', $compcode)
						->where('item_id', '=', $itemid)
						->where('uom', '=', $uom)
						->where('cust_group_code', '=', $custgrp)
						->get();
				//echo 'price2:'.$price;
				$curr_price = $price;
				//echo $curr_price;
			}	
			else if ($table=='item_period_price'){
				$price = ItemPeriodPrice::
						  where('comp_code', '=', $compcode)
						->where('item_id', '=', $itemid)
						->where('uom', '=', $uom)
						//->where('cust_group_code', '=', $custgrp)
						//->where('cust_no', '=', $custcode)
						->where('from_date', '>', $orderdate)
						->where('to_date', '<', $orderdate)
						->get();
				//echo 'price3:'.$price;
				$curr_price = $price;
				//echo $curr_price;
			}					
			else if ($table=='item_list_price'){
				
				$price = ItemListPrice::
						  where('comp_code', '=', $compcode)
						->where('item_id', '=', $itemid)
						->where('uom', '=', $uom)
						->get();
				//echo 'price4:'.$price;
				$curr_price = $price;
				//echo $curr_price;
			}
			else{
				$curr_price = 0;
			}
			//echo count($curr_price);
			if(count($curr_price)>0){return $curr_price;}
		}
	}




}