<?php

class MobileStoreController extends \BaseController {


public function showStoreItems(){
	return View::make('mobile.mstore');
}

public function processPurchase(){
	$input = Input::all();

	//return $input;
	//$url = 'http://sdsystem.dev/demo/item/purchase';
	$url = 'http://50.112.173.46/demo/item/purchase';
	$data = array('items' => 
				array(
					['comp_code' 		=> $input['app_compcode'], 
					 'item_id'			=> $input['purchase_item_id'], 
					 'item_no'		 	=> $input['purchase_item_no'],  
					 'item_descr' 		=> $input['purchase_item_descr'], 
					 'item_qty' 		=> $input['purchase_item_qty'], 
					 'item_uom' 		=> $input['purchase_item_uom'],
					 'item_price'		=> $input['purchase_item_price']]
				 ), 'customer'=>array(
				 			'comp_code'=>$input['app_compcode'], 
				 			'cust_no'=>$input['app_custno'], 
				 			'cust_username'=>$input['app_username'],
				 			'cust_name'=>'Arun', 
				 			'cust_country' => 'Singapore', 
				 			'cust_addr' => $input['app_custaddrref'], 
				 			'cust_contact' => $input['app_custcont'],
				 			'cust_curr_code' => $input['app_currcode']
				 )
			);
	//echo json_encode($data, 128);
	//dd($data_send);
	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'content' => http_build_query($data),
	    ),
	);

	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	
	$rmsg = json_decode($result)->message;	

	$return_url = '/mcomm/items/purchase/message/' . $rmsg;

	return Redirect::to($return_url );
}

public function showPurchaseMsg($msg){
	return View::make('mobile.purchaseMsg')->with('msg', $msg);
}

}