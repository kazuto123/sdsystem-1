<?php

class CurrencyController extends \BaseController {

	public function getCurrencyRate($code, $period){
		$curr_rate = CurrExchRate::where('curr_code', '=', $code)
								->orderBy('ac_period', 'desc')->first();

		return Response::json($curr_rate);
	}

}