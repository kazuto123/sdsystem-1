<?php

class SdapiController extends BaseController{

	protected $login = 'kremote:kremote132';
	protected $comp_code = '01';
	protected $server_ip = '203.125.118.241:8080/konvergefb/';
	public function __construct(){}


	public function test($user_id){
		$cust = Customer::find($user_id)->address()->get();
		echo $cust;
		//return $cust;
	}

	//function to obtained data give the API URI
	private function getData($apiurl){
		try
		{
			$json_string = file_get_contents($apiurl);			
		}
		catch(Exception $e){
			$json_string = 'NOT AVAILABLE DUE TO' . $e->getMessage();
		}

		return json_decode($json_string);
	}

	//BOM API ------------------------------------------------------------------------------------------
	public function API_getListBOM($bomcode){	
		if($bomcode == 'all'){
			$content = $this->getData('http://'.Session::get('server_info').'web_listBOM?compcode=' 
				. $this->comp_code . '&bomcode=%&itemno=%&status=%');
					//return $content;
			$response = Response::json($content);
			//$response->header('Content-Type', 'application/json');
			return $response;
		}
		//if is is not all
		$content = $this->getData('http://'.Session::get('server_info').'web_listBOM?compcode=' 
			. $this->comp_code . '&bomcode=' . $bomcode . '&itemno=%&status=%');
		
		//return $content;
		$response = Response::json($content);
		//$response->header('Content-Type', 'application/json');
		return $response;
	}
	
	#check the availability of the bom code
	public function API_checkBomCodeValid($bomcode){
		$content = $this->getData('http://'.Session::get('server_info').'web_check_BOMCode?compcode=' 
			. $this->comp_code . '&bomcode=' . $bomcode);
		return Response::json($content);
	}

	#get the item by category
	public function API_bomGetItembyCAT($catcode){
		$content = $this->getData('http://'.Session::get('server_info').'web_getitem?compcode=' 
			. $this->comp_code . '&catcode=' . $catcode);
		return Response::json($content);

	}

	#search for item base on their item desc, item no and category code.
	public function API_bomSearchItem($itemdesc, $itemno, $catcode, $itemtype){
		if($itemdesc == '0') $itemdesc = '%';
		if($itemno == '0') $itemno = '%';
		if($catcode == '0') $catcode = '%';
		if($itemtype == '0') $itemtype= '%';
		$content = $this->getData('http://'.Session::get('server_info').'web_Search_item?compcode=' 
			. $this->comp_code . '&itemdesc='. $itemdesc .'&itemno=' . $itemno . '&catcode='.$catcode . '&itemtype=' . $itemtype);
		
		return Response::json($content);
	}

	#get item uom base on item id.
	public function API_bomGetItemUOM($itemid){
		$content = $this->getData('http://'.Session::get('server_info').'web_get_item_uom_dtls?compcode=' 
			. $this->comp_code . '&itemid=' . $itemid);
		return Response::json($content);
	}



	#SD Stock Taking API ----------------------------------------------------------------------------------------
	public function API_stockGetWarehouses(){
		$content = $this->getData('http://'.Session::get('server_info').'web_st_getwarehouse?compcode=' 
			. $this->comp_code);

		return Response::json($content);
	}

	public function API_stockGetWarehouseItems($whid, $trandate){
		$date = explode('-', $trandate);
		$transfer_date = $date[0].'/'.$date[1].'/'.$date[2];
		//echo $transfer_date;
		$content = $this->getData('http://'.Session::get('server_info').'web_st_storage_info?compcode=' 
			. $this->comp_code . '&whid=' . $whid . '&trandate=' . $transfer_date);
		
		$return = [];
		foreach ($content as $item){
			$itemuom = $this->getData('http://'.Session::get('server_info').'web_st_get_itemuomdtls?compcode=' 
				. $this->comp_code . '&itemid=' . $item->item_id);

			$return [] = array('item'=>$item, 'itemuom'=>$itemuom);
		}

		//echo $content[0]->item_id;
		//$items = array('item'=>$content, 'itemuom'=>'');
		return Response::json($return);
	}

	public function API_fail(){
		$content = $this->getData('http://'.Session::get('server_info').'web_st_storage?compcode=01');
		return $content;
	}

	#SD Ordertaking and OrderPlacing API ------------------------------------------------------------------------

	//Insert the user based on the provide field to the users table
    private function insertUser($email, $username, $password, $issalesman, $role, $id){
	    $found = User::where('username', '=', $username)->first();
	    if($found){
	    	echo 'username existed.. <br>';
	    }
	    else{
		    $user = new User;
	        $user->username = $username;
	        $user->email = $email;
	        $user->password = $password;
	        $user->issalesman = $issalesman;
	        $user->user_role = $role;
	        $user->user_role_id = $id; //their id no. eq. vend_no, cust_no, admin_no, etc...
	        $user->confirmed = 1;

	        $user->password_confirmation = $password;

	        $done = $user->save();        	
	        //echo $done;
	        if ($done){
	            echo 'Successfully added user: ' . $username . ' to user table. <br>';
	        }
	        else{
	            return $user->errors()->all(':message') . '<br>';
	        }	    	
	    }

	}

	//function to insert Customer Data from Solution Detail API
	public function API_getCustomer(){
		//$json_string = file_get_contents('http://'.Session::get('server_info').'web_ar_customer?compcode=01');
        //$content = json_decode($json_string);
        $content = $this->getData('http://'.Session::get('server_info').'web_ar_customer?compcode=01');
        foreach ($content as &$item){  
        	$found = Customer::where('cust_no', '=', $item->cust_no)->first();
        	if($found){
        		echo $item->cust_no . 'Exist in Database. Not inserted ...' . '<br>';
        	}
        	else{
				DB::table('users')->insert(array(
	            array(
	                'username'          => 'customer'.$item->cust_no,
	                'email'             => 'customer'.$item->cust_no . '@sample.com',
	                'password'          => Hash::make('pass123'),
	                'confirmation_code' => 'somerandomcode',
	                'confirmed'         => 1
	            )));
				$newuserid = User::where('username', '=', 'customer'.$item->cust_no)->first()->id;

	        	$cust = new Customer();
	        	$cust->id 					= $item->cust_no;
	        	$cust->comp_code 			= $item->comp_code;
	        	$cust->user_id 				= $newuserid;
	        	$cust->cust_group_code		= $item->cust_group_code;
	        	$cust->cust_no				= $item->cust_no;
	        	$cust->cust_name 		 	= $item->cust_name;
	        	$cust->cust_cont_name 		= $item->cont_name;
	        	$cust->cust_curr_code		= $item->curr_code;
	        	$cust->cust_tax_type 		= $item->tax_type;
	        	$cust->cust_absorb_tax		= $item->absorb_tax;
	        	$cust->inactive 			= $item->inactive;
	        	$cust->salesman 			= $item->salesman;

	        	$cust->save();        		
        		echo 'NEW Customer No:' . $item->cust_no . 'Inserted.' . '<br>';

        		//$this->insertUser('cust_'. $item->cust_no . '@gmail.com' ,'cust_' . $item->cust_no, $item->cust_no, 0, 'customers', $item->cust_no);
				  		
        	}
        }	        
	    return 'DONE Inserting All Customers From API -- <br>';
	}


	//function to obtain customer_address data from Solution Detail API -- No Duplicate Checkings!!
	public function API_getCustAddr(){
		$json_string = file_get_contents('http://'.Session::get('server_info').'web_ar_cust_addr?compcode=01');
        $content = json_decode($json_string);
        foreach ($content as &$item){  
        	$addr = new Customeraddr();
        	$addr->comp_code 	= $item->comp_code;
        	$addr->cust_no 		= $item->cust_no;
        	$addr->addr_ref 	= $item->addr_ref;
        	$addr->addr 		= $item->addr;
        	$addr->zip 			= $item->zip;
        	$addr->ctry_code 	= $item->ctry_code;
        	$addr->cont_name 	= $item->cont_name;
        	$addr->cont_off_tel = $item->cont_off_tel;
        	$addr->cont_res_tel	= $item->cont_res_tel;
        	$addr->cont_hp_no	= $item->cont_hp_no;
        	$addr->cont_fax_no  = $item->cont_fax;
        	$addr->cont_email	= $item->cont_email;
        	$addr->active 		= 1;

        	$done = $addr->save();
        	echo 'Custommer Addr Zip:' . $item->zip . ' -Is Inserted <br>';
        }

        if($done) return 'All Customer_addr data Inserted <br>';
        return 'Something is wrong!! not updated!! <br>';
	}

	//Get Vendor Informations from Solution detail API
	public function API_getVendor(){
        $content = $this->getData('http://'.Session::get('server_info').'web_ap_vendor?compcode=01');
        foreach ($content as &$item){  
        	$found = Vendor::where('vend_no', '=', $item->vend_no)->first();
	        if($found){
	        	echo 'Vendor No: ' . $item->vend_no . " Exist in Database. Not Inserted." . '<br>';
	        }
	        else{
				DB::table('users')->insert(array(
	            array(
	                'username'          => 'vendor'.$item->vend_no,
	                'email'             => 'vendor'.$item->vend_no . '@sample.com',
	                'password'          => Hash::make('pass123'),
	                'confirmation_code' => 'somerandomcode',
	                'confirmed'         => 1
	            )));
				$newuserid = User::where('username', '=', 'vendor'.$item->vend_no)->first()->id;

	        	$vendor 					= new Vendor();
	        	$vendor->id 				= $item->vend_no;
	        	$vendor->comp_code 			= $item->comp_code;
	        	$vendor->user_id 			= $newuserid;
	        	$vendor->vend_grp_code 		= $item->vend_grp_code;
	        	$vendor->vend_no 			= $item->vend_no;
	        	$vendor->vend_name 			= $item->vend_name;
	        	$vendor->curr_code 			= $item->curr_code;
	        	$vendor->tax_type 			= $item->tax_type;
	        	$vendor->addr 	  			= $item->addr;
	        	$vendor->ctry_code 			= $item->ctry_code;
	        	$vendor->cont_name 			= $item->cont_name;
	        	$vendor->phone_no1			= $item->phone_no1;
	        	$vendor->phone_no2 			= $item->phone_no2;
	        	$vendor->fax_no 			= $item->fax_no;
	        	$vendor->cont_email			= $item->cont_email;

	        	$done = $vendor->save();
	        	if($done)
	        		echo "New Vendor No. :" . $item->vend_no . "Inserted <br>";
	        	else echo $item->vend_no . "not inserted" ;

	        }
       	}
       	return 'All Vendors Insertion from API are Done!! <br>';
	}

	//function to Get Items from Solution Detail API
	public function API_getItems(){
        $content = $this->getData('http://'.Session::get('server_info').'web_in_item?compcode=01');

        foreach ($content as &$i){  
	        $found = Item::where('item_no', '=', $i->item_no)->first();
	        if($found){
	        	echo "Item No: " . $i->item_no . " Existed, Not Inserted. <br>";
	        }
	        else{
	        	$item = new Item();
	        	$item->id 					= $i->item_id;
	        	$item->comp_code 			= $i->comp_code;
	        	$item->item_no 				= $i->item_no;
	        	$item->item_descr		 	= $i->item_descr;
	        	$item->cat_code				= $i->cat_code;
	        	$item->uom 					= $i->uom;
	        	$item->is_sell				= $i->is_sell;
	        	$item->is_buy 				= $i->is_buy;
	        	$item->is_inventory			= $i->is_inventory;
	        	$item->flang_descr			= $i->flang_descr;

	        	$item->save();

	        	echo "New Item No: " . $i->item_no . "Inserted. <br>";
	        }
    	}
        return 'Finished Get Item ...... <br>';
	}

	//get category from the API
	public function API_getCategory(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_sub_cat?compcode=01');
		
		foreach ($content as &$i){

			$cat = new Category();
			$cat->comp_code 			= $i->comp_code;
			$cat->cat_pos 				= $i->cat_pos;
			$cat->cat_len 				= $i->cat_len;
			$cat->sub_cat 				= $i->sub_cat;
			$cat->descr 				= $i->descr;

			$done = $cat->save();
			if($done){
				echo 'New Category: ' . $i->descr . 'Inserted. <br>';	
			}
			else{
				echo $cat->errors()->all(':message') . '<br>';
			}

		}
		return 'Finished Get Catogory .... <br>';
	}

	//get UOM from API -- No Duplicate checkings ...
	public function API_getUom(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_uom_dtls?compcode=01');
		foreach ($content as &$i){
			$uom = new Uom();
			$uom->comp_code 				= $i->comp_code;
			$uom->item_id 					= $i->item_id;
			$uom->item_no 					= $i->item_no;
			$uom->s_no 						= $i->s_no;
			$uom->uom 						= $i->uom;
			$uom->cf_qty 					= $i->cf_qty;
			$uom->uom_basic					= $i->uom_basic;

			$done = $uom->save();

			if($done){
				echo 'Inserted:' . $i->uom . ' - into uom table <br>';
			}
			else{
				echo $uom->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get UOM ....';
	}

	//get price item seq
	public function API_getPriceItemSeq(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_price_seq?compcode=01');
		foreach ($content as &$i){
			$pis = new PriceItemSeq();

			$pis->comp_code     			= $i->comp_code;
			$pis->price_type				= $i->price_type;
			$pis->seq 						= $i->seq;

			$done = $pis->save();
			if($done){
				echo 'Inserted: ' . $i->price_type . ' - into price Item Seq table. <br>';
			}
			else{
				echo $pis->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get Price item seq ... <br>';
	}
	
	
	//get system params
	public function API_getSystemParams(){
		$content = $this->getData('http://'.Session::get('server_info').'web_sys_system_param?compcode=01');
		foreach($content as &$i){
			$sys = new Configs();
			$sys->comp_code 				= $i->comp_code;
			$sys->type 						= 'sys_param';
			$sys->key 						= $i->param_id;
			$sys->value 					= $i->param_value;
			$sys->descr 					= $i->remarks;

			$done = $sys->save();
			if($done){
				echo 'Inserted: ' . $i->param_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $sys->errors()->all(':message') . '<br>';
			}
		}
		return 'Finished get sys param ... <br>';
	}

	//get AC_period
	public function API_getAcPeriod(){
		$content = $this->getData('http://'.Session::get('server_info').'web_gl_ac_period?compcode=01');
		foreach($content as &$i){
			$acp = new AcPeriod();

			$acp->comp_code 				= $i->comp_code;
			$acp->ac_year 					= $i->ac_year;
			$acp->ac_period 				= $i->ac_period;
			$acp->st_date 					= $i->st_date;
			$acp->ed_date 					= $i->ed_date;
			$acp->status 					= $i->status;

			$done = $acp->save();
			if($done){
				echo 'Inserted: ' . $i->ac_period . ' - into price Item Seq table. <br>';
			}
			else{
				echo $acp->errors()->all(':message') . '<br>';
			}			
		}
		return 'Finished get Ac Period ... <br>';
	}

	//get user master .... SKIP .....


	//get currency Exchange info ...  API got issue ---
	public function API_getCurrExchRate(){
		$content = $this->getData('http://'.Session::get('server_info').'web_currency_exch?compcode=01');
		
		foreach($content as &$i){

			$cer = new CurrExchRate();

			$cer->comp_code  				= $i->comp_code;
			$cer->curr_code 				= $i->curr_code;
			$cer->ac_period  				= $i->ac_period;
			$cer->exch_rate 				= $i->exch_rate;

			$done = $cer->save();
			if($done){
				echo 'Inserted: ' . $i->curr_code . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cer->errors()->all(':message') . '<br>';
			}						
		}
		return 'Finished get Currency Exchange Rate ... <br>';
	}

	//get item list price
	public function API_getItemListPrice(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_list_price?compcode=01');
		
		foreach ($content as &$i){
			$ilp = new ItemListPrice();
			$ilp->comp_code  				= $i->comp_code;
			$ilp->item_id  					= $i->item_id;
			$ilp->uom 						= $i->uom;
			$ilp->price 					= $i->price;
			$done = $ilp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $ilp->errors()->all(':message') . '<br>';
			}					

		}
		return 'Finished get Item List Price ';
	}


	//get cust_grp_price ....
	public function API_getCustGrpItemPrice(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_custgrp_price?compcode=01');
		foreach($content as &$i){
			$cgp = new ItemCustGrpPrice();
			$cgp->comp_code 				= $i->comp_code;
			$cgp->item_id 					= $i->item_id;
			$cgp->cust_group_code			= $i->cust_group_code;
			$cgp->uom 						= $i->uom;
			$cgp->price 					= $i->price;
			$done = $cgp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cgp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Cust Group item price ... <br>';
	}

	//get customer price
	public function API_getCustPrice(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_cust_price?compcode=01');
		foreach($content as &$i){
			$cp = new ItemCustPrice();
			$cp->comp_code 					= $i->comp_code;
			$cp->item_id 					= $i->item_id;
			$cp->cust_no					= $i->cust_no;
			$cp->uom 						= $i->uom;
			$cp->price 						= $i->price;
			$done = $cp->save();
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $cp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Cust item price ... <br>';		
	}

	//get period price
	public function API_getPeriodPrice(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_period_price?compcode=01');
		foreach($content as &$i){
			$pp = new ItemPeriodPrice();
			$pp->comp_code 					= $i->comp_code;
			$pp->item_id 					= $i->item_id;
			$pp->cust_group_code			= $i->cust_group_code;
			$pp->cust_no 					= $i->cust_no;
			$pp->uom 						= $i->uom;
			$pp->s_no   					= $i->s_no;
			$pp->from_date 					= $i->from_date;
			$pp->to_date 					= $i->to_date;
			$pp->price 						= $i->price;
			$done = $pp->save();
			
			if($done){
				echo 'Inserted: ' . $i->item_id . ' - into price Item Seq table. <br>';
			}
			else{
				echo $pp->errors()->all(':message') . '<br>';
			}		
		}
		return 'Finished get Period item price ... <br>';
	}

	public function API_getPreset(){
		$content = $this->getData('http://'.Session::get('server_info').'web_in_item_crossref?compcode=01');
		foreach($content as $i){
			$preset = new PresetItem();
			$preset->comp_code 					= $i->comp_code;
			$preset->type    					= $i->cust_or_vend;
			$preset->item_id 					= $i->item_id;
			$preset->s_no 						= $i->s_no;
			$preset->preset_by					= $i->cust_vend_no;
			$preset->item_no 					= $i->ref_itemno;
			$preset->descr 						= $i->ref_item_descr;
			$preset->uom 						= $i->uom;
			$preset->curr_code 					= $i->curr_code;
			$preset->price 						= $i->price;
			$done = $preset->save();
			if($done){
				echo 'Inserted: ' . $i->ref_itemno . ' - into price preset table. <br>';
			}
			else{
				echo $preset->errors()->all(':message') . '<br>';
			}		
		}	
		return 'Finished getting preset items ... <br>';
	}

	public function API_getMember(){
		$content = $this->getData('http://'.Session::get('server_info').'web_user_master?compcode=01');
		foreach($content as &$i){
			DB::table('users')->insert(array(
            array(
                'username'          => $i->userid,
                'email'             => $i->userid.'@sample.comm.sg',
                'password'          => Hash::make($i->passwd1),
                'confirmation_code' => 'somerandomcode',
                'confirmed'         => 1
            )));
			$userid = User::where('username', '=', $i->userid)->first()->id;
			$stf = new Staff();
			$stf->comp_code 					= $i->comp_code;
			$stf->type 							= $i->groupid;
			$stf->user_id						= $userid;
			$stf->first_name					= $i->name;
			$stf->issalesman 					= $i->issalesman;

			$done = $stf->save();
			if($done){
				echo 'Inserted: ' . $i->userid . ' - into price Staff table. <br>';
			}
			else{
				echo $stf->errors()->all(':message') . '<br>';
			}				
		}
		return 'Finished get User Master ... <br>';
	}

	public function API_getCurrCode(){
		$content = $this->getData('http://'.Session::get('server_info').'web_currency?compcode=01');
		foreach($content as $i){
			$curr = new Configs();
			$curr->comp_code = $i->comp_code;
			$curr->type = 'currency';
			$curr->key = 'code';
			$curr->value = $i->curr_code;
			$curr->descr = $i->descr;
			$save = $curr->save();

			if($save){
				echo 'Inserted: ' . $i->curr_code . ' - into Configs table. <br>';
			}
			else{
				echo $stf->errors()->all(':message') . '<br>';
			}
		}
	}

	// public function API_getAcPeriod(){
	// 	$content = $this->getData('http://'.Session::get('server_info').'web_gl_ac_period?compcode=01');
	// 	foreach($content as $i){
	// 		$acp = new AcPeriod();
	// 		$acp->comp_code = $i->comp_code;
	// 		$acp->ac_year  	= $i->ac_year;
	// 		$acp->ac_period = $i->ac_period;
	// 		$acp->st_date 	= $i->st_date;
	// 		$acp->ed_date	= $i->ed_date;
	// 		$acp->status 	= $i->status;
	// 		$save = $acp->save();
	// 		if($save){
	// 			echo 'Inserted: ' . $i->ac_period . ' - into ac_period table. <br>';
	// 		}
	// 		else{
	// 			echo $stf->errors()->all(':message') . '<br>';
	// 		}
	// 	}
	// }

	public function API_getAll(){
		$this->API_getCustomer();
		$this->API_getCustAddr();
		$this->API_getVendor();
		$this->API_getItems();
		$this->API_getCategory();
		$this->API_getUom();
		$this->API_getPriceItemSeq();
		$this->API_getSystemParams();
		$this->API_getAcPeriod();
		$this->API_getCurrExchRate();
		$this->API_getItemListPrice();
		$this->API_getCustGrpItemPrice();
		$this->API_getCustPrice();
		$this->API_getPeriodPrice();
		$this->API_getPreset();
		$this->API_getMember();
		$this->API_getCurrCode();
		//$this->API_getAcPeriod();
		return 'DONE !! ------------------------------';
	}


	public function API_getdemoItem(){
		$url = 'http://sddemo.mooo.com:8080/konvergefb/web_in_item?compcode=01';
		$content = $this->getData($url);
		foreach($content as $item){
			echo $item;
			// if($item->cat_code == 'CAK' || $item->cat_code == 'DRK' ){
			// 	$return[] = $item;
			// }
		}
		//echo $url;
		return Response::json($return);
	}

	#------------------------------------------------------------------------------------------------------
	#function to synchro back to solution details backend
	public function API_so_sync(){
		$data = Input::all();

		$items = $data['items'];
		$cust_info = $data['customer'];

		$count = 1;
		$total_amt = 0;
		$total_gst = 0;
		$order_subtotal = 0;
		$total_order_amt = 0;
		$so_item = "";
		$so_num = Configs::where('type', '=', 'to_counts')->first();
		$doc_no = 'SOR-07' .  date("ym") . str_pad($so_num->value, 5, '0', STR_PAD_LEFT);
		$so_num->value = $so_num->value + 1;
		$so_num->save();

		//generate the XML for item details
		foreach ($items as $t){
			$total_amt = $t['item_price'] * $t['item_qty']; 
			$gst_amt = ($total_amt * 7) / 100; 
			$so_item .= $this->generateItemDetailsXML($t, $cust_info, $count, $doc_no, $total_amt, 'GST7', '7', $gst_amt, date('Y-m-d'));
			$total_gst += $gst_amt;
			$order_subtotal += $total_amt;
			$count += 1;
			//$total_order_amt += $order_subtotal;
		}

		//generate master detail xml and combine it with item details XML.
		$so_mainxml = $this->generateOrderMasterXML($cust_info, $doc_no, $total_gst, $order_subtotal, $so_item);

		//echo $so_mainxml;

		$order_xml = new OrderXml();
		$order_xml->type 			= 'SO';
		$order_xml->xml_name 		= $doc_no;
		$order_xml->xml_contents 	= $so_mainxml;
		$order_xml->save();

		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;
		$server_url=$url_server.'/konvergefb/web_createSO';

		$url = 'http://'.$server_username.':'.$server_password.'@'.$server_url;

		$post = 'compcode='.$cust_info['comp_code'].'&docno='. $doc_no. '&docdate=' . date('Y/m/d') . '&xmlfile='.$so_mainxml . '&userid=arun';

		$status_code = $this->sendByCurl($post, $url);

		$order_xml->send_status = $status_code;
		$order_xml->save();

		
		if($status_code == '0'){
			$return_status = array('status'=>'0', 'message'=>'Transaction Success. Thank you for your purchase.');
		}
		else{
			$return_status = array('status'=>'-1', 'message'=>'Transaction Not Success. Something Went Wrong, Status Code:' . $status_code);
		}

		return json_encode($return_status);
		//return $return_status;
	}

	protected function generateItemDetailsXML($item, $so, $sno_count, $doc_no, $total_amt, $tax_type, $tax_value, $gst_amt, $date){
		return 
			'<details>'.
				'<comp_code>'.$so['comp_code'].'</comp_code>'.
				'<doc_no>'.$doc_no.'</doc_no>'.
				'<s_no>'.$sno_count.'</s_no>'.
				'<item_id>'.$item['item_id'].'</item_id>'.
				'<item_no>'.$item['item_no'].'</item_no>'.
				'<item_descr>'.$item['item_descr'].'</item_descr>'.
				'<cust_part_no>0</cust_part_no>'. //need to change
				'<uom>'.$item['item_uom'].'</uom>'.
				'<uom_cf> 1.00000 </uom_cf>'.
				'<qty_order>'.$item['item_qty'].'</qty_order>'.
				'<price>'.$item['item_price'].'</price>'.
				'<total>'.$total_amt.'</total>'.
				'<disc_type>0</disc_type>'.
				'<disc_value>0</disc_value>'.
				'<disc_amount>0</disc_amount>'.
				'<net_amount>'.$total_amt.'</net_amount>'.
				'<tax_type>'.$tax_type.'</tax_type>'.
				'<tax_rate>'.$tax_value.'</tax_rate>'.
				'<tax_amt>'.$gst_amt.'</tax_amt>'.
				'<so_etd>'.$date.'</so_etd>'.
				'<so_eta>'.$date.'</so_eta>'.
			'</details>';
	}

	protected function generateOrderMasterXML($so, $doc_no, $total_gst, $subtotal ,$itemdetailXML){	
		//process the dates
		$ac_period = date('Ym'); //current date format 1
		$date_1 = date('Y/m/d'); //current date format 2

		return
		'<?xml version="1.0" encoding="UTF-8"?>'.
		'<plist version="1.0">'.
		'<array>'.
			'<master>'.
				'<comp_code>'.$so['comp_code'].'</comp_code>'. //hardcode
				'<doc_no>'. $doc_no.'</doc_no>'.
				'<doc_date>'.$date_1.'</doc_date>'.
				'<cust_no>'.$so['cust_no'].'</cust_no>'.
				'<ac_period>'.$ac_period.'</ac_period>'. //hardcode
				'<cust_po_no>-</cust_po_no>'.
				'<cust_po_date>'.$date_1.'</cust_po_date>'.
				'<cont_name>'.$so['cust_name'].'</cont_name>'.
				'<ship_addr_ref>'.$so['cust_addr'].'</ship_addr_ref>'. //hardcode
				'<salesman>DBA</salesman>'.
				'<curr_code>'.$so['cust_curr_code'].'</curr_code>'.
				'<exch_rate>1.00000</exch_rate>'. //hardcoded
				'<absorb_tax>0</absorb_tax>'. //needed - currently hardcoded
				'<tax_amt>'.$total_gst.'</tax_amt>'.
				'<disc_amt>0</disc_amt>'.
				'<total>'.$subtotal.'</total>'.
				'<so_status>N</so_status>'.
				'<remarks>Some Comments</remarks> '.
			'</master>'.
			
			$itemdetailXML.
			
			'</array>'.
			'</plist>';
	}

	public function sendByCurl($post_field, $url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		//curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
		
		//Execute the request and also time the transaction ( optional )
		$start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		
		//$responseInfo = curl_getinfo($ch);
		//$httpResponseCode = $responseInfo['http_code'];
		//echo $responseInfo;

		$stop = array_sum(explode(' ', microtime()));
		$totalTime = $stop - $start;
		//echo $result; 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

		    switch($returnCode){
		        case 200:
		        	//echo 'success:-';
		        	//echo $result;
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            //echo 'failed';
		            //echo $result;
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);
		
		$json = json_decode($result, true);
		return $json[0]['ReturnValue']; 
	}	








}//end of class


