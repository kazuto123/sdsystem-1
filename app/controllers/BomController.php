<?php

class BomController extends \BaseController {

	//protected $server_login = 'kremote:kremote132';
	protected $comp_code = '01';
	//protected $server_url = '203.125.118.241:8080/konvergefb/';

	private function getCompCode(){
		$compcode = Configs::where('type', '=', 'compcode')->first()->value;
		return $compcode;
	}

	//Landing page after user login
	public function index(){
		if(Session::get('access') == 'Y' && Session::get('app_type') == 'bom'){
			//list all the BOM that belongs to the logged in user.
			$access_right = array(
				'add'=>Session::get('access_add'),
				'upd'=>Session::get('access_upd'),
				'del'=>Session::get('access_del'),
				'view'=>Session::get('access_view')
			);

			$username = Session::get('userid');
			return View::make('bom.index')
				->with('username', $username)
				->with('acc_right', $access_right)
				->with('message', '');
		}
		else{
			return Redirect::to('sdbom/login')
				->with('message', 'Please Login to the System');
		}
	}

	public function create(){
		if(Session::get('access') == 'Y' && Session::get('app_type') == 'bom'){
			//create new BOM 
			//get all items
			$access_right = array(
				'add'=>Session::get('access_add'),
				'upd'=>Session::get('access_upd'),
				'del'=>Session::get('access_del'),
				'view'=>Session::get('access_view')
			);
			if(Session::get('access_add') == 1){
				$sub_cat = $this->getResponse('http://'.Session::get('server_info'). 'web_get_sub_cat?compcode='.$this->comp_code);

				//getall items
				$url = 'http://'.Session::get('server_info').'web_Search_item?compcode=' . $this->comp_code . '&itemdesc=%&itemno=%&catcode=%&itemtype=F';
				$json_string = file_get_contents($url);
				$allitems = json_decode($json_string);

				date_default_timezone_set('Asia/Singapore');
				$curr_datetime = date('m/d/Y h:i:s a', time());

				return View::make('bom.create')
					->with('approve_status', 'NEW')
					->with('acc_right', $access_right)
					->with('allitems', $allitems)
					->with('sub_cats', $sub_cat)
					->with('curr_datetime', $curr_datetime)
					->with('username', Session::get('userid'));				
			}
			
			return Redirect::to('/sdbom/bom')
				->with('message', 'User Does not has right to create a BOM.');
		}
		else{
			return Redirect::to('/sdbom/login')
				->with('message', 'Please Login to the System');
		}
	}

	public function store(){
		//this will process all the information from the POST
		$input = Input::all();
		$curr_bom_id = Configs::where('type', '=', 'bom_id_count')->first();

		//process the input form.
		foreach($input as $key=>$value){
			$isItem = explode("_", $key);
			if($isItem[0] == 'oditem'){
				$itemdetail[$isItem[2]] = $value;
				$oditem[$isItem[1]] = $itemdetail;
			}
			else{
				$orderdetail[$key] = $value;
				$odform[0] = $orderdetail;
				//$order['order'] = $odform;
			}
		}

		//return $orderdetail;
		$itemid = explode('-', $orderdetail['bom_itemno'])[0];
		$itemnumber = explode('-', $orderdetail['bom_itemno'])[1];

		//echo $itemno;

		
		$count = 1;
		$bomitemxml = "";
		foreach ($oditem as $item){
			$bomitemxml .= $this->generateItemDetailXML($item, $curr_bom_id->value, $count);
			$count += 1;
		}

		//echo 'itemno-'.$itemnumber;
		$bomxml = $this->generateOrderMasterXML($orderdetail, $bomitemxml, $itemid, $itemnumber);


		$bomxml_log = new OrderXml();
		$bomxml_log->type 			= 'bo';
		$bomxml_log->xml_name 		= 'BOMXML-'.$orderdetail['bom_code'];
		$bomxml_log->xml_contents	= $bomxml;
		$bomxml_log->save();

		// Current time zone
		date_default_timezone_set('Asia/Singapore');
		$trandate = date('Y/m/d', time());
 
		$url = 'http://'.Session::get('server_info').'web_CreateBOM';
		$post = 'compcode='.$this->comp_code.'&bomid=0&bomcode=' . $orderdetail['bom_code'] . '&xmlfile='. $bomxml . '&trandate='.$trandate . '&userid=' . Session::get('userid');
		$status_code = $this->sendByCurl($post, $url);

		//return $status_code;
		if(isset($status_code)){
			if($status_code == 0){
				$bomxml_log->send_status = $status_code;
				$bomxml_log->save();		
				$return_message = 
				'<div class="alert alert-success alert-dismissable bom-message">' .
				   '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
				   'New BOM is created. (Status Code: ' . $status_code . ')' .
				'</div>';			
			}
			else{
				$bomxml_log->send_status = $status_code;
				$bomxml_log->save();
				$return_message = 
				'<div class="alert alert-error alert-dismissable bom-message">' .
				   '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
				   'New BOM not successfully Created (Status Code: ' . $status_code . ')' .
				'</div>';	
			}
		}
		else{
			$errorMsg = $this->createAlert('The server Does not return the Status Code ' , 'error');
			return Redirect::to('/sdbom/bom')->with('message', $errorMsg);			
		}
		
		$curr_bom_id->value += 1;
		$curr_bom_id->save();

		return Redirect::to('/sdbom/bom')
					->with('message', $return_message);
	}

	// generate the xml for itemdetails
	public function generateItemDetailXML($item, $bomid , $count){
		//$bom_created_date = date('Y/m/d', strtotime($bomorder->created_at));
		$uom = explode('-', $item['uom'])[0];

		return
			'<details>'. 
				'<comp_code>'.$this->comp_code.'</comp_code>'.
				'<bom_id>0</bom_id>'.
				'<s_no>'.$count.'</s_no>'.
				'<item_id>'.$item['itemid'].'</item_id>'. 
				'<item_no>'.$item['itemno'].'</item_no>'. 
				'<qty>'.$item['qty'].'</qty>'.
				'<uom>'.$uom.'</uom>'.
				'<uom_cf>'.$item['uomcf'].'</uom_cf>'.
				'<bom_type>'.$item['type'].'</bom_type>'.
			'</details>';		
	}

	//generate the xml for item master
	public function generateOrderMasterXML($bomorder, $itemxml, $itemid, $itemnum){
		//if ($bomorder['bom_iskittling'] == 'on')?$isKitting = 1: $isKitting = 0;
		//echo 'ITNO:'.$itemnum;
		return
			'<?xml version="1.0" encoding="UTF-8"?>'.
			'<plist version="1.0">'.
			'<array>'.
				'<master>'.
					'<comp_code>'.$this->comp_code.'</comp_code>'. //need to include compcode in orders form ....
					'<bom_id>0</bom_id>'.
					'<bom_code>'.$bomorder['bom_code'].'</bom_code>'.
					'<revision_no>1</revision_no>'.
					'<descr>'.$bomorder['bom_descr'].'</descr>'.
					'<item_id>'.$itemid.'</item_id>'.
					'<item_no>'.$itemnum.'</item_no>'.
					'<qty>'.$bomorder['bom_itemqty'].'</qty>'.
					'<uom>'.explode('-',$bomorder['bom_itemuom'])[0].'</uom>'.
					'<uom_cf>'.$bomorder['bom_itemconv'].'</uom_cf>'.
					'<status>1</status>'.
					'<is_kitting_bom>'.$bomorder['bom_iskittling'].'</is_kitting_bom>'.
					'<remarks>'.$bomorder['bom_remark'].'</remarks>'.
					'<approved>1</approved>'.
				'</master>'.
				$itemxml .
			'</array>'.
			'</plist>';	
	}


	#Show BOM ---------------------------------------------------------------------------------
	public function show($bomid){
		//display the information about the BOM
		if(Session::get('access') == 'Y' && Session::get('app_type') == 'bom'){
			//create new BOM 
			//get all sub category

			//get all items
			$access_right = array(
				'add'=>Session::get('access_add'),
				'upd'=>Session::get('access_upd'),
				'del'=>Session::get('access_del'),
				'view'=>Session::get('access_view')
			);
			
			$bomdetails = explode('_', $bomid);
			$bom_infos = array(
							'bomid'			=> $bomdetails[0],
							'bomcode'		=> $bomdetails[1],
							'bomrevno'		=> $bomdetails[2],
							'bomdesc'		=> $bomdetails[3],
							'bomitemid' 	=> $bomdetails[4],
							'bomitemno'		=> $bomdetails[5],
							'bomqty'		=> $bomdetails[6],
							'bomuom' 		=> $bomdetails[7],
							'bomuomcf' 		=> $bomdetails[8],
							'bomkitting' 	=> $bomdetails[9],
							'bomstatus'		=> $bomdetails[10],
							'bomapproved'	=> $bomdetails[11],
							'bomremark'		=> $bomdetails[12],
							'bomcuserid'	=> $bomdetails[13],
							'bomcdate'		=> $bomdetails[14],
							'bommuserid'	=> $bomdetails[15],
							'bommdate' 		=> $bomdetails[16]
						);


			$url = 'http://'.Session::get('server_info').'web_BOM_Details?compcode=' . $this->comp_code . '&bomid=' . $bom_infos['bomid'];
			$json_string = file_get_contents($url);
			$bom_itemdetails = json_decode($json_string);

			return View::make('bom.show')
				->with('acc_right', $access_right)
				->with('bominfo', $bom_infos)
				->with('bominforaw', $bomid)
				->with('bomitemdetails', $bom_itemdetails)
				->with('username', Session::get('userid'));				
		}

		else{
			return Redirect::to('/sdbom/login')
				->with('message', 'Please Login to the System');
		}

	}

	#EDIT BOM -----------------------------------------------------------------------------
	public function edit($bomcode){
		//show the edit bom form
		if(Session::get('access') == 'Y' && Session::get('app_type') == 'bom'){
			$bom_items = $this->getResponse('http://'.Session::get('server_info'). 'web_get_sub_cat?compcode='.$this->comp_code);

			//get all items
			$access_right = array(
				'add'=>Session::get('access_add'),
				'upd'=>Session::get('access_upd'),
				'del'=>Session::get('access_del'),
				'view'=>Session::get('access_view')
			);

			$sub_cat = $this->getResponse('http://'.Session::get('server_info'). 'web_get_sub_cat?compcode='.$this->comp_code);

			return View::make('bom.edit')
				->with('acc_right', $access_right)
				->with('sub_cats', $sub_cat)
				->with('username', Session::get('userid'));
		}
		else{
			return Redirect::to('/sdbom/login')
				->with('message', 'Please Login to the System');
		}		
	}

	public function update(){
		//process the update from edit form
	}

	public function delete(){
		//process the delete BOM function
		//to be completed
	}


	//function to obtained data give the API URI -------------------------------
	private function getResponse($apiurl){
		$json_string = file_get_contents($apiurl);
		return json_decode($json_string);
	}

	#Display the Login form
	public function showLogin(){
		
		//$session = Session::all();
		if (Session::get('access') == 'Y'){
			if(Session::get('app_type') !== 'bom') {
				Session::flush();
				return View::make('bom.login')->with('message', 'Errror');		
			}
			return Redirect::to('sdbom/bom');
		}
		else{
			return View::make('bom.login');	
		}		
	}

	#Do the login procedure and save all the user information into the session.
	public function doLogin(){
		//process the login form
		$post = Input::all();
		
		$content = $this->getResponse('http://'. $this->getServerInfos() . 'web_user_login?compcode='
			.$this->comp_code . '&userid=' . $post['username'] . '&pwd=' . $post['password']);
		$compinfo = $this->getResponse('http://'.$this->getServerInfos() . 'web_getcompany?compcode='
			.$this->comp_code);
		$access_right = $this->getResponse('http://'.$this->getServerInfos() . 'web_access_rights?compcode='
			.$this->comp_code . '&userid=' . $post['username']);
		
		if($content[0]->access == 'Y'){
			Session::put('access', $content[0]->access);
			Session::put('userid', $post['username']);
			//Session::put('pwd', $post['password']);
			
			Session::put('compcode', $compinfo[0]->comp_code);
			Session::put('compname', $compinfo[0]->name);
			Session::put('access_add', $access_right[0]->add_access);
			Session::put('access_upd', $access_right[0]->upd_access);
			Session::put('access_del', $access_right[0]->del_access);
			Session::put('access_view', $access_right[0]->view_access);
			Session::put('app_type', 'bom');
			Session::put('server_info', $this->getServerInfos() );

			return Redirect::intended('sdbom/bom');

		}
		else{
			$errmsg = $this->createAlert('Login Failed, Please Checked your credential.', 'error');
			return Redirect::to('/sdbom/login')->with('message', $errmsg);
		}
	}

	#logout function, clear cache
	public function doLogout(){
		Session::flush();
		return Redirect::to('sdbom/login');
	}


	//create alert message.
	private function createAlert($message, $type){
		if($type == 'success'){
			$return = 
				'<div class="alert alert-success alert-dismissable bom-message">' .
			   	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
			   	$message .
				'</div>';
			return $return;
		}
		else if($type == 'error'){
			$return = 
				'<div class="alert alert-danger alert-dismissable bom-message">' .
			   	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
			   	$message .
				'</div>';
			return $return;	
		}
	}

	//get server informations
	private function getServerInfos(){
		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;
		$server = $server_username . ':' . $server_password . '@' . $url_server . '/konvergefb/';
		return $server;
	}

	//function for sending XML by CURL
	public function sendByCurl($post_field, $url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		//curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
		
		//Execute the request and also time the transaction ( optional )
		$start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		
		$responseInfo = curl_getinfo($ch);
		$httpResponseCode = $responseInfo['http_code'];
		//echo $responseInfo;

		$stop = array_sum(explode(' ', microtime()));
		$totalTime = $stop - $start;
		//echo $result; 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    //print_r(curl_getinfo($ch)); 

		    switch($returnCode){
		        case 200:
		        	echo 'success';
		        	echo $result;
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            echo 'failed';
		            
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);
		 
		//Output the results and time
		echo 'Total time for request: ' . $totalTime . "\n";
		echo $result;
		//return $result;
		$json = json_decode($result, true);
		return $json[0]['retvalue']; 
	}	


}