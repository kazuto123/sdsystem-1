<?php

class UserTorderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($user)
	{
		$search = Request::get('q'); //query string
		$loggeduser = User::where('id', '=', $user)->first();

		//Input::get('filterby');

		if($loggeduser){
			if($loggeduser->staff){
				$staff_id = $loggeduser->id;
				//search query builder
				$orderlist = $search 
					? DB::table('torders')
						->where('torder_from', '=', $staff_id)
						->whereNull('deleted_at')
						->where(function($query) use ($search){
							$query->where('torder_to', 'LIKE', '%'.$search.'%')
								  ->orwhere('cust_name', 'LIKE', '%'.$search.'%')
								  ->orwhere('delivery_addr_text', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_driver', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_date', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_time', 'LIKE', '%'.$search.'%');
						})
						->orderBy('created_at', 'desc')
						->paginate(10)	
					: TOrder::where('torder_from', '=', $staff_id)
							->orderBy('created_at', 'desc')
							->paginate(10);

			}
			else if($loggeduser->customer){
				$cust_no = $loggeduser->customer->cust_no;
				$user_id = $loggeduser->id;

				//search query builder ...
				$orderlist = $search
					? DB::table('torders')
						->where('torder_to', '=', $cust_no)
						->whereNull('deleted_at')
						->where(function($query) use ($search){
							$query->where('torder_to', 'LIKE', '%'.$search.'%')
								  ->orwhere('cust_name', 'LIKE', '%'.$search.'%')
								  ->orwhere('delivery_addr_text', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_driver', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_date', 'LIKE', '%'.$search.'%')
								  ->orWhere('delivery_time', 'LIKE', '%'.$search.'%');
						})
						->orderBy('created_at', 'desc')
						->paginate(10)

					: TOrder::where('torder_to', '=', $cust_no)
					->orWhere('torder_from', '=', $user_id)
					->orderBy('created_at', 'desc')
					->paginate(10);
				
			}
			else{
				return Redirect::action('UserController@showDashboard');
			}
		}

		$filterby = Configs::where('type', '=', 'orderlistfilter')->get();
		
		
		// if($query){
		// 	$orderlist = TOrder::where('cust_name', 'LIKE', "%$query%")->get();
		// }

		return View::make('torders.index')
			->with('allorder', $orderlist)
			->with('userid', $user)
			->with('filterby', $filterby);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($user)
	{
		$cats = Category::all();
		$items = Item::all();
		$users = User::find($user);
		$timeslots = Configs::where('type', '=', 'deliverytimeslot')->get();
		
		$drivers = Staff::where('issalesman', '=', 1)->get();
		foreach($drivers as $driver){
			$driver_users[] = $driver->user->username;
		}

		$conf_cgrp = Configs::where('type', '=', 'cgroup')->get();
		foreach($conf_cgrp as $cgrp){
			$cust_grp[] = array($cgrp->value, $cgrp->descr);
		}

		//get all currency code
		$all_curr_codes = Configs::where('type', '=', 'currency')->get();
		foreach($all_curr_codes as $codes){
			#get all currency exchange rate	
			$curr_codes [] = array('curr'=>$codes);
		}

		#get all the outlet code of client
		$outlets = Customer::where('comp_code', '=', '01')->get();
		foreach($outlets as $outlt){
			$outlets_code[] = $outlt->cust_no; 
		}

		//return $cust_grp;
		//Create Order
		return View::make('torders.create')
		 	->with('cust_grp', $cust_grp)
		 	->with('cats', $cats)
		 	->with('items', $items)
		 	->with('curr_codes', $curr_codes)
		 	->with('drivers', $driver_users)
		 	->with('user', $users)
		 	->with('userid', $user)
		 	->with('timeslots', $timeslots)
		 	->with('outlets', $outlets_code);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$compcode = Configs::where('type', '=', 'compcode')->first()->value;
		
		//get the SO type from global config
		

		//returning data
		//return Input::all();
		$input = Input::all();
		$orderdetail = [];
		$itemdetail = [];
		
		$odform = [];
		$oditem = [];
		
		//process the input form.
		foreach($input as $key=>$value){
			$isItem = explode("_", $key);
			if($isItem[0] == 'oditem'){
				//echo $isItem[1] . ',' . $isItem[2];
				$itemdetail[$isItem[2]] = $value;
				$oditem[$isItem[1]] = $itemdetail;
				//$item['items'] = $oditem;
			}
			else{
				$orderdetail[$key] = $value;
				$odform[0] = $orderdetail;
				//$order['order'] = $odform;
			}
		}

		//customer infos related
		$customer_info = Customer::where('cust_no', '=', $odform[0]['outlet_code'])->first();
		$cust_tax_type = $customer_info->cust_tax_type;
		$cust_absorb_tax = $customer_info->cust_absorb_tax;
		$custaddr = Customeraddr::where('cust_no', '=', $odform[0]['outlet_code'])
								->where('addr_ref', '=', $odform[0]['delivery_addr'])
								->first();

		$so_type = DB::table('configs')->where('type', '=', 'sys_param')
									->where('key', '=', 'web_so_subtype')
									->first();

		#get tax information fro global config		
		$tax_rate = DB::table('configs')->where('type', '=', 'gst_rate')
										->where('key', '=', $cust_tax_type)
										->first();

		
		//$data = [$odform, $oditem];
		#insert the order into database

		$torder = new TOrder();
		$torder->comp_code 			= $compcode;
		$torder->cust_grp 			= $odform[0]['cust_group'];
		$torder->torder_from		= $odform[0]['order_from'];
		$torder->torder_to	 		= $odform[0]['outlet_code'];
		$torder->cust_name 			= $odform[0]['cust_name'];
		$torder->cont_person 		= $odform[0]['cont_person'];
		$torder->curr_code          = $odform[0]['curr_code'];
		$torder->curr_rate          = $odform[0]['curr_rate'];
		$torder->cont_no  			= $odform[0]['cont_no'];
		$torder->absorb_tax 		= $cust_absorb_tax;
		$torder->tax_type 			= $cust_tax_type;
		$torder->tax_value			= $tax_rate->value;
		$torder->delivery_addr 		= $odform[0]['delivery_addr'];
		$torder->delivery_addr_text = $custaddr->addr. ' zip: ' . $custaddr->zip;
		$torder->delivery_driver	= $odform[0]['delivery_driver'];
		$torder->delivery_date  	= $odform[0]['delivery_date'];
		$torder->delivery_time  	= $odform[0]['delivery_time'];
		$torder->subtotal			= $odform[0]['order_subtotal'];
		$torder->gst 				= $odform[0]['order_gst'];
		$torder->total 				= $odform[0]['order_total'];
		$torder->remark 			= $odform[0]['order_remark'];
		$save = $torder->save();

		$torderid = $torder->id;
		//return $torder;
		
		//owner of the SO related infos
		$owneruser = User::where('id', '=', $odform[0]['order_from'])->first();
		$user_name = $owneruser->username;

		//customer infos related
		$customer_info = Customer::where('cust_no', '=', $odform[0]['outlet_code'])->first();
		$cust_tax_type = $customer_info->cust_tax_type;
		$cust_absorb_tax = $customer_info->cust_absorb_tax;

		//get all running number
		$running_no = Configs::where('type', '=', 'to_counts')
									->first();
		$prev_running_no = $running_no->value;
		$curr_running_no = $prev_running_no + 1;
		$running_no->value = $curr_running_no;
		$running_no->save();
		
		//date use to store the xml files
		$torderdate = date('Y/m/d', strtotime($torder->created_at));
		$torderdate2 = date('ymd', strtotime($torder->created_at));
		$torderdate_sor = date('ym', strtotime($torder->created_at));
		
		#create a doc no
		$doc_no = 'SOR-'.$so_type->value.$torderdate_sor.str_pad($curr_running_no, 5, '0', STR_PAD_LEFT);
		$torder->doc_no = $doc_no;
		$torder->save();
		
		#insert item related to the order into the database
		$toitemxml = '';
		$count = 1; //sn_no count
		foreach($oditem as $item){
			echo $item['total'];
			$tax_amount = ($tax_rate->value / 100) * $item['total'];

			$odi = new TOrderedItem();
			$odi->comp_code = $compcode;
			$odi->item_no = $item['itemno'];
			$odi->item_id = $item['itemid'];
			$odi->item_desc = $item['descr'];
			$odi->torder_no = $torderid;
			$odi->qty = $item['qty'];
			$odi->uom = $item['uom'];
			$odi->uom_cf = $item['uomcf']; 
			$odi->gst = $tax_amount;
			$odi->unitprice = $item['unitprice'];
			$odi->totalamt = $item['total'];
					
			$itemsave = $odi->save();
			//if($itemsave){ }
			//else{ dd($odi->errors()); }

			$toitemxml .= $this->generateItemDetailsXML($odi, $torder, $count);
			$count += 1; //s_no count
		}

		$to_xml = $this->generateOrderMasterXML($torder, $toitemxml);



		$order_xml = new OrderXml();
		$order_xml->type 			= 'to';
		$order_xml->xml_name 		= $doc_no;
		$order_xml->xml_contents 	= $to_xml;
		$order_xml->save();

		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;
		$server_url=$url_server.'/konvergefb/web_createSO';

		$url = 'http://'.$server_username.':'.$server_password.'@'.$server_url;

		$post = 'compcode='.$torder->comp_code.'&docno='. $doc_no. '&docdate=' . $torderdate . '&xmlfile='.$to_xml . '&userid='.$user_name;
		$status_code = $this->sendByCurl($post, $url);

		$order_xml->send_status = $status_code;
		$order_xml->save();

//		return "";

		//echo $odform[0]['cust_grp'];
		if($save){
			return Redirect::to('/user/'. $odform[0]['order_from'] . '/torder')->with('message', 'New Order has been added');
		} else{
			Redirect::back()->withInput()
							->with('errors', $torder->errors());
		}

	}

	protected function generateItemDetailsXML($item, $torder, $sno_count){
		return 
			'<details>'.
				'<comp_code>'.$torder->comp_code.'</comp_code>'.
				'<doc_no>'.$torder->doc_no.'</doc_no>'.
				'<s_no>'.$sno_count.'</s_no>'.
				'<item_id>'.$item->item_id.'</item_id>'.
				'<item_no>'.$item->item_no.'</item_no>'.
				'<item_descr>'.$item->item_desc.'</item_descr>'.
				'<cust_part_no>'.$item->item_no.'</cust_part_no>'. //need to change
				'<uom>'.$item->uom.'</uom>'.
				'<uom_cf>'.$item->uom_cf.'</uom_cf>'.
				'<qty_order>'.$item->qty.'</qty_order>'.
				'<price>'.$item->unitprice.'</price>'.
				'<total>'.$item->totalamt.'</total>'.
				'<disc_type>0</disc_type>'.
				'<disc_value>0</disc_value>'.
				'<disc_amount>0</disc_amount>'.
				'<net_amount>'.$item->totalamt.'</net_amount>'.
				'<tax_type>'.$torder->tax_type.'</tax_type>'.
				'<tax_rate>'.$torder->tax_value.'</tax_rate>'.
				'<tax_amt>'.$item->gst.'</tax_amt>'.
				'<so_etd>'.$torder->delivery_date.'</so_etd>'.
				'<so_eta>'.$torder->delivery_date.'</so_eta>'.
			'</details>';
	}

	protected function generateOrderMasterXML($torder, $itemdetailXML){	
		//process the dates
		$ac_period = date('Ym', strtotime($torder->created_at));
		$date_1 = date('Y/m/d', strtotime($torder->created_at));

		return
		'<?xml version="1.0" encoding="UTF-8"?>'.
		'<plist version="1.0">'.
		'<array>'.
			'<master>'.
				'<comp_code>'.$torder->comp_code.'</comp_code>'. //hardcode
				'<doc_no>'. $torder->doc_no.'</doc_no>'.
				'<doc_date>'.$date_1.'</doc_date>'.
				'<cust_no>'.$torder->torder_to.'</cust_no>'.
				'<ac_period>'.$ac_period.'</ac_period>'. //hardcode
				'<cust_po_no>-</cust_po_no>'.
				'<cust_po_date>'.$date_1.'</cust_po_date>'.
				'<cont_name>'.$torder->cont_person.'</cont_name>'.
				'<ship_addr_ref>'.$torder->delivery_addr.'</ship_addr_ref>'. //hardcode
				'<salesman>'.$torder->delivery_driver.'</salesman>'.
				'<curr_code>'.$torder->curr_code.'</curr_code>'.
				'<exch_rate>'.$torder->curr_rate.'</exch_rate>'. //hardcoded
				'<absorb_tax>'.$torder->absorb_tax.'</absorb_tax>'. //needed - currently hardcoded
				'<tax_amt>'.$torder->gst.'</tax_amt>'.
				'<disc_amt>0</disc_amt>'.
				'<total>'.$torder->subtotal.'</total>'.
				'<so_status>N</so_status>'.
				'<remarks>'.$torder->remark.'</remarks> '.
			'</master>'.
			
			$itemdetailXML.
			
			'</array>'.
			'</plist>';
	}

	public function sendByCurl($post_field, $url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		//curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
		
		//Execute the request and also time the transaction ( optional )
		$start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		
		//$responseInfo = curl_getinfo($ch);
		//$httpResponseCode = $responseInfo['http_code'];
		//echo $responseInfo;

		$stop = array_sum(explode(' ', microtime()));
		$totalTime = $stop - $start;
		//echo $result; 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    echo $returnCode;

		    switch($returnCode){
		        case 200:
		        	echo 'success:';
		        	echo $result;
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            echo 'failed';
		            echo $result;
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);
		 
		//Output the results and time
		echo 'Total time for request: ' . $totalTime . "\n";
		echo $result;
		$json = json_decode($result, true);
		return $json[0]['ReturnValue']; 

	}	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($user, $torder)
	{
		//$orderget = User::find($user)->torder->find($torder);
		//$orderitems = $orderget->ordereditem->toArray();
		$orderget = TOrder::where('id', '=', $torder)->first();
		$orderitems = $orderget->ordereditem->toArray();
		$torderitems = [];
		foreach($orderitems as $odi){
			$itemdetail = Item::where('item_no', '=', $odi['item_no'])->first();	
			$torderitems[] = ['item'=>$odi, 'descr'=>$itemdetail->item_descr];
			//$odi['descr'] = $itemdetail->item_descr;
		}

		//$test = $torderitems['item'][0]['id'];
		//$test = $torderitems['descr'];
		return View::make('torders.show')
			->with('torder', $orderget)
			->with('torder_items', $torderitems)
			->with('userid', $user)
			->with('torderid', $torder);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($user,$id)
	{
		//get timeslots
		$timeslots = Configs::where('type', '=', 'deliverytimeslot')->get();

		$drivers = Staff::where('issalesman', '=', 1)->get();
		foreach($drivers as $driver){
			$driver_users[] = $driver->user->username;
		}

		//get all the category
		$cats = Category::all();

		$items = Item::all();

		//get customer group from the config table
		$cust_grp = [];
		$conf_cgrp = Configs::where('type', '=', 'cgroup')->get();
		foreach($conf_cgrp as $cgrp){ $cust_grp[] = $cgrp->value; }

		#show the order details
		//retrieve all the Order detail
		$torder = TOrder::where('id', '=', $id)->first();

		//return $torder;
		//get all ordered item
		$tordereditems = $torder->ordereditem;
		$to_items = [];
		
		foreach ($tordereditems as $toi){
			//echo $toi;
			$itemuom = Uom::where('item_id', '=', $toi->item_id)->get()->toArray();
			//echo $itemuom;
			$to_items[] = array('details'=>$toi->toArray(), 'uom'=>$itemuom);
		}

		#find all the delivery addr related to the customer in the order form
		$addr = [];

		$customer = Customer::where('cust_no', '=', $torder->torder_to)->first();
		$customer_addr = $customer->address;
		foreach($customer_addr as $address){ $addr[] = $address; }
		//get outlet detail from customer table
		
		$alloutlets = Customer::where('comp_code', '=', '01')
							->where('cust_group_code', '=', $torder->cust_grp)
							->get();

		foreach($alloutlets as $outlt){ 
			$outlets[] = array(
				'code'=>$outlt->cust_no,
				'name'=>$outlt->cust_name
				); 
		}

		//get all currency code
		$all_curr_codes = Configs::where('type', '=', 'currency')->get();
		foreach($all_curr_codes as $codes){
			#get all currency exchange rate	
			$curr_codes [] = array('curr'=>$codes);
		}

		//retrieve all the item belongs to that order and list then out.
		return View::make('torders.edit')
				->with('user', $user)
				->with('torderid', $id)
				->with('cats', $cats)
				->with('curr_codes', $curr_codes)
				->with('drivers', $driver_users)
				->with('items', $items)
				->with('timeslots', $timeslots)
				->with('orderdetail', $torder)
				->with('ordereditems', $to_items)
				->with('cust_grp', $cust_grp)
				->with('outlets', $outlets)
				->with('address', $addr);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($user, $id)
	{
		// similar to store, update the editted field to database
		$input = Input::all();
		$orderdetail = [];
		$itemdetail = [];
		
		$old_itemlist = [];
		$new_itemlist = [];

		$odform = [];
		$oditem = [];
		foreach($input as $key=>$value){
			$isItem = explode("_", $key);
			if($isItem[0] == 'oditem'){
				//echo $isItem[1] . ',' . $isItem[2];
				$itemdetail[$isItem[2]] = $value;
				$oditem[$isItem[1]] = $itemdetail;
				//$item['items'] = $oditem;
			}
			else{
				$orderdetail[$key] = $value;
				$odform[0] = $orderdetail;
				//$order['order'] = $odform;
			}
		}
			
		$torder = TOrder::where('id', '=', $odform[0]['order_id'])->first();
		$torder->cust_grp 			= $odform[0]['cust_group'];
		$torder->torder_from		= $odform[0]['order_from'];
		$torder->torder_to	 		= $odform[0]['outlet_code'];
		$torder->cont_person 		= $odform[0]['cont_person'];
		$torder->delivery_addr 		= $odform[0]['delivery_addr'];
		$torder->delivery_driver	= $odform[0]['delivery_driver'];
		$torder->delivery_date 		= $odform[0]['delivery_date'];
		$torder->delivery_time 		= $odform[0]['delivery_time'];
		$torder->subtotal			= $odform[0]['order_subtotal'];
		$torder->gst 				= $odform[0]['order_gst'];
		$torder->total 				= $odform[0]['order_total'];
		$save = $torder->save();

		//return ($torder->errors());

		$torderid = $torder->id;

		$ordered_items = TOrderedItem::where('torder_no', '=', $torderid)->get();
		foreach ($ordered_items as $items){
			$old_itemlist [] = $items->item_id;
		}


		foreach ($oditem as $item){
			$new_itemlist [] = $item['itemid'];
		}

		$tobe_del = array_diff($old_itemlist, $new_itemlist);
		$tobe_add = array_diff($new_itemlist, $old_itemlist);
		$tobe_upd = array_intersect($new_itemlist, $old_itemlist);

		//return [$tobe_del, $tobe_add, $tobe_upd];
		
		foreach ($tobe_add as $itemno){
			$add 			= new TOrderedItem();
			$add->item_no 	= Input::get('oditem_'.$itemno.'_itemno');
			$add->item_id	= Input::get('oditem_'.$itemno.'_itemid');
			$add->torder_no = $torderid;
			$add->qty 		= Input::get('oditem_'.$itemno.'_qty');
			$add->uom 		= Input::get('oditem_'.$itemno.'_uom'); 
			$add->uom_cf 	= Input::get('oditem_'.$itemno.'_uomcf'); 
			$add->unitprice = Input::get('oditem_'.$itemno.'_unitprice');
			$add->totalamt 	= Input::get('oditem_'.$itemno.'_total');		
			$add->save();
			// echo "Add_array:";
			// echo json_encode($tobe_add);
			// echo 'ADD:';
			// echo $add;
		}

		foreach ($tobe_upd as $itemno){
			//echo 'tobe update:';
			$upd = TOrderedItem::where('item_id', '=', $itemno)
						->where('torder_no', '=', $torderid)
						->first();			 
			$upd->item_no 	= Input::get('oditem_'.$itemno.'_itemno');
			$upd->qty 		= Input::get('oditem_'.$itemno.'_qty');
			$upd->uom 		= Input::get('oditem_'.$itemno.'_uom'); 
			$upd->unitprice = Input::get('oditem_'.$itemno.'_unitprice');
			$upd->totalamt 	= Input::get('oditem_'.$itemno.'_total');
			$upd->save();
			// echo "upd_array:";
			// echo json_encode($tobe_upd);
			// echo "UPD:";
			//echo $upd;
		}

		foreach ($tobe_del as $itemno){
			//echo 'tobe del:';
			$delete = TOrderedItem::where('item_no', '=', $itemno)->where('torder_no', '=', $torderid)->first();			 
			$delete->delete();
			//echo "DEL:";
			//echo $delete;
		}

		//generate the XML and send to Solution details core
		$updatedTOrder = TOrder::where('id', '=', $torderid)->first();

		$updatedTOitems = TOrderedItem::where('torder_no', '=', $torderid)->get();
		$count = 1;
		$updxml = '';
		foreach($updatedTOitems as $upditem){
			$updxml .= $this->generateItemDetailsXML($upditem, $updatedTOrder, $count);
			$count += 1;
		}

		$updatedPOrderPost = $this->generateOrderMasterXML($updatedTOrder, $updxml);

		$owneruser = User::where('id', '=', $odform[0]['order_from'])->first();
		$user_name = $owneruser->username;

		$torderdate = date('Y/m/d', strtotime($updatedTOrder->created_at));
		$order_xml = new OrderXml();
		$order_xml->type 			= 'toc';
		$order_xml->xml_name 		= $updatedTOrder->doc_no;
		$order_xml->xml_contents 	= $updatedPOrderPost;
		$order_xml->save();

		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;
		$server_url=$url_server.'/konvergefb/web_createSO';

		$url 		 = 'http://'.$server_username.':'.$server_password.'@'.$server_url;
		$post 		 = 'compcode='.$updatedTOrder->comp_code.'&docno='. $updatedTOrder->doc_no. '&docdate=' . $torderdate . '&xmlfile='.$updatedPOrderPost . '&userid='.$user_name;
		$status_code = $this->sendByCurl($post, $url);

		$order_xml->send_status = $status_code;
		$order_xml->save();

		//return '';
		return Redirect::to('/user/'. $odform[0]['order_from'] . '/torder');
		//return "finish";

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($user, $id)
	{
		//delete order record
		$record = TOrder::where('id', '=', $id);
		$record->delete();

		//Session::flash('message', 'successfully deleted the order!');
		return Redirect::to('/user/'. $user . '/torder');
	}

}