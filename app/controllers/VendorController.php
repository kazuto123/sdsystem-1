<?php

class VendorController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//get vendor details
		//return $id;
		$vendors = Vendor::where('vend_no', '=', $id)->first()->toArray();
		//get user currency rate 
		$curr_rate = CurrExchRate::where('curr_code', '=', $vendors['curr_code'])
								->orderBy('ac_period', 'desc')->first()->toArray();

        $vendor_presets = PresetItem::where('type', '=', 'V')
        							->where('preset_by','=', $id)->get()->toArray();

        $last_order = POrder::where('vend_no', '=', $id)->orderBy('created_at', 'desc')->first();
        $last_orderid = "";
        if($last_order){
        	$last_orderid = $last_order->id;
        }
        //return $last_orderid;
		//return $vendor_presets;
		foreach($vendor_presets as $vpreset){
			//$test [] = $vpreset;
			$itemid = Item::where('item_no', '=', $vpreset['item_no'])->first()->id;
			$prev_price = '';
			if($last_orderid != ""){
				$prev_item = POrderedItem::where('item_id', '=', $vpreset['item_no'])
											->where('porder_id', '=', $last_orderid)->first();
				if($prev_item){
					$prev_price = $prev_item->unitprice;
				}
			}



			$uom = Uom::where('item_no', '=', $vpreset['item_no'])->get()->toArray();
			$presets [] = array('itemid'=>$itemid,'item_detail'=>$vpreset, 'uom'=>$uom, 'prev_price'=>$prev_price);
		}
		
		
		return Response::json(['vendors'=>$vendors, 'curr_rate'=>$curr_rate, 'presets'=>$presets]); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getVendorByGrp($grp){
		$vendors = Vendor::where('vend_grp_code', '=', $grp)->get()->toArray();

		return Response::json(['vendors'=>$vendors]); 
	}

	public function getVendorItems($vendno){
		
		//return Response::json(['vendors'=>$vendors]);	
	}

}