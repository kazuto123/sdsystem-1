<?php

class CustomerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$cust = Customer::where('cust_no', '=', $id)->get()->toArray();
		$addr = Customer::find($id)->address->toArray();
		$preset = PresetItem::where('preset_by', '=', $id)
					->where('type', '=', 'C')->get()->toArray();
		
		//dd($data);
		return Response::json(['cust'=>$cust, 'addr'=>$addr, 'preset'=>$preset]); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getCustomerCodeByGroup($cgrp){
		$custno = [];
		$all_cust = Customer::where('cust_group_code', '=', $cgrp)->get();
		foreach($all_cust as $cust){
			$custno[] = $cust->cust_no;
		}

		return Response::json($all_cust);
	}

	public function getCustomerInfo($custno){
				//
		$cust = Customer::where('cust_no', '=', $custno)->first()->toArray();
		$addr = Customer::find($custno)->address->toArray();
		$presets = PresetItem::where('preset_by', '=', $custno)
					->where('type', '=', 'C')->get()->toArray();
		$preset_items = [];
		foreach($presets as $preset){
			$uom = Uom::where('item_no', '=', $preset['item_no'])->get()->toArray();
			$preset_items [] = array($preset, 'uom_list'=>$uom);
		}

		$curr_rate = CurrExchRate::where('curr_code', '=', $cust['cust_curr_code'])
							->orderBy('ac_period', 'desc')->first()->toArray();
			
		//dd($data);
		return Response::json(['cust'=>$cust, 'addr'=>$addr, 'preset'=>$preset_items, 'curr_rate'=>$curr_rate]); 
	} 

}