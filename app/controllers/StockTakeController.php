<?php

class StockTakeController extends \BaseController {

	protected $comp_code = '01';

	private function getCompCode(){
		$compcode = Configs::where('type', '=', 'compcode')->first()->value;
		return $compcode;
	}
	
	//function to obtained data give the API URI -------------------------------
	private function getResponse($apiurl){
		$json_string = file_get_contents($apiurl);
		return json_decode($json_string);
	}

	private function getServerInfos(){
		$server_username=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_username')
								->first()->value;
		$server_password=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_password')
								->first()->value;
		$url_server=Configs::where('type', '=', 'server_settings')
								->where('key', '=', 'server_url')
								->first()->value;
		$server = $server_username . ':' . $server_password . '@' . $url_server . '/konvergefb/';
		return $server;
	}	

	private function createAlert($message, $type){
		if($type == 'success'){
			$return = 
				'<div class="alert alert-success alert-dismissable bom-message">' .
			   	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
			   	$message .
				'</div>';
			return $return;
		}
		else if($type == 'error'){
			$return = 
				'<div class="alert alert-danger alert-dismissable bom-message">' .
			   	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' .
			   	$message .
				'</div>';
			return $return;			
		}
	}

	#Display the Login form
	public function showLogin(){
		if(Session::get('app_type') != 'sto') Session::flush();
		//$session = Session::all();
		if (Session::get('access') == 'Y'){
			return Redirect::to('sdstock/warehouse');
		}
		else{
			return View::make('stock.login')->with('message', '');	
		}		
	}

	#process the login
	#Do the login procedure and save all the user information into the session.
	public function doLogin(){
		//process the login form
		$post = Input::all();
		
		$content = $this->getResponse('http://'. $this->getServerInfos() . 'web_user_login?compcode='
			.$this->comp_code . '&userid=' . $post['username'] . '&pwd=' . $post['password']);
		$compinfo = $this->getResponse('http://'.$this->getServerInfos() . 'web_getcompany?compcode='
			.$this->comp_code);
		$access_right = $this->getResponse('http://'.$this->getServerInfos() . 'web_access_rights?compcode='
			.$this->comp_code . '&userid=' . $post['username']);
		
		if($content[0]->access == 'Y'){
			Session::put('access', $content[0]->access);
			Session::put('userid', $post['username']);
			//Session::put('pwd', $post['password']);
			
			Session::put('compcode', $compinfo[0]->comp_code);
			Session::put('compname', $compinfo[0]->name);
			Session::put('access_add', $access_right[0]->add_access);
			Session::put('access_upd', $access_right[0]->upd_access);
			Session::put('access_del', $access_right[0]->del_access);
			Session::put('access_view', $access_right[0]->view_access);
			Session::put('app_type', 'sto');
			Session::put('server_info', $this->getServerInfos());

			return Redirect::intended('sdstock/warehouse');

		}
		else{
			$errorMsg = $this->createAlert('Login not Successful. Please Check your credential.', 'error');
			return Redirect::to('sdstock/login')->with('message', $errorMsg);
		}
	}

	#logout function, clear cache
	public function doLogout(){
		Session::flush();
		return Redirect::to('sdstock/login');
	}

	public function showWarehouse(){
		if(Session::get('access') == 'Y' && Session::get('app_type') == 'sto'){
			//list all the BOM that belongs to the logged in user.
			$access_right = array(
				'add'=>Session::get('access_add'),
				'upd'=>Session::get('access_upd'),
				'del'=>Session::get('access_del'),
				'view'=>Session::get('access_view')
			);

			$all_warehouses = $this->getResponse('http://'.Session::get('server_info'). 'web_st_getwarehouse?compcode='
			.$this->comp_code);

			$username = Session::get('userid');
			return View::make('stock.index')
				->with('username', $username)
				->with('acc_right', $access_right)
				->with('warehouses', $all_warehouses)
				->with('message', '');
		}
		else{
			$errorMsg = $this->createAlert('You are not logging to the system. Please login.', 'error');
			return Redirect::to('sdstock/login')
				->with('message', $errorMsg);
		}
	}

	public function processWarehouse(){
		//return Input::all();

		$input = Input::all();
		foreach ($input as $key=>$value){
			$isItem = explode('_', $key);
			if($isItem[0] == 'stoitem'){
				$stoitemdetail[$isItem[2]] = $value;
				$stoitem[$isItem[1]] = $stoitemdetail;
			}
			else{
				$stodetail[$key] = $value;
				$stoform[0] = $stodetail;
			}
		}
		$stoxml = $this->createStoXml($stoitem, $this->comp_code, $stoform);

		//get the date
		date_default_timezone_set('Asia/Singapore');
		$sto_date = new DateTime($stoform[0]['stocktake_date']);
		//$timestamp = $d->getTimestamp();

		$trandate = $sto_date->format('Y/m/d');
		
		$wh_info = $stoform[0]['warehouse_name'];
		//[0]-id, [1]-number, [2]-description
		$wh_id = explode('-', $wh_info)[0];

		//send the XML by CURL
		$url = 'http://'.Session::get('server_info').'web_st_PutDetails';
		$post = 'compcode='.$this->comp_code.'&whid='.$wh_id.'&trandate='.$trandate . '&xmlfile='.$stoxml . '&userid=' . Session::get('userid');
		$status_code = $this->sendByCurl($post, $url);

		//stored a backup XML into the database
		$stoxml_log = new OrderXml();
		$stoxml_log->type 			= 'sto';
		$stoxml_log->xml_name 		= 'STOXML-'.$trandate;
		$stoxml_log->xml_contents	= $stoxml;
		$stoxml_log->save();

		//check the status code whether it passed. 
		if(isset($status_code)){
			if($status_code == 0){
				$successMsg = $this->createAlert('Successfully Updated the Stocks for Warehouse ID:' . $wh_id, 'success');
				$stoxml_log->send_status = $status_code;
				$stoxml_log->save();				
				return Redirect::to('/sdstock/warehouse')->with('message', $successMsg);					
			}
			else
			{
				$errorMsg = $this->createAlert('Unsuccess updating the Stocks due to: ' . $status_code, 'error');
				$stoxml_log->send_status = $status_code;
				$stoxml_log->save();				
				return Redirect::to('/sdstock/warehouse')->with('message', $errorMsg);					
			}

		}
		else{
			$errorMsg = $this->createAlert('The server Does not return the Status Code ' , 'error');
			return Redirect::to('/sdstock/warehouse')->with('message', $errorMsg);				
		}
		
	}

	// generate the xml for stocktake
	public function createStoXml($stocktake, $compcode, $stoform){
		$sto_xml = '';
		$wh_info = $stoform[0]['warehouse_name'];
		//[0]-id, [1]-number, [2]-description
		$wh_id = explode('-', $wh_info)[0];
		$count = 0;

		//return $wh_id; 
		foreach($stocktake as $sto){
			$count += 1;
			$sto_xml .= 
			'<stocktake>' .
				'<comp_code>' . $compcode . '</comp_code>' .
				'<wh_id>' . $wh_id . '</wh_id>' .
				'<doc_date>' . $stoform[0]['stocktake_date'] . '</doc_date>' .
				'<s_no>' . $count . '</s_no>' .
				'<item_id>' . $sto['itemid'] . '</item_id>' .
				'<item_no>' . $sto['itemno'] . '</item_no>' .
				'<uom>' . $sto['uom'] . '</uom>' .
				'<qty>' . $sto['qty'] . '</qty>' .
			'</stocktake>';
		}

		$resultXML = 
		'<?xml version="1.0" encoding="UTF-8"?>' .
		'<plist version="1.0">' .
		'<array>'. 
			$sto_xml .
		'</array>' .
		'</plist>';

		return $resultXML;			
	}

	//function for sending XML by CURL
	public function sendByCurl($post_field, $url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		//curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
		
		//Execute the request and also time the transaction ( optional )
		$start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		
		//$responseInfo = curl_getinfo($ch);
		//$httpResponseCode = $responseInfo['http_code'];
		//echo $responseInfo;

		$stop = array_sum(explode(' ', microtime()));
		$totalTime = $stop - $start;
		echo $result; 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    //print_r(curl_getinfo($ch)); 

		    switch($returnCode){
		        case 200:
		        	echo 'success';
		        	//echo $result;
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            echo 'failed';
		            break;
		    }
		}

		//Close the handle
		curl_close($ch);
		 
		//Output the results and time
		echo 'Total time for request: ' . $totalTime . "\n";
		echo $result;
		//return $result;
		$json = json_decode($result, true);
		return $json[0]['ReturnValue']; 
	}	



}