<?php namespace odtpf\Composers;	

use odtpf\Repositories\NavRepository;
//Presenter class or ViewModel class
class NavComposer{

	protected $nav;
	public function __construct(NavRepository $nav){
		$this->nav = $nav;
	}


	public function compose($view){
		$view->with('links', $this->nav->getAll() );
	}

}

/*
View::composer('includes.common._navbar', function($view) {
	$navs = App::make('odtpf\Repositories\NavRepository')->getAll();
	$view->with('links', $navs);
});
*/