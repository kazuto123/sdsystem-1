<?php namespace odtpf\Repositories;

class NavRepository{

	public function getAll(){
		$result =  ['OrderTaking'=>['label'=>'Order Taking', 'url'=>"order/index"], 
					'OrderPlacing'=>['label'=>'Order Placing', 'url'=>'/url'],
					'Forecasting'=>['label'=>'Forecasting', 'url'=>'/url']
				   ];
		return $result;
	}
}