<?php

use LaravelBook\Ardent\Ardent;

class PresetItem extends Ardent{
	protected $table = 'preset_items';
	protected $softDelete = true;

	public function item(){
		$this->belongsTo('Item');
	}

	public function user(){
		$this->belongsTo('User');
	}

	
}