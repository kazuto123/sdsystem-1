<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Confide\ConfideUser;
use Zizaco\Entrust\HasRole;

class User extends ConfideUser {
	use HasRole; //role permission traits in entrust
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	#protected $hidden = array('password');
	protected $fillable = array('email', 'password', 'username');
	public static $rules = [
		'email'=>'required|email',
		'username'=>'required'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function forecast(){
		//user has many forecast
		return $this->hasMany('Forecast', 'user_id');
	}

	public function torder(){
		//user has many torder
		return $this->hasMany('TOrder', 'torder_from');
	}

	public function porder(){
		//user has many porder
		return $this->hasMany('POrder', 'user_id');
	}

	public function preset_item(){
		//user has many preset_item
		return $this->hasMany('PresetItem', 'user_id');
	}

	public function log(){
		return $this->hasMany('Log', 'user_id');
	}

	public function customer(){
		return $this->hasOne('Customer', 'user_id');
	}

	public function vendor(){
		return $this->hasOne('Vendor', 'user_id');
	}
	
	public function staff(){
		return $this->hasOne('Staff', 'user_id');
	}

}