<?php

use LaravelBook\Ardent\Ardent;

class Item extends Ardent{
	protected $table = 'items';

	public function category(){
		return $this->belongsTo('Category');
	}

	public function uom(){
		return $this->hasMany('Uom', 'item_id');
	}

	public function forecasted_item(){
		return $this->hasOne('ForecastedItem', 'item_id');
	}

	public function tordered_item(){
		return $this->hasOne('TOrderedItem', 'item_no');
	}

	public function pordered_item(){
		return $this->hasOne('POrderedItem', 'item_id');
	}

	public function preseted_item(){
		return $this->hasOne('PresetItem', 'item_id');
	}


}