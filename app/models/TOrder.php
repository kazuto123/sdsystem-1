<?php
use LaravelBook\Ardent\Ardent;

class TOrder extends Eloquent{
	protected $table = 'torders';

	protected $softDelete = true;
	
	// public static  $rules = array(
	// 	#validation rules
	// 	'cust_grp'			=> 'Required',
	// 	'torder_from'		=> 'Required',
	// 	'torder_to' 		=> 'Required|integer',
	// 	'cont_person'		=> 'Required',
	// 	'delivery_addr'		=> 'Required',
	// 	'delivery_driver'	=> 'Required',
	// 	'delivery_date'		=> 'Required|date_format:Y-m-d',
	// 	'delivery_time'		=> 'Required',
	// 	'subtotal'			=> 'Required|numeric|min:0.01',
	// 	'gst'				=> 'Required|numeric|min:0.01',
	// 	'total'				=> 'Required|numeric|min:0.01'
	// );

	//relationship -----------------------------------------------------------
	public function user(){
		return $this->belongsTo('User');
	}

	public function ordereditem(){
		return $this->hasMany('TOrderedItem', 'torder_no');
	}
}