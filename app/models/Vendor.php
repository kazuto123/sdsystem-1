<?php

use LaravelBook\Ardent\Ardent;

class Vendor extends Ardent{
	protected $table = 'vendors';

	public function user(){
		return $this->belongsTo('User');
	}
}