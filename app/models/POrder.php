<?php
use LaravelBook\Ardent\Ardent;

class POrder extends Ardent{
	protected $table = 'porders';
	protected $softDelete = true;

	public function user(){
		return $this->belongsTo('User');
	}

	public function pordered_item(){
		return $this->hasMany('POrderedItem', 'itemid');
	}	
}