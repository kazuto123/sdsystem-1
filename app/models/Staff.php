<?php
use LaravelBook\Ardent\Ardent;

class Staff extends Ardent{
	protected $table = 'staffs';
	protected $softDelete = true;

	public function user(){
		return $this->belongsTo('User');
	}
}
