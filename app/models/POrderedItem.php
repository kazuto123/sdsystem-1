<?php
use LaravelBook\Ardent\Ardent;

class POrderedItem extends Ardent{
	protected $table = 'pordered_items';
	protected $softDelete = true;

	public function porder(){
		return $this->belongsTo('POrder');
	}

	public function item(){
		return $this->belongsTo('Item');
	}
	
}