<?php

use LaravelBook\Ardent\Ardent;

class ForecastedItem extends Ardent{
	protected $table = 'forecast_items';

	public function item(){
		return $this->belongsTo('Item');
	}

	public function forecast(){
		return $this->belongsTo('Forecast');
	}

}