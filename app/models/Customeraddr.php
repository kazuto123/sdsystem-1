<?php

use LaravelBook\Ardent\Ardent;

class Customeraddr extends Ardent{
	protected $table = 'customeraddrs';

	public function customer(){ 
		return $this->belongsTo('Customer', 'cust_no');
	}

}