<?php

use LaravelBook\Ardent\Ardent;

class OrderXml extends Ardent {
    protected $fillable = [];
    protected $table = 'orders_xml';
}