<?php
use LaravelBook\Ardent\Ardent;

class Uom extends Ardent {
	protected $table = 'uom';

	public static $rules = array();

	public function item(){
		return $this->belongsTo('Item');
	}
}
