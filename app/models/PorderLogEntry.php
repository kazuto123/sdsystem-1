<?php

use LaravelBook\Ardent\Ardent;

class PorderLogEntry extends Ardent{
	protected $table = 'porder_log_entries';
	protected $softDelete = true;

	public function user(){
		return $this->belongsTo('User');
	}
}