<?php

use LaravelBook\Ardent\Ardent;

class Category extends Ardent{
	protected $table = 'categories';

	public function item(){
		return $this->hasMany('Item', 'item_id');
	}

}