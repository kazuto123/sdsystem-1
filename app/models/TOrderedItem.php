<?php
use LaravelBook\Ardent\Ardent;

class TOrderedItem extends Ardent{
	protected $table = 'torder_items';
	protected $softDelete = true;

	public function torder(){
		return $this->belongsTo('TOrder', 'torder_no');
	}

	public function items(){
		return $this->belongsTo('Item', 'item_no');
	}
}