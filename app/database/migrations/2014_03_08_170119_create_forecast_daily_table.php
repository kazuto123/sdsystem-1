<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForecastDailyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('forecast_daily', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('forecast_id');
            $table->integer('forecast_weekly_id');
            $table->date('forecast_day');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('forecast_daily');
	}

}
