<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('drivers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('cont_no');
            $table->integer('user_id');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('drivers');
	}

}
