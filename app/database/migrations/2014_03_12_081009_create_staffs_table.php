<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('staffs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code', 4)->default('01');
            $table->string('type', 20)->default('STAFF');
            $table->string('staff_no', 20)->nullable();
			$table->integer('user_id')->nullable();
			$table->string('first_name');
			$table->string('last_name')->nullable();
			$table->tinyInteger('issalesman')->nullable();
			$table->softDeletes();            
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('staffs');
	}

}
