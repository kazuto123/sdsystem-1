<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('lots', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->string('type', 1); //R or D
            $table->integer('item_id'); //delivery_item_id or return_item_id
            $table->string('lot_name')->nullable();
            $table->string('lot_barcode');
            $table->integer('qty')->nullable();
            $table->string('uom', 4)->nullable();
            
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('lots');
	}

}
