<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresetItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preset_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code', 4)->default('01');
            $table->string('type', 2);
            $table->integer('s_no');
            $table->integer('preset_by');
            $table->integer('item_no');
            $table->integer('item_id');
            $table->string('descr')->nullable();
            $table->decimal('price', 8, 2);
            $table->string('uom', 4);
            $table->string('curr_code');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preset_items');
	}

}
