<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('delivery_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code', 4)->default('01');
            $table->integer('delivery_id');
            $table->integer('item_id');
            $table->integer('qty');
            $table->string('uom', 4);
            $table->string('status', 2);
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('delivery_items');
	}

}
