<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReturnOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('return_order', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code', 4)->default('01');
            $table->integer('delivery_driver'); //user_id
            $table->integer('cust_no');
            $table->text('remarks');
            $table->string('status', 2);
            $table->date('return_date');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('return_order');
	}

}
