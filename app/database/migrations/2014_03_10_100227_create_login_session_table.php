<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('login_session', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('client_id');
            $table->string('cleint_key', 40);
            $table->integer('user_id');
            $table->string('user_ref_table', 20);
            $table->integer('user_red_id');
            $table->string('session_id', 100);
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('login_session');
	}

}
