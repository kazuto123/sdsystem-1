<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForecastMonthlyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('forecast_monthly', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('forecast_id');
            $table->date('forecast_from');
            $table->date('forecast_to');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('forecast_monthly');
	}

}
