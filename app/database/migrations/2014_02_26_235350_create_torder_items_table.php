<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTorderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('torder_items', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->integer('item_no'); 
			$table->integer('item_id')->nullable();
			$table->integer('item_desc')->nullable();
			$table->integer('torder_no'); //FK for torder
			$table->integer('qty')->unsigned();
			$table->string('uom');
			$table->string('uom_cf')->nullable();
			$table->decimal('unitprice', 8, 2);
			$table->decimal('gst', 8, 2);
			$table->decimal('totalamt', 8, 2);
			$table->boolean('inactive')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('torder_items');
	}

}
