<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePorderedItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pordered_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('item_id');
            $table->integer('item_no')->nullable();
            $table->string('descr')->nullable();
            $table->integer('porder_id');
            $table->integer('added_by')->nullable();
            $table->integer('qty');
            $table->string('uom', 4);
            $table->decimal('uom_cf', 8, 4)->nullable();
            $table->decimal('unitprice', 8, 2);
            $table->decimal('total', 8, 2);
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('pordered_items');
	}

}
