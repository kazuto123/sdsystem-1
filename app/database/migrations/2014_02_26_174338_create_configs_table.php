<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('comp_code', 4)->default('01');
			$table->string('type');
			$table->string('key');
			$table->string('value')->nullable();
			$table->text('descr')->nullable();
			$table->tinyInteger('display_order')->nullable();
			$table->boolean('inactive')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configs');
	}

}
