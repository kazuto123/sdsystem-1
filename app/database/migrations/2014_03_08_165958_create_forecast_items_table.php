<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForecastItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('forecast_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('item_id');
            $table->string('uom', 4);
            $table->integer('forecast_daily_id');
            $table->integer('forecast_qty');
            $table->integer('forecast_qty_produced')->nullable();
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('forecast_items');
	}

}
