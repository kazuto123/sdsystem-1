<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('torders', function(Blueprint $table) {
			$table->increments('id')->unique(); //order id
			$table->string('comp_code', 4)->default('01');
			$table->string('cust_grp', 50)->nullable();
			$table->integer('torder_from'); //Company
			$table->integer('torder_to'); //Outlet code
			$table->string('cont_person'); //contact person for outlet
			$table->string('cont_no')->nullable();
			$table->string('cust_name')->nullable();
			$table->tinyInteger('delivery_addr'); 
			$table->string('delivery_addr_text')->nullable();
			$table->date('delivery_date');
			$table->string('delivery_time', 5);
			$table->string('delivery_driver');
			$table->string('absorb_tax', 1)->nullable();
			$table->string('tax_type', 4)->nullable();
			$table->tinyInteger('tax_value')->nullable();
			$table->string('curr_code')->nulllable();
			$table->decimal('curr_rate', 8, 4)->nulllable();
			$table->decimal('subtotal', 8, 2)->default(0.00);
			$table->decimal('gst', 8, 2)->default(0.00);
			$table->decimal('total', 8, 2)->default(0.00);
			$table->text('remark')->nullable();
			$table->boolean('inactive')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('torders');
	}

}
