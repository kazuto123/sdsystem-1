<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
			$table->increments('id')->unique(); //cust_no
			$table->string('comp_code', 4)->default('01');
			$table->integer('cust_no');
			$table->integer('user_id')->nullable();
			$table->string('cust_group_code', 4);
			$table->string('cust_name', 200);
			$table->string('cust_cont_name', 200);
			$table->string('cust_curr_code', 4);
			$table->string('cust_tax_type', 4);
			$table->tinyInteger('cust_absorb_tax');
			$table->string('salesman');	
			$table->boolean('inactive');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
