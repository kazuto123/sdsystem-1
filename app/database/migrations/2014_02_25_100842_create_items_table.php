<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('item_no');
			$table->string('comp_code', 4)->default('01');
			$table->text('item_descr')->nullable();
			$table->string('cat_code', 10); //FK for category
			$table->string('uom', 4);
			$table->boolean('is_sell')->default(0);
			$table->boolean('is_buy')->default(0);
			$table->boolean('is_inventory')->default(0);
			$table->text('flang_descr')->nullable();
			$table->boolean('is_new')->default(0);
			$table->boolean('inactive')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
