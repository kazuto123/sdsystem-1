<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('porders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('user_id');
            $table->integer('vend_no');            
            $table->string('vend_grp', 4)->nullable();
            $table->string('vend_name')->nullable();
			$table->string('cont_person'); //contact person for outlet
			$table->string('cont_no')->nullable();
			$table->string('curr_code', 4)->nullable();
			$table->decimal('curr_rate', 8,2)->nullable();
            $table->text('remark');
            $table->decimal('subtotal', 8, 2);
            $table->decimal('gst', 8, 2);
            $table->decimal('total', 8, 2);
            $table->string('status', 4);
            $table->softDeletes();
			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('porders');
	}

}
