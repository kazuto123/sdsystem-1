<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('delivery', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code', 4)->default('01');
            $table->integer('delivery_driver'); //user id
            $table->integer('cust_no');
            $table->string('status');
            $table->date('delivery_date');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('delivery');
	}

}
