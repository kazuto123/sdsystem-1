<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('clients', function(Blueprint $table) {
            $table->increments('id');
            $table->string('comp_code')->default('01');
            $table->integer('user_id');
            $table->string('key', 40);
            $table->string('platform', 20);
            $table->string('device_token', 100);
            $table->string('gcm_device_token');
            $table->decimal('version', 5, 3);
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('clients');
	}

}
