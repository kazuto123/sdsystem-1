<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersXmlTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('orders_xml', function(Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('xml_name');
            $table->string('curl_msg_raw')->nullable();
            $table->string('send_status', 2)->nullable();
            $table->text('xml_contents');
            $table->softDeletes();
			$table->timestamps('');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::drop('orders_xml');
	}

}
