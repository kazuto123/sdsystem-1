<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ConfigsTableSeeder');
		//$this->call('UsersTableSeeder');
		// $this->call('TestsTableSeeder');
	}

}