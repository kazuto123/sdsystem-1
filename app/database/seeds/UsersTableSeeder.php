<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
    	//User::truncate();
        DB::table('users')->insert(array(
            array(
                'id'                => 1,
                'username'          => 'user01',
                'email'             => 'user01@sample.comm.sg',
                'password'          => Hash::make('12345'),
                'confirmation_code' => '123456789',
                'confirmed'         => 1
            ),
            array(
                'id'                => 2,
                'username'          => 'user02',
                'email'             => 'user2@sample.comm.sg',
                'password'          => Hash::make('12345'),
                'confirmation_code' => '123456789',
                'confirmed'         => 1
            ),
            array(
                'id'                => 3,
                'username'          => 'vendor1',
                'email'             => 'v1@sample.comm.sg',
                'password'          => Hash::make('12345'),
                'confirmation_code' => '123456789',
                'confirmed'         => 1
            ),
            array(
                'id'                => 4,
                'username'          => 'vendor2',
                'email'             => 'v2@sample.comm.sg',
                'password'          => Hash::make('12345'),
                'confirmation_code' => '123456789',
                'confirmed'         => 1
            ),

        ));
    }

}