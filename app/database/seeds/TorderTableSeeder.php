<?php

class TorderTableSeeder extends Seeder{
	public function run() {
		DB::table('torders')->insert(array(
				array(
					'id'  			=> 1, 
					'cust_grp'		=> 'LCL', 
					'torder_from'	=> 1,
					'torder_to'		=> 1001,
					'cont_person'	=> 'James',
				),
		));
	}
}