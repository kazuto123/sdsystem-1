<?php

class ConfigsTableSeeder extends Seeder {

    public function run()
    {
    	Configs::truncate();
		DB::table('configs')->insert(array(
			array(  
				'type'			=> 'vgroup',
				'key'			=> 'vend_grp',
				'value'			=> 'LOC',
				'descr'			=> 'Local',
				'display_order' => '1'
			),
			array(  
				'type'			=> 'vgroup',
				'key'			=> 'vend_grp',
				'value'			=> 'OVS',
				'descr'			=> 'Oversea',
				'display_order' => '2'
			),
			array(  
				'type'			=> 'cgroup',
				'key'			=> 'cust_grp',
				'value'			=> 'LOC',
				'descr'			=> 'Local',
				'display_order' => '1'
			),
			array(  
				'type'			=> 'cgroup',
				'key'			=> 'cust_grp',
				'value'			=> 'OVS',
				'descr'			=> 'Oversea',
				'display_order' => '2'
			),
			array(  
				'type'			=> 'orderlistfilter',
				'key'			=> 'filterby',
				'value'			=> 'driver',
				'descr'			=> 'Driver',
				'display_order' => '3'
			),
			array(  
				'type'			=> 'orderlistfilter',
				'key'			=> 'filterby',
				'value'			=> 'latest_date',
				'descr'			=> 'Latest Date',
				'display_order' => '1'
			),
			array(  
				'type'			=> 'orderlistfilter',
				'key'			=> 'filterby',
				'value'			=> 'order_number',
				'descr'			=> 'Order Number',
				'display_order' => '2'
			),
			array(  
				'type'			=> 'deliverytimeslot',
				'key'			=> 'time',
				'value'			=> '07:00',
				'descr'			=> 'EARYLY MORNING DELIVERY',
				'display_order' => '1'
			),
			array(  
				'type'			=> 'deliverytimeslot',
				'key'			=> 'time',
				'value'			=> '15:00',
				'descr'			=> 'AFTERNOON DELIVERY',
				'display_order' => '2'
			),
			array(  
				'type'			=> 'itempriceseq',
				'key'			=> 'priceseq',
				'value'			=> 'in_item_cust_price',
				'descr'			=> 'none',
				'display_order' => '1'
			),
			array(  
				'type'			=> 'itempriceseq',
				'key'			=> 'priceseq',
				'value'			=> 'in_item_custgrp_price',
				'descr'			=> 'none',
				'display_order' => '2'
			),
			array(  
				'type'			=> 'itempriceseq',
				'key'			=> 'priceseq',
				'value'			=> 'in_item_period_price',
				'descr'			=> 'none',
				'display_order' => '4'
			),
			array(  
				'type'			=> 'itempriceseq',
				'key'			=> 'priceseq',
				'value'			=> 'in_item_list_price',
				'descr'			=> 'none',
				'display_order' => '5'
			)		
		));    	
    }

}