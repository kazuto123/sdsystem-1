@extends('layouts.default')

@section('title')
	404
@stop

@section('content')
<div class="jumbotron">
   <div class="container">
      <h2>The Page Requested is not found. (404)</h2>
      <p>Sorry For any inconvinience cause.</p>
   </div>
   <p><a class="btn btn-primary btn-lg" role="button" onclick="history.go(-1);"> Back </a>
   </p>
</div>

@stop