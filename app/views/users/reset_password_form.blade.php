@extends('_layouts.default')

@section('title')
@parent
	::Reset Password
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 bc-login-form-container">
			<div class="bc-login-form-title" style="top:20%;"> Password Reset! </div>
			<div class="bc-login-form-title" style="top:35%; font-size:12px; "> Enter Reset Token </div>
			
			<div class="bc-login-form">
				{{  }}
			</div>
				
					
		</div>
	</div>
</div>


@stop