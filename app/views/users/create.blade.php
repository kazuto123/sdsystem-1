@extends('layouts.default')

@section('title')
@parent
	::Forgot Password
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 bc-login-form-container">
			<div class="bc-login-form-title" style="top:20%;"> Forgot Password? </div>
			<div class="bc-login-form-title" style="top:35%; font-size:12px; "> Enter your email register email address to reset password. </div>
			<div class="bc-login-form">
				{{ View::make(Config::get('confide::signup_form')); }}
			</div>
				
					
		</div>
	</div>
</div>


@stop