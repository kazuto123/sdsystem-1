@extends('layouts.default')

@section('title')
	User Login
@stop

@section('content')
<div class="container" style="margin-top: 40px;">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 bc-login-form-container">
			<div class="well" style="margin-bottom: 3px; background: #FF454B;">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="col-md-12 text-center" style="margin-bottom: 20px;">
							<span class=""><img src="/images/logo-kfc.png" alt="logo" class=""></span>
						</div>
						<br>

						<h4 style="color: #eaeaea; text-shadow: 1px 1px 1px #999;">KFC Ordering System</h4>
						
					</div>
				</div>
				
				<div class="well bc-login-form-container" style="border: 0px; box-shadow: 0px;-webkit-box-shadow: inset 0 0px 0px; -moz-box-shadow: inset 0 0px 0px ; ">
					<div class="row">
						<div class="col-md-12" style="color: white;">
							{{ Confide::makeLoginForm()->render() }}	
						</div>
					</div>				
				</div>
				
			</div>
						
		</div>
	</div>
</div>


@stop