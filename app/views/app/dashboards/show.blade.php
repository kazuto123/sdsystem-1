@extends('layouts.jqm-default')

@section('title')
	Dashboard
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
{{ HTML::style('/css/app-commons.css') }}
@stop

<!-- Main content area -->
@section('content')
<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}">
<!-- Page Header -->
<div data-role="header" data-position="fixed" >
	<a href="#" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-user ui-btn-icon-left" style="display:none;">Profile</a> 
	<h1 style="height:30px; "><img src="/images/logo-company-24.png" alt="logo"></h1>
	<a href="{{ URL::to('/user/logout') }}" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-power ui-btn-icon-right">Logout</a>
</div>


<!-- Main Content page -->
<div data-role="main" class="ui-content">
<div class="ui-grid-a ui-responsive">
	<div class="ui-block-a">
	<ul>
		<li>
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<h1>Order Taking</h1>
					<div id="custom-border-radius">	
						<a href="/user/{{$user->id}}/torder" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>View Orders</span>
							</div>
						</a>	
						<a href="/user/{{$user->id}}/torder/create" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>Take Order</span>
							</div>
						</a>	
					</div>
				</div>			
			</div>			
		</li>

		<li>
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<h1>Order Placing</h1>
					<div id="custom-border-radius">	
						<a href="/user/{{$user->id}}/porder" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>View Orders</span>
							</div>
						</a>	
						<a href="/user/{{$user->id}}/porder/create" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>Place Order</span>
							</div>
						</a>	
					</div>
				</div>			
			</div>			
		</li>

		<li>
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<h1>Forecast</h1>
					<div id="custom-border-radius">	
						<a href="#" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>View Forecasts</span>
							</div>
						</a>	
						<a href="#" class="ui-corner-all ui-btn ui-btn-inline ">
							<div class='icon-button-128'>
								<span>New Forecast</span>
							</div>
						</a>	
					</div>		
				</div>			
			</div>			
		</li>
	</ul>
	</div> <!-- ui-block-a -->

	<div class="ui-block-b">
		<ul data-role="collapsible" data-collapsed="false" data-theme="{{$theme}}" data-inset="false" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
            <h1>User Details</h1>
            <li data-form="ui-body" data-theme="{{$theme}}" class="ui-li-static ui-body">
				<div class='ui-grid-solo'>
					<div class="ui-block">
						<p>Username: <strong>{{$user->username}}</strong></p>
						<p>Roles: <strong>{{$role->type}}</strong></p>
						<p>isSalesman: <strong> @if($role->issalesman == 1 ) Yes @else No @endif</strong></p>
					</div>
				</div>					
            </li>
		</ul>
	</div> <!-- ui-block-b -->

</div><!-- ui-grid-a -->
</div><!-- main content ended -->
</div> <!-- End of page -->
@stop

<!-- javascripts -->
@section('scripts')
@parent
<!-- any extra Javascript related to this screen add here-->

@stop
