<div class="ui-field-contain">
    <label for="{{$input_name}}">{{$input_label}}</label>
    <input type="text" name="{{$input_name}}" id="{{$input_id}}" placeholder="{{input_placeholder}}" value="" {{$optional}}>
</div>