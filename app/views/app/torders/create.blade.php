@extends('layouts.jqm-default')

@section('title')
	Create Order
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
{{ HTML::style('/css/app-commons.css') }}
@stop

<!-- Main content area -->
@section('content')

<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}">
<!-- Page Header -->
<div data-role="header" data-position="fixed" >
	<a href="/user/{{$userid}}/torder" class="ui-btn ui-shadow ui-icon-carat-l ui-btn-inline ui-corner-all ui-btn-icon-left">Back</a>  
	<h1 style="height:30px; "><img src="/images/logo-company-24.png" alt="logo"></h1>
	<a href="{{ URL::to('/user/logout') }}" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-power ui-btn-icon-right">Logout</a>
</div>

<div data-role="main" class="ui-content">

{{ Form::open(['route'=>'user.torder.store', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'order_form', 'id'=>'order_form']) }}
	<div class="ui-grid-a ui-responsive">
		<div class="ui-block-a">
			<div class="ui-body">
				<div class="ui-field-contain">
				    <label for="cust_group">Customer Group</label>
				    <select name="cust_group" id="input_custgrp" data-mini="true" required>
				        <option value="None">None</option>
				        @foreach($cust_grp as $option)
				        	<option value="{{$option}}">{{$option}}</option>
				        @endforeach
				    </select>
				</div>
				<div class="hidden-input"> <input type="hidden" name="order_from" value="{{$user->id}}"></div>
				<div class="ui-field-contain">
				    <label for="outlet_code">Outlet Code</label>
				    <select name="outlet_code" id="input_outletcode" data-mini="true" required>
				    	
				    </select>
				</div>

				<div class="ui-field-contain">
				    <label for="cust_name">Customer Name</label>
				    <input type="text" name="cust_name" id="input_cust_name" placeholder="Customer Name" value="" required>
				</div>

				<div class="ui-field-contain">
				    <label for="cont_person">Contact Person</label>
				    <input type="text" name="cont_person" id="input_cont_person" placeholder="Contact Person" value="" required>
				</div>

				<div class="ui-field-contain">
				    <label for="cont_no">Contact Number</label>
				    <input type="text" name="cont_no" id="input_cont_no" placeholder="Contact Number" value="" required>
				</div>

				<div class="ui-field-contain">
				    <label for="delivery_addr">Delivery Address</label>
				    <select name="delivery_addr" id="input_delivery_addr" data-mini="true" required>
				    	
				    </select>
				</div>

				<div class="ui-field-contain">
				    <label for="delivery_driver">Delivery Driver</label>
				    <select name="delivery_driver" id="input_delivery_driver" data-mini="true" required>
				    	@foreach($drivers as $driver)
							<option value="{{$driver}}">{{$driver}}</option>
						@endforeach
				    </select>
				</div>

			</div>
		</div>
		<div class="ui-block-b">
			<div class="ui-body">
				<div class="ui-grid-a ui-responsive">
					<div class="ui-block-a">
						<div class="ui-field-contain" style="margin-top: 8px;">
							<input id="input_delivery_date" name="delivery_date" type="text" data-role="date" data-inline="true" required>		    
						</div>						
					</div> <!-- date pick block end-->

					<div class="ui-block-b">
						<div class="ui-body">
							<select name="delivery_time" id="input_delivery_time" data-mini="true" required>
						    	@foreach($timeslots as $timeslot)
									<option value="{{strtolower($timeslot)}}">{{$timeslot}}</option>
								@endforeach
						    </select>		
						</div>
					</div> <!-- time pick end -->
				</div> <!-- ui-grid-a end -->				
			</div>
		</div> <!-- ui-block-b-->
	</div> <!-- ui-grid-a -->
	<hr class="custom-hr">
	
	<div class="ui-grid-a ui-responsive">
		<div class="ui-block-a">
			<div class="ui-body">
				<h4>Additional Order</h4>
				<div class="ui-block">
					<a id="ALL" href="" class="ui-corner-all ui-btn ui-btn-inline cat-button">
						<div id="ALL" class='icon-button-70'>ALL</div>
					</a>
					@foreach($cats as $cat)
					<a id="{{$cat->sub_cat}}" href="" class="ui-corner-all ui-btn ui-btn-inline cat-button">
						<div id="{{$cat->sub_cat}}" class='icon-button-70'>{{ $cat->sub_cat }}</div>
					</a>
					@endforeach						
				
				</div>
				<div class="ui-block">
					<ul id="cat_itemlist" data-role="listview" data-filter="true" data-inset="true" style="height:300px; overflow: auto;">
						@foreach($items as $item)
							<li class="list-item-add cat-item {{ $item->uom }} {{$item->cat_code}}"> 
								<span>
								{{ $item->item_no }} &nbsp&nbsp {{$item->item_descr}}
								<a id="{{ $item->item_no }}" class="btn-item-add fa fa-plus-circle" style="float:right;"></a>
								</span>
							</li>							
						@endforeach 
					</ul>
				</div>
			</div>
		</div> <!-- ui-block-a -->	

		<!-- Order Item and Summarize payment-->
		<div class="ui-block-b">
			<div class="ui-body">
				<h4>Ordered Item</h4>
				<div class="ui-block">
					<ul id="ordered_itemlist" class="oded_itemlist" style="height: 340px; overflow: auto; margin-top: 3px;" data-role="listview" data-inset="true" data-split-icon="plus" data-split-theme="b">
						<!-- Ordered Item List -->


					</ul>
				</div>
			</div>

			<div class="ui-body">
				<div class="ui-grid-solo">
					<div class="ui-block-a">
						<div class="ui-field-contain" style="">
						    <label for="order_subtotal">Subtotal(SGD): </label>
						    <input type="text" name="order_subtotal" id="order_subtotal" placeholder="Subtotal" value="" readonly>
						</div>		
					</div>
				</div>

				<div class="ui-grid-solo">
					<div class="ui-block-a">
						<div class="ui-field-contain" style="">
						    <label for="order_gst">GST(SGD): </label>
						    <input type="text" name="order_gst" id="order_gst" placeholder="GST" value="" readonly>
						</div>		
					</div>
				</div>

				<div class="ui-grid-solo">
					<div class="ui-block-a">
						<div class="ui-field-contain" style="">
						    <label for="order_total">Total(SGD): </label>
						    <input type="text" name="order_total" id="order_total" placeholder="Total Amount" value="" readonly>
						</div>		
					</div>
				</div>

				<div class="ui-grid-b">
					<div class="ui-block-a">
						<a href="#" class="ui-corner-all ui-btn">Cancel</a> 	
					</div>
					<div class="ui-block-b">
						<a href="#" class="ui-corner-all ui-btn">Clear</a>	
					</div>
					<div class="ui-block-c">
						{{ Form::submit('Submit', ['class'=>''] )}}
					</div>					
				</div>

			</div>						
		</div> <!-- ui-block-b -->	


	</div><!-- ui-grid-a -->
{{Form::close()}}

</div>

</div> <!-- page -->




@stop

<!-- javascripts -->
@section('scripts')
@parent
<!-- any extra Javascript related to this screen add here-->

@stop
