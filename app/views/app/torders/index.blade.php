@extends('layouts.jqm-default')

@section('title')
	View Orders
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
{{ HTML::style('/css/app-commons.css') }}
@stop

<!-- Main content area -->
@section('content')

<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}">
<!-- Page Header -->
<div data-role="header" data-position="fixed" >
	<a href="/user/{{$userid}}/dashboard" class="ui-btn ui-shadow ui-icon-carat-l ui-btn-inline ui-corner-all ui-btn-icon-left">Back</a> 
	<h1 style="height:30px; "><img src="/images/logo-company-24.png" alt="logo"></h1>
	<a href="{{ URL::to('/user/logout') }}" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-power ui-btn-icon-right">Logout</a>
</div>


<div class="ui-grid-solo">
	<div class="ui-block-a">
		<div class="ui-body">
			<a href="/user/{{$userid}}/torder/create" class="ui-btn ui-shadow ui-icon-plus ui-btn-inline ui-corner-all ui-btn-icon-right ui-mini" style="float:right;">Create</a>
			<h3>Order List</h3>
			<hr style="border-top:1px solid #ccc;">
			<ul data-role="listview" data-filter="true" data-inset="true">
			    @foreach($orders as $order)
			    	<li><a href="/user/{{$userid}}/torder/{{$order->id}}">{{$order->torder_to}} {{ Customer::where('cust_no', '=', $order->torder_to)->first()->cust_name}} - {{$order->delivery_addr}}<span class="list-date">{{$order->delivery_date}} {{$order->delivery_time}}</span></a></li>
			    @endforeach
			</ul>					
		</div>
	</div>
</div>


</div> <!-- page -->


@stop

<!-- javascripts -->
@section('scripts')
@parent
<!-- any extra Javascript related to this screen add here-->

@stop
