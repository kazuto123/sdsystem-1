@extends('layouts.jqm-default')

@section('title')
	View Orders
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
{{ HTML::style('/css/app-commons.css') }}
@stop

<!-- Main content area -->
@section('content')

<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}">

<!-- Page Header -->
<div data-role="header" data-position="fixed" >
	<a href="/user/{{$userid}}/torder" class="ui-btn ui-shadow ui-icon-carat-l ui-btn-inline ui-corner-all ui-btn-icon-left">Back</a> 
	<h1 style="height:30px; "><img src="/images/logo-company-24.png" alt="logo"></h1>
	<a href="{{ URL::to('/user/logout') }}" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-power ui-btn-icon-right">Logout</a>
</div>

<div data-role="main" class="ui-content">

<div class="ui-grid-solo">
	<div class="ui-body ui-corner-all">  
	  <a href="#deletePopup" data-rel="popup" class="ui-btn ui-shadow ui-corner-all ui-icon-delete ui-btn-icon-notext" style="float:right; margin-right:5px;">Delete</a>
	  <div data-role="popup" id="deletePopup" data-position-to="window" data-theme="{{$theme}}" data-dismissible="false" style="max-width:400px;">
	    <h1>Delete Order?</h1>
	    <p class="ui-title">Are you sure you want to delete this Order?</p>
    	<p>This action cannot be undone.</p>
		{{ Form::open(array('url' => 'user/' . $userid . '/torder/' . $torderid) ) }}
			{{ Form::hidden('_method', 'DELETE') }}
			<fieldset>
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<a href="#" class="ui-btn ui-corner-all ui-shadow" data-rel="back" data-theme="b">Cancel</a>			
					</div>
					<div class="ui-block-b">
						<input type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" data-transition="flow" value="Delete">			
					</div>
				</div>
			</fieldset>
		{{ Form::close() }}	    	
    
	  </div>

	  <a href="javascript:window.print()" class="ui-btn ui-shadow ui-icon-action ui-btn-inline ui-corner-all ui-btn-icon-right ui-mini" style="float:right;">Print</a>	
	  <a href="/user/{{$userid}}/torder/{{$torder->id}}/edit" class="ui-btn ui-shadow ui-icon-edit ui-btn-inline ui-corner-all ui-btn-icon-right ui-mini" style="float:right;">Edit</a>	
	  <h3>Order No. {{$torder->id}}</h3>
	  <hr class="custom-hr">
	  <div class="ui-grid-a ui-responsive" style="font-size: 14px;">
	  	<div class="ui-block-a">
	  	  <div class="ui-body">
		    <ul data-role="listview" data-inset="true">
				<li>Customer Group: <strong style="float:right;">{{$torder->cust_grp}}</strong> </li>
				<li>Outlet Code: <strong style="float:right;">{{$torder->torder_to}}</strong> </li>
				<li>Customer Name: <strong style="float:right;">{{Customer::where('cust_no', '=', $torder->torder_to)->first()->cust_name}}</strong> </li>
				<li>Office Number: <strong style="float:right;">{{Customer::where('cust_no', '=', $torder->torder_to)->first()->address->first()->cont_off_tel}}</strong> </li>
		        <li>Contact Person: <strong style="float:right;">{{$torder->cont_person}}</strong> </li>
		    </ul>		    	  	
	  	  </div>
	  	</div>

	  	<div class="ui-block-b">
	  	  <div class="ui-body">
		    <ul data-role="listview" data-inset="true">
				<li>Delivery Address: <strong style="float:right;">{{$torder->delivery_addr}}</strong> </li>
				<li>Delivery Date: <strong style="float:right;">{{$torder->delivery_date}}</strong> </li>
				<li>Devlivery Time: <strong style="float:right;">{{$torder->delivery_time}}</strong> </li>
				<li>Driver Incharge: <strong style="float:right;">{{$torder->delivery_driver}}</strong> </li>
		    </ul>  	  	
	  	  </div>
	  	</div>
	  </div> <!-- ui-grid-a -->
	</div>

	<div class="ui-body ui-body-a ui-corner-all">  
		<h3>Order Details:</h3>
		<div class="ui-grid-solo">
		<table data-role="table" class="ui-responsive table-stripe" data-mode="column" style="font-size: 12px;">
		    <thead>
		       <tr>
		         <th>Item No</th>
		         <th>Item Descr</th>
		         <th>Qty</th>
		         <th>UOM</th>
		         <th>Price</th>
		         <th>Total</th>
		       </tr>
		    </thead>
			<tbody>
				@foreach($torder_items as $oditem)
				<tr>
				    <td>{{ $oditem['item']['item_no'] }}</td>
				    <td>{{ $oditem['descr'] }}</td>
				    <td>{{ $oditem['item']['qty'] }}</td>
				    <td>{{ $oditem['item']['uom'] }}</td>
				    <td>{{ $oditem['item']['unitprice'] }}</td>
				    <td>{{ $oditem['item']['totalamt'] }}</td>
				</tr>
				@endforeach
		    </tbody>    
		</table>						
		</div>
		<br><br><br>
		<div class="ui-grid-a">
			<div class="ui-block-a"></div>
			<div class="ui-block-b">
				<div class="ui-body">
					<ul data-role="listview" style="font-size: 12px;">
						<li>Subtotal <span style="float:right">${{ $torder->subtotal }}</span></li>
						<li>GST <span style="float:right">${{ $torder->gst }}</span></li>
						<li>Total <span style="float:right">${{ $torder->total }}</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
</div><!-- ui-grid-solo -->
	

</div> <!-- main -->	


</div> <!-- page -->

@stop

<!-- javascripts -->
@section('scripts')
@parent
<!-- any extra Javascript related to this screen add here-->

@stop
