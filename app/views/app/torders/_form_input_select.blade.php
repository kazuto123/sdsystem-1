<div class="ui-field-contain">
    <label for="{{$input_name}}">{{$input_label}}</label>
    <select name="{{$input_name}}" id="{{$input_id}}">
        @foreach($input_options as $option)
        	<option value="{{$option}}">{{$option}}</option>
        @endforeach
    </select>
</div>