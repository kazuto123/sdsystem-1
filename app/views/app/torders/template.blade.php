@extends('layouts.jqm-default')

@section('title')
	View Orders
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->

@stop

<!-- Main content area -->
@section('content')

<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}">
<!-- Page Header -->
<div data-role="header" data-position="fixed" >
	<a href="/user/{{$userid}}/torder" class="ui-btn ui-shadow ui-icon-carat-l ui-btn-inline ui-corner-all ui-btn-icon-left">Back</a>  
	<h1 style="height:30px; "><img src="/images/logo-company-24.png" alt="logo"></h1>
	<a href="{{ URL::to('/user/logout') }}" class="ui-btn ui-shadow ui-btn-inline ui-corner-all ui-icon-power ui-btn-icon-right">Logout</a>
</div>

<div data-role="main" class="ui-content">


</div>



</div> <!-- page -->




@stop

<!-- javascripts -->
@section('scripts')
@parent
<!-- any extra Javascript related to this screen add here-->
{{ HTML::script('js/app-to.js')}}
@stop
