@extends('layouts.jqm-default')

@section('title')
	Login
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
{{ HTML::style('/css/app-commons.css') }}
@stop

<!-- Main content area -->
@section('content')
<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}" id="login-page">

<div class="ui-grid-a ui-responsive" style="margin-top: 8%;">
    
    <div class="ui-block-a" data-theme="{{$theme}}" style="padding: 20px;">
		<ul data-role="listview" data-theme="{{$theme}}" data-inset="true" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
            <li data-role="list-divider" data-theme="{{$theme}}" data-form="ui-bar" role="heading" class="ui-li-divider ui-bar ui-first-child" style="text-align:center;">
            	<h1><img src="/images/logo-konverge.png" alt="logo"></h1>
            </li>
            <li data-form="ui-body" data-theme="{{$theme}}" class="ui-li-static ui-body">
				{{ Confide::makeLoginForm()->render() }}	
            </li>
		</ul>
    </div>

    <div class="ui-block-b">
		<div class="ui-body">
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<h1>Welcome to Konverge Web System</h1>
					<p>
						Konverge is a system that generates sales documents easily and check on customers.
					</p>
		            <p>
		          		Konverge Distribution comprises of a complete suite of comprehensive modules to effectively manage your business operations. It also enables effective processing of Sales Orders and Purchases, and the Vehicle Allocation module helps streamline delivery schedules and makes optimal use of the delivery vehicle through load balancing capabilities.
		            </p>
				</div>
			</div>

			<div class="ui-grid-a">
				<div class="ui-block-a">
					
				</div>
				<div class="ui-block-b">
					<a href="#" class="ui-btn ui-corner-all">Signup</a>
				</div>
			</div>
		</div>
    </div><!-- end of block-b -->

</div><!-- /grid-b -->

	
</div> <!--end of page -->
@stop


<!-- javascripts -->
@section('csslinks')
@parent
<!-- any extra Javascript related to this screen add here-->
<script>
$(document).on('pagecontainerbeforeload', '[data-role="page"]', function(){     
    $.mobile.loading('show');
});

$(document).on('pageload', '[data-role="page"]', function(){  
    $.mobile.loading('hide');
});

</script>
@stop
