@extends('layouts.jqm-default')

@section('title')
	Login
@stop

<!-- CSS -->
@section('csslinks')
@parent
<!-- any extra css related to this screen add here-->
@stop

<!-- Main content area -->
@section('content')
<!-- Page Dashboard -->
<div data-role="page" data-theme="{{$theme}}" id="login-page">

<div class="ui-grid-b ui-responsive" style="margin-top: 6%;">
    
    <div class="ui-block-a" data-theme="{{$theme}}" style="padding: 20px;">

    </div>

    <div class="ui-block-b">
		<div class="ui-body">
			<ul data-role="listview" data-theme="{{$theme}}" data-inset="true" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
	            <li data-role="list-divider" data-theme="{{$theme}}" data-form="ui-bar" role="heading" class="ui-li-divider ui-bar ui-first-child" style="text-align:center;">
	            	<h1>Create New Account</h1>
	            </li>
	            <li data-form="ui-body" data-theme="{{$theme}}" class="ui-li-static ui-body">
					{{ View::make(Config::get('confide::signup_form')); }}
	            </li>
			</ul>	
		</div>
			
    </div><!-- end of block-b -->

</div><!-- /grid-b -->

	
</div> <!--end of page -->
@stop


<!-- javascripts -->
@section('csslinks')
@parent
<!-- any extra Javascript related to this screen add here-->
<script>
$(document).on('pagecontainerbeforeload', '[data-role="page"]', function(){     
    $.mobile.loading('show');
});

$(document).on('pageload', '[data-role="page"]', function(){  
    $.mobile.loading('hide');
});

</script>
@stop
