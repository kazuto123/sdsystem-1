@extends('bom.master')

@section('title')
New BOM
@stop

@section('content')

<div id="wrapper">

<!-- Navigation Panel -->
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="/sdbom/bom" style="padding: 10px;">
      	<img alt="Test" src="/images/logo-konverge-30.png" style="height:28px;">    
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">
        <li class=""> <a href="/sdbom/bom"> List Bom </a></li>
        <li class="active"> <a href="/sdbom/bom/create"> New Bom </a></li>	
      </ul>	
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{$username}} <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <div class="media">
			  <a class="pull-left" href="#" style="padding-left: 5px;">
			    <img class="media-object" src="/images/user.png" alt="test" style="height:28px; width:28px;">
			  </a>
			  <div class="media-body">
          <h5 class="media-heading">{{$username}}</h5>
          <strong>Access Rights</strong><br>
          Add: {{($acc_right['add'] == 1) ? 'YES':'NO' }}<br>
          Update: {{($acc_right['upd'] == 1) ? 'YES':'NO' }}<br>
          Delete: {{($acc_right['del'] == 1) ? 'YES':'NO' }}<br>
          View: {{($acc_right['view'] == 1) ? 'YES':'NO' }}<br>
			  </div>
			</div>	
            </li>
            <li class="divider"></li>
            <li><a href="/sdbom/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid --> 
</nav>


<!-- Main BOM form panel -->
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <span style="font-size: 18px; font-weight: bold;">New Bill of Material (BOM)</span>
    </div>
    <div class="col-md-12"><hr class='divider'></div>
  </div>

  {{ Form::open(['url'=>'/sdbom/bom/create', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'bom_form', 'id'=>'bom_form']) }}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="bom_code" class="col-md-3 control-label">BOM Code</label>  
        <div class="col-md-9">
          <div class='input-group'>
            <input type="text" name="bom_code" id="input_bom_code" class="form-control" value="" placeholder="Key in new BOM code" data-container="body" data-toggle="popover" data-placement="bottom" required>
            <span class="btn btn-primary input-group-addon" id="btn_checkbomcode">
              <i class="fa fa-search"></i>
            </span>            
          </div>
        <span id="message_bomcode" class="bom-msg-box"></span>
        </div>
      </div><!-- end bom code -->

      <div class="form-group">
        <label for="bom_descr" class="col-md-3 control-label"> 
          BOM Description
        </label>
        <div class="col-md-9">
          <textarea name="bom_descr" class="form-control" rows="2" placeholder="Key in the Description of BOM" required></textarea>   
        </div>
      </div>  <!-- BOM description -->

      <div class="form-group">
        <label for="bom_itemno" class="col-md-3 control-label">Item No</label>  
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-5" style="padding-right: 5px;">
              <select name="bom_itemno" id="select_bom_itemno" class="form-control" >
                <option value="none">NONE</option>
                @foreach($allitems as $item)
                  <option value="{{$item->item_id}}-{{$item->item_no}}">{{$item->item_no}} {{$item->item_descr}}</option>
                @endforeach
              </select>               
            </div>

            <div class="col-md-2" style="padding-right: 5px; padding-left: 0px;">
               <input type="text" name="bom_itemqty" id="input_bom_itemqty" class="form-control" value="" placeholder="qty">

            </div>                        

            <div class="col-md-2" style="padding-right: 5px; padding-left: 0px;">
              <select name="bom_itemuom" id="select_bom_itemuom" class="form-control">
                <option value="none">NONE</option>
              </select>               
            </div>

            <div class="col-md-3" style="padding-left: 0;">
               <input type="text" name="bom_itemconv" id="input_bom_itemconv" class="form-control" value="" placeholder="uom conv" readonly required>
            </div>                     
          </div>
        </div>
      </div> <!-- item No -->
      
      <div class="form-group">
        <label for="bom_itemdescr" class="col-md-3 control-label"> 
          Item Description
        </label>
        <div class="col-md-9">
          <textarea name="bom_itemdescr" class="form-control" rows="2" placeholder="Item Description" required></textarea>   
        </div>
      </div>  <!-- Item Description -->
    
      <div class="form-group">
        <label for="bom_remarks" class="col-md-3 control-label"> 
          BOM Remarks
        </label>
        <div class="col-md-9">
          <textarea name="bom_remark" class="form-control" rows="2" placeholder="Add in remark for BOM"></textarea>   
        </div>
      </div> <!-- BOM remark -->
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">
             BOM Status
          <span style="float:right;" id="bom_status">{{$approve_status}}</span>
          </h3>
        </div>        
        <div class="panel-body" style="padding-left: 15px;">
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> Approved status </div> 
            <div class="col-md-7 col-sm-7 col-xs-6"> {{$approve_status}} </div>  
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> <div style="margin-top: 5px;">Is Kitting BOM?</div> </div> 
            <div class="col-md-7 col-sm-7 col-xs-6">
              <div class="checkbox">
                <label>
                  <input type="hidden" name="bom_iskittling" id="bom_iskittling_chkbox_no" value="0">
                  <input type="checkbox" name="bom_iskittling" id="bom_iskittling_chkbox" value="1"> Yes
                </label>
              </div>
            </div>  
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> Created By </div> 
            <div class="col-md-7 col-sm-7 col-xs-6"> {{$username}} </div>  
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> Created On </div> 
            <div class="col-md-7 col-sm-7 col-xs-6"> {{$curr_datetime}} </div>  
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> Last Updated By </div> 
            <div class="col-md-7 col-sm-7 col-xs-6"> NULL </div>  
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-6"> Last Updated On </div> 
            <div class="col-md-7 col-sm-7 col-xs-6"> NULL </div>  
          </div>
        </div> <!-- end panel body -->
      </div>
    </div>
  </div>

  <hr>

  <div class="row">
    <!-- aditional item-->
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12"><h4>Items</h4></div>
        <div class="col-md-12" id="cat_listings">
          @foreach($sub_cats as $cat)
            <div id="{{ $cat->sub_cat }}" class="cat-button bom-category" title="{{$cat->descr}}">
              {{ $cat->sub_cat }}
            </div>
          @endforeach 
        </div>
      </div>
      <br>
      
      <div class="row">
        <div class="col-md-12">
          <div class='input-group'>
            <input type="text" name="item_search" id="input_item_search" class="form-control" value="" placeholder="Key in itemno">
            <span class="bom-msg-box" id="message_item_search"></span>
            <span class="btn btn-primary input-group-addon" id="btn_search_items"><i class="fa fa-search"></i></span>            
          </div>
        </div>
      </div>

      <br>
      <div class="row">
        <!-- items -->
        <div id="alert_msg_item_add" class="alert alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" 
              aria-hidden="true">
              &times;
           </button>
           <span id="msg_item_add"> Error ! Change few things. </span>
        </div>
        <ul class="col-md-12" id="bom_itemlists">

        </ul>
      </div>
      <br>
    </div>

    <!-- BOM item List -->
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12"><h4> BOM Recipe Details </h4></div>
        <div class="col-md-12">
            <div id="bom_itemlist_ordered" class="bom_itemlist bom_itemlist_container">
              <!-- Ordered Item List -->
              
            </div>
          </div>        
      </div>
      <hr>
    </div>
  </div> <!--end row -->

  

  <div class="row" style="">
    <div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 15px;">
      <div class="row">
        {{ Form::submit('Submit', ['class'=>'btn btn-primary col-md-3 pull-right', 'style'=>'margin-top:3px'] )}}
        <button class="btn btn-warning col-md-3 pull-right" type="button" data-toggle="modal" data-target="#clearformModal" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Clear</button>
        <button class="btn btn-danger col-md-3 pull-right" type="button" data-toggle="modal" data-target="#cancelformModal" style="margin:3px;">Cancel</button> 
      </div>
    </div>
  </div>
  {{ Form::close() }}

</div>


</div>

  <!-- Division for the clear order element-->
  <div class="row">
    <div class="modal fade" id="clearformModal" tabindex="-1" role="dialog" aria-labelledby="clearformModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width:auto; margin:auto;">
        <div class="modal-content" style=";">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="clearformModalLabel">Clear order form?</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to clear this new BOM?
          </div>
          <div class="modal-footer">
        <a class="btn btn-default" data-dismiss="modal">Close</a>
        <a href="/sdbom/bom/create" class="btn btn-danger">OK</a>
          </div>
        </div>
      </div>
    </div>    
  </div>

  <!-- Division for the cancel order element-->
  <div class="row">
    <div class="modal fade" id="cancelformModal" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
      <div class="modal-dialog" style="width:auto; margin:auto;">
        <div class="modal-content" style=";">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Cancel order form?</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to cancel this new BOM?
          </div>
          <div class="modal-footer">
        <a class="btn btn-default" data-dismiss="modal">Close</a>
        <a href="/sdbom/bom" class="btn btn-danger">OK</a>
          </div>
        </div>
      </div>
    </div>    
  </div>


@stop