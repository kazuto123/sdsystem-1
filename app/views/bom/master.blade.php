<!DOCTYPE html>
<html>
    <head>
        <title> @yield('title') </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS are placed here -->
        @section('css')
            <!-- Core CSS - Include with every page -->

            <!-- CSS are placed here -->
            {{ HTML::style('packages/css/bootstrap.css') }} 
            {{ HTML::style('packages/css/bootstrap-modal-bs3patch.css') }}
            {{ HTML::style('packages/css/bootstrap-modal.css') }}
            {{ HTML::style('packages/css/bootstrap-datetimepicker.css')}}
            {{ HTML::style('packages/css/bootstrap-select.min.css')}}
            {{ HTML::style('packages/css/font-awesome.min.css') }}
            {{ HTML::style('packages/css/docs.min.css') }}
            {{ HTML::style('css/app.css') }}

            
        @show
    </head>

    <body>        
        @yield('content')


        <!-- Script put here !! -->
        @section('scripts')
            {{ HTML::script('packages/js/jquery-2.1.0.js')}}
            {{ HTML::script('packages/js/jquery.validate.min.js')}}
            {{ HTML::script('packages/js/moment.js')}}
            {{ HTML::script('packages/js/bootstrap.js')}}
            {{ HTML::script('packages/js/bootstrap-modalmanager.js')}}
            {{ HTML::script('packages/js/bootstrap-modal.js')}}
            {{ HTML::script('packages/js/bootstrap-select.min.js')}}
            {{ HTML::script('packages/js/bootstrap-datetimepicker.min.js')}}
            {{ HTML::script('packages/js/locales/bootstrap-datetimepicker.uk.js')}}

            {{ HTML::script('js/app-bom.js')}} 
        @show
    </body>
</html>
