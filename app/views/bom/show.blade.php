@extends('bom.master')

@section('title')
New BOM
@stop

@section('content')

<div id="wrapper">

<!-- Navigation Panel -->
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="/sdbom/bom" style="padding: 10px;">
      	<img alt="Test" src="/images/logo-konverge-30.png" style="height:28px;">    
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">
        <li class=""> <a href="/sdbom/bom"> List Bom </a></li>
        <li class=""> <a href="/sdbom/bom/create"> New Bom </a></li>	
      </ul>	
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{$username}} <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <div class="media">
  			         <a class="pull-left" href="#" style="padding-left: 5px;">
  			           <img class="media-object" src="/images/user.png" alt="test" style="height:28px; width:28px;">
  			         </a>
  			         <div class="media-body">
                 <h5 class="media-heading">{{$username}}</h5>
                 <strong>Access Rights</strong><br>
                   Add: {{($acc_right['add'] == 1) ? 'YES':'NO' }}<br>
                   Update: {{($acc_right['upd'] == 1) ? 'YES':'NO' }}<br>
                   Delete: {{($acc_right['del'] == 1) ? 'YES':'NO' }}<br>
                   View: {{($acc_right['view'] == 1) ? 'YES':'NO' }}<br>
  			         </div>
  			      </div>	
        </li>
        <li class="divider"></li>
        <li><a href="/sdbom/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid --> 
</nav>


<!-- Show BOM information panel -->
<div class="container">
  <div class="row">
    <!-- left panel -->
    <div class="col-md-8">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title">
               BOM Id. {{$bominfo['bomid']}}
            </h3>
         </div>
         <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                   <strong><caption> </caption></strong>
                   <thead></thead>
                   <tbody>
                      <tr>
                         <td>Master BOM Code: </td>
                         <td>{{$bominfo['bomcode']}}</td>
                      </tr>
                      <tr>
                         <td>BOM Revision Number</td>
                         <td>{{$bominfo['bomrevno']}}</td>
                      </tr>
                      <tr>
                         <td>Bom Descriptions</td>
                         <td>{{$bominfo['bomdesc']}}</td>
                      </tr>
                      <tr>
                         <td>Bom Item No</td>
                         <td>{{$bominfo['bomitemno']}}</td>
                      </tr>                      
                      <tr>
                         <td>Bom Quantity</td>
                         <td>{{$bominfo['bomqty']}} {{$bominfo['bomuom']}}</td>
                      </tr>                    
                      <tr>
                         <td>Bom Status</td>
                         <td>{{($bominfo['bomstatus'] == '1')? 'Approved' : 'Not Approved'}}</td>
                      </tr>
                      <tr>
                         <td>Bom Remarks</td>
                         <td>{{$bominfo['bomremark'] }}</td>
                      </tr>
                      
                   </tbody>
                </table>                
              </div>
            </div>
         </div>
      </div>      
    </div>

    <!-- right panel -->
    <div class="col-md-4">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title">
               BOM Extra Infos
            </h3>
         </div>
         <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                   <strong><caption> </caption></strong>
                   <thead></thead>
                   <tbody>
                      <tr>
                         <td>Created by: </td>
                         <td>{{$bominfo['bomcuserid']}}</td>
                      </tr>
                      <tr>
                         <td>Created on: </td>
                         <td>{{$bominfo['bomcdate']}}</td>
                      </tr>
                      <tr>
                         <td>Modify by: </td>
                         <td>{{$bominfo['bommuserid']}}</td>
                      </tr>
                      <tr>
                         <td>Modify on: </td>
                         <td>{{$bominfo['bommdate']}}</td>
                      </tr>                         
                   </tbody>
                </table>                
              </div>
            </div>
         </div> <!-- end panel body -->
      </div>         
    </div> <!--end right panel -->
  </div> <!-- emd row -->

  <hr class="divider">

  <!-- list of item belongs to bom -->
  <div class="row">
    <div class="col-md-12">
      <h4> BOM Recipes </h4>
      <table class="table table-striped">
         <thead>
            <tr>
               <th>S.No</th>
               <th>Item No</th>
               <th>Qty</th>
               <th>UOM</th>
               <th>BOM Type</th>
            </tr>         
         </thead>
         <tbody>
            @foreach($bomitemdetails as $bomitem)
              <tr>
                 <td>{{$bomitem->s_no}}</td>
                 <td>{{$bomitem->item_no}}</td>
                 <td>{{$bomitem->qty}}</td>
                 <td>{{$bomitem->uom}}</td>
                 <td>{{($bomitem->bom_type == 'R') ? 'RAW' : 'BOM'}}</td>
              </tr>            
            @endforeach
         </tbody>
      </table>            
    </div>
  </div> <!-- end row -->

  <hr class="divider">
  
  <div class="row" style="">
    <div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 15px;">
      <div class="row">
        <a id="btn_bom_edit" href="/sdbom/bom/{{$bominforaw}}/edit" class="btn btn-primary col-md-3 pull-right" type="button" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Edit</a>

        <a id="btn_bom_back" class="btn btn-warning col-md-3 pull-right" type="button" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;" onclick="history.go(-1);">Back</a>
      </div>
    </div>
  </div>

</div>

@stop