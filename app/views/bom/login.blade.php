@extends('bom.master')

@section('title')
BOM Login
@stop

@section('content')

<div class="container" style="margin-top:100px;">

	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="well">
			{{ Form::open(array('url' => 'sdbom/login', 'class'=>'form-horizontal')) }}
			
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="/images/logo-kfc.png" alt="logo">
					</div>	
				</div>

				<h3 class="text-center">
					Bill of Material System
				</h3>
				
				<br>
					<p>
						{{ $errors->first('email') }}
						{{ $errors->first('password') }}
					</p>

					<div class="form-group">
						<label for="username" class="col-md-12">Username</label>
						<div class="col-md-12"><input type="text" class="form-control" name="username"/></div>
					</div>

					<div class="form-group">
						<label for="password" class="col-md-12">Password</label>
						<div class="col-md-12"><input type="password" class="form-control" name="password"/></div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
							<p>{{ Form::submit('Login', array('class'=>'btn btn-primary btn-block')) }}</p>					
						</div> 
					</div>
					<br>	
				
				<!-- if there are login errors, show them here -->
			
			{{ Form::close() }}	

			<div class="bom-login-msg">
				{{Session::get('message')}}
			</div>	

			</div>
		</div>
	</div>
</div>

@stop