@extends('bom.master')

@section('title')
BOM Index
@stop

@section('content')



<div id="wrapper">
<!--
<div class="navbar navbar-default navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid" style="text-align: center;">
      <a href="/sdbom/" class="brand" style="margin:0 auto; float: none;">
        <img alt="Test" src="/images/logo-konverge-30.png" style="height:24px; margin-top:10px;">    
      </a>
      <a class="brand" href="/logout" style="margin:0 auto; float: right;">
        <div class="btn btn-default" style="margin: 8px;">
          Logout <span class="fa fa-power-off"></span>
        </div>   
      </a>
      <div class="brand" style="margin:0 auto; float: left;">

      </div>
    </div>    
  </div>
</div>
-->

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="/sdbom/bom/" style="padding: 10px;">
      	<img alt="Test" src="/images/logo-konverge-30.png" style="height:28px;">    
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">
        <li class="active"> <a href="/sdbom/bom"> List Bom </a></li>
        <li class=""> <a href="/sdbom/bom/create"> New Bom </a></li>  	
      </ul>	
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{$username}} <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <div class="media">
			  <a class="pull-left" href="" style="padding-left: 5px;">
			    <img class="media-object" src="/images/user.png" alt="test" style="height:28px; width:28px;">
			  </a>
			  <div class="media-body">
			    <h5 class="media-heading">{{$username}}</h5>
			    <strong>Access Rights</strong><br>
			    Add: {{($acc_right['add'] == 1) ? 'YES':'NO' }}<br>
			    Update: {{($acc_right['upd'] == 1) ? 'YES':'NO' }}<br>
			    Delete: {{($acc_right['del'] == 1) ? 'YES':'NO' }}<br>
			    View: {{($acc_right['view'] == 1) ? 'YES':'NO' }}<br>
			  </div>
			</div>	
            </li>
            <li class="divider"></li>
            <li><a href="/sdbom/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid --> 
</nav>

<div class="container">
	<div class="row">
  		<div class="col-md-6 col-sm-5 col-xs-4 text-left" style="font-weight:bold; font-size:18px; margin-top: 3px;">BOM List</div>
      <div class="col-md-6 col-sm-7 col-xs-8 text-right">
        <div class="row">
          <div class="col-md-9 col-sm-9 col-xs-9">
            <div class='input-group'>
              <input type="text" name="bom_searchbox" id="input_item_search" class="form-control" value="" placeholder="Key in BOM Code to search">
              <span class="btn btn-primary input-group-addon" id="bom_searchbox"><i class="fa fa-search"></i></span>            
            </div>          
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3 text-right">
            <a href="/sdbom/bom/create">
                <div class="btn btn-primary btn-large">
                  <i class="fa fa-plus-circle"></i>
                </div>    
              </a>
          </div>

        </div>
  		</div>  	  		
	</div> <!--bom header -->
  	
  <hr>
	
  <div class="row">
		<div class="col-md-12">
			<ul class="list-group" id="bom_listings">
			  <!-- BOM List -->
        
        {{Session::get('message')}}
		  </ul>
		</div>
	</div>
</div>






@stop