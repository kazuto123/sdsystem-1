@extends('layouts.default-app')

@section('title')
	New TOrder
@stop

@section('content')
	@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/torder', 'link2'=>'/user/'.$userid.'/dashboard'])	
	<div class="row" style="margin-top:50px;">
		<div class="col-xs-3 text-left">
			
		</div>
		<div class="col-xs-6 text-center">
			<h4 class="header-text">New Order</h4>		
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
		</div>
	</div>
	
	@include('includes._divider')
	
	{{ Form::open(['route'=>'user.torder.store', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'order_form', 'id'=>'order_form']) }}
	<div class="row">
		<div class="col-md-12">
			<div id="error_code" class='alert alert-danger text-center'>
			</div>
		</div>
	</div>
	


	<div class="row" id="torder_details" >
		<div class="col-md-6">
			<input type="hidden" name="order_from" value="{{$user->id}}">
			<input type="hidden" name="cust_name" id="input_torder_cust_name" value="">
			<div class="form-group">
				<label for='cust_grp' class="col-md-3 control-label">Customer Group</label>
				<div class="col-md-9">
					<select name="cust_group" id="input_custgrp" class="form-control">
						<option value="None"> None </option>
						@foreach($cust_grp as $custgrp)
							<option value="{{$custgrp[0]}}"> {{$custgrp[0]}} - {{$custgrp[1]}} </option>
						@endforeach
					</select>	
				</div>
			</div>

			<div class="form-group">
				<label for='outlet_code' class="col-md-3 control-label">Outlet Code</label>
				<div class="col-md-9">
					<select name="outlet_code" id="input_outletcode" class="form-control">
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for='contact_person' class="col-md-3 control-label">Contact Person</label>
				<div class="col-md-9">
					<input type="text" id="input_contactperson" name="cont_person" class="form-control" required >
				</div>
			</div>

			<div class="form-group">
				<label for='cont_no' class="col-md-3 control-label">Contact Number</label>
				<div class="col-md-9">
					<input type="text" id="input_contactno" name="cont_no" class="form-control" required >
				</div>
			</div>

			<div class="form-group">
				<label for='delivery_addr' class="col-md-3 control-label">Delivery Address</label>
				<div class="col-md-9" id="input_deliveryaddr_parent">
					<select name="delivery_addr" id="input_deliveryaddr" class="form-control">
						<option value="NONE">NONE</option>
					</select>
					<input type="hidden" name='delivery_addr_text' id="input_delivery_addr_ref" value="">
				</div>
			</div>

		</div>


		<div class="col-md-6">

			<div class="form-group">
				<label for='delivery_driver' class="col-md-3 control-label">Delivery Driver</label>
				<div class="col-md-9">
					<select name="delivery_driver" id="input_deliverydriver" class="form-control">
						@foreach($drivers as $driver)
							<option value="{{$driver}}"> {{$driver}} </option>
						@endforeach
					</select>
				</div>
			</div>


			<div class="form-group">
				<label for='curr_code' class="col-md-3 control-label">Currency Code</label>
				<div class="col-md-9">
					<select name="curr_code" id="select_customer_curr_code" class="form-control">
						<option value="0">None</option>
						@foreach($curr_codes as $curr_code)
							<option value="{{$curr_code['curr']->value}}">{{$curr_code['curr']->value}} - {{$curr_code['curr']->descr}}</option>
						@endforeach
					</select>
				</div>
			</div>			


			<div class="form-group">
				<label for='curr_rate' class="col-md-3 control-label">Exchange Rate</label>
				<div class="col-md-9">
					<input type="text" id="input_curr_rate" name="curr_rate" class="form-control" required readonly>
				</div>
			</div>


			<div class="form-group">
				<label for='delivery_datetime' class="col-md-3 control-label">Delivery Datetime</label>
				<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
						<input id="delivery_date" type="text" name="delivery_date" class="form-control form_datetime" placeholder="Click to input date and time" style="" required/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> 
						<select id="delivery_time" name="delivery_time" class="form-control" placeholder="Choose a time">
							@foreach($timeslots as $timeslot)
								<option value="{{$timeslot->value}}">{{$timeslot->value}}</option>
							@endforeach
						</select>						
					</div>
				</div>	
			</div>
			
			<div class="form-group">
				<label for="order_remark" class="col-md-3 control-label"> 
					Remarks:
				</label>
				<div class="col-md-9">
					<textarea name="order_remark" class="form-control" rows="3"></textarea>
				</div>
			</div>

		</div>
	</div> <!-- end of form details -->
	<div class="row"><div id="btn_hidedetail" class="col-md-12 btn btn-xs btn-block fa fa-angle-double-up"></div></div>
	
	@include('includes._divider')

	<div class="row">
		<!-- Additional Order Panel -->
		<div class="col-md-6">
			<h3 class="header-text">Additional Order</h3>
			<div class="row">
				<div class="col-md-12">
					@foreach($cats as $cat)
						<div id="{{ $cat->sub_cat }}" class="cat-button" title="{{$cat->descr}}">
							{{ $cat->sub_cat }}
						</div>
					@endforeach	
				</div>
			</div>
			
			<br>
			
			<div class="row">
				<div class="col-md-12">
					<h6 id="search_header"></h6>
					<div class="row">
						<div class="col-md-12">
							<div id="error_item" class='alert alert-danger text-center'></div>
							
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12">
							
							<div id="msg_success" class='alert alert-success text-center'></div>
						</div>
					</div>											
				
					<ul id="cat_itemlist" class="list-group all_item_list" style="height: 240px; overflow: auto; margin-top: 3px;">
						@foreach($items as $item)
							<li class="list-group-item list-item-add cat-item {{ $item->uom }} {{$item->cat_code}}">
								<span>
								{{ $item->item_no }} &nbsp&nbsp {{$item->item_descr}}
								<div id="{{ $item->id }}" class="btn btn-sm btn-primary btn-item-add fa fa-plus-circle" style="float:right;"></div>
								</span>
							</li>
						@endforeach 
					</ul>
				</div>	
			</div>
		</div>


		<!-- ordered item panel and summerize payment -->
		<div class="col-md-6">
			<h3 class="header-text">Ordered Items</h3>
			<div class="row">
				<div class="col-md-12">
					
						<div id="ordered_itemlist" class="oded_itemlist" style="height: 460px; overflow: auto; margin-top: 3px;">
							<!-- Ordered Item List -->
						</div>

				</div>
			</div>
			@include('includes._divider')

			<div class="row">
				<div class="col-md-offset-6 col-md-6">
					<!-- form sub total -->
					<div class="form-group">
						<label for="customerGroup" class="col-md-5 control-label">Sub Total</label>
						<div class="col-md-7">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" id="order_subtotal" name="order_subtotal" class="form-control input-sm text-right" placeholder="Subtotal" value="0.00" readonly>
							</div>
						</div>
					</div>
					<!-- form gst amount -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Add GST 7%:</label>
					    <div class="col-md-7">
					    	<div class="input-group">
					    		<span class="input-group-addon">$</span>
					    		<input type="text" id="order_gst" name="order_gst" class="form-control input-sm text-right" placeholder="GST" value="0.00" readonly>		
					    	</div>
					    
					    </div>
					</div>
					<!-- form total -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Total</label>
					    <div class="col-md-7">
					    	<div class="input-group">
						    	<span class="input-group-addon">$</span>
						    	<input type="text" id="order_total" name="order_total" class="form-control input-sm text-right" placeholder="Total" value="0.00" readonly>
					    	</div>
							
					    </div>
					</div>
				</div>
			</div>

		</div>	

	</div>
	
	@include('includes._divider')

	<div class="row" style="">
		<div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 5px;">
			<div class="row">
				{{ Form::submit('Submit', ['class'=>'btn btn-primary col-md-3 pull-right', 'style'=>'margin-top:3px'] )}}
				<button class="btn btn-warning col-md-3 pull-right" type="button" data-toggle="modal" data-target="#clearformModal" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Clear</button>
				<button class="btn btn-danger col-md-3 pull-right" type="button" data-toggle="modal" data-target="#cancelformModal" style="margin:3px;">Cancel</button> 
			</div>
		</div>
	</div>

	
	{{ Form::close() }}
	<!-- Division for the clear order element-->
	<div class="row">
		<div class="modal fade" id="clearformModal" tabindex="-1" role="dialog" aria-labelledby="clearformModalLabel" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="clearformModalLabel">Clear order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to clear this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/torder/create" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>

	<!-- Division for the cancel order element-->
	<div class="row">
		<div class="modal fade" id="cancelformModal" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title">Cancel order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to cancel this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/torder" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>



@stop