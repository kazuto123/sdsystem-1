<div class="form-group">
    <label for="{{$func_id}}" class="col-md-2 control-label">{{$label}}</label>
    <div class="input-group date form_datetime col-md-5" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="{{$func_id}}">
        <input class="form-control" size="16" type="text" value="" readonly>
		<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
		<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>	        
    </div>
	<input type="hidden" id="{{$func_id}}" value="" /><br/>
</div>
