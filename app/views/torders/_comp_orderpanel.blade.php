<div class="row">
    <div class="panel panel-default" style="margin-bottom:5px;">
      <div class="panel-body">
            <div class="col-md-1">{{ $odr->id }}</div>
            <div class="col-md-1">{{ $odr->torder_from }}</div>
            <div class="col-md-1">{{ $odr->torder_to }}</div>
            <div class="col-md-4">{{ $odr->delivery_addr }}</div>
            <div class="col-md-1">{{ $odr->delivery_driver }}</div>
            <div class="col-md-2">{{ $odr->delivery_datetime }}</div>
            <div class="col-md-1">${{ $odr->total }}</div>
            <div class="col-md-1">
                <div class="row" style="float:right;">
                    <div class="btn-group">
                        <a href="/user/{{ $userid }}/torder/{{ $odr->id }}" class="btn btn-info fa fa-file-text-o"></a>
                        <a href="/user/{{ $userid }}/torder/{{ $odr->id }}/edit" class="btn btn-info fa fa-edit"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
