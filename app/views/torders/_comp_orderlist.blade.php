<li class="list-group-item">
    <div class="row">
        <a href="/user/{{$userid}}/torder/{{$odr->id}}" class="col-xs-12">
            <div class="col-xs-12">
                <span style="padding-right: 2px;" class="col-xs-12 col-md-4">
                    {{ $odr->torder_to }} - {{ Customer::where('cust_no', '=', $odr->torder_to)->first()->cust_name }}
                </span>
                <span style="padding-right: 2px;" class="col-xs-12 col-md-2">{{ $odr->cont_person }}</span>
                <span style="padding-right: 2px;" class="col-xs-12 col-md-4">{{ $odr->delivery_addr }}</span>
                <span style="padding-right: 2px;" class="col-xs-12 col-md-2 text-right">
                    {{ $odr->delivery_date . ' ' . $odr->delivery_time }}
                </span>
            </div>
        </a>
    </div>
</li>
