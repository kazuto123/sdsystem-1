@extends('layouts.default-app')

@section('title')
	Order Details
@stop

@section('content')
	
	@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/torder', 'link2'=>'/user/'.$userid.'/dashboard'])	

	<div class="row" style="margin-top: 50px;">
		<div class="col-xs-3 text-left">
		</div>
		<div class="col-xs-6 text-center">
			<h3 class="header-text" >Order Details</h3>		
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
			<!-- {{ HTML::image('images/logo-company.png', 'test', ['class'=>'img-responsive']) }} -->
		</div>
	</div>

@include('includes._divider')
<div class="col-md-2"></div>
<div class="col-md-8">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">
						Order Number: {{$torder->id}}
						<!-- <a class="pull-right btn btn-xs btn-link" href="/user/{{$userid}}/torder/{{$torder->id}}/xml">XML</a> -->
					</h3>
				</div>
				<div class="panel-body" style="padding: 15px;">
					@include('torders._comp_label', ['label'=>'Customer Group', 'value'=> $torder->cust_grp])
					@include('torders._comp_label', ['label'=>'Outlet Code', 'value'=> $torder->torder_to])
					@include('torders._comp_label', ['label'=>'Contact Person', 'value'=> $torder->cont_person])
					@include('torders._comp_label', ['label'=>'Contact Number', 'value'=> $torder->cont_no])
					@include('torders._comp_label', ['label'=>'Delivery Address', 'value'=> $torder->delivery_addr_text])
					@include('torders._comp_label', ['label'=>'Designated Driver', 'value'=> $torder->delivery_driver])
					@include('torders._comp_label', ['label'=>'Tax Type', 'value'=> $torder->tax_type])
					@include('torders._comp_label', ['label'=>'Currency Code', 'value'=> $torder->curr_code])
					@include('torders._comp_label', ['label'=>'Exchange Rate', 'value'=> $torder->curr_rate])
					@include('torders._comp_label', ['label'=>'Delivery Date', 'value'=> $torder->delivery_date])
					@include('torders._comp_label', ['label'=>'Delivery Time', 'value'=> $torder->delivery_time])
					@include('torders._comp_label', ['label'=>'Remarks', 'value'=> $torder->remark])									
				</div>
			</div>

		</div>
		<div class="col-md-1">
		</div>
	</div>
	
	@include('includes._divider')
	
	<div class="row">
		<div>
			
		</div>
		<table class="table table-striped">
	      <thead>
	        <tr>
	          <th>Item No</th>
	          <th>Item Desc</th>
	          <th>Qty</th>
	          <th>Uom</th>
	          <th>Unit Price</th>
	          <th>Total</th>
	        </tr>
	      </thead>
	      <tbody>
			@foreach($torder_items as $oditem)
				<tr>
				    <td>{{ $oditem['item']['item_no'] }}</td>
				    <td>{{ $oditem['descr'] }}</td>
				    <td>{{ $oditem['item']['qty'] }}</td>
				    <td>{{ $oditem['item']['uom'] }}</td>
				    <td>{{ $oditem['item']['unitprice'] }}</td>
				    <td>{{ $oditem['item']['totalamt'] }}</td>
				</tr>
			@endforeach
	      </tbody>
	    </table>
	</div>

	@include('includes._divider')

	<div class="row">
		<div class="col-md-3 col-md-offset-9">
			<div class="row">
				<div class="col-md-12">Subtotal <span style="float:right">${{ $torder->subtotal }}</span></div>
			</div>
			
			<div class="row">
				<div class="col-md-12">GST <span style="float:right">${{ $torder->gst }}</span></div>
			</div>
			
			<div class="row">
				<div class="col-md-12">Total <span style="float:right">${{ $torder->total }}</span></div>
			</div>
			
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-8">
			<div class="row">
				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="javascript:window.print()" class="btn btn-primary btn-block">Print</a></div>

				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#delModal">Delete</a></div>
				<!-- Modal -->
				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="/user/{{$userid}}/torder/{{$torderid}}/edit" class="btn btn-warning btn-block">Edit</a></div>

				<!-- Division for the delete order element-->
				<div class="row">
					<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" style="width:auto; margin:auto;">
					    <div class="modal-content" style=";">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" id="delModalLabel">Delete Order?</h4>
					      </div>
					      <div class="modal-body">
					        Are you sure you want to delete this order?
					      </div>
					      <div class="modal-footer">
							{{ Form::open(array('url' => 'user/' . $userid . '/torder/' . $torderid) ) }}
								{{ Form::hidden('_method', 'DELETE') }}
								<a class="btn btn-default" data-dismiss="modal">Close</a>
								{{ Form::submit('Delete', array('class' => 'btn btn-danger btn-del-order-confirm')) }}
							{{ Form::close() }}
					      </div>
					    </div>
					  </div>
					</div>		
				</div> <!-- end row -->

			</div>
		</div>
	</div>				
	@include('includes._divider')

	
</div>
<div class="col-md-2"></div>


@stop