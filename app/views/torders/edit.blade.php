@extends('layouts.default-app')

@section('title')
	Edit Order
@stop

@section('content')
	@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$user.'/torder', 'link2'=>'/user/'.$user.'/dashboard'])
	<div class="row">
		<div class="col-xs-3 text-left">
			<h3><a href="/user/{{$user}}/torder" class="fa fa-chevron-left"></a></h3>	
		</div>
		<div class="col-xs-6 text-center">
			<h3 class="header-text">Edit Order</h3>		
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
		</div>
	</div>
	
	@include('includes._divider')
	
	{{ Form::open(['route'=>'user.torder.update', 'method'=>'PUT', 'class'=>'form-horizontal', 'name'=>'order_form']) }}
	<div class="row">
		<div class="col-md-12">
			<div id="error_code" class='alert alert-danger text-center'>
			</div>
		</div>
	</div>
	<input type="hidden" name="order_id" value="{{$orderdetail->id}}">
	<div class="row" id="torder_details" >
		<div class="col-md-6">
			<div class="hidden-input"> <input type="hidden" name="order_from" value="{{$orderdetail->torder_from}}"></div>
			<div class="form-group">
				<label for='cust_grp' class="col-md-3 control-label"> Customer Group</label>
				<div class="col-md-9">
					<select name="cust_group" id="input_custgrp" class="form-control">
						@foreach($cust_grp as $custgrp)
							@if($custgrp == $orderdetail->cust_grp)
								<option value="{{$custgrp}}" selected> {{$custgrp}} </option>
							@else
								<option value="{{$custgrp}}"> {{$custgrp}} </option>
							@endif
						@endforeach
					</select>	
				</div>
			</div>

			<div class="form-group">
				<label for='outlet_code' class="col-md-3 control-label">Outlet Code</label>
				<div class="col-md-9">
					<select name="outlet_code" id="input_outletcode" class="form-control">
						@foreach($outlets as $outlet)
							@if($outlet['code'] == $orderdetail->torder_to)
								<option value="{{$outlet['code']}}" selected> {{$outlet['code']}} - {{$outlet['name']}}</option>
							@else
								<option value="{{$outlet['code']}}"> {{$outlet['code']}} - {{$outlet['name']}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for='cont_person' class="col-md-3 control-label">Contact Person</label>
				<div class="col-md-9">
					<input id="input_contactperson" type="text" name="cont_person" class="form-control" value="{{$orderdetail->cont_person}}" required >	
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('cont_no', 'Contact Number', ['class'=>'col-md-3 control-label']) }}
				<div class="col-md-9">
					<input id="input_contactno" type="text" name="cont_no" class="form-control" value="{{$orderdetail->cont_no}}" required>
				</div>
			</div>	

			<div class="form-group">
				<label for='delivery_addr' class="col-md-3 control-label">Delivery Address</label>
				<div class="col-md-9" id="input_deliveryaddr_parent">
					<select name="delivery_addr" id="input_deliveryaddr" class="form-control">
						@foreach($address as $addr)
						@if($addr->addr_ref == $orderdetail->delivery_addr)
							<option value="{{$addr->addr_ref}}" selected> {{$addr->addr}} </option>
						@else
							<option value="{{$addr->addr_ref}}"> {{$addr->addr}} </option>
						@endif
						@endforeach
					</select>
				</div>
			</div>			
					
		</div>


		<div class="col-md-6">

			<div class="form-group">
				<label for='curr_code' class="col-md-3 control-label">Currency Code</label>
				<div class="col-md-9">
					<select name="curr_code" id="select_customer_curr_code" class="form-control">
						<option value="0">None</option>
						@foreach($curr_codes as $curr_code)
							@if($curr_code['curr']->value == $orderdetail->curr_code)
								<option value="{{$curr_code['curr']->value}}" selected>{{$curr_code['curr']->value}} - {{$curr_code['curr']->descr}}</option>
							@else
								<option value="{{$curr_code['curr']->value}}">{{$curr_code['curr']->value}} - {{$curr_code['curr']->descr}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>			


			<div class="form-group">
				<label for='curr_rate' class="col-md-3 control-label">Exchange Rate</label>
				<div class="col-md-9">
					<input type="text" id="input_curr_rate" name="curr_rate" class="form-control" value="{{$orderdetail->curr_rate}}" required readonly>
				</div>
			</div>


			<div class="form-group">
				<label for='delivery_driver' class="col-md-3 control-label">Delivery Driver</label>
				<div class="col-md-9 col-xs-12">
					<select name="delivery_driver" id="input_deliverydriver" class="form-control">
						@foreach($drivers as $driver)
							@if($driver == $orderdetail->delivery_driver)
								<option value="{{$driver}}" selected> {{$driver}} </option>
							@else
								<option value="{{$driver}}"> {{$driver}} </option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for='delivery_datetime' class="col-md-3 control-label">Delivery Datetime</label>
				<div class="col-md-5">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
						<input id="delivery_date" type="text" name="delivery_date" class="form-control form_datetime" placeholder="Click to input date and time" style="" value="{{$orderdetail->delivery_date}}" required/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span> 
						<select id="delivery_time" name="delivery_time" class="form-control" placeholder="Choose a time">
							@foreach($timeslots as $timeslot)
								@if($timeslot->value == $orderdetail->delivery_time)
									<option value="{{$timeslot->value}}" selected> {{$timeslot->value}} <option>
								@else
									<option value="{{$timeslot->value}}"> {{$timeslot->value}} <option>
								@endif
							@endforeach
						</select>						
					</div>
				</div>	
			</div>

			
			<div class="form-group">
				<label for="order_remark" class="col-md-3 control-label"> 
					Remarks:
				</label>
				<div class="col-md-9">
					<textarea name="order_remark" class="form-control" rows="3">{{$orderdetail->remark}}</textarea>
				</div>
			</div>

		</div>
	</div> <!-- end of form details -->

	<div class="row"><div id="btn_hidedetail" class="col-md-12 btn btn-xs btn-block fa fa-angle-double-up"></div></div>
	
	@include('includes._divider')

	<div class="row">
		<!-- Additional Order Panel -->
		<div class="col-md-6">
			<h3 class="header-text">Additional Order</h3>
			<div class="row">
				<div class="col-md-12">
					@foreach($cats as $cat)
						<div id="{{ $cat->sub_cat }}" class="cat-button" title="{{$cat->descr}}">
								{{ $cat->sub_cat }}
						</div>
					@endforeach	
				</div>
			</div>
			
			<br>
			
			<div class="row">
				<div class="col-md-12">
					<h6 id="search_header"></h6>
					<div class="row"><div class="col-md-12">
						<div id="error_item" class='alert alert-danger text-center'>
					</div></div>
					</div>					
					<ul id="cat_itemlist" class="list-group all_item_list" style="height: 240px; overflow: auto; margin-top: 3px;">
						@foreach($items as $item)
							<li class="list-group-item list-item-add cat-item {{ $item->uom }} {{$item->cat_code}}"> 
								<span>
								{{ $item->item_no }} &nbsp&nbsp {{$item->item_descr}}
								<div id="{{ $item->id }}" class="btn btn-sm btn-primary btn-item-add fa fa-plus-circle" style="float:right;"></div>
								</span>
							</li>
						@endforeach 
					</ul>
				</div>	
			</div>
		</div>


		<!-- ordered item panel and summerize payment -->
		<div class="col-md-6">
			<h3 class="header-text">Ordered Items</h3>
			<div class="row">
				<div class="col-md-12">
					<div id="ordered_itemlist" class="oded_itemlist" style="height: 470px; overflow: auto; margin-top: 3px;">
						<!-- Ordered Item List -->
						@foreach($ordereditems as $oditem)
							<li class="list-group-item item-{{$oditem['details']['item_id']}}">
								<div class="form-group row ordered_item">
									<div class="col-xs-12" style="margin-bottom:5px;">
										<div class="row">
											<div class="col-xs-1" style="padding-right: 2px;"> 
												<label class="control-label" for="oditem_{{$oditem['details']['item_id']}}_itemno">{{$oditem['details']['item_no']}}
											</div>
											<div class="col-xs-11"><label class="control-label" >{{ $oditem['details']['item_desc'] }}</label></div>
											<input type="hidden" name="oditem_{{$oditem['details']['item_id']}}_itemno" value="{{$oditem['details']['item_no']}}"/>
											<input type="hidden" name="oditem_{{$oditem['details']['item_id']}}_itemid" value="{{$oditem['details']['item_id']}}"/>
										</div>
									</div>
									<div class="col-xs-2" style="padding-right: 2px;"> 
										<input id="{{$oditem['details']['item_id']}}" class="form-control input-sm input-qty qty_{{$oditem['details']['item_id']}}" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_{{$oditem['details']['item_id']}}_qty" placeholder="Qty" value="{{$oditem['details']['qty']}}" required>
									</div>
									<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;"> 
										<select id="{{$oditem['details']['comp_code']}}_{{$oditem['details']['item_no']}}_{{$orderdetail['details']['cust_grp']}}_{{$orderdetail['details']['torder_to']}}" name="oditem_{{$oditem['details']['item_id']}}_uom" class="form-control input-sm opt-uom">
											@foreach($oditem['uom'] as $ituom)
												@if($ituom['uom'] == $oditem['details']['uom'])
													<option value="{{$ituom['uom']}}" selected>{{$ituom['uom']}}</option>
												@else
													<option value="{{$ituom['uom']}}">{{$ituom['uom']}}</option>
												@endif
											@endforeach
										</select>
									</div>
									
									<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> 
										<div class="input-group"> 
											<span class="input-group-addon">$</span> 
											<input class="text-right form-control input-sm input-unitprice unp_{{$oditem['details']['item_id']}}" type="text" name="oditem_{{$oditem['details']['item_id']}}_unitprice" placeholder="@Unit Price" value="{{$oditem['details']['unitprice']}}" readonly></input> 
										</div>
									</div>
									<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> 
										<div class="input-group"> 
											<span class="input-group-addon">$</span> 
											<input class="text-right form-control input-sm input-total tot_{{$oditem['details']['item_id']}}" type="text" name="oditem_{{$oditem['details']['item_id']}}_total" placeholder="Total" value="{{$oditem['details']['totalamt']}}" readonly>
										</div> 
									</div>
									<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;"> 
										<a id="{{$oditem['details']['item_id']}}" class="btn btn-danger fa fa-minus-circle btn-del-item"></a>
									</div>
								</div>
							</li>						

						@endforeach
					</div>
				</div>
			</div>
			@include('includes._divider')

			<div class="row">
				<div class="col-md-offset-6 col-md-6">
					<!-- form sub total -->
					<div class="form-group">
						<label for="customerGroup" class="col-md-5 control-label">Sub Total</label>
						<div class="col-md-7">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input id="order_subtotal" name="order_subtotal" type="text" class="text-right form-control" value="{{$orderdetail->subtotal}}" readonly>
							</div>
							
						</div>
					</div>
					<!-- form gst amount -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Add GST 7%:</label>
					    <div class="col-md-7">
					    	<div class="input-group">
					    		<span class="input-group-addon">$</span>
					    		<input id="order_gst" name="order_gst" type="text" class="text-right form-control" value="{{$orderdetail->gst}}" readonly>
					    	</div>
					    
					    </div>
					</div>
					<!-- form total -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Total</label>
					    <div class="col-md-7">
					    	<div class="input-group">
						    	<span class="input-group-addon">$</span>
						    	<input id="order_total" name="order_total" type="text" class="text-right form-control" value="{{$orderdetail->total}}" readonly>
					    	</div>
					    </div>
					</div>

				</div>
			</div>
		</div>	

	</div>
	
	@include('includes._divider')

	<div class="row" style="">
		<div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 5px;">
			<div class="row">
				{{ Form::submit('Update', ['class'=>'btn btn-primary col-md-3 pull-right', 'style'=>'margin:3px'] )}}
				<button class="btn btn-danger col-md-3 pull-right" type="button" data-toggle="modal" data-target="#cancelformModal" style="margin:3px;">Cancel</button> 
				
			</div>
		</div>
	</div>

	
	{{ Form::close() }}

	<!-- Division for the cancel order element-->
	<div class="row">
		<div class="modal fade" id="cancelformModal" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title">Cancel editting?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to cancel editting?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$user}}/torder" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>


@stop