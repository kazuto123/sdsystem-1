@extends('layouts.default-app')

@section('title')
	Order Taken List
@stop

@section('content')
	@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/dashboard', 'link2'=>'/user/'.$userid.'/dashboard'])	

	<div class="row" style="margin-top:50px;">
		<div class="col-md-4 col-sm-4 col-xs-4 text-left" style="margin-top: 14px;">
			{{ Form::open(['method'=>'GET', 'class'=>'form-horizontal', 'name'=>'order_form', 'id'=>'order_form']) }}
				{{ Form::input('search', 'q', null, ['class'=>'form-control', 'placeholder'=>'Enter Search Term']) }}
			{{ Form::close() }}
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-center" style="margin-top:8px;">
			<a href="/user/{{$userid}}/torder"><h4 class="header-text">Order List</h4></a>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-right" style="margin-top: 10px;">
			<div class="row">
				<div class="col-md-10 col-sm-9 hidden-xs" style="margin-top:4px;">
					<select name="sortby" id="select_sortby" class="form-control" onChange="this.form.submit();">
						<option value="None"> None </option>
						@foreach($filterby as $filter)
							<option value="{{$filter->value}}"> {{$filter->descr}} </option>
						@endforeach
					</select>	
				</div>
				<div class="col-md-2 col-sm-3">
					<a href="/user/{{ $userid }}/torder/create" class="fa fa-plus-circle btn btn-lg btn-info btn-create-order" data-content="Create New Order" rel="popover" data-placement="bottom"></a>
					
				</div>
			</div>
		</div>
	</div>	
	
	@include('includes._divider')
		
	<div class="row">
		<div class="col-md-12">
			<ul class="list-group">
			@foreach($allorder as $order)
				<li class="list-group-item">
				    <div class="row">
				        <a href="/user/{{$userid}}/torder/{{$order->id}}" class="col-xs-12">
				            <div class="col-xs-12">
				                <span style="padding-right: 2px;" class="col-xs-12 col-md-4">
				                    {{ $order->torder_to }} - {{ Customer::where('cust_no', '=', $order->torder_to)->first()->cust_name }}
				                </span>
				                <span style="padding-right: 2px;" class="col-xs-12 col-md-1">{{ $order->delivery_driver }}</span>
				                <span style="padding-right: 2px;" class="col-xs-12 col-md-3">{{ $order->delivery_addr_text }}</span>
				                <span style="padding-right: 2px;" class="col-xs-12 col-md-2">${{ $order->total }}</span>
				                <span style="padding-right: 2px;" class="col-xs-12 col-md-2 text-right">
				                    {{ $order->delivery_date . ' ' . $order->delivery_time }}
				                </span>

				            </div>
				        </a>
				    </div>
				</li>
			@endforeach
			{{$allorder->links();}}

			</div>

		</ul>

	</div>
	

@stop