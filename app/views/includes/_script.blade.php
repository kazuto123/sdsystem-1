
<!-- Scripts are placed here -->
{{ HTML::script('packages/js/jquery-2.1.0.js')}}
{{ HTML::script('packages/js/jquery.validate.min.js')}}
{{ HTML::script('packages/js/moment.js')}}
{{ HTML::script('packages/js/bootstrap.js')}}
{{ HTML::script('packages/js/bootstrap-modalmanager.js')}}
{{ HTML::script('packages/js/bootstrap-modal.js')}}
{{ HTML::script('packages/js/bootstrap-select.min.js')}}
{{ HTML::script('packages/js/bootstrap-datetimepicker.min.js')}}
{{ HTML::script('packages/js/locales/bootstrap-datetimepicker.uk.js')}}

{{ HTML::script('js/app.js')}}