<div class="navbar navbar-default navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid" style="text-align: center;">
      <a href="{{$link2}}" class="brand" style="margin:0 auto; float: none;">
        <img alt="Test" src="/images/logo-konverge.png" style="height:24px; margin-top:10px;">    
      </a>
      <a class="brand" href='/user/logout' style="margin:0 auto; float: right;">
        <div class="btn btn-default" style="margin: 8px;">
          Logout <span class="fa fa-power-off"></span>
        </div>   
      </a>
      <a href="{{$link}}" class="brand" style="margin:0 auto; float: left;">
        <div class="btn btn-default" style="margin: 8px;">
          {{$button_left}}
        </div>      
      </a>
    </div>    
  </div>

</div>