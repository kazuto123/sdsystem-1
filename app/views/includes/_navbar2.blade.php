<!-- Navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navhead">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">
         <div class="img-banner">
          {{ HTML::image('images/logo-company.png', 'test') }}
         </div>
      </div>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navhead">
      {{ Menu::handler('main')->render() }}
          
      <!-- Right Panel -->
      <ul class="nav navbar-nav navbar-right" style="margin-right: 10px;float:right;">
        @if($user)
          <p class="navbar-text navbar-right">Hi, <strong>{{ $user->username }}</strong> 
            <a href="{{ URL::to('/user/logout') }}" class="navbar-link">Logout?</a></p>
        @else
          <p class="navbar-text navbar-right">User Not Login</p>
        @endif

      </ul> 
    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>