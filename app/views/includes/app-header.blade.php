<div class="row">
	<div class="col-xs-3 text-left">
		<h3><a href="{{ $targeturl }}" class="fa fa-chevron-left"></a></h3>	
	</div>
	<!-- javascript:history.go(-1) -->
	<div class="col-xs-6 text-center">
		<h3 style="color: #fff; text-shadow: 1px 1px 1px #999;">{{ $htitle }}</h3>		
	</div>
	<div class="col-xs-3 text-right" style="margin-top: 10px;">
		@if($test==1)
			<a id="create_order" href="/user/{{ $userid }}/torder/create" class="fa fa-plus-circle btn btn-lg btn-info" data-content="Create New Order" rel="popover" data-placement="bottom"></a>
		@elseif($test==2)
          	{{ HTML::image('images/logo-company.png', 'test', ['class'=>'img-responsive']) }}
		@else
			
		@endif
	</div>
</div>
