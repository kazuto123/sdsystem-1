<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS are placed here -->
{{ HTML::style('packages/css/bootstrap.css') }} 
{{ HTML::style('packages/css/bootstrap-modal-bs3patch.css') }}
{{ HTML::style('packages/css/bootstrap-modal.css') }}
{{ HTML::style('packages/css/bootstrap-datetimepicker.css')}}
{{ HTML::style('packages/css/bootstrap-select.min.css')}}
{{ HTML::style('packages/css/font-awesome.min.css') }}
{{ HTML::style('packages/css/docs.min.css') }}
{{ HTML::style('css/app.css') }}


