@extends('stock.master')

@section('title')
Stock Taking Application
@stop

@section('content')



<div id="wrapper">

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="/sdstock/warehouse/" style="padding: 10px;">
      	<img alt="Test" src="/images/logo-konverge-30.png" style="height:28px;">    
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">

      </ul>	
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{$username}} <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li>
              <div class="media">
        			  <a class="pull-left" href="" style="padding-left: 5px;">
        			    <img class="media-object" src="/images/user.png" alt="test" style="height:28px; width:28px;">
        			  </a>
        			  <div class="media-body">
        			    <h5 class="media-heading">{{$username}}</h5>
        			    
                </div>
    			    </div>	
            </li>
            <li class="divider"></li>
            <li><a href="/sdstock/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid --> 
</nav>

<div class="container">

  {{ Form::open(['url'=>'/sdstock/warehouse', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'sto_form', 'id'=>'sto_form']) }}
	<div class="row">
  		<div id="label_warehouse_name" class="col-md-6 col-sm-6 col-xs-12 text-left" style="font-weight:bold; font-size:18px; margin-top: 3px;">Warehouse Name</div>
      <div class="col-md-6 col-sm-6 col-xs-12 text-right">
        <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12">            
            <select name="warehouse_name" class="form-control" id="select_warehouses">
              <option value="none">Select Warehouses</option>
              @foreach($warehouses as $wh)
                <option value="{{$wh->wh_id}}-{{$wh->wh_code}}-{{$wh->wh_name}}">{{$wh->wh_name}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <input id="select_stocktake_date" type="text" name="stocktake_date" class="form-control form_datetime" placeholder="Choose Date" style="" required/>           
          </div>

          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="btn btn-primary" id="btn-get-warehouse">Get Warehouse</div>
          </div>

          <div class='col-md-1 col-sm-1 col-xs-1'>
            <div class="btn btn-primary btn-large" id="btn_newstoitem">
              <i class="fa fa-plus-circle"></i>
            </div>    
          </div>

        </div> <!-- Row -->
  		</div> <!-- col-12 -->
	</div> <!-- header -->
  	
  <hr class="divider">  

  <div class="row" style="height: 400px;">
		<div class="col-md-12">
			<ul class="list-group" id="whitem_listings">
         {{Session::get('message')}}
          <div class="progress progress-striped">
             <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progress">
             </div>
          </div>      
		  </ul>
		</div>
	</div>

  <div class="row" style="">
    <div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 15px;">
      <div class="row">
        {{ Form::submit('Submit', ['class'=>'btn btn-primary col-md-3 pull-right disabled', 'style'=>'margin-top:3px', 'id'=>'btn_sto_submit'] )}}
        <button id="btn_sto_reset" class="btn btn-danger col-md-3 pull-right disabled" type="button" data-toggle="modal" data-target="#clearformModal" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Reset</button>
      </div>
    </div>
  </div>

  {{Form::close()}}
</div> <!-- end container -->

  <!-- Division for the clear order element-->
  <div class="row">
    <div class="modal fade" id="clearformModal" tabindex="-1" role="dialog" aria-labelledby="clearformModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width:auto; margin:auto;">
        <div class="modal-content" style=";">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="clearformModalLabel">Clear order form?</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to reset this new form?
          </div>
          <div class="modal-footer">
        <a class="btn btn-default" data-dismiss="modal">Close</a>
        <a href="/sdstock/warehouse" class="btn btn-danger">Reset</a>
          </div>
        </div>
      </div>
    </div>    
  </div>


@stop