<!DOCTYPE html>
<html>
<head>
<title> @yield('title') </title>
@section('csslinks')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS are placed here -->


{{ HTML::style('packages/themes/sdsystem.min.css') }}
{{ HTML::style('packages/themes/jquery.mobile.structure-1.4.2.min.css') }}
{{ HTML::style('packages/themes/jquery.mobile.icons.min.css') }}

{{ HTML::style('packages/css/jquery.mobile.datepicker.css') }}

{{ HTML::style('packages/css/font-awesome.min.css') }}
{{ HTML::style('packages/css/docs.min.css') }}

@show

</head>

<body>

@yield('content')    


<!-- All Created JS script goes here -->
@section('scripts')
    <!-- Scripts are placed here -->
    {{ HTML::script('packages/js/jquery-2.1.0.js')}}
    {{ HTML::script('packages/js/jquery.validate.min.js')}}
    {{ HTML::script('packages/themes/jquery.mobile-1.4.2.min.js')}}

    
    {{ HTML::script('packages/js/jquery.ui.datepicker.js')}}
    {{ HTML::script('packages/js/jquery.mobile.datepicker.js')}}
    {{ HTML::script('packages/js/moment.js')}}
    {{ HTML::script('packages/js/holder.js')}}
    {{ HTML::script('js/app-to.js')}}
@show
</body>
</html>