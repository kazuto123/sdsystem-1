<!DOCTYPE html>
<html>
<head>
    <title> @yield('title') </title>
    @section('csslinks')
        @include('includes._head')
    @show
</head>

<body>
    <!-- Content's Container -->
    <div>
        @yield('navbar')
    </div>

    <div class="container">
        @yield('content')
    </div>
    <!-- All Created JS script goes here -->
    @section('scripts')
        @include('includes._script')
    @show
</body>
</html>