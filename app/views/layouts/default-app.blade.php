<!DOCTYPE html>
<html>
<head>
    <title> @yield('title') </title>
    @section('csslinks')
        @include('includes._head')
    @show
</head>

<body>

    <!-- Navigation Menu -->
    <div>
        @yield('navbar')
    </div>

    <!-- Content's Container -->
    <div class="container">
        @yield('content')
    </div>
    
    <!-- All Created JS script goes here -->
    @section('scripts')
        @include('includes._script')
    @show
</body>
</html>