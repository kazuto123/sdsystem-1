@extends('layouts.jqm-default')

@section('content')
<div data-role="page" data-theme="{{$theme}}">
	<div data-role="header" data-position="fixed" >
		<h1><img src="/images/logo-konverge-30.png" alt="logo"></h1>
	</div>




  <div data-role="main" class="ui-content">
    <h3>Slowly resize the width of your browser window. The layout will adjust itself to fit the "new" width of the browser.</h3>
    <div class="ui-grid-a ui-responsive">
      <div class="ui-block-a">
      	<div class="ui-grid-a ui-responsive">
      		<div class="ui-block-a">
		        <a href="#" class="ui-btn ui-corner-all ui-shadow">First Column Button</a><br>
		        <span>First Column: This is some text. This is some text. This is some text. This is some text. This is some text.</span>      			
      		</div>
      		<div class="ui-block-b">
		        <a href="#" class="ui-btn ui-corner-all ui-shadow">First Column Button</a><br>
		        <span>First Column: This is some text. This is some text. This is some text. This is some text. This is some text.</span>      			
      		</div>
      	</div>

      </div>
      <div class="ui-block-b">
        <a href="#" class="ui-btn ui-corner-all ui-shadow">Second Column Button</a><br>
        <span>Second Column: This is some text. This is some text. This is some text. This is some text.</span>
      </div>

    </div>
  </div>






    <div data-role="footer" data-position="fixed">
        <h4>My Footer</h4>
    </div><!-- /footer -->	
</div>

@stop