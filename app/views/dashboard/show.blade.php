@extends('layouts.default')

@section('title')
	Main DashBoard
@stop

@section('navbar')

@include('includes._navbar', ['link'=>'/user/'.$user->id.'/profile', 'button_left'=>'<span class="fa fa-user"></span> Profile', 'link2'=>'/user/'.$user->id.'/dashboard'])	
@stop

@section('content')
@if($usertype=='staff' || $usertype=="customer")

<div class="row icon-group-bg">	
	<div class="col-md-4 col-sm-5 icon-group-title"><h3>Order Taking Functions</h3></div>
	<div class="col-md-8 col-sm-7 col-xs-12">
		<a href="/user/{{$user->id}}/torder">
			<div class="btn-icon-160">
				<i class="fa fa-list"></i>
				<div class="text">View Orders</div>
			</div>
		</a>					
		<a href="/user/{{$user->id}}/torder/create">
			<div class="btn-icon-160">
				<i class="fa fa-pencil" ></i>
				<div class="text">Take Orders</div>
			</div>	
		</a>							
	</div>
</div>
<br>
@endif

@if($usertype=="staff" || $usertype=="vendor")
<div class="row icon-group-bg">	
	<div class="col-md-4 col-sm-5 icon-group-title"><h3>Order Placing Functions</h3></div>
	<div class="col-md-8 col-sm-7 col-xs-12">
		<a href="/user/{{$user->id}}/porder">
			<div class="btn-icon-160">
				<i class="fa fa-list"></i>
				<div class="text">View Orders</div>
			</div>
		</a>
		@if($usertype == 'staff')					
		<a href="/user/{{$user->id}}/porder/create">
			<div class="btn-icon-160">
				<i class="fa fa-file-text-o" ></i>
				<div class="text">Place Orders</div>
			</div>	
		</a>
		@endif		
	</div>
</div>
<br>
<div class="row icon-group-bg">
	<div class="col-md-4 col-sm-5 icon-group-title"><h3>Order Forecast</h3></div>
	<div class="col-md-8 col-sm-7 col-xs-12">
		<a href="/user/{{$user->id}}/torder">
			<div class="btn-icon-160">
				<i class="fa fa-list"></i>
				<div class="text">View Forecast</div>
			</div>
		</a>
		@if($usertype=='staff')					
		<a href="#">
			<div class="btn-icon-160">
				<i class="fa fa-bar-chart-o" ></i>
				<div class="text">Create Forecast</div>
			</div>	
		</a>		
		@endif
	</div>
</div>
@endif
@stop
