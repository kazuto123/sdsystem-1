@extends('layouts.default')

@section('title')
	Main DashBoard
@stop

@section('content')

<div class="container">
   <div class="jumbotron">
      <h1>Page Is Still in Construction ...</h1>
      <p>Sorry For the Inconvinience Caused.</p>
      <p>
      	<a class="btn btn-primary btn-lg" href="/user/{{$user->id}}/dashboard" role="button">
         	Back to Previous
        </a>
      </p>
   </div>
</div>

	
@stop
