<li class="list-group-item">
    <div class="row">
        <a href="/user/{{$userid}}/porder/{{$porder_id}}" class="col-xs-12">

            <span style="padding-right: 2px;" class="col-xs-2 col-sm-1 col-md-1 polist">
                {{$porder_id}}
            </span>
            <span style="padding-right: 2px;" class="col-xs-8 col-sm-10 col-md-9 polist">
                {{$vend_no}} - {{Vendor::where('vend_no', '=', $vend_no)->first()->vend_name;}}
            </span>
            <span style="padding-right: 2px;" class="hidden-xs hidden-sm col-md-1 polist">
                {{$status}}
            </span>
            <span class="text-right col-md-1 col-xs-4 col-sm-2 pull-right" style="font-size:10px;margin-top:3px;">{{$date}}</span>
        </a>

    </div>

</li>
