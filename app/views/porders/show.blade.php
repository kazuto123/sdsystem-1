@extends('layouts.default-po')

@section('csslinks')
@parent
<style>
label{
	color: #454545;
	text-shadow: 1px 0px 0px #e3e3e3;
}

#logs_list{
	height: 300px;
}
#ordered_items_list{
	height: 300px;
}
</style>
@stop

@section('scripts')
@parent
{{ HTML::script('js/app-po.js') }}
@stop

@section('title')
	Order Details
@stop

@section('content')
	@section('navbar')
		@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/porder', 'link2'=>'/user/'.$userid.'/dashboard'])	
	@stop

	<div class="row" style="margin-top: 50px;">
		<div class="col-xs-3 text-left">
		</div>
		<div class="col-xs-6 text-center">
			<h3 class="header-text" >Order Details</h3>		
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
			<!-- {{ HTML::image('images/logo-company.png', 'test', ['class'=>'img-responsive']) }} -->
		</div>
	</div>
	
	<!-- order details -->
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">
							Order Number: {{$porder_details->id}}
							<!-- <a class="pull-right btn btn-xs btn-link" href="/user/{{$userid}}/porder/{{$porder_details->id}}/xml">XML</a> -->
						</h3>
					</div>
					<div class="panel-body" style="padding: 15px;">
						@include('torders._comp_label', ['label'=>'Vendor Group', 'value'=> Vendor::where('vend_no', '=', $porder_details->vend_no)->first()->vend_grp_code ])
						@include('torders._comp_label', ['label'=>'Order Placed By', 'value'=> User::where('id', '=', $porder_details->user_id)->first()->username . ' (' . $porder_details->user_id . ')'])
						@include('torders._comp_label', ['label'=>'Order Placed To', 'value'=> Vendor::where('vend_no', '=', $porder_details->vend_no)->first()->vend_name . ' (' . $porder_details->vend_no . ')'])
						@include('torders._comp_label', ['label'=>'Contact Person', 'value'=> $porder_details->cont_person])
						@include('torders._comp_label', ['label'=>'Contact Number', 'value'=> $porder_details->cont_no])
						@include('torders._comp_label', ['label'=>'Currency Code', 'value'=> $porder_details->curr_code])
						@include('torders._comp_label', ['label'=>'Currency Rate', 'value'=> $porder_details->curr_rate])
						@include('torders._comp_label', ['label'=>'Remark', 'value'=> $porder_details->remark])									
					</div>
				</div>

			</div>
			<div class="col-md-1">
			</div>
		</div>
		
		@include('includes._divider')	

	<!-- Ordered items -->
	
	
	<div class="row">
		<table class="table table-striped">
	      <thead>
	        <tr>
	          <th>Item id</th>
	          <th>Item Desc</th>
	          <th>Qty</th>
	          <th>Uom</th>
	          <th>Unit Price</th>
	          <th>Total</th>
	        </tr>
	      </thead>
	      <tbody>
			@foreach($porder_item as $item)
				<tr>
				    <td>{{ $item->item_no }}</td>
				    <td>{{ $item->descr }}</td>
				    <td>{{ $item->qty }}</td>
				    <td>{{ $item->uom }}</td>
				    <td>{{ $item->unitprice }}</td>
				    <td>{{ $item->total }}</td>
				</tr>
			@endforeach
	      </tbody>
	    </table>
	</div>	


	@include('includes._divider')

	<div class="row">
		<div class="col-md-3 col-md-offset-9">
			<div class="row">
				<div class="col-md-12">Subtotal <span style="float:right">${{ $porder_details->subtotal }}</span></div>
			</div>
			
			<div class="row">
				<div class="col-md-12">GST <span style="float:right">${{ $porder_details->gst }}</span></div>
			</div>
			
			<div class="row">
				<div class="col-md-12">Total <span style="float:right">${{ $porder_details->total }}</span></div>
			</div>
			
		</div>
	</div>

	@include('includes._divider')
	<!-- buttons elements -->
	<div class="row">
		<div class="col-md-6 col-md-offset-6">
			<div class="row">
				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="javascript:window.print()" class="btn btn-primary btn-block">Print</a></div>

				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#delModal">Delete</a></div>
				
				<div class="col-xs-4 noprint" style="margin-top:5px;"><a href="/user/{{$userid}}/porder/{{$porder_details->id}}/edit" class="btn btn-warning btn-block">Update</a></div>

				<!-- Division for the delete order element-->
				<div class="row">
					<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" style="width:auto; margin:auto;">
					    <div class="modal-content" style=";">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" id="delModalLabel">Delete Order?</h4>
					      </div>
					      <div class="modal-body">
					        Are you sure you want to delete this order?
					      </div>
					      <div class="modal-footer">
							{{ Form::open(array('url' => 'user/' . $userid . '/porder/' . $porder_details->id) ) }}
								{{ Form::hidden('_method', 'DELETE') }}
								<a class="btn btn-default" data-dismiss="modal">Close</a>
								{{ Form::submit('Delete', array('class' => 'btn btn-danger btn-del-order-confirm')) }}
							{{ Form::close() }}
					      </div>
					    </div>
					  </div>
					</div>		
				</div> <!-- end row -->

			</div>
		</div>
	</div>					

@include('includes._divider')
@stop