<div class="form-group">
	<label for='{{$name}}' class="col-md-3 control-label" style="color: #454545; ">{{ $label }}</label>
	<div class="col-md-9">
		<select name="{{$name}}" id="{{$id}}" class="form-control">
			<option value="">NONE</option>
		</select>	
	</div>
</div>