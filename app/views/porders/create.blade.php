@extends('layouts.default-po')

@section('csslinks')
@parent
<style>
label{
	color: #454545;
	text-shadow: 1px 0px 0px #e3e3e3;
}

#logs_list{
	height: 300px;
}
#ordered_items_list{
	height: 300px;
}
</style>
@stop

@section('scripts')
@parent
{{ HTML::script('js/app-po.js') }}
@stop

@section('title')
	Place New Order
@stop

@section('content')
	@section('navbar')
		@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/porder', 'link2'=>'/user/'.$userid.'/dashboard'])	
	@stop
	<div class="row" style="margin-top: 50px;">
		<div class="col-xs-3 text-left">
		</div>
		<div class="col-xs-6 text-center">
			<h4 style="color: #454545; text-shadow: 1px 1px 1px #cdcdcd;">Placed New Order</h4>
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
		</div>
	</div>
	@include('includes._divider')
	
	{{ Form::open(['route'=>'user.porder.store', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'porder_form', 'id'=>'porder_form']) }}
	<div class="row">
		<div class="col-md-12">
			<div id="error_code" class='alert alert-danger text-center'>
			</div>
		</div>
	</div>

	<div class="row" id="porder_details" >
	  <div class="col-md-6">
		@include('porders._comp_form_selectbox', ['name'=>'vendor_group', 'label'=>'Vendor Group', 'id'=>'select_vendor_grp', 'options'=>$vgroups])
		@include('porders._comp_form_selectbox_blank', ['name'=>'vendor_no', 'label'=>'Vendor Number', 'id'=>'select_vendor_no'])
		@include('porders._comp_form_text', ['name'=>'vendor_name', 'label'=>'Vendor Name', 'id'=>'input_vendor_name'])
	  	@include('porders._comp_form_text', ['name'=>'vendor_addr', 'label'=>'Vendor Address', 'id'=>'input_vendor_addr'])		
	  	@include('porders._comp_form_text', ['name'=>'vendor_contact_person', 'label'=>'Contact Person', 'id'=>'input_vendor_contact_person'])
	  	@include('porders._comp_form_text', ['name'=>'vendor_contact_phone', 'label'=>'Contact Number', 'id'=>'input_vendor_contact_no'])
	  	@include('porders._comp_form_hidden', ['name'=>'user_id', 'value'=>$userid , 'id'=>'user_id'])
	  </div>
	  <div class="col-md-6">
		@include('porders._comp_form_selectbox', ['name'=>'vendor_curr_code', 'label'=>'Currency Code', 'id'=>'select_vendor_curr_code', 'options'=>$curr_codes])
		@include('porders._comp_form_text', ['name'=>'vendor_curr_rate', 'label'=>'Currency Rate', 'id'=>'input_vendor_curr_rate'])

		<div class="form-group">
			<label for="order_remark" class="col-md-3 control-label"> 
				Remarks:
			</label>
			<div class="col-md-9">
				<textarea name="order_remark" class="form-control" rows="3"></textarea>
			</div>
		</div>		
		
	  </div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h4>Additional Order</h4>
			<div class="row">
				<div class="col-md-12">
					@foreach($cats as $cat)
						<div id="{{ $cat->sub_cat }}" class="cat-button" title="{{$cat->descr}}">
							{{ $cat->sub_cat }}
						</div>
					@endforeach	
				</div>
			</div>
			<br>
			<div class="row">
				<!-- items -->
				<div class="col-md-12">
					<h6 id="search_header"></h6>
					<div class="row">
						<div class="col-md-12">
							<div id="error_item" class='alert alert-danger text-center'></div>
							
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12">
							
							<div id="msg_success" class='alert alert-success text-center'></div>
						</div>
					</div>											
				
					<ul id="cat_itemlist" class="list-group all_item_list" style="height: 240px; overflow: auto; margin-top: 3px;">
						@foreach($items as $item)
							<li class="list-group-item list-item-add cat-item {{ $item->uom }} {{$item->cat_code}}"> 
								<span>
								{{ $item->item_no }} &nbsp&nbsp {{$item->item_descr}}
								<div id="{{ $item->id }}" class="btn btn-sm btn-primary btn-vend-item-add fa fa-plus-circle" style="float:right;"></div>
								</span>
							</li>
						@endforeach 
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12"><h4>Order Placed</h4></div>
				<div class="col-md-12">
					<div id="pordered_itemlist" class="poded_itemlist" style="height: 500px; overflow: auto; margin-top: 3px;">
						<!-- Ordered Item List -->
						
					</div>
				</div>
			</div>

			@include('includes._divider')

			<div class='row'>
				<div class="col-md-12">
					<!-- form sub total -->
					<div class="form-group">
						<label for="customerGroup" class="col-md-5 control-label">Sub Total</label>
						<div class="col-md-7">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								{{Form::text('order_subtotal', '0.00', ['class'=>'text-right form-control input-sm', 'readonly', 'placeholder'=>'Subtotal', 'id'=>'order_subtotal'])}}
							</div>
						</div>
					</div>
					<!-- form gst amount -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Add GST 7%:</label>
					    <div class="col-md-7">
					    	<div class="input-group">
					    		<span class="input-group-addon">$</span>
					    		{{Form::text('order_gst', '0.00', ['class'=>'text-right form-control input-sm', 'readonly', 'placeholder'=>'GST', 'id'=>'order_gst'])}}		
					    	</div>
					    
					    </div>
					</div>
					<!-- form total -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Total</label>
					    <div class="col-md-7">
					    	<div class="input-group">
						    	<span class="input-group-addon">$</span>
						    	{{Form::text('order_total', '0.00', ['class'=>'text-right form-control input-sm', 'readonly', 'placeholder'=>'Total', 'id'=>'order_total'])}}	
					    	</div>
					    </div>
					</div>
					
				</div>
			</div>
			<br>
			@include('includes._divider')

			<div class="row" style="">
				<div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 5px;">
					<div class="row">
						{{ Form::submit('Submit', ['class'=>'btn btn-primary col-md-3 pull-right', 'style'=>'margin-top:3px'] )}}
						<button class="btn btn-warning col-md-3 pull-right" type="button" data-toggle="modal" data-target="#clearformModal" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Clear</button>
						<button class="btn btn-danger col-md-3 pull-right" type="button" data-toggle="modal" data-target="#cancelformModal" style="margin:3px;">Cancel</button> 
					</div>
				</div>
			</div>

		</div> <!-- col-md-6 -->		
	</div> 

	{{ Form::close() }}

	<!-- Division for the clear order element-->
	<div class="row">
		<div class="modal fade" id="clearformModal" tabindex="-1" role="dialog" aria-labelledby="clearformModalLabel" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="clearformModalLabel">Clear order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to clear this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/porder/create" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>

	<!-- Division for the cancel order element-->
	<div class="row">
		<div class="modal fade" id="cancelformModal" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title">Cancel order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to cancel this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/porder" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>




@stop