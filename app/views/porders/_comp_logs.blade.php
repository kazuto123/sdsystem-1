<div class="bs-callout bs-callout-info">
	<h4>{{$title}} {{$date}}</h4>
	<p>{{$content}}</p>
</div>