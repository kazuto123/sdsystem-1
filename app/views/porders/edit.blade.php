@extends('layouts.default-po')

@section('csslinks')
@parent
<style>
label{
	color: #454545;
	text-shadow: 1px 0px 0px #e3e3e3;
}

#logs_list{
	height: 300px;
}
#ordered_items_list{
	height: 300px;
}
</style>
@stop

@section('scripts')
@parent
	{{ HTML::script('js/app-po.js') }}
@stop

@section('title')
	Edit Order
@stop

@section('content')
	@section('navbar')
		@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/porder', 'link2'=>'/user/'.$userid.'/dashboard'])	
	@stop

	<div class="row" style="margin-top: 50px;">
		<div class="col-xs-3 text-left">
		</div>
		<div class="col-xs-6 text-center">
			<h4 style="color: #454545; text-shadow: 1px 1px 1px #cdcdcd;">Edit Order</h4>
		</div>
		<div class="col-xs-3 text-right" style="margin-top: 10px;">
		
		</div>
	</div>
	
	@include('includes._divider')
	
	{{ Form::open(['route'=>'user.porder.update', 'method'=>'PUT', 'class'=>'form-horizontal', 'name'=>'porder_form', 'id'=>'porder_form']) }}
	<div class="row">
	  	<div class="col-md-12">
			<div id="error_code" class='alert alert-danger text-center'>
			</div>
	 	</div>
	</div>

	<div class="row" id="porder_details" >
	  <div class="col-md-6">
		<div class="form-group">
			<label for='vendor_group' class="col-md-3 control-label" style="color: #454545; ">Vendor Group</label>
			<div class="col-md-9">
				<select name="vendor_group" id="select_vendor_grp" class="form-control">
					<option value="">NONE</option>
					@foreach($vgroups as $vgrp)
						@if($vgrp == $porder_info->vend_grp)
							<option value="{{$vgrp}}" selected> {{$vgrp}}</option>
						@else
							<option value="{{$vgrp}}"> {{$vgrp}}</option>
						@endif
					@endforeach
				</select>	
			</div>
		</div> <!-- end vendor group -->

		<div class="form-group">
			<label for='vendor_group' class="col-md-3 control-label" style="color: #454545; ">Vendor Number</label>
			<div class="col-md-9">
				<select name="vendor_no" id="select_vendor_no" class="form-control">
					<option value="">NONE</option>
					@foreach($vnos as $vno)
						@if($vno['code'] == $porder_info->vend_no)
							<option value="{{$vno['code']}}" selected> {{$vno['code']}} - {{$vno['name']}}</option>
						@else
							<option value="{{$vno['code']}}"> {{$vno['code']}} - {{$vno['name']}}</option>
						@endif
					@endforeach
				</select>	
			</div>
		</div> <!-- end vendor no -->		

		<div class="form-group">
			<label for="vendor_name" class="col-md-3 control-label"> Vendor Name </label>
			<div class="col-md-9">
				<input type="text" name="vendor_name" id="input_vendor_name" class="form-control" value="{{$porder_info->vend_name}}" readonly>
			</div>
		</div><!-- end vendor name -->

		<div class="form-group">
			<label for="vendor_addr" class="col-md-3 control-label"> Vendor Address </label>
			<div class="col-md-9">
				<input type="text" name="vendor_addr" id="input_vendor_addr" class="form-control" value="{{$vend_addr}}" readonly>
			</div>
		</div><!-- end vendor name -->

		<div class="form-group">
			<label for="vendor_contact_person" class="col-md-3 control-label"> Contact Person </label>
			<div class="col-md-9">
				<input type="text" name="vendor_contact_person" id="input_vendor_contact_person" class="form-control" value="{{$porder_info->cont_person}}">
			</div>
		</div><!-- end vendor name -->		

		<div class="form-group">
			<label for="vendor_contact_phone" class="col-md-3 control-label"> Contact Number </label>
			<div class="col-md-9">
				<input type="text" name="vendor_contact_phone" id="input_vendor_contact_no" class="form-control" value="{{$porder_info->cont_no}}">
			</div>
		</div><!-- end vendor name -->		

		<input type="hidden" name="user_id" value="{{$userid}}" id="user_id">
	  	<input type="hidden" name="porder_id" value="{{$porder_info->id}}" id="porder_id">
	  </div>
	  <div class="col-md-6">
		<div class="form-group">
			<label for='vendor_curr_code' class="col-md-3 control-label" style="color: #454545; ">Currency Code</label>
			<div class="col-md-9">
				<select name="vendor_curr_code" id="select_vendor_curr_code" class="form-control">
					<option value="">NONE</option>
					@foreach($curr_codes as $curr_code)
						@if($curr_code == $porder_info->curr_code)
							<option value="{{$curr_code}}" selected> {{$curr_code}} </option>
						@else
							<option value="{{$curr_code}}"> {{$curr_code}} </option>
						@endif
					@endforeach
				</select>	
			</div>
		</div> <!-- end vendor no -->

		<div class="form-group">
			<label for="vendor_curr_rate" class="col-md-3 control-label"> Currency Rate </label>
			<div class="col-md-9">
				<input type="text" name="vendor_curr_rate" id="input_vendor_curr_rate" class="form-control" value="{{$porder_info->curr_rate}}" readonly>
			</div>
		</div><!-- end currency rate -->		


		<div class="form-group">
			<label for="order_remark" class="col-md-3 control-label"> 
				Remarks:
			</label>
			<div class="col-md-9">
				<textarea name="order_remark" class="form-control" rows="3" placeholder="Key in the remark">{{$porder_info->remark}}</textarea>		
			</div>
		</div>	<!-- order remark -->

	  </div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h4>Additional Order</h4>
			<div class="row">
				<div class="col-md-12">
					@foreach($cats as $cat)
						<div id="{{ $cat->sub_cat }}" class="cat-button" title="{{$cat->descr}}">
							{{ $cat->sub_cat }}
						</div>
					@endforeach	
				</div>
			</div>
			<br>
			<div class="row">
				<!-- items -->
				<div class="col-md-12">
					<h6 id="search_header"></h6>
					<div class="row">
						<div class="col-md-12">
							<div id="error_item" class='alert alert-danger text-center'></div>	
						</div>
					</div>	
					<div class="row">
						<div class="col-md-12">
							<div id="msg_success" class='alert alert-success text-center'></div>
						</div>
					</div>											
				
					<ul id="cat_itemlist" class="list-group all_item_list" style="height: 240px; overflow: auto; margin-top: 3px;">
						@foreach($items as $item)
							<li class="list-group-item list-item-add cat-item {{ $item->uom }} {{$item->cat_code}}"> 
								<span>
								{{ $item->item_no }} &nbsp&nbsp {{$item->item_descr}}
								<div id="{{ $item->id }}" class="btn btn-sm btn-primary btn-vend-item-add fa fa-plus-circle" style="float:right;"></div>
								</span>
							</li>
						@endforeach 
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12"><h4>Order Placed</h4></div>
				<div class="col-md-12">
					<div id="pordered_itemlist" class="poded_itemlist" style="height: 500px; overflow: auto; margin-top: 3px;">
						<!-- Ordered Item List -->	
						@foreach($pordered_items as $po_item)
						
						<li class="list-group-item item-{{$po_item['item_id']}}">
						    <div class="form-group row ordered_item">
						 	    <div class="col-xs-12" style="margin-bottom:5px;">
						 	        <div class="row">
						     		    <div class="col-xs-1" style="padding-right: 2px;">
						       				<label class="control-label" for="oditem_{{$po_item['item_id']}}_itemno" >{{$po_item['item_no']}}</label>
						    			</div>
						    			<div class="col-xs-11">
						    				<label class="control-label">{{$po_item['item_descr']}}</label>
						    			</div>
						    			<input type="hidden" name="oditem_{{$po_item['item_id']}}_descr" value="{{$po_item['item_descr']}}"/>
						    			<input type="hidden" name="oditem_{{$po_item['item_id']}}_itemid" value="{{$po_item['item_id']}}"/>
						    			<input type="hidden" name="oditem_{{$po_item['item_id']}}_itemno" value="{{$po_item['item_no']}}" />
						    		</div>
						    	</div>
						    	<div class="col-xs-2" style="padding-right: 2px;">
						    		<input id="{{$po_item['item_id']}}" class="form-control input-sm input-qty qty_{{$po_item['item_id']}}" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_{{$po_item['item_id']}}_qty" placeholder="Qty" value="{{$po_item['qty']}}" required>
						    	</div>
						    	<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;"> 
						    		<select id="" name="oditem_{{$po_item['item_id']}}_uom" class="form-control input-sm opt-uom">
						    			@foreach($po_item['uom_all'] as $uom)
											@if($po_item['uom'] == $uom['uom'])
												<option value="{{$uom['uom']}}" selected> {{$uom['uom']}} </option>
											@else
												<option value="{{$uom['uom']}}"> {{$uom['uom']}} </option>
											@endif
						    			@endforeach
									<input type="hidden" name="oditem_{{$po_item['item_id']}}_uomcf" value="{{$po_item['uom_cf']}}" />
						    		</select>
						    	</div>
						    
						    	<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;">
						    		<div class="input-group">
						    			<span class="input-group-addon">$</span>
						    			<input id="{{$po_item['item_id']}}" class="text-right form-control input-sm input-unitprice unp_{{$po_item['item_id']}}" type="text" name="oditem_{{$po_item['item_id']}}_unitprice" placeholder="@Unit Price" value="{{$po_item['price']}}" onkeypress="return isNumberKey(event)"/>
						    		</div>
						    	</div>
						    	<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;">
						    		<div class="input-group">
						    			<span class="input-group-addon">$</span>
						    			<input class="text-right form-control input-sm input-total tot_{{$po_item['item_id']}}" type="text" name="oditem_{{$po_item['item_id']}}_total" placeholder="Total" value="{{$po_item['total']}}" readonly>
						    		</div>
						    	</div>
						 	 	<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;">
						 	 	    <a id="{{$po_item['item_id']}}" class="btn btn-danger fa fa-minus-circle btn-vend-del-item"></a>
						 	 	</div>
						    </div>
						</li>
						@endforeach
					</div>
				</div>
			</div>
			<div class='row'>
				<div class="col-md-12">
					<!-- form sub total -->
					<div class="form-group">
						<label for="customerGroup" class="col-md-5 control-label">Sub Total</label>
						<div class="col-md-7">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input id="order_subtotal" name="order_subtotal" type="text" class="text-right form-control" value="{{$porder_info->subtotal}}" readonly>
							</div>
						</div>
					</div>
					<!-- form gst amount -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Add GST 7%:</label>
					    <div class="col-md-7">
					    	<div class="input-group">
					    		<span class="input-group-addon">$</span>
					    		<input id="order_gst" name="order_gst" type="text" class="text-right form-control" value="{{$porder_info->gst}}" readonly>
					    				
					    	</div>
					    
					    </div>
					</div>
					<!-- form total -->
					<div class="form-group">
					    <label for="customerGroup" class="col-md-5 control-label">Total</label>
					    <div class="col-md-7">
					    	<div class="input-group">
						    	<span class="input-group-addon">$</span>
						    	<input id="order_total" name="order_total" type="text" class="text-right form-control" value="{{$porder_info->total}}" readonly>
					    	</div>
					    </div>
					</div>
					
				</div>
			</div>
			<br>
			@include('includes._divider')

			<div class="row" style="">
				<div class="col-md-6 pull-right" style="margin-right:10px; margin-bottom: 5px;">
					<div class="row">
						{{ Form::submit('Submit', ['class'=>'btn btn-primary col-md-3 pull-right', 'style'=>'margin-top:3px'] )}}
						<button class="btn btn-warning col-md-3 pull-right" type="button" data-toggle="modal" data-target="#clearformModal" style="margin-right: 3px; margin-top:3px; margin-bottom:3px;">Clear</button>
						<button class="btn btn-danger col-md-3 pull-right" type="button" data-toggle="modal" data-target="#cancelformModal" style="margin:3px;">Cancel</button> 
					</div>
				</div>
			</div>

		</div> <!-- col-md-6 -->		
	</div> 

	{{ Form::close() }}

	
	<div class="row">
		<div class="col-md-12"><h3>Order Logs</h3></div> 
		
		<div class="col-md-12">
			@foreach($logs as $log)
			<div class="bs-callout bs-callout-info">
				<h4>{{$log->title}}</h4>
				<p>{{$log->content}}</p>
			</div>
			@endforeach
		</div> 

		<br>
		
	</div>


	<!-- Division for the clear order element-->
	<div class="row">
		<div class="modal fade" id="clearformModal" tabindex="-1" role="dialog" aria-labelledby="clearformModalLabel" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="clearformModalLabel">Clear order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to clear this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/porder/create" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>

	<!-- Division for the cancel order element-->
	<div class="row">
		<div class="modal fade" id="cancelformModal" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
		  <div class="modal-dialog" style="width:auto; margin:auto;">
		    <div class="modal-content" style=";">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title">Cancel order form?</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure you want to cancel this order form?
		      </div>
		      <div class="modal-footer">
				<a class="btn btn-default" data-dismiss="modal">Close</a>
				<a href="/user/{{$userid}}/porder" class="btn btn-danger">OK</a>
		      </div>
		    </div>
		  </div>
		</div>		
	</div>




@stop