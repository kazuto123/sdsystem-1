<div class="form-group">
	<label for="{{$name}}" class="col-md-3 control-label"> {{$label}} </label>
	<div class="col-md-9">
		<input type="text" name="{{$name}}" id="{{$id}}" class="form-control">
	</div>
</div>