@extends('layouts.default-po')

@section('title')
	Order Placed
@stop

@section('content')

@section('navbar')
@include('includes._navbar', ['button_left'=>'<span class="fa fa-arrow-left"></span> Back', 'link'=>'/user/'.$userid.'/dashboard', 'link2'=>'/user/'.$userid.'/dashboard'])	
@stop

	<div class="row" style="margin-top: 50px;">
		<div class="col-md-4 col-sm-4 col-xs-4 text-left" style="margin-top: 15px;">
			{{ Form::open(['method'=>'GET', 'class'=>'form-horizontal', 'name'=>'order_form', 'id'=>'order_form']) }}
				{{ Form::input('search', 'q', null, ['class'=>'form-control', 'placeholder'=>'Enter Search Term']) }}
			{{ Form::close() }}
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-center" style="margin-top:8px;">
			<a href="/user/{{$userid}}/porder"><h4 class="header-text">POrder List</h4></a>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-right" style="margin-top: 10px;">
			<div class="row">
				<div class="col-md-10 col-sm-9 hidden-xs" style="margin-top:4px;">
					<select name="sortby" id="select_sortby" class="form-control" onChange="this.form.submit();">
						<option value="None"> None </option>
						@foreach($filterby as $filter)
							<option value="{{$filter->value}}"> {{$filter->descr}} </option>
						@endforeach
					</select>	
				</div>			
				
				<div class="col-md-2 col-sm-3">
					@if($usertype == "vendor")
						
					@elseif($usertype == "staff")
						<a href="/user/{{ $userid }}/porder/create" class="fa fa-plus-circle btn btn-lg btn-info btn-create-order" data-content="Create New Order" rel="popover" data-placement="bottom"></a>
					@endif
				</div>
			</div>
		</div>
	</div>
	
	@include('includes._divider')
		
	<div class="row">
		<div class="col-md-12">
			<ul class="list-group">
					@foreach($porders as $po)
					<li class="list-group-item">
					    <div class="row">
					        <a href="/user/{{$userid}}/porder/{{$po->id}}" class="col-xs-12">
					            <span style="padding-right: 2px;" class="col-xs-12 col-sm-10 col-md-4 polist">
					                {{$po->vend_no}} - {{$po->vend_name}}
					            </span>
					            <span style="padding-right: 2px;" class="col-xs-12 col-sm-10 col-md-5 polist">
					                {{$po->vend_addr}}
					            </span>					            
					            <span style="padding-right: 2px;" class="col-xs-2 col-sm-1 col-md-1 polist">${{$po->total}}</span>
					            <span style="padding-right: 2px;" class="hidden-xs hidden-sm col-md-1 polist">
					                {{$po->status}}
					            </span>
					            <span class="text-right col-md-1 col-xs-4 col-sm-2 pull-right" style="font-size:10px;margin-top:3px;">
					            	{{$po->updated_at}}
					            </span>
					        </a>
					    </div>
					</li>
					@endforeach
					{{$porders->links();}}
		</ul>

	</div>
	

@stop