@extends('mobile.mobile-master')

@section('title')
@parent
	Purchase Message
@stop


@section('content')
<br><br><br><br><br><br>
<div class="jumbotron">
   	<div class="container">
      	<h2>{{$msg}}</h2>
      	<p>Place another order</p>
   		<p><a href="/mcomm/items" class="btn btn-primary btn-lg" role="button"> Back </a>
   	</div>
  	
   </p>
</div>
	

@stop



