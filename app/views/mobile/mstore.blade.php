@extends('mobile.mobile-master')

@section('title')
@parent
	Mobile Store
@stop


@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-3"></div>
		<div class="col-xs-6 text-center">
			<h3 style="font-family: Geosans, Arial;">Mobile Store</h3>
		</div>
		<div class="col-xs-3">
			<div class="font-geosans">Hi, Arun</div>
		</div>
	</div>
	<hr class="divider" style="margin-top: 10px;">
	
	<div class="row">
		<div class="col-xs-6 text-center">
			<div id="39-803-CAK" class="item-container one-edge-shadow btn-item-select" data-toggle="modal" data-target="#purchase_modal">
				<div class="image-container">
					<div class="font-geosans item-price">$<span class="item-price-val">6.00</span></div>
					<img class="item-image" src="/images/items/item1.jpg" alt="item1">
				</div>
				<div class="text-container">
					<span class="item-name">Chocolate Cake</span>
				</div>				
			</div>
		</div> <!--item 1 -->

		<div class="col-xs-6 text-center">
			<div id="40-804-CAK" class="item-container one-edge-shadow btn-item-select" data-toggle="modal" data-target="#purchase_modal">
				<div class="image-container">
					<div class="font-geosans item-price">$<span class="item-price-val">5.00</span></div>
					<img class="item-image" src="/images/items/item2.jpg" alt="item1">
				</div>
				<div class="text-container">
					<span class="item-name">Chocolate Brownies</span>
				</div>				
			</div>
		</div> <!--item 2 -->

		<div class="col-xs-6 text-center margin-10">
			<div id="41-805-DRK" class="item-container one-edge-shadow btn-item-select" data-toggle="modal" data-target="#purchase_modal">
				<div class="image-container">
					<div class="font-geosans item-price">$<span class="item-price-val">3.00</span></div>
					<img class="item-image" src="/images/items/item3.jpg" alt="item1">
				</div>
				<div class="text-container">
					<span class="item-name">Cappuccino</span>
				</div>				
			</div>
		</div> <!--item 3 -->

		<div class="col-xs-6 text-center margin-10">
			<div id="42-806-DRK" class="item-container one-edge-shadow btn-item-select" data-toggle="modal" data-target="#purchase_modal">
				<div class="image-container">
					<div class="font-geosans item-price">$<span class="item-price-val">3.00</span></div>
					<img class="item-image" src="/images/items/item4.jpg" alt="item1">
				</div>
				<div class="text-container">
					<span class="item-name">Black Coffee</span>
				</div>				
			</div>
		</div> <!--item 4 -->
		
		<div class="col-xs-6 text-center margin-10">
			<div id="43-807-DRK" class="item-container one-edge-shadow btn-item-select" data-toggle="modal" data-target="#purchase_modal">
				<div class="image-container">
					<div class="font-geosans item-price">$<span class="item-price-val">2.00</span></div>
					<img class="item-image" src="/images/items/item5.jpg" alt="item1">
				</div>
				<div class="text-container">
					<span class="item-name">Milk Tea</span>
				</div>				
			</div>
		</div> <!--item 5 -->	
	</div><!-- items contents -->

</div>

<!--Modal Box -->
  <!-- Division for the cancel order element-->
  <div class="row" style="position: relative;">
    <div class="modal fade" id="purchase_modal" tabindex="-1" role="dialog" aria-labelledby="purchaseModalLabel" aria-hidden="true" style="top: 240px;">
      <div class="modal-dialog" style="width:auto; margin:auto;">
        <div class="modal-content" style=";">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title font-geosans">Placing Order</h4>
          </div>
          {{ Form::open(['url'=>'/mcomm/items/purchase', 'method'=>'POST', 'class'=>'form-horizontal', 'name'=>'mcom-odform', 'id'=>'mcomm_form']) }}
          <input type="hidden" id="app_compcode" name="app_compcode" value="01">
          <input type="hidden" id="app_username" name="app_username" value="arun">
          <input type="hidden" id="app_userid" name="app_userid" value="21">
          <input type="hidden" id="app_custno" name="app_custno" value="1001">
          <input type="hidden" id="app_custaddrref" name="app_custaddrref" value="1">
		  <input type="hidden" id="app_custcont" name="app_custcont" value="90999755">
		  <input type="hidden" id="app_currcode" name="app_currcode" value="SGD">
		  
          <div class="modal-body">
			<div class="row">
				<div class="col-xs-6 text-center">
					<div class="row">
						<input type="hidden" id="purchase_item_no" name="purchase_item_no" value="">
						<input type="hidden" id="purchase_item_id" name="purchase_item_id" value="">
						<input type="hidden" id="purchase_item_descr" name="purchase_item_descr" value="">

						<div class="col-xs-12">
							<div id="" class="item-container one-edge-shadow">
								<div class="image-container">
									<input id="purchase_item_price" type="hidden" class="font-geosans item-price" name="purchase_item_price" value="$ Price" readonly>
									<img id="purchase_item_img" src="/images/items/itemx.png" alt="item1">
								</div>
								<div class="text-container">
									<span id="purchase_item_name">Item Name</span>
								</div>				
							</div>							
						</div>						
					</div>

					<div class="row">
						<hr class="divider">
						<div id="purchase_item_descr" class="col-xs-12 font-geosans">
							Descriptions of the items to be purchase. 	
						</div>
					</div>
				</div> <!--left -->		
            
		        <div class="col-xs-6">
		        	<div class="row">
		        		<div class="col-xs-12">
							  <h4 class="font-geosans">Select Qty:</h4>
			        	      <div class="input-group">
						         <span id="btn-add" class="input-group-addon btn "><i class="fa fa-chevron-up"></i></span>
						         <input class="form-control text-center input-lg" style="font-size: 18px;" type="text" id="purchase_item_qty" name="purchase_item_qty" value="1" readonly>
						         <span id="btn-minus" class="input-group-addon btn"><i class="fa fa-chevron-down"></i></span>
						      </div>		        			
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-xs-12">
		        			<h4 class="font-geosans">UOM</h4>
		        			<select name="purchase_item_uom" id="purchase_item_uom" class="form-control font-geosans text-center input-lg">
		        				<option value="SLICE">SLICE</option>
		        			</select>
		        		</div>
		        	</div>
		        </div> <!-- right -->
			</div> <!-- row panel -->
          </div>
          <div class="modal-footer">
	        <a class="btn btn-lg btn-danger" data-dismiss="modal">Cancel Order</a>
	        <input type="submit" class="btn btn-lg btn-success" value="Submit Order">
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>    
  </div>





@stop

@section('scripts')
@parent
<script type="text/javascript">
	$('.btn-item-select').on('click', function(){
		var item = $(this)[0].id;
		var item_infos = item.split('-');
		var item_id = item_infos[0];
		var item_no = item_infos[1];
		var item_type = item_infos[2];
		var item_price = $(this).find('.item-price-val')[0].innerHTML;

		//console.log(item_no);
		//console.log($(this).find('.item-name')[0].innerText);
		$('#purchase_item_img')[0].src = $(this).find('.item-image')[0].src;
		$('#purchase_item_name')[0].innerHTML = $(this).find('.item-name')[0].innerHTML;
		$('#purchase_item_descr')[0].value = $(this).find('.item-name')[0].innerHTML;
		$('#purchase_item_price')[0].value = item_price;
		$('#purchase_item_no')[0].value = item_no;
		$('#purchase_item_id')[0].value = item_id;
		$('#purchase_item_qty')[0].value = '1';

		if(item_type == 'CAK') {
			$('#purchase_item_uom').html('<option value="SLICE">SLICE</option>');
		}
		else if (item_type == 'DRK'){
			$('#purchase_item_uom').html('<option value="CUP">CUP</option>');
		}	

	});

	$(document).ready( function() {
		var el = $('#purchase_item_qty');
			function change( amt ) {
			if(el.value <= 0) el.value = 0;
			else el.val( parseInt( el.val(), 10 ) + amt );
		}

		$('#btn-add').click( function() {
			change( 1 );
		});
		$('#btn-minus').click( function() {
			change( -1 ); 
		});
	});

</script>

@stop



