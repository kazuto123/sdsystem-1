$('document').ready(function(){

	
});

//date picker setup
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    todayBtn: true,
    autoclose: true,
    todayHighlight: true,
    minView:2
});


$('#select_warehouses').on('change', function(){
	var warehouse_infos = $(this)[0].value.split('-');
	var wh_id = warehouse_infos[0];
	var wh_code = warehouse_infos[1];
	var wh_name = warehouse_infos[2];
	//console.log(warehouse_infos);
	$('#label_warehouse_name').html(wh_name);
});

$('#btn_newstoitem').on('click', function(){
	$.ajax({

		type: "GET",
		dataType: "json",
		url: '/sdapi/get/stock/warehouse/wh_item/'+ wh_id +'/'+date,
		success: function (data) {
			
			
		},
		complete: function(){
			//if complete do the following

		},
		error: function(xhr, status, error) {
			//if error, do the following
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}

	});//end of ajax
});


$('#btn-get-warehouse').on('click', function(){
	var warehouse_infos = $('#select_warehouses')[0].value.split('-');
	var wh_id = warehouse_infos[0];
	var wh_code = warehouse_infos[1];
	var wh_name = warehouse_infos[2];
	var date = $('#select_stocktake_date')[0].value;
	var percent = 0;
	var progressbar = 
		'<div class="progress progress-striped">' +
            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="progress">' +
            '</div>' +
      	'</div>';
	$('#whitem_listings').html(progressbar);
	
	$.ajax({
		type: "GET",
		dataType: "json",
		url: '/sdapi/get/stock/warehouse/wh_item/'+ wh_id +'/'+date,
		beforeSend: function(){
			setInterval(function(){
				$("#progress").css('width',percent+'%');
				percent += 10;
			}, 500);
		},
		success: function (data) {
			var itemnode = '';
			$('#whitem_listings').html('');
			console.log(data);
			data.forEach(function(entry){
				//get item uom for each item retrieve from warehouse.
				var options = "";

				entry.itemuom.forEach(function(uom){
					options += '<option value="'+uom.uom+'">' + uom.uom + '</option>'
				});

				//console.log(options);
				//construct the item division
				itemnode = 
					'<li class="list-group-item" >' +
					   '<div class="row">' +
					   		'<input type="hidden" name="stoitem_'+entry.item.item_id+'_itemid" value="' + entry.item.item_id + '">' +
					   		'<input type="hidden" name="stoitem_'+entry.item.item_id+'_itemno" value="' + entry.item.item_no + '">' +
					   		'<input type="hidden" name="stoitem_'+entry.item.item_id+'_itemdescr" value="' + entry.item.item_descr + '">' +
						  '<div class="col-md-6">' + ' Item No: ' +  entry.item.item_no + ' &nbsp&nbsp' + entry.item.item_descr + 
						  '</div>' +
						  
						  '<div class="col-md-3" >' +
						  	 '<input id="itid-' + entry.item.item_id + '" class="form-control input-sm input-qty qty_'+entry.item.item_id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="stoitem_'+entry.item.item_id+'_qty" placeholder="Qty" required>' +
						  '</div>' +

						  '<div class="col-md-3" >' +
						  	 '<select class="form-control" name="stoitem_'+entry.item.item_id+'_uom" id="select_sto_itemuom">' +
						  	 	options +
						  	 '</select>' +
						  '</div>' +
					   '</div>'
					'</li>';
				$('#whitem_listings').append(itemnode);	
			});
			percent = 100;
			$('#btn_sto_submit').removeClass('disabled');
			$('#btn_sto_reset').removeClass('disabled');
			$('#btn_sto_delete').removeClass('disabled');

		},
		complete: function(){
			//if complete do the following

		},
		error: function(xhr, status, error) {
			//if error, do the following
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}
	});


});

//validation
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57) return false;
    //return true;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46) return true;
    return false;
}

