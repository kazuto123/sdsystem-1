
//category item 
$('.cat-button').click(function(event){
	console.log(event.target.id);
	//var target = '.'+event.target.id;
	var containClass = $('#cat_itemlist').find('li').hasClass(event.target.id);
	var allList = $('#cat_itemlist').find('li');
	//console.log(allList);
	if(event.target.id == 'ALL'){
		for(var i=0; i < allList.length; i++){
			$(allList[i]).fadeIn(300, function(){
					$(this).show();
			});
		}
	}else{
		for(var i=0; i < allList.length; i++){
			if($(allList[i]).hasClass(event.target.id)){
				$(allList[i]).fadeIn(300, function(){
					$(this).show();
				});	
				continue;
			}
			else{
				$(allList[i]).fadeOut(300, function(){
					$(this).hide();
				});	
			}
		}		
	}

});

//retrieve outlet code/customer number based on their group
$('#input_custgrp').on('change', function(event){
	var data_url = '/customer/group/' + $(event.target).val(); 
	$.ajax({
		type: "GET",
		dataType: "json",
		url: data_url,
		success: function (d) {
			$('#input_outletcode').html('<option value="None" selected>None</option>');
			d.forEach(function(entry){
				$('#input_outletcode').append(
					'<option value="' + entry + '">'+ entry +'</option>'
				);
			})
		},
		complete: function(){
			//if complete do the following
		},
		error: function(xhr, status, error) {
			//if error, do the following
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}
	});	
});

//based on the customer code, get the customer info
$('#input_outletcode').on('change', function(event){
	var data_url = '/customer/info/' + $(event.target).val(); 
	$.ajax({
		type: "GET",
		dataType: "json",
		url: data_url,
		success: function (d) {
			//console.log(d);
			
			$('#input_cust_name').val(d.cust[0].cust_name);
			$('#input_cont_person').val(d.cust[0].cust_cont_name);
			$('#input_cont_no').val(d.addr[0].cont_off_tel);

			$('#input_delivery_addr-button span').html(d.addr[0].addr);
			d.addr.forEach(function(entry){
				$('#input_delivery_addr').append(
					'<option value="' + entry.addr + '">'+ entry.addr +'</option>'
				);
			});
			$('#input_delivery_driver').val(d.cust[0].salesman);
			$('#input_delivery_driver-button span').html(d.cust[0].salesman);

			//load all the preset ...
		},
		complete: function(){
			//if complete do the following
		},
		error: function(xhr, status, error) {
			//if error, do the following
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}
	});
})

$('.btn-item-add').click(function(event){
	//console.log(event.target);
	var cust_code = $('#input_outletcode').val();
	var ajax_url  = '/item/'+event.target.id+'/cust/'+cust_code;
	//console.log(ajax_url);
	$.ajax({
		type: "GET",
		dataType: "json",
		url: ajax_url,
		success: function (d) {
			//console.log(d);
			var uom_opt = "";
			for (var i = 0; i < d.uom.length; i++) {
				if(d.item[0].uom == d.uom[i].uom) uom_opt += '<option value="' + d.uom[i].uom + '" selected class="opt-uom">' + d.uom[i].uom + '</option>';	
				else uom_opt += '<option value="' + d.uom[i].uom + '" class="opt-uom">' + d.uom[i].uom + '</option>';	
    		}
    		var itemnode = 
			'<li class="list-group-item item-'+d.item[0].item_no+' ui-li-static ui-body-inherit ui-first-child ui-last-child">' +
				'<h2>'+ d.item[0].item_no +' - '+ d.item[0].item_descr + '<a id="'+d.item[0].item_no+'" class="btn btn-danger fa fa-minus-circle btn-del-item ui-link" style="float:right;"></a></h2>' +
				'<input type="hidden" name="oditem_'+d.item[0].item_no+'_item_desc" value="'+ d.item[0].item_descr +'"/>' +
				'<input type="hidden" name="oditem_'+d.item[0].item_no+'_itemid" value=""/>' +
				'<input type="hidden" name="oditem_'+d.item[0].item_no+'_itemno" value="'+d.item[0].item_no+'"/>' +
				'<div class="ui-grid-a ui-responsive">' +
					'<div class="ui-block-a">' +
						'<div class="ui-grid-a">' +
							'<div class="ui-block-a">' +
								'<div class="ui-input-text ui-body-inherit ui-corner-all ui-mini ui-shadow-inset">' +
									'<input id="' + d.item[0].item_no + '" class="input-qty qty_'+d.item[0].item_no+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+d.item[0].item_no+'_qty" placeholder="Qty" required>' +
								'</div>' +
							'</div>' +
							'<div class="ui-block-b">' +
								'<div class="ui-select ui-mini"> ' +
									'<div id="select-'+d.item[0].item_no+'-button" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-corner-all ui-shadow ui-focus">' +
										'<span>'+d.item[0].uom+'</span>' +
										'<select id="'+ d.item[0].comp_code + '_' + d.item[0].item_no + '_' + d.custgrp + '_' + d.custcode + '" name="oditem_'+ d.item[0].item_no +'_uom" class="opt-uom">'+ 
											uom_opt +
										'</select>' +
									'</div>' +
								'</div>' +											
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="ui-block-b">' +
						'<div class="ui-grid-a">' +
							'<div class="ui-block-a">' +
								'<div class="ui-input-text ui-body-inherit ui-corner-all ui-mini ui-shadow-inset">' +
									'<input class="input-unitprice unp_' + d.item[0].item_no + '" type="text" name="oditem_'+d.item[0].item_no+'_unitprice" placeholder="@Unit Price" value="'+ d.price[0].price+'" readonly>' +
								'</div>' +
							'</div>' +
							'<div class="ui-grid-b">' +
								'<div class="ui-input-text ui-body-inherit ui-corner-all ui-mini ui-shadow-inset">' +
									'<input class="input-total tot_' + d.item[0].item_no + '" type="text" name="oditem_'+d.item[0].item_no+'_total" placeholder="Total" value="0" readonly>' +
								'</div>' +
							'</div>' +
						'</div> '+
					'</div>' +
				'</div>' +
			'</li>';

   /* 		var itemnode2 = 
			'<li class="list-group-item item-'+d.item[0].item_no+'">' 
			+ '<div class="form-group row ordered_item">'
			+ 	 '<div class="col-xs-12" style="margin-bottom:5px;"><div class="row">'
			+    '<div class="col-xs-1" style="padding-right: 2px;"> <label class="control-label" for="oditem_'+d.item[0].item_no+'_itemno">'+ d.item[0].item_no+'</div>'
			+    '<div class="col-xs-11"><label class="control-label">'+ d.item[0].item_descr +'</label> </div>'
			+    '<input type="hidden" name="oditem_'+d.item[0].item_no+'_item_desc" value="'+ d.item[0].item_descr +'"/>'
			+    '<div class="col-xs-12"><input type="hidden" name="oditem_'+d.item[0].item_no+'_itemid" value=""/> </div>'
			+    '<div class="col-xs-12"><input type="hidden" name="oditem_'+d.item[0].item_no+'_itemno" value="'+d.item[0].item_no+'" /> </div>'
			+    '</div></div>'
			+    '<div class="col-xs-2" style="padding-right: 2px;"> <input id="' + d.item[0].item_no + '" class="form-control input-sm input-qty qty_'+d.item[0].item_no+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+d.item[0].item_no+'_qty" placeholder="Qty" required></div>'
			+    '<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;"> <select id="'+ d.item[0].comp_code + '_' + d.item[0].item_no + '_' + d.custgrp + '_' + d.custcode + '" name="oditem_'+ d.item[0].item_no +'_uom" class="form-control input-sm opt-uom">'+ uom_opt +' </select></div>'
			+    '<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="form-control input-sm input-unitprice unp_' + d.item[0].item_no + '" type="text" name="oditem_'+d.item[0].item_no+'_unitprice" placeholder="@Unit Price" value="'+ d.price[0].price+'" readonly></input> </div> </div>'	
			+    '<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="form-control input-sm input-total tot_' + d.item[0].item_no + '" type="text" name="oditem_'+d.item[0].item_no+'_total" placeholder="Total" value="0" readonly></div> </div>'
			+ 	 '<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;"> <a id="'+d.item[0].item_no+'" class="btn btn-danger fa fa-minus-circle btn-del-item"></a></div>'
			+ '</div>'
			+'</li>';
*/

			//set an timer delay when adding a new item node
			
			var exist = $('#ordered_itemlist').find('li.item-'+d.item[0].item_no)
			//console.log(exist);
			if (exist.length>0) {
				//console.log($(event.target).parent());
				$('#error_item').html('Item existed in the list').fadeIn('slow').delay(1000).hide(1);
			}
			else{
				$('#msg_success').html('Item No:'+ d.item[0].item_no +' has been added to order list').fadeIn('slow').delay(1000).hide(1);
				setTimeout(function(){
					$('#ordered_itemlist').fadeIn(500, function(){
						$(this).append(itemnode);
					});					
				}, 300)
			}
		},
		complete: function(){
			
		},
		error: function(xhr, status, error) {
		  var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}
	});
});


$('#ordered_itemlist').on('click', function(event){
	if($(event.target).hasClass('btn-del-item')){
		$(event.target).parent().parent().fadeOut(500, function(){			
			var curr_subtotal = $('#order_subtotal')[0].value;
			//var new_subtotal = curr_subtotal - 
			var item_value = $('#ordered_itemlist').find('.tot_'+event.target.id)[0].value;
			console.log(item_value);
			var new_subtotal = curr_subtotal - item_value;

			$('#order_subtotal').val(CurrencyFormatted(new_subtotal));
			$('#order_gst').val(CurrencyFormatted(new_subtotal*0.07));
			$('#order_total').val(CurrencyFormatted(new_subtotal + (new_subtotal*0.07)));
			//console.log(curr_subtotal);
			$(this).remove();

		});
	}
})


$('#ordered_itemlist').on('change', function(event){
	var subtotal = 0;
	console.log(event.target);
	//console.log(event.target);
	itemtotal = $('#ordered_itemlist').find('.input-total');
	console.log('change'+itemtotal);
	for(var i = 0; i < itemtotal.length; i++){
		var itemtot =itemtotal[i].value;
		subtotal += parseInt(itemtot); 
		console.log('here?'+itemtot);
	}

	gst = (subtotal*7)/100;
	total = +subtotal + +gst; 
	//console.log(subtotal);
	$('#order_subtotal').val(CurrencyFormatted(subtotal));
	$('#order_gst').val(CurrencyFormatted(gst));
	$('#order_total').val(CurrencyFormatted(total));	

	if($(event.target).hasClass('opt-uom')){
		subtotal = 0;
		getpriceinfo = event.target.id;
		//console.log($('#ordered_itemlist').find('.unp_'+itemid)[0].value);
		//if the input change, get new price and update the element.
		var iteminfo = getpriceinfo.split('_');
		//console.log(iteminfo);
		var uominfo = event.target.value;
		var urlgetprice = '/price/'+ iteminfo[0] +'/'+ iteminfo[1] +'/'+ uominfo +'/'+ iteminfo[2] +'/'+ iteminfo[3] + '/' + '0';
		//console.log(urlgetprice);
		$.ajax({
			type: "GET",
			dataType: "json",
			url: urlgetprice,
			success: function (d) {	
				uomprice = d[0].price;
				$($('#ordered_itemlist').find('.unp_'+iteminfo[1])[0]).val(uomprice);
				
				var itemid = event.target.id;
				//console.log(this);
				var unitprice = $('#ordered_itemlist').find('.unp_'+iteminfo[1]).val();
				var qty = $('#ordered_itemlist').find('.qty_'+iteminfo[1]).val();
				//console.log(unitprice);
				var unformat_totalprice = qty*unitprice;
				var totalprice = CurrencyFormatted(unformat_totalprice);
				console.log($('#ordered_itemlist').find('.tot_'+iteminfo[1]).val(totalprice));
	
				itemtotal = $('#ordered_itemlist').find('.input-total');
				console.log('change'+itemtotal);
				for(var i = 0; i < itemtotal.length; i++){
					var itemtot =itemtotal[i].value;
					subtotal += parseInt(itemtot); 
					console.log('here?'+itemtot);
				}

				gst = (subtotal*7)/100;
				total = +subtotal + +gst; 
				//console.log(subtotal);
				$('#order_subtotal').val(CurrencyFormatted(subtotal));
				$('#order_gst').val(CurrencyFormatted(gst));
				$('#order_total').val(CurrencyFormatted(total));	
							
			}
		});	
	}
});


//Dynamically calculate the input by key in the QTY...
$('#ordered_itemlist').on('keyup', function(event){
	//console.log('inhere');
	var itemid = event.target.id;
	var unitprice = $(this).find('.unp_'+itemid).val();
	var unformat_totalprice = event.target.value*unitprice;
	var totalprice = CurrencyFormatted(unformat_totalprice);
	$(this).find('.tot_'+itemid).val(totalprice);
});


//validation
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

//function to format the input text to a currency format e.q. 130.00
function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

$(document).bind('mobileinit',function(){

    $.extend(  $.mobile , {
        ajaxFormsEnabled: true,
        ajaxLinksEnabled: true,
        ajaxEnabled: true

    });
});


