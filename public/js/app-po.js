//get vendor no based on the group code
$('#select_vendor_grp').on('change', function(){
	//console.log('click');
	$.ajax({
		type:"GET",
		dataType: "json",
		url:'/vendor/'+$('#select_vendor_grp').val()+'/group',
		success: function(d){
			//console.log(d.vendors);
			$('#select_vendor_no').html('<option value="">NONE</option>');
			d.vendors.forEach(function(entry){
				//console.log(entry.vend_no);
				$('#select_vendor_no').append(
					'<option value="' + entry.vend_no + '">'+ entry.vend_no + ' - ' + entry.vend_name +'</option>'
				);
			});
		},
		error: function(xhr, status, error) {
			//handler when ajax return errors
		  	var err = eval("(" + xhr.responseText + ")");
		},
		complete: function(){
			//handler when ajax is completed
		}
	});
});

//get vendor details
$('#select_vendor_no').on('change', function(){
	//console.log('click');
	$.ajax({
		type:"GET",
		dataType: "json",
		url:'/vendor/' + $('#select_vendor_no').val(),
		success: function(d){
			console.log(d);
			//$('#select_vendor_no').html('<option value="">NONE</option>');
			
			$('#input_vendor_name').val(d.vendors.vend_name);
			$('#input_vendor_addr').val(d.vendors.addr);
			$('#input_vendor_contact_person').val(d.vendors.cont_name);
			$('#input_vendor_contact_no').val(d.vendors.phone_no1);
			$('#select_vendor_curr_code').val(d.vendors.curr_code);
			$('#input_vendor_curr_rate').val(d.curr_rate.exch_rate);

			//reset the list
			$('#pordered_itemlist').html(''); 
			d.presets.forEach(function(entry){
				console.log(entry);
				var uom_option = "";
				entry.uom.forEach(function(uomi){
					//console.log(uomi);
					if(entry.item_detail.uom == uomi.uom) uom_option += '<option value="' + uomi.uom + '" selected class="opt-uom">' + uomi.uom + '</option>';	
					else uom_option += '<option value="' + uomi.uom + '" class="opt-uom">' + uomi.uom + '</option>';	
				})
				var price = entry.item_detail.price;
				if(entry.prev_price){
					price = entry.prev_price;
				}

				//create preset item list
	    		var itemnode = 
				'<li class="list-group-item item-'+entry.item_detail.item_id+'" id="po'+entry.item_detail.s_no+'" >' +
				    '<div class="form-group row ordered_item">' +
				 	   '<div class="col-xs-12" style="margin-bottom:5px;">' +
				 	        '<div class="row">' +
				     		    '<div class="col-xs-1" style="padding-right: 2px;">' +
				       				'<label class="control-label" for="oditem_'+entry.item_detail.item_id+'_itemno" >'+ entry.item_detail.item_no+'</label>' +
				    			'</div>' +
				    			'<div class="col-xs-11">' +
				    				'<label class="control-label">'+ entry.item_detail.descr +'</label>' +
				    			'</div>' +
				    			'<input type="hidden" name="oditem_'+entry.item_detail.item_id+'_descr" value="'+ entry.item_detail.descr +'"/>' +
				    			'<input type="hidden" name="oditem_'+entry.item_detail.item_id+'_itemno" value="'+entry.item_detail.item_no+'"/>' +
				    			'<input type="hidden" name="oditem_'+entry.item_detail.item_id+'_itemid" value="'+entry.item_detail.item_id+'"/>' +
				    		'</div>' +
				    	'</div>' +
				    	'<div class="col-xs-2" style="padding-right: 2px;">' +
				    		'<input id="' + entry.item_detail.item_id + '" class="form-control input-sm input-qty qty_'+entry.item_detail.item_id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+entry.item_detail.item_id+'_qty" placeholder="Qty" required>' +
				    	'</div>' +
				    	//uom
				    	'<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;">' + 
				    		'<select id="'+ entry.item_detail.comp_code + '_' + entry.item_detail.item_no + '_' + d.custgrp + '_' + d.custcode + '" name="oditem_'+ entry.item_detail.item_id +'_uom" class="form-control input-sm opt-uom">'+ uom_option +' </select>'+
				    		'<input type="hidden" name="oditem_'+entry.item_detail.item_id+'_uomcf" value="1.0000"/>' +
				    	'</div>'+
				    
				    	'<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> ' +
				    		'<div class="input-group">' + 
				    			'<span class="input-group-addon">$</span> ' +
				    			'<input id="'+entry.item_detail.item_id+'" class="text-right form-control input-sm input-unitprice unp_' + entry.item_detail.item_id + '" type="text" name="oditem_'+entry.item_detail.item_id+'_unitprice" placeholder="@Unit Price" value="'+ price +'" onkeypress="return isNumberKey(event)"/> ' +
				    		'</div>' + 
				    	'</div>' +
				    	'<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> ' +
				    		'<div class="input-group"> ' +
				    			'<span class="input-group-addon">$</span> ' +
				    			'<input class="text-right form-control input-sm input-total tot_' + entry.item_detail.item_id + '" type="text" name="oditem_'+entry.item_detail.item_id+'_total" placeholder="Total" value="0" readonly>' +
				    		'</div>' +
				    	'</div>' +
				 	 	'<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;">' + 
				 	 	    '<a id="'+entry.item_detail.item_id+'" class="btn btn-danger fa fa-minus-circle btn-vend-del-item"></a>' +
				 	 	'</div>' +
				    '</div>' +
				'</li>';				
			
				$('#pordered_itemlist').append(itemnode);

			});

			//sort the list by their s_no from preset item
			var POlist = $('#pordered_itemlist');
			var listitems = POlist.children('li').get();
			
			listitems.sort(function(a, b) {
			   var compA = $(a)[0].id;
			   console.log(compA);
			   var compB = $(b)[0].id;
			   console.log(compB);
			   return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
			})
			console.log(listitems);
			$.each(listitems, function(idx, itm) { POlist.append(itm); });
			
		},
		error: function(xhr, status, error) {
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		},
		complete: function(){	

		}
	});
});

//get currency exchange rate
$('#select_vendor_curr_code').on('change', function(){
	//console.log('click');
	$.ajax({
		type:"GET",
		dataType: "json",
		url:'/currency/exch_rate/' + $('#select_vendor_curr_code').val() + '/period/' + '2014',
		success: function(d){
			//console.log(d);
			//$('#select_vendor_no').html('<option value="">NONE</option>');
			$('#input_vendor_curr_rate').val(d.exch_rate);
		},
		error: function(xhr, status, error) {
			$('#input_vendor_curr_rate').val('NONE');
		  	//var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		},
		complete: function(){		

		}
	});
});

//add item to pordered list
$('.btn-vend-item-add').click(function(event){
	console.log(event.target.id);
	var cust_code = $('#select_vendor_no').val();
	var userid = $('#user_id').val();
	var ajax_url  = '/item/'+event.target.id+'/vend/'+cust_code+'/user/' + userid;
	console.log(ajax_url);
	$.ajax({
		type: "GET",
		dataType: "json",
		url: ajax_url,
		success: function (d) {
			console.log(d);
			var uom_opt = "";
			for (var i = 0; i < d.uom.length; i++) {
				if(d.item.uom == d.uom[i].uom) 
					uom_opt += '<option value="' + d.uom[i].uom + '" selected class="opt-uom">' + d.uom[i].uom + '</option>';	
				else uom_opt += '<option value="' + d.uom[i].uom + '" class="opt-uom">' + d.uom[i].uom + '</option>';	
    		}
    		console.log(d);
    		var itemnode = 
			'<li class="list-group-item item-'+d.item.id+'">' +
			    '<div class="form-group row ordered_item">' +
			 	   '<div class="col-xs-12" style="margin-bottom:5px;">' +
			 	        '<div class="row">' +
			     		    '<div class="col-xs-1" style="padding-right: 2px;">' +
			       				'<label class="control-label" for="oditem_'+d.item.id+'_itemno" >'+ d.item.item_no+'</label>' +
			    			'</div>' +
			    			'<div class="col-xs-11">' +
			    				'<label class="control-label">'+ d.item.item_descr +'</label>' +
			    			'</div>' +
			    			'<input type="hidden" name="oditem_'+d.item.id+'_descr" value="'+ d.item.item_descr +'"/>' +
			    			'<input type="hidden" name="oditem_'+d.item.id+'_itemno" value="'+d.item.item_no+'"/>' +
			    			'<input type="hidden" name="oditem_'+d.item.id+'_itemid" value="'+d.item.id+'"/>' +
			    		'</div>' +
			    	'</div>' +
			    	'<div class="col-xs-2" style="padding-right: 2px;">' +
			    		'<input id="' + d.item.id + '" class="form-control input-sm input-qty qty_'+d.item.id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+d.item.id+'_qty" placeholder="Qty" required>' +
			    	'</div>' +
			    	'<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;">' + 
			    		'<select id="'+ d.item.comp_code + '_' + d.item.item_no + '_' + d.custgrp + '_' + d.custcode + '" name="oditem_'+ d.item.id +'_uom" class="form-control input-sm opt-uom">'+ uom_opt +' </select>'+
			    		'<input type="hidden" name="oditem_'+d.item.id+'_uomcf" value="1.0000"/>' +
			    	'</div>'+
			    
			    	'<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> ' +
			    		'<div class="input-group">' + 
			    			'<span class="input-group-addon">$</span> ' +
			    			'<input id="'+d.item.id+'" class="text-right form-control input-sm input-unitprice unp_' + d.item.id + '" type="text" name="oditem_'+d.item.id+'_unitprice" placeholder="@Unit Price" value="'+ d.price+'" onkeypress="return isNumberKey(event)"/> ' +
			    		'</div>' + 
			    	'</div>' +
			    	'<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> ' +
			    		'<div class="input-group"> ' +
			    			'<span class="input-group-addon">$</span> ' +
			    			'<input class="text-right form-control input-sm input-total tot_' + d.item.id + '" type="text" name="oditem_'+d.item.id+'_total" placeholder="Total" value="0" readonly>' +
			    		'</div>' +
			    	'</div>' +
			 	 	'<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;">' + 
			 	 	    '<a id="'+d.item.id+'" class="btn btn-danger fa fa-minus-circle btn-vend-del-item"></a>' +
			 	 	'</div>' +
			    '</div>' +
			'</li>';
			//set an timer delay when adding a new item node
			
			var exist = $('#pordered_itemlist').find('li.item-'+d.item.id)

			if (exist.length>0) {
				//console.log($(event.target).parent());
				$('#error_item').html('Item existed in the list').fadeIn('slow').delay(1000).hide(1);
			}
			else{
				$('#msg_success').html('Item No:'+ d.item.item_no +' has been added to order list').fadeIn('slow').delay(1000).hide(1);
				setTimeout(function(){
					$('#pordered_itemlist').fadeIn(500, function(){
						$(this).append(itemnode);
					});					
				}, 300)

			}
		},
		complete: function(){
			
		}
	});
});


//Order list items events -------------------------------------------------------------------------------------------
$('#pordered_itemlist').on('click', function(event){
	if($(event.target).hasClass('btn-vend-del-item')){
		var curr_subtotal = $('#order_subtotal')[0].value;
		//var new_subtotal = curr_subtotal - 
		var item_value = $('#pordered_itemlist').find('.tot_'+event.target.id)[0].value;
		//console.log(item_value);
		var new_subtotal = curr_subtotal - item_value;

		$('#order_subtotal').val(CurrencyFormatted(new_subtotal));
		$('#order_gst').val(CurrencyFormatted(new_subtotal*0.07));
		$('#order_total').val(CurrencyFormatted(new_subtotal + (new_subtotal*0.07)));	
		$(event.target).parent().parent().parent()[0].remove();

	}
})

$('#pordered_itemlist').on('change', function(event){
	var subtotal = 0;
	console.log(event.target);

	itemtotal = $('#pordered_itemlist').find('.input-total');
	console.log('change'+itemtotal);
	for(var i = 0; i < itemtotal.length; i++){
		var itemtot =itemtotal[i].value;
		subtotal += parseInt(itemtot); 
		console.log('here?'+itemtot);
	}

	gst = (subtotal*7)/100;
	total = +subtotal + +gst; 
	//console.log(subtotal);
	$('#order_subtotal').val(CurrencyFormatted(subtotal));
	$('#order_gst').val(CurrencyFormatted(gst));
	$('#order_total').val(CurrencyFormatted(total));	

	//when selecting UOM selection box in each item
/*	if($(event.target).hasClass('opt-uom')){
		subtotal = 0;
		getpriceinfo = event.target.id;
		//console.log($('#ordered_itemlist').find('.unp_'+itemid)[0].value);
		//if the input change, get new price and update the element.
		var iteminfo = getpriceinfo.split('_');
		//console.log(iteminfo);
		var uominfo = event.target.value;
		//var urlgetprice = '/price/'+ iteminfo[0] +'/'+ iteminfo[1] +'/'+ uominfo +'/'+ iteminfo[2] +'/'+ iteminfo[3] + '/' + '0';
		//console.log(urlgetprice);
		$.ajax({
			type: "GET",
			dataType: "json",
			url: 'url',
			success: function (d) {	

			}
		});	
	}
*/

});

//Dynamically calculate the input by key in the QTY...
$('#pordered_itemlist').on('keyup', function(event){
	var unitprice;
	var unformat_totalprice;
	var totalprice;
	var itemid = event.target.id;

	if($(event.target).hasClass('input-qty')){	
		unitprice = $(this).find('.unp_'+itemid).val();
		unformat_totalprice = event.target.value*unitprice;
		totalprice = CurrencyFormatted(unformat_totalprice);
	}
	else if($(event.target).hasClass('input-unitprice')){
		//console.log('got');
		var quantity = $(this).find('.qty_'+itemid).val();
		//console.log(quantity);
		unformat_totalprice = event.target.value*quantity;
		totalprice = CurrencyFormatted(unformat_totalprice);
	}

	$(this).find('.tot_'+itemid).val(totalprice);		
	
});


//---------------------------------------------------------------------------------------------------------------




