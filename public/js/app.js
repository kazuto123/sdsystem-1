
//date picker setup
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    todayBtn: true,
    autoclose: true,
    todayHighlight: true,
    minView:2
});


//enable the bootstrap-selectpicker
$('.selectpicker').selectpicker();

$(".btn-create-order").popover({ trigger: "hover" });
$(".btn-view-order").popover({ trigger: "hover" });
$(".btn-edit-order").popover({ trigger: "hover" });

$('#error_code').hide();
$('#error_item').hide();
$('#msg_success').hide();

$('#btn_hidedetail').on('click', function() {
	if ( $( "#torder_details" ).is( ":hidden" ) ) {
    	$( "#torder_details" ).slideDown();
  	} else {
    	$( "#torder_details" ).slideUp();
  	}
});

//retrieve outlet code/customer number based on their group
$('#input_custgrp').on('change', function(event){
	var data_url = '/customer/group/' + $(event.target).val(); 

	$.ajax({
		type: "GET",
		dataType: "json",
		url: data_url,
		success: function (d) {
			//console.log(d);
			$('#input_outletcode').html('<option value="None" selected>None</option>');
			d.forEach(function(entry){
				$('#input_outletcode').append(
					'<option value="' + entry.cust_no + '">'+ entry.cust_no + " - " + entry.cust_name +'</option>'
				);
			})
		},
		complete: function(){
			//if complete do the following
		},
		error: function(xhr, status, error) {
			//if error, do the following
		  	var err = eval("(" + xhr.responseText + ")");
		  	//$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		  	console.log(err);
		}
	});	
});


//retrieve order detail base on the order code selected...
$('#input_outletcode').change(function () {
	var data_url = '/customer/info/' + $(event.target).val(); 
	//console.log(data_url);
	$.ajax({
		type:"GET",
		dataType: "json",
		url:data_url,
		success: function(d){
			console.log(d);
			$('#input_contactperson').val(d.cust.cust_cont_name);
			var addr_opt = "";
			for(var i = 0; i < d.addr.length; i++){
				addr_opt += '<option value="' + d.addr[i].addr_ref + '"> '+ d.addr[i].addr + '</option>';
			}

			$('#input_deliveryaddr').html(addr_opt);
        	//$('#input_deliverydriver').val(d.cust[0].salesman).attr('value', d.cust.salesman);
        	$('#input_deliverydriver').val(d.cust.salesman);
        	//console.log(d.cust[0].salesman);
        	$('#input_torder_cust_name').val(d.cust.cust_name);
        	//console.log(d.cust[0]);
        	$('#input_contactno').val(d.addr[0].cont_off_tel);
        	console.log(d.cust.cust_curr_code);
        	$('#select_customer_curr_code').val(d.cust.cust_curr_code);
        	$('#input_curr_rate').val(d.curr_rate.exch_rate);
        	//$('#input_delivery_addr').val(d.addr[0].addr_ref);
        	//create preset list
        	$('#ordered_itemlist').html('');
        	d.preset.forEach(function(entry){

        		//console.log(entry);
        		var uom_option = "";
        		entry.uom_list.forEach(function(uom){
        			if(entry[0].uom == uom.uom){	
        				uom_option += '<option value="' + uom.uom + '" selected class="opt-uom-to">' + uom.uom + '</option>';
        			}else{
        				uom_option += '<option value="' + uom.uom + '" class="opt-uom-to">' + uom.uom + '</option>';
        			}
        		})

        		console.log(entry);
        		$('#ordered_itemlist').append(
				'<li class="list-group-item item-'+entry[0].item_id+'">' 
				+ '<div class="form-group row ordered_item">'
				+ 	 '<div class="col-xs-12" style="margin-bottom:5px;"><div class="row">'
				+    '<div class="col-xs-1" style="padding-right: 2px;"> <label class="control-label" for="oditem_'+entry[0].item_id+'_itemno">'+ entry[0].item_no+'</div>'
				+    '<div class="col-xs-11"><label class="control-label">'+ entry[0].descr +'</label> </div>'
				+    '<input type="hidden" name="oditem_'+entry[0].item_id+'_descr" value="'+ entry[0].descr +'"/>'
				+    '<input type="hidden" name="oditem_'+entry[0].item_id+'_itemid" value="'+ entry[0].item_id +'"/>'
				+    '<input type="hidden" name="oditem_'+entry[0].item_id+'_itemno" value="'+entry[0].item_no+'"/>'
				+    '<input type="hidden" name="oditem_'+entry[0].item_id+'_uomcf" value="1.0000"/>'
				+    '</div></div>'
				+    '<div class="col-xs-2" style="padding-right: 2px;"> <input id="' + entry[0].item_id + '" class="form-control input-sm input-qty qty_'+entry[0].item_id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+entry[0].item_id+'_qty" placeholder="Qty" required></div>'
				+    '<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;"> <select id="'+ entry[0].comp_code + '_' + entry[0].item_no + '_' + d.cust.cust_group_code + '_' + entry[0].preset_by + '" name="oditem_'+ entry[0].item_id +'_uom" class="form-control input-sm opt-uom">'+ uom_option +' </option></select></div>'
				+    '<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="text-right form-control input-sm input-unitprice unp_' + entry[0].item_id + '" type="text" name="oditem_'+entry[0].item_id+'_unitprice" placeholder="@Unit Price" value="'+ entry[0].price+'" readonly></input> </div> </div>'	
				+    '<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="text-right form-control input-sm input-total tot_' + entry[0].item_id + '" type="text" name="oditem_'+entry[0].item_id+'_total" placeholder="Total" value="0" readonly></div> </div>'
				+ 	 '<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;"> <a id="'+entry[0].item_id+'" class="btn btn-danger fa fa-minus-circle btn-del-item"></a></div>'
				+ '</div>'
				+'</li>'
        		);
        	});

		},
		error: function(xhr, status, error) {
		  var err = eval("(" + xhr.responseText + ")");
		  	$('#error_code').hide().html('Item Not Found').fadeIn('slow').delay(2000).hide(1);
		},
		complete: function(){
		}
	});
});


$('.btn-item-add').click(function(event){
	//console.log(event.target.id);
	var cust_code = $('#input_outletcode').val();
	var ajax_url  = '/item/'+event.target.id+'/cust/'+cust_code;
	//console.log(ajax_url);
	$.ajax({
		type: "GET",
		dataType: "json",
		url: ajax_url,
		success: function (d) {
			//console.log(d);
			var uom_opt = "";
			for (var i = 0; i < d.uom.length; i++) {
				if(d.item[0].uom == d.uom[i].uom) uom_opt += '<option value="' + d.uom[i].uom + '" selected class="opt-uom">' + d.uom[i].uom + '</option>';	
				else uom_opt += '<option value="' + d.uom[i].uom + '" class="opt-uom">' + d.uom[i].uom + '</option>';	
    		}
    		console.log(d);
    		var itemnode = 
			'<li class="list-group-item item-'+d.item[0].id+'" >' 
			+ '<div class="form-group row ordered_item">'
			+ 	 '<div class="col-xs-12" style="margin-bottom:5px;"><div class="row">'
			+    '<div class="col-xs-1" style="padding-right: 2px;"> <label class="control-label" for="oditem_'+d.item[0].id+'_itemno">'+ d.item[0].item_no+'</div>'
			+    '<div class="col-xs-11"><label class="control-label">'+ d.item[0].item_descr +'</label> </div>'
			+    '<input type="hidden" name="oditem_'+d.item[0].id+'_descr" value="'+ d.item[0].item_descr +'"/>'
			+    '<input type="hidden" name="oditem_'+d.item[0].id+'_itemid" value="'+d.item[0].id+'"/>'
			+    '<input type="hidden" name="oditem_'+d.item[0].id+'_itemno" value="'+d.item[0].item_no+'"/>'
			+    '<input type="hidden" name="oditem_'+d.item[0].id+'_uomcf" value="1.0000"/>' //hardcoded value
			+    '</div></div>'
			+    '<div class="col-xs-2" style="padding-right: 2px;"> <input id="' + d.item[0].id + '" class="form-control input-sm input-qty qty_'+d.item[0].id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+d.item[0].id+'_qty" placeholder="Qty" required></div>'
			+    '<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;">' 
			+        '<select id="'+ d.item[0].comp_code + '_' + d.item[0].item_no + '_' + d.custgrp + '_' + d.custcode + '" name="oditem_'+ d.item[0].id +'_uom" class="form-control input-sm opt-uom">'+ uom_opt +' </select>'
			+    '</div>'
			+    '<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="text-right form-control input-sm input-unitprice unp_' + d.item[0].id + '" type="text" name="oditem_'+d.item[0].id+'_unitprice" placeholder="@Unit Price" value="'+ d.price[0].price+'" readonly></input> </div> </div>'	
			+    '<div class="col-xs-4" style="padding-right: 2px;padding-left: 2px;"> <div class="input-group"> <span class="input-group-addon">$</span> <input class="text-right form-control input-sm input-total tot_' + d.item[0].id + '" type="text" name="oditem_'+d.item[0].id+'_total" placeholder="Total" value="0" readonly></div> </div>'
			+ 	 '<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;"> <a id="'+d.item[0].id+'" class="btn btn-danger fa fa-minus-circle btn-del-item"></a></div>'
			+ '</div>'
			+'</li>';

			//set an timer delay when adding a new item node
			
			var exist = $('#ordered_itemlist').find('li.item-'+d.item[0].id)

			if (exist.length>0) {
				//console.log($(event.target).parent());
				$('#error_item').html('Item existed in the list').fadeIn('slow').delay(1000).hide(1);
			}
			else{
				$('#msg_success').html('Item No:'+ d.item[0].item_no +' has been added to order list').fadeIn('slow').delay(1000).hide(1);
				//append the items added into the order list
				//setTimeout(function(){}, 300)
				$('#ordered_itemlist').fadeIn(500, function(){
					$(this).append(itemnode);
				});

			}

		},
		complete: function(){
			
		}
	});
});

$('#ordered_itemlist').on('click', function(event){
	if($(event.target).hasClass('btn-del-item')){
		$(event.target).parent().parent().parent().fadeOut(500, function(){			
			var curr_subtotal = $('#order_subtotal')[0].value;
			//var new_subtotal = curr_subtotal - 
			var item_value = $('#ordered_itemlist').find('.tot_'+event.target.id)[0].value;
			console.log(item_value);
			var new_subtotal = curr_subtotal - item_value;

			$('#order_subtotal').val(CurrencyFormatted(new_subtotal));
			$('#order_gst').val(CurrencyFormatted(new_subtotal*0.07));
			$('#order_total').val(CurrencyFormatted(new_subtotal + (new_subtotal*0.07)));
			//console.log(curr_subtotal);
			$(this).remove();

		});
	}
})

$('#ordered_itemlist').on('change', function(event){
	var subtotal = 0;
	//console.log(event.target);
	//console.log(event.target);
	itemtotal = $('#ordered_itemlist').find('.input-total');
	console.log('change'+itemtotal);
	for(var i = 0; i < itemtotal.length; i++){
		var itemtot =itemtotal[i].value;
		subtotal += parseInt(itemtot); 
		//console.log('here?'+itemtot);
	}

	gst = (subtotal*7)/100;
	total = +subtotal + +gst; 
	//console.log(subtotal);
	$('#order_subtotal').val(CurrencyFormatted(subtotal));
	$('#order_gst').val(CurrencyFormatted(gst));
	$('#order_total').val(CurrencyFormatted(total));	

	if($(event.target).hasClass('opt-uom')){
		subtotal = 0;
		getpriceinfo = event.target.id;
		//console.log($('#ordered_itemlist').find('.unp_'+itemid)[0].value);
		//if the input change, get new price and update the element.
		var iteminfo = getpriceinfo.split('_');
		//console.log(iteminfo);
		var uominfo = event.target.value;
		var urlgetprice = '/price/'+ iteminfo[0] +'/'+ iteminfo[1] +'/'+ uominfo +'/'+ iteminfo[2] +'/'+ iteminfo[3] + '/' + '0';
		//console.log(urlgetprice);
		$.ajax({
			type: "GET",
			dataType: "json",
			url: urlgetprice,
			success: function (d) {	
				uomprice = d[0].price;
				$($('#ordered_itemlist').find('.unp_'+iteminfo[1])[0]).val(uomprice);
				
				var itemid = event.target.id;
				//console.log(this);
				var unitprice = $('#ordered_itemlist').find('.unp_'+iteminfo[1]).val();
				var qty = $('#ordered_itemlist').find('.qty_'+iteminfo[1]).val();
				//console.log(unitprice);
				var unformat_totalprice = qty*unitprice;
				var totalprice = CurrencyFormatted(unformat_totalprice);
				console.log($('#ordered_itemlist').find('.tot_'+iteminfo[1]).val(totalprice));
	
				itemtotal = $('#ordered_itemlist').find('.input-total');
				console.log('change'+itemtotal);
				for(var i = 0; i < itemtotal.length; i++){
					var itemtot =itemtotal[i].value;
					subtotal += parseInt(itemtot); 
					console.log('here?'+itemtot);
				}

				gst = (subtotal*7)/100;
				total = +subtotal + +gst; 
				//console.log(subtotal);
				$('#order_subtotal').val(CurrencyFormatted(subtotal));
				$('#order_gst').val(CurrencyFormatted(gst));
				$('#order_total').val(CurrencyFormatted(total));	
							
			}
		});	
	}
});

//Dynamically calculate the input by key in the QTY...
$('#ordered_itemlist').on('keyup', function(event){
	//console.log('inhere');
	var itemid = event.target.id;
	var unitprice = $(this).find('.unp_'+itemid).val();
	var unformat_totalprice = event.target.value*unitprice;
	var totalprice = CurrencyFormatted(unformat_totalprice);
	$(this).find('.tot_'+itemid).val(totalprice);
});

//category item 
$('.cat-button').click(function(event){
	console.log(event.target.id);
	//var target = '.'+event.target.id;
	var containClass = $('#cat_itemlist').find('li').hasClass(event.target.id);
	var allList = $('#cat_itemlist').find('li');
	//console.log(test);
	for(var i=0; i < allList.length; i++){
		if($(allList[i]).hasClass(event.target.id)){
			$(allList[i]).fadeIn(300, function(){
				$(this).show();
			});	
			continue;
		}else{
			$(allList[i]).fadeOut(300, function(){
				$(this).hide();
			});	
		}
	}
});

$('#searchbox').keyup(function(){
   var valThis = $(this).val().toLowerCase();
    $('.all_item_list>li').each(function(){
     var text = $(this).text().toLowerCase();
    //($.trim(text).indexOf(valThis) == 0) ? $(this).show() : $(this).hide();  
   });
});
//Search Filter Functions
(function ($) {
  // custom css expression for a case-insensitive contains()
  jQuery.expr[':'].Contains = function(a,i,m){
      return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
  };

  function listFilter(header, list) { // header is any element, list is an unordered list
    // create and add the filter form to the header
    var form = $("<form>").attr({"class":"filterform","action":"#"}),
        input = $("<input>").attr({"class":"filterinput form-control","type":"text", "placeholder":"Search For Items"});
    $(form).append(input).appendTo(header);

    $(input).change( function () {
        var filter = $(this).val(); //value from the input box
        if(filter) {
          // this finds all links in a list that contain the input,
          // and hide the ones not containing the input while showing the ones that do
          $(list).find("span:not(:Contains(" + filter + "))").parent().slideUp();
          $(list).find("span:Contains(" + filter + ")").parent().slideDown();
        } else {
          $(list).find("li").slideDown();
        }
        return false;
      })
    .keyup( function () {
        // fire the above change event after every letter
        $(this).change();
    });
  }

  //ondomready
  $(function () {
    listFilter($("#search_header"), $("#cat_itemlist"));
  });
}(jQuery));

//validation
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57) return false;
    //return true;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46) return true;
    return false;
}

//function to format the input text to a currency format e.q. 130.00
function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

//sort the unordered list 
function sortUnorderedList(ul, sortDescending) {
  if(typeof ul == "string")
    ul = document.getElementById(ul);

  var lis = ul.getElementsByTagName("LI");
  var vals = [];

  for(var i = 0, l = lis.length; i < l; i++)
    vals.push(lis[i].innerHTML);

  vals.sort();

  if(sortDescending)
    vals.reverse();

  for(var i = 0, l = lis.length; i < l; i++)
    lis[i].innerHTML = vals[i];
}



function sortOrders(){
	//var myYears = ['Freshman', 'Sophomore', 'Junior', 'Senior'];
	var myList = $('#ordered_itemlist ul');
	var listItems = myList.children('li').get();
	console.log('Item:')
	console.log(listItems);
	listItems.sort(function(a,b){
	var compA = $.inArray($(a).find('.year').text(), myYears);
	var compB = $.inArray($(b).find('.year').text(), myYears);
	return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
	});
	$(myList).append(listItems);
}