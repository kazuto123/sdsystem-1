//hidden division
$('#alert_msg_item_add').hide();


$('#bom_form').bind("keyup keypress", function(e) {
  var code = e.keyCode || e.which; 
  if (code  == 13) {               
    e.preventDefault();
    return false;
  }
});

$('#bom_searchbox').on('click', function(){
	bomCode = $('#input_item_search')[0].value;
	$.ajax({
		type:"GET",
		dataType: "json",
		url:'/sdapi/get/list_bom/' + bomCode,
		success: function(data){
			//console.log('here');
			//console.log(data);
			if(!$.isEmptyObject(data)){
				var bomlist = "";
				$('#bom_listings').html('');
				data.forEach(function(entry){
				  console.log(entry);
		          bomlist = 
		          '<li class="list-group-item">' + 
		              '<div class="row">' +
		                  '<a href="/sdbom/bom/'+entry.bom_id+'_'
		                  						+entry.bom_code+'_'
		                  						+entry.revision_no+'_'
		                  						+entry.descr+'_'
		                  						+entry.item_id+'_'
		                  						+entry.item_no+'_'
		                  						+entry.qty+'_'
		                  						+entry.uom+'_'
		                  						+entry.uom_cf+'_'
		                  						+entry.is_kitting_bom+'_'
		                  						+entry.status+'_'
		                  						+entry.approved+'_'
		                  						+entry.remarks+'_'
		                  						+entry.c_userid+'_'
		                  						+entry.c_date+'_'
		                  						+entry.m_userid+'_'
		                  						+entry.m_date+'" class="col-xs-12">' +
		                      '<span style="padding-right: 2px;" class="col-xs-12 col-sm-10 col-md-4 bomlist">' +
		                          entry.bom_code +
		                      '</span>' +
		                      '<span style="padding-right: 2px;" class="col-xs-12 col-sm-10 col-md-5 bomlist">' +
		                          entry.descr +
		                      '</span>' +                      
		                      '<span style="padding-right: 2px;" class="col-xs-2 col-sm-1 col-md-1 bomlist">' +
		                          entry.status +
		                      '</span>' +
		                      '<span style="padding-right: 2px;" class="hidden-xs hidden-sm col-md-1 bomlist">' +
		                          entry.c_userid + 
		                      '</span>' +
		                      '<span class="text-right col-md-1 col-xs-4 col-sm-2 pull-right" style="font-size:10px;margin-top:3px;">' +
		                      		entry.c_date +
		                      '</span>' +
		                  '</a>' +
		              '</div>' +
		          '</li> ' ;
				
				  $('#bom_listings').append(bomlist);
				});
			}
			else{
				//console.log('empty');
				var result = 
				'<div class="alert alert-danger alert-dismissable bom-message">' +
			   	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times; </button>' +
			   	'The Bom Code Searched is not found. please try with other code.' +
				'</div>';
				$('#bom_listings').html(result);
			}

		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error');
		}
	
	});
});

$('#btn_checkbomcode').on('click', function(){
	var bomcode = $('#input_bom_code')[0].value;
	$('#message_bomcode').html('');
	$.ajax({
		type:'GET',
		dataType: 'json',
		url:'/sdapi/get/chk_bomcode/' + bomcode,
		success: function(data){
			if(data[0]['ReturnValue'] == -1 ){
				$('#message_bomcode').show(function(){
					$(this).removeClass('bom-msg-box-success');
					$(this).html("Bom code is taken, please choose another one.");
				}).delay('3000').fadeOut();
			}
			else{
				$('#message_bomcode').show(function(){
					$(this).addClass('bom-msg-box-success');
					$(this).html("BOM Code available.");
				}).delay('3000').fadeOut();	
			}
		},

		error: function(jqXHR, textStatus, errorThrown){
			console.log('error');
			//console.log(textStatus + errorThrown);
		}
	});
});

$('#select_bom_itemno').on('change', function(){
	var itemid = $(this)[0].value.split('-')[0];
	console.log(itemid);
	$.ajax({
		type:'GET',
		dataType: 'json',
		url:'/sdapi/get/bom/itemuom/' + itemid,
		success: function(data){
			console.log(data);
			//reset list
			
			$('#bom_itemlists').html('');
			$('#input_bom_itemconv').val('');
			var itemuom = "";
			$('#select_bom_itemuom').html('<option value="none"> NONE </option>');
			data.forEach(function(entry){
				itemuom = 
				'<option value="'+ entry.uom +'-'+ entry.cf_qty +'">' +
					entry.uom +
				'</option>';
				$('#select_bom_itemuom').append(itemuom);
			});
		},

		error: function(jqXHR, textStatus, errorThrown){
			console.log('error');
			//console.log(textStatus + errorThrown);
		}
	});		
});

$('#select_bom_itemuom').on('change', function(){
	var itemuom = $(this)[0].value;
	var itemsplit = itemuom.split('-');
	$('#input_bom_itemconv').val(itemsplit[1]);
	//console.log(itemsplit[1]);
});

$('.bom-category').on('click', function(){
	var catcode = $(this)[0].id;
	$.ajax({
		type:'GET',
		dataType: 'json',
		url:'/sdapi/get/bom/item/cat/' + catcode,
		success: function(data){
			console.log(data);
			//reset list
			
			$('#bom_itemlists').html('');
			var itemnode = "";

			if($.isEmptyObject(data)){
				$('#bom_itemlists').html('<div class="alert alert-danger">No Item in this category</div>');	
			}
			
			data.forEach(function(entry){
				itemnode = 
				'<li class="list-group-item list-item-add cat-item '+entry.uom + ' ' + entry.catcode+ '"> ' +
					'<span>' +
					 entry.item_no + '&nbsp&nbsp ' + entry.item_descr +
					'<div id="'+entry.item_no+'" class="btn btn-sm btn-primary btn-bom-item-add fa fa-plus-circle" style="float:right;"></div>' +
					'</span>' +
				'</li>';
				
				$('#bom_itemlists').append(itemnode);			

			});

		},

		error: function(jqXHR, textStatus, errorThrown){
			console.log('error');
			console.log(textStatus + errorThrown);
		}
	});	
});

//search item based on the item code keyed in
$('#btn_search_items').on('click', function(){
	var itemno = $('#input_item_search')[0].value;
	$.ajax({
		type:'GET',
		dataType: 'json',
		url:'/sdapi/get/bom/search/itemdesc/0/itemno/'+itemno+'/catcode/0/itemtype/0',
		success: function(data){
			console.log(data);
			if($.isEmptyObject(data)){
				$('#message_item_search').show(function(){
					$(this).removeClass('bom-msg-box-success');
					$(this).html("Item Not Found!!");
					$('#bom_itemlists').html('');
				}).delay('2000').fadeOut();				
			}
			else{
				//foreach data found, create an item list.
				data.forEach(function(entry){
					console.log(entry);	
					$('#bom_itemlists').html('');
					var itemnode = "";
					data.forEach(function(entry){
						itemnode = 
						'<li class="list-group-item list-item-add cat-item '+entry.uom + ' ' + entry.catcode+ '"> ' +
							'<span>' +
							 entry.item_no + '&nbsp&nbsp ' + entry.item_descr +
							'<div id="'+entry.item_no+'" class="btn btn-sm btn-primary btn-bom-item-add fa fa-plus-circle" style="float:right;"></div>' +
							'</span>' +
						'</li>';
						
						$('#bom_itemlists').append(itemnode);			

					});

				});
			}
		},

		error: function(jqXHR, textStatus, errorThrown){
			console.log('error');
			//console.log(textStatus + errorThrown);
		}
	});	

});

//add item to bom order list when click at add button
$('#bom_itemlists').on('click', function(e){
	if($(e.target).hasClass('btn-bom-item-add')){
		var itemno = e.target.id;
		console.log(itemno);
		$.ajax({
			type:'GET',
			dataType: 'json',
			url:'/sdapi/get/bom/search/itemdesc/0/itemno/'+itemno+'/catcode/0/itemtype/0',			
			success: function(d){
				console.log(d)
				
				var itemnode = 
				'<li class="list-group-item item-'+d[0].item_id+'" >' 
				+ '<div class="form-group row ordered_item">'
				+ 	 '<div class="col-xs-12" style="margin-bottom:5px;"><div class="row">'
				+    '<div class="col-xs-1" style="padding-right: 2px;"> <label class="control-label" for="oditem_'+d[0].item_id+'_itemno">'+ d[0].item_no+'</div>'
				+    '<div class="col-xs-11"><label class="control-label">'+ d[0].item_descr +'</label> </div>'
				+    '<input type="hidden" name="oditem_'+d[0].item_id+'_descr" value="'+ d[0].item_descr +'"/>'
				+    '<input type="hidden" name="oditem_'+d[0].item_id+'_itemid" value="'+d[0].item_id+'"/>'
				+    '<input type="hidden" name="oditem_'+d[0].item_id+'_itemno" value="'+d[0].item_no+'"/>'
				+    '<input type="hidden" name="oditem_'+d[0].item_id+'_uomcf" value="1.0000"/>' //hardcoded value
				+    '</div></div>'
				+    '<div class="col-xs-3" style="padding-right: 2px;"> <input id="' + d[0].item_id + '" class="form-control input-sm input-qty qty_'+d[0].item_id+'" type="text" maxlength="12" onkeypress="return isNumberKey(event)" placeholder="Key in Qty" name="oditem_'+d[0].item_id+'_qty" placeholder="Qty" required></div>'
				+    '<div class="col-xs-2" style="padding-right: 2px;padding-left: 2px;">' 
				+        '<select id="" name="oditem_'+ d[0].item_id +'_uom" class="form-control input-sm opt-uom"><option value="'+ d[0].uom +'">'+d[0].uom+'</option> </select>'
				+    '</div>'
				+    '<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;">' 
				+ 	     '<input class="text-right form-control input-sm input-unitconv unconv_' + d[0].item_id + '" type="text" name="oditem_'+d[0].item_id+'_unitconv" placeholder="unit conversion" value="1.0000" readonly>'
				+    '</div>'	
				+    '<div class="col-xs-3" style="padding-right: 2px;padding-left: 2px;"> ' 
				+ 		'<select class="text-left form-control input-sm input-type type_' + d[0].item_id + '" name="oditem_'+d[0].item_id+'_type"> '  
				+ 			'<option value="R" selected>Raw Material</option> <option value="B">BOM</option>'
				+       '</select>'
				+    '</div>'
				+ 	 '<div class="col-xs-1" style="padding-right: 0px;padding-left: 4px; float: left;"> <a id="'+d[0].item_id+'" class="btn btn-danger fa fa-minus-circle btn-del-item"></a></div>'
				+ '</div>'
				+'</li>';

				var exist = $('#bom_itemlist_ordered').find('li.item-'+d[0].item_id);

				if (exist.length>0) {
					$('#alert_msg_item_add').addClass('alert-danger');
					$('#alert_msg_item_add').removeClass('alert-success');
					$('#msg_item_add').html('Item existed in the list');
					$('#alert_msg_item_add').fadeIn('slow').delay(1000).hide(1);
				}
				else{
					$('#alert_msg_item_add').addClass('alert-success');
					$('#alert_msg_item_add').removeClass('alert-danger');
					$('#msg_item_add').html('Item Successfully added');
					$('#alert_msg_item_add').fadeIn('slow').delay(1000).hide(1);
					$('#bom_itemlist_ordered').fadeIn(500, function(){
						$(this).append(itemnode);
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log('error');
				//console.log(textStatus + errorThrown);
			}
		});//end ajax
	}//end if
})

//
$('#bom_itemlist_ordered').on('click', function(e){
	if($(e.target).hasClass('btn-del-item')){
		$('#bom_itemlist_ordered').find('.item-'+e.target.id).remove();
	}
});


$('document').ready(function(){
	$('.bom-message').hide(4000, function(){
		$(this).remove();
	})
})


//common function
//validation
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57) return false;
    //return true;
    if ((charCode >= 48 && charCode <= 57) || charCode == 46) return true;
    return false;
}

//function to format the input text to a currency format e.q. 130.00
function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

//sort the unordered list 
function sortUnorderedList(ul, sortDescending) {
  if(typeof ul == "string")
    ul = document.getElementById(ul);

  var lis = ul.getElementsByTagName("LI");
  var vals = [];

  for(var i = 0, l = lis.length; i < l; i++)
    vals.push(lis[i].innerHTML);

  vals.sort();

  if(sortDescending)
    vals.reverse();

  for(var i = 0, l = lis.length; i < l; i++)
    lis[i].innerHTML = vals[i];
}



function sortOrders(){
	//var myYears = ['Freshman', 'Sophomore', 'Junior', 'Senior'];
	var myList = $('#ordered_itemlist ul');
	var listItems = myList.children('li').get();
	console.log('Item:')
	console.log(listItems);
	listItems.sort(function(a,b){
	var compA = $.inArray($(a).find('.year').text(), myYears);
	var compB = $.inArray($(b).find('.year').text(), myYears);
	return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
	});
	$(myList).append(listItems);
}


